import asyncio
import ruamel.yaml
from string import Formatter

from sanic import Sanic
from sanic.response import html
from sanic import Blueprint, response
from sanic.request import Request
from sanic.response import HTTPResponse
from typing import Optional, Text, Any, List, Dict, Iterable, Callable, Awaitable
from datetime import date

import socketio
import logging

logging.basicConfig(level = logging.INFO)
logger = logging.getLogger(__name__)


sio = socketio.AsyncServer(async_mode='sanic', cors_allowed_origins=[])
app = Sanic()
sio.attach(app,"/socket.io")

yaml = ruamel.yaml.YAML()
domain = yaml.load(open('domain.yml', 'r'))

@app.route("/", methods=["GET"])
async def health(_: Request) -> HTTPResponse:
    return response.json({"status": "ok"})

@app.route("/nlg", methods=["POST"])
async def nlg(request: Request) -> HTTPResponse:
    """
    NLG replacement that only returns the utter name and button payload.
    Used for end to end testing
    """
    utter_name = request.json["template"]

    utter_text = domain["responses"][utter_name][0]["text"]

    format_vars = [fn for _, fn, _, _ in Formatter().parse(utter_text) if fn is not None]
    format_dict = {}
    for var in format_vars:
        format_dict[var] = request.json["tracker"]["slots"].get(var, request.json["arguments"].get(var))

    utter_response = f'{utter_name}{format_dict}'

    buttons = domain["responses"][utter_name][0].get("buttons", [])
    for button in buttons:
        button["title"] = button["payload"]

    return response.json({
        "text": utter_response,
        "buttons": buttons,
        "image": None,
        "elements": [],
        "attachments": []
    })

@sio.event
async def connect(sid, _) -> None:
    logger.info(f"User {sid} connected to socketIO endpoint.")


@sio.event
async def disconnect(sid) -> None:
    logger.info(f"User {sid} disconnected from socketIO endpoint.")


@sio.event
async def session_request(sid: Text, data: Optional[Dict]):
    logger.info(f"User {sid} connected to socketIO endpoint.")
    if data is None:
        data = {}
    if "session_id" not in data or data["session_id"] is None:
        id = 123
        key = "L"
        data["session_id"] = "{:.1}{:04d}/{:%Y%m%d}".format(key,id, date.today())
    await sio.emit("session_confirm", data["session_id"], room=sid)
    logger.info(f"User {sid} connected to socketIO endpoint.")


@sio.event
async def user_uttered(sid, data):
    logger.info(f"User {sid} connected to socketIO endpoint.")
    await sio.emit("bot_uttered", {"text": data["message"]}, room=sid)


if __name__ == '__main__':
    app.run(host="0.0.0.0")