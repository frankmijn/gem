@echo off 
rem clear model directory
IF "_%1" == "_clean" (
  echo "Cleaning Model directory"
  del /Q models\*.gz
) ELSE (
  echo "To clean the model directory: %0 clean"
)

docker-compose run rasa train

echo "Training done"
