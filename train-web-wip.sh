#!/bin/bash

# clear model directory
FIRST_ARGUMENT="$1"
if [ "$FIRST_ARGUMENT" = "clean" ] ; then
    echo "Cleaning Model directory"
    rm models/web/wip.tar.gz
else
    echo "To clean the model directory:"
    echo "$0 clean"
fi

docker-compose run rasa train --force --debug --out models/web --fixed-model-name wip --data data-wip -d domain-wip.yml -c config-wip.yml

echo "Training done"