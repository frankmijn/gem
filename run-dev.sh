if [ ! -d "db-dev" ]; then
    mkdir db-dev
fi

if [ ! -d "pg-admin" ]; then
    mkdir pg-admin
fi

docker-compose -f docker-compose.dev.yml up
