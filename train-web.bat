@echo off 
rem clear model directory
IF "_%1" == "_clean" (
  echo "Cleaning Model directory"
  del /Q models\web\*.gz
) ELSE (
  echo "To clean the model directory: %0 clean"
)

docker-compose run rasa train --out models/web --fixed-model-name latest

echo "Training done"
