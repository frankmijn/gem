import ast
import os

import pytest
import requests
import ruamel.yaml
from dotenv import load_dotenv

load_dotenv()

rasa_webhook_url = os.getenv(
    "RASA_WEBHOOK_URL", "http://localhost/webhooks/rest/webhook"
)

yaml = ruamel.yaml.YAML()

data_files = os.listdir("end2end/test_data")

all_data = []
for filename in data_files:
    data = yaml.load(open(f"end2end/test_data/{filename}", "r"))
    all_data += [[filename] + list(story.values()) for story in data]

results_path = "end2end/results"
results_filename = "failed_stories.md"

if not os.path.exists(results_path):
    os.mkdir(results_path)

with open(f"{results_path}/{results_filename}", "w+") as f:
    f.close()


@pytest.mark.parametrize("filename,story_name,conversation", all_data)
def test_story(filename, story_name, conversation):
    # Reset the session for this story
    requests.post(rasa_webhook_url, json={"sender": story_name, "message": "opnieuw"})

    story = f"## Filename: {filename} -- Story name: {story_name}"
    for iteration in conversation:
        # Send the user input
        response = requests.post(
            rasa_webhook_url,
            json={"sender": story_name, "message": iteration["message"]},
        ).json()
        story += f'\n* {iteration["message"]}'

        # Verify that the responses match the expected responses
        for i, expected_response in enumerate(iteration["expected_responses"]):
            story += f"\n  - {expected_response}"

            utter_name, entities = response[i]["text"].split("{", 1)
            entities = ast.literal_eval(f"{{{entities}")

            expected_utter_name, expected_entities = expected_response.split("{", 1)
            expected_entities = ast.literal_eval(f"{{{expected_entities}")

            try:

                assert (
                    utter_name == expected_utter_name
                ), "The utter name does not match the expected utter name"
                assert sorted(entities.keys()) == sorted(
                    expected_entities.keys()
                ), "The entity names do not match the expected entity names"

                for entity, value in entities.items():
                    if expected_entities[entity] != "*":
                        assert (
                            value == expected_entities[entity]
                        ), "The entity does not have the correct value"
            except AssertionError as e:
                story += f' <!-- PREDICTED: {response[i]["text"]} -->'
                with open(f"{results_path}/{results_filename}", "a") as f:
                    f.write(story)
                raise e
