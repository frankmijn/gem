#!/bin/bash

# clear model directory
FIRST_ARGUMENT="$1"
if [ "$FIRST_ARGUMENT" = "clean" ] ; then
    echo "Cleaning Model directory"
    rm models/*
else
    echo "To clean the model directory:"
    echo "$0 clean"
fi

docker-compose run rasa train

echo "Training done"