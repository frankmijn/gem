Postman/Newman Test Suites
==========================

# Requirements

- Docker
- git
- Running instance of https://gitlab.com/gemeente-tilburg/chatbot-prototype


# How to

## Setup docker environment

    ./setup.sh | setup.bat    

## Test scripts

Place you test scripts from Postman into the collections folder


## Reports

Reports are placed into the collections/reports folder, files include timestamp

You can clean up the reports folder using:

    ./cleanup.sh | cleanup.bat


## Running scripts

Run test scripts

    ./run_callscripts_test_dev.sh | run_callscripts_test_dev.bat
    ./run_intents_test_dev.sh | run_intents_test_dev.bat
    
The resulting HTML report with be opened after the test scripts have run.
