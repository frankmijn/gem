@echo off
SETLOCAL
set POSTMAN_ENVIRONMENT="chatbot_dev.postman_environment.json"
set POSTMAN_COLLECTION="chatbot_callscripts.postman_collection.json"
set POSTMAN_ITERATION_DATA="testdata_callscripts.csv"
set REPORT_PREFIX="testdata_callscripts"

rem Define a timestamp
for /f "tokens=2-8 delims=.:/ " %%a in ("%date% %time%") do set TIMESTAMP=%%c%%a%%b-%%d%%e%%f

rem Report filename
set REPORT_NAME="%REPORT_PREFIX%_%TIMESTAMP%

rem Starting newman
docker run --network=my-rasa-network -v %cd%/collections:/etc/newman --entrypoint /bin/sh -t postman/newman:alpine -c "npm i -g newman-reporter-htmlextra; newman run %POSTMAN_COLLECTION% --environment=%POSTMAN_ENVIRONMENT% --reporters="json,htmlextra,cli"  --reporter-json-export=reports/%REPORT_NAME%.json --reporter-htmlextra-export=reports/%REPORT_NAME%.html --iteration-data=%POSTMAN_ITERATION_DATA%"  

rem Opening report
start collections/reports/%REPORT_NAME%.html
ENDLOCAL
