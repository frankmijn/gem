from rasa.nlu.components import Component
import typing
import logging
from typing import Any, Optional, Text, Dict
if typing.TYPE_CHECKING:
    from rasa.nlu.model import Metadata

import io
import json
import re
from collections import Counter, OrderedDict
from spellchecker import SpellChecker as spellingscontrole
from interruptingcow import timeout
#from langdetect import detect
#from translate import Translator

spellchecker = spellingscontrole(language=None,local_dictionary= "dutch_words.json")

logger = logging.getLogger(__name__)


class SpellChecker(Component):

    def __init__(self, component_config=None):
        super(SpellChecker, self).__init__(component_config)

    def process(self, message, **kwargs):
        """Process an incoming message.

        This is the components chance to process an incoming
        message. The component can rely on
        any context attribute to be present, that gets created
        by a call to :meth:`components.Component.pipeline_init`
        of ANY component and
        on any context attributes created by a call to
        :meth:`components.Component.process`
        of components previous to this one."""
        logger.info("spellchecker")
        # get original message
        mesg = message.text 
        # retrieve unique char in string
        unique_val = len(set(mesg))
        if unique_val == 1:
            pass
        else:
            # remove punctuations
            mesg = re.sub(r'[^\w\s]','',mesg)
        
        # lower mesg
        text = mesg.lower()
        
        # translate language
        #translator= Translator(to_lang="nl")
        #text = translator.translate(text)

        text = text.split()
        new_sentence = ""

        try:
            with timeout(0.2, exception=RuntimeError):
                # perform a potentially very slow operation
                for word in text:
                    new_word = spellchecker.correction(word)
                    new_sentence += new_word + " "
        except RuntimeError:
            new_sentence = " ".join(text)
            logger.info("didn't finish within 0.2 seconds")

        logger.info("old_sentence: {}".format(" ".join(text)))
        logger.info("new_sentence: {}".format(new_sentence))
        message.text = new_sentence.rstrip()

    def persist(self, file_name: Text, model_dir: Text) -> Optional[Dict[Text, Any]]:
        """Persist this component to disk for future loading."""  
        pass

    @classmethod
    def load(
        cls,
        meta: Dict[Text, Any],
        model_dir: Optional[Text] = None,
        model_metadata: Optional["Metadata"] = None,
        cached_component: Optional["Component"] = None,
        **kwargs: Any
    ) -> "Component":
        """Load this component from file."""

        if cached_component:
            return cached_component
        else:
            return cls(meta)
