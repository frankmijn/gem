from rasa.nlu.components import Component
import typing
import logging
import unicodedata
import difflib
import os
import rasa.utils.io
import copy
from typing import Any, Dict, List, Optional, Text, Union, Type
from rasa.nlu.training_data import Message, TrainingData
from rasa.nlu.config import RasaNLUModelConfig
from rasa.nlu.model import Metadata
from rasa.constants import DOCS_URL_TRAINING_DATA_NLU
from rasa.nlu import utils
from rasa.utils.common import raise_warning

import rasa.utils.common as common_utils

if typing.TYPE_CHECKING:
    from rasa.nlu.model import Metadata

logger = logging.getLogger(__name__)


class PreProcessor(Component):
    
    @staticmethod 
    def process_text(text):
        text = text.lower()
        text = unicodedata.normalize('NFKD', text).encode('ascii', 'ignore').decode("utf-8")
        text = text.replace("-"," ").replace("'","").replace(" s "," des ").replace(" t "," het ")
        if text.startswith("s "):
            text = "des " + text[2:]
        elif text.startswith("t "):
            text = "het " + text[2:]

        #logger.info("process_text")
        #logger.info(text)
        return text

    def process(self, message, **kwargs):
        logger.debug("preprocessor: {0}".format(message.text))
        text = PreProcessor.process_text(message.text)
        message.text = text
        logger.debug("preprocessor: {0}".format(message.text))

    def train(
        self,
        training_data: TrainingData,
        config: Optional[RasaNLUModelConfig] = None,
        **kwargs: Any,
    ) -> None:

        for example in training_data.training_examples:
            entities = example.data.get("entities",[])
            index = 0
            orig_text = example.text
            new_text = ''
            log = orig_text.startswith("Waar kan ik pasfoto")
            if log:
                logger.debug(str(example.as_dict()))
            if log:
                logger.debug("'{}'".format(new_text))

            for entity in entities:
                if "value" in entity and isinstance(entity["value"], str):
                    # Detect synonym, if synonym the keep synonym, if not also replace entity
                    start = entity["start"]
                    end = entity["end"]
                    new_text = new_text + PreProcessor.process_text(orig_text[index:start])
                    if log:
                        logger.debug("'{}'".format(new_text))
                    new_start = len(new_text)
                    text_val = orig_text[start : end]
                    entity_val = str(entity.get("value"))
                    index = end
                    if text_val == entity_val:
                        entity["value"] = PreProcessor.process_text(entity_val)
                        entity["start"] = new_start
                        entity["end"] = new_start+len(entity["value"])
                        new_text = new_text + entity["value"]
                    else:
                        text_val = PreProcessor.process_text(text_val)
                        entity["start"] = new_start
                        entity["end"] = new_start+len(text_val)
                        new_text = new_text + text_val
                    if log:
                        logger.debug("'{}'".format(new_text))
            new_text = new_text + PreProcessor.process_text(orig_text[index:len(orig_text)])
            if log:
                logger.debug("'{}'".format(new_text))

            example.text = new_text
            if log:
                logger.debug(str(example.as_dict()))


        
class EntityMatcher(Component):

    def __init__(
        self,
        component_config: Optional[Dict[Text, Any]] = None,
        lookup_tables: Optional[Dict[Text, List]] = None,
    ) -> None:

        super().__init__(component_config)

        self.lookup_tables = lookup_tables if lookup_tables else {}
        self.lookup_tables_nodia = {}
        for key, lookup_list in self.lookup_tables.items():
            nodia_list = []
            for value in lookup_list:
                nodia_list.append(PreProcessor.process_text(value))
            self.lookup_tables_nodia[key] = nodia_list
        
        #logger.info("ENTITY MATCHER")
        #logger.info(str(self.lookup_tables))
        #logger.info(str(self.lookup_tables_nodia))


    def train(
        self,
        training_data: TrainingData,
        config: Optional[RasaNLUModelConfig] = None,
        **kwargs: Any,
    ) -> None:

        logger.debug(str(training_data.lookup_tables))
        self._add_lookup_tables(training_data.lookup_tables)
        logger.debug(str(self.lookup_tables))

        self.lookup_tables_nodia = {}
        for key, lookup_list in self.lookup_tables.items():
            nodia_list = []
            for value in lookup_list:
                nodia_list.append(PreProcessor.process_text(value))
            self.lookup_tables_nodia[key] = nodia_list

        for example in training_data.training_examples:
            entities = example.data.get("entities",[])
            for entity in entities:
                if "value" in entity and isinstance(entity["value"], str):
                    # Detect synonym, if synonym the keep synonym, if not also replace entity
                    entity_type = entity["entity"]
                    if entity_type in self.lookup_tables:
                        text_val = example.text[entity["start"] : entity["end"]]
                        if text_val not in self.lookup_tables_nodia[entity_type]:
                            raise_warning(
                                f"Entity value not in lookup list, not having value in lookup list will lead to reduced entity detection. Add the value to the lookuplist for list value functionality '{text_val}', {entity_type}"
                            )


        # for example in training_data.training_examples:
        #     for attribute in [TEXT, RESPONSE]:
        #         self._text_features_with_regex(example, attribute)

    def _add_lookup_tables(
        self, lookup_tables: List[Dict[Text, Union[Text, List]]]
    ) -> None:
        """appends the regex features from the lookup tables to self.known_patterns"""
        for table in lookup_tables:
            lookup_list = self._expand_lookup_list(table)
            self.lookup_tables[table["name"]] = lookup_list

    def _expand_lookup_list(
        self, lookup_table: Dict[Text, Union[Text, List[Text]]]
    ) -> List:
        """creates a regex out of the contents of a lookup table file"""
        lookup_elements = lookup_table["elements"]
        elements = []

        # if it's a list, it should be the elements directly
        if isinstance(lookup_elements, list):
            elements = lookup_elements
            common_utils.raise_warning(
                f"Directly including lookup tables as a list is deprecated since Rasa "
                f"1.6.",
                FutureWarning,
                docs=DOCS_URL_TRAINING_DATA_NLU + "#lookup-tables",
            )

        # otherwise it's a file path.
        else:

            try:
                f = open(lookup_elements, "r", encoding=rasa.utils.io.DEFAULT_ENCODING)
            except OSError:
                raise ValueError(
                    f"Could not load lookup table {lookup_elements}. "
                    f"Please make sure you've provided the correct path."
                )

            with f:
                for line in f:
                    new_element = line.strip()
                    if new_element:
                        elements.append(new_element)

        return elements

    def process(self, message, **kwargs):
        logger.debug("entitymatcher: {0}".format(message.text))
        entities = message.get("entities")
        for entity in entities:
            entity_type = entity["entity"]
            if entity_type in self.lookup_tables:
                value = entity["value"].lower()
                logger.debug("lookup entity: {0}, value {1}".format(entity_type,value))
                #results = difflib.get_close_matches(value, self.lookup_tables[entity_type])
                results = []
                if value in self.lookup_tables_nodia[entity_type]:
                    idx =  self.lookup_tables_nodia[entity_type].index(value)
                    results = [self.lookup_tables[entity_type][idx]]

                logger.debug("results: {0}".format(results))
                for result in results:
                    score = difflib.SequenceMatcher(None, value, result).ratio()
                    logger.debug("match: {0} score {1}".format(result,score))
                if results:
                    entity["value"] = results[0]
                    processors = entity.get("processors",[])
                    processors.append("EntityMatcher")
                    entity["processors"] = processors
        
    @classmethod
    def load(
        cls,
        meta: Dict[Text, Any],
        model_dir: Optional[Text] = None,
        model_metadata: Optional[Metadata] = None,
        cached_component: Optional["EntityMatcher"] = None,
        **kwargs: Any,
    ) -> "EntityMatcher":

        logger.debug(model_dir)
        logger.debug(str(meta))
        file_name = meta.get("file")
        lookup_tables_file = os.path.join(model_dir, file_name)
        
        if os.path.exists(lookup_tables_file):
            lookup_tables = rasa.utils.io.read_json_file(lookup_tables_file)
            return EntityMatcher(meta, lookup_tables=lookup_tables)
        else:
            return EntityMatcher(meta)

    def persist(self, file_name: Text, model_dir: Text) -> Optional[Dict[Text, Any]]:
        """Persist this model into the passed directory.

        Return the metadata necessary to load the model again."""
        file_name = file_name + ".pkl"
        lookup_tables_file = os.path.join(model_dir, file_name)
        utils.write_json_to_file(lookup_tables_file, self.lookup_tables, indent=4)

        return {"file": file_name}