from rasa.core.slots import Slot
import logging

logger = logging.getLogger(__name__)

class CountrySlot(Slot):

    def feature_dimensionality(self):
        return 2

    def as_feature(self):
        r = [0.0] * self.feature_dimensionality()
        if self.value:
            if self.is_idvalid(self.value):
                r[0] = 1.0
            else:
                r[1] = 1.0
        return r

    def is_idvalid(self,value):
        countries = ["andorra","belgië","bosnië - herzegovina","bulgarije","cyprus","denemarken","duitsland","estland","finland","engeland",
            "frankrijk","griekenland","groot-brittannië","noord ierland","hongarije","ierland","ijsland","italië","kroatië","letland",
            "liechtenstein","litouwen","luxemburg","noord-macedonië","macedonië","malta","monaco","noorwegen","oostenrijk","polen",
            "portugal","madeira","roemenië","san marino","servië","slovenië","slowakije","spanje","canarische eilanden","tsjechië",
            "turkije","zweden","zwitserland"]
        return value.lower() in countries