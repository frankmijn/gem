from rasa.nlu.extractors.extractor import EntityExtractor
import typing
import logging
from typing import Any, Optional, Text, Dict

if typing.TYPE_CHECKING:
    from rasa.nlu.model import Metadata

logger = logging.getLogger(__name__)


class ProductExtractor(EntityExtractor):
    """A new component"""



    def __init__(self, component_config=None):
        super(ProductExtractor, self).__init__(component_config)

    def train(self, training_data, cfg, **kwargs):
        """Train this component.

        This is the components chance to train itself provided
        with the training data. The component can rely on
        any context attribute to be present, that gets created
        by a call to :meth:`components.Component.pipeline_init`
        of ANY component and
        on any context attributes created by a call to
        :meth:`components.Component.train`
        of components previous to this one."""
        logger.debug(str(training_data.nlg_stories))
        # self._add_lookup_tables(training_data.lookup_tables)
        # logger.info(str(self.lookup_tables))

    def strip_intent_rankings(self, intent_ranking, products):
        result = []
        total = float(0.0)
        has_algemeen = False
        for ranking in intent_ranking:
            product = self.get_product_from_intent(ranking["name"])
            if ranking["name"] == "producten_algemeen":
                has_algemeen = True
            confidence = ranking["confidence"]
            if (not product or product in products) and confidence >= 0.2:
                result.append(ranking)
                total = total + ranking["confidence"]
        if not has_algemeen:
            rest = 1.0 - total
            result.append({"name":"producten_algemeen","confidence":rest})
        # factor = 1.0 / total
        # for i in result:
        #     i["confidence"] = i["confidence"] * factor
        intent_ranking = result
        return result


    def get_product_from_intent(self,intent):
        if intent is None:
            return
        intentparts = intent.split("_");
           
        # Getting length of list 
        length = len(intentparts) 
        i = 0
           
        product = None
        # Iterating using while loop 
        while i < length:
            part = intentparts[i]
            if (part == "product" and i < length-1):
                product = intentparts[i+1]
                i = length+1
            else:    
                i += 1
        if i == length:
            return

        return product


    def process(self, message, **kwargs):
        """Process an incoming message.

        This is the components chance to process an incoming
        message. The component can rely on
        any context attribute to be present, that gets created
        by a call to :meth:`components.Component.pipeline_init`
        of ANY component and
        on any context attributes created by a call to
        :meth:`components.Component.process`
        of components previous to this one."""

        """ Als de entity product al gevonden is elimineer alle intens specifiek voor ander producten dan de gevonden entities
        én controleren of entity product 
        Als entity product ontbreekt toevoegen 
        """
        logger.debug("process product extractor")
        # logger.debug(str(message.as_dict()))
        # logger.debug(str(message.get("text_sparse_features","")))
        entities = message.get("entities")
        intent_ranking = message.get("intent_ranking")
        logger.debug("data = {}".format(message.data))
        logger.debug("entities = {}".format(entities))

        products = []
        for entity in entities:
            entity_type = entity["entity"]
            if entity_type == "product":
                value = entity["value"]
                products.append(value)

        if not products:
            intent = message.get("intent").get("name")
            product = self.get_product_from_intent(intent)
            if product:
                products.append(product)
                entity = {'entity': 'product', 'value': product, 'confidence': 1.0, 'extractor': 'ProductExtractor', 'start': 0, 'end': 0}
                entities.append(entity)
        
        new_intent_ranking = self.strip_intent_rankings(intent_ranking, products)
        message.set("intent_ranking",new_intent_ranking)
        
        if not new_intent_ranking:
            message.set("intent",None)
            return

        message.set("intent",new_intent_ranking[0])
        intent = message.get("intent").get("name")
        product = self.get_product_from_intent(intent)
        logger.debug("intent = {}".format(intent))
        if not product and products:
            product = products[0]
        logger.debug("product = {}".format(product))
        
        # products_in_scope = self.component_config.get("products")
        # if products_in_scope:
        #     if product and product not in products_in_scope:
        #         out_of_scope_intent = {'name':'out_of_scope','confidence':1.0}
        #         message.set("intent_ranking",[out_of_scope_intent])
        #         message.set("intent",out_of_scope_intent)

        # existingentity = next((item for item in entities if item["entity"] == "product"), None)
        # logger.info("existingentity = {}".format(existingentity))
        # precedence = self.component_config.get("precedence")
        # if existingentity is None: #what is the correct behaviour? set precedence in pipeline config
        # elif existingentity['value'] != product:
        #     existingentity.clear()
        #     existingentity['value'] = product
        #     existingentity['entity'] = 'product'
        #     existingentity['confidence'] = 1.0
        #     existingentity['extractor'] = 'ProductExtractor'
        #     existingentity['start'] = 0
        #     existingentity['end'] = 0
        intent = message.get("intent").get("name")
        entities = message.get("entities")
        intent_ranking = message.get("intent_ranking")
        logger.debug("data = {}".format(message.data))
        logger.debug("intent = {}".format(intent))
        logger.debug("entities = {}".format(entities))

    def persist(self, file_name: Text, model_dir: Text) -> Optional[Dict[Text, Any]]:
        """Persist this component to disk for future loading."""

        pass

    @classmethod
    def load(
        cls,
        meta: Dict[Text, Any],
        model_dir: Optional[Text] = None,
        model_metadata: Optional["Metadata"] = None,
        cached_component: Optional["Component"] = None,
        **kwargs: Any
    ) -> "Component":
        """Load this component from file."""

        if cached_component:
            return cached_component
        else:
            return cls(meta)


