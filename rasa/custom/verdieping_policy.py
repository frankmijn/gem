import logging
import os
import json
import rasa
from typing import List, Optional, Dict, Text, Any

from rasa.core import utils
from rasa.core.actions.action import (
    ACTION_BACK_NAME,
    ACTION_LISTEN_NAME,
    ACTION_RESTART_NAME,
)
from rasa.core.constants import USER_INTENT_BACK, USER_INTENT_RESTART
from rasa.core.domain import PREV_PREFIX, ACTIVE_FORM_PREFIX, Domain
from rasa.core.policies.policy import Policy
from rasa.core.events import FormValidation, ActionExecuted
from rasa.core.featurizers import TrackerFeaturizer
from rasa.core.policies.memoization import MemoizationPolicy
from rasa.core.trackers import DialogueStateTracker

logger = logging.getLogger(__name__)

class verdieping_policy(Policy):
    """De verdieping_policy heeft de hoogste prioriteit. Hij checkt
    of er in de domain.yml een utter beschikbaar is die genaamd is naar 
    de naam van de vorige utter/action van de bot + _ + de naam van de huidige intent.
    
    Hiermee kan er snel antwoord gegeven worden op specifieke verdiepingsvragen, zoals 'hoe dan, wat dan, wanneer dan?', 
    zonder dat een story noodzakelijk is en houdt rekening met prediction sequence."""

    def __init__(self, priority: int = 6) -> None:
        super().__init__(priority=priority)

    def train(
        self,
        training_trackers: List[DialogueStateTracker],
        domain: Domain,
        **kwargs: Any,
    ) -> None:
        """Does nothing. This policy is deterministic."""

        pass

    def predict_action_probabilities(
        self, tracker: DialogueStateTracker, domain: Domain
    ) -> List[float]:

        prediction = [0.0] * domain.num_actions
        intent = tracker.latest_message.intent.get("name")

        if tracker.latest_action_name == ACTION_LISTEN_NAME:
            entities = tracker.latest_message.entities

            if intent and not entities:
                # action = domain.intent_properties.get(intent, {}).get("triggers")
                suffix = "_" + intent 
                # dirty hack, hardcoded landingpages = temorarily TODO
                landingpage_actions = ["utter_landingpage_paspoort","utter_landingpage_verhuizen","utter_landingpage_verhuisformulier","utter_landingpage_id-kaart","utter_landingpage_reisdocument_toestemmingsformulier_kind","utter_landingpage_brp_uittreksel","utter_landingpage_emigratie","utter_landingpage_verhuisformulier_hoe"]
                action_names_to_exclude = ["action_listen", "action_helpmore", "utter_helpmore"]
                action_names_to_exclude.extend(landingpage_actions)
                last_action = tracker.get_last_event_for(ActionExecuted, action_names_to_exclude=action_names_to_exclude)
                logger.debug('Latest valid action: '+ str(last_action))
                

                if last_action is not None and last_action.action_name.startswith('utter_'):
                    utter_template = last_action.action_name.rsplit('_', 1)[0] + suffix
                    if utter_template in domain.user_actions:
                        action = utter_template
                        idx = domain.index_for_action(action)
                        prediction[idx] = 1
                        logger.warning("There is a mapped utter found for the current intent and previous utter \'{0}\' \'{1}\'".format(utter_template,idx))
                    else:
                        logger.warning("There is no mapped utter for the current intent and previous utter \'{0}\'".format(utter_template))
                else:
                    logger.debug("There is no previous utter")
            else:
                logger.debug("The intent has no name or contains entities to match this policy")
  
        # elif tracker.latest_action_name == action and action is not None:
        else:
            latest_action = tracker.get_last_event_for(ActionExecuted)
            # assert latest_action.action_name == action
            if latest_action.policy and latest_action.policy.endswith(
                type(self).__name__
            ):
                # this ensures that we only predict listen, if we predicted
                # the action
                logger.debug(
                    "The action, '{}', for this intent, '{}', was "
                    "executed last so 'verdieping_policy' is returning to "
                    "action_listen.".format(str(latest_action), intent)
                )

                idx = domain.index_for_action(ACTION_LISTEN_NAME)
                prediction[idx] = 1
            else:
                logger.debug(
                    "The  action, '{}', for this intent, '{}', was "
                    "executed last, but it was predicted by another policy, '{}', so 'verdieping_policy' is not"
                    "predicting any action.".format(
                        latest_action, intent, latest_action.policy
                    )
                )

        return prediction


    def persist(self, path: Text) -> None:
        """Only persists the priority."""

        config_file = os.path.join(path, "mapping_policy.json")
        meta = {"priority": self.priority}
        rasa.utils.io.create_directory_for_file(config_file)
        rasa.utils.io.dump_obj_as_json_to_file(config_file, meta)

    @classmethod
    def load(cls, path: Text) -> "MappingPolicy":
        """Returns the class with the configured priority."""

        meta = {}
        if os.path.exists(path):
            meta_path = os.path.join(path, "mapping_policy.json")
            if os.path.isfile(meta_path):
                meta = json.loads(rasa.utils.io.read_file(meta_path))

        return cls(**meta)