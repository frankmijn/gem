## ## product_verhuismelding_kind_opkamers_uitwonend_meerderjarig_aangeslotengemeente 2a/2 + ## overig_digid_hebgeendigid 1/3 + ## overig_digid_wilgeendigid_aangeslotengemeente 2/3  2547~20200617
* greet: /greet{"municipality":"Dongen"}
    - slot{"municipality": "Dongen"}
    - utter_greet
    - utter_greet2
    - utter_howcanihelp
    - action_back
* product_verhuismelding_kind_opkamers: [](product:verhuismelding)mijn [oudste](namens:kind) gaat op kamers
    - slot{"product": "verhuismelding"}
    - action_check_product
    - slot{"product": "verhuismelding"}
    - utter_overig_kindjongerdan18-q
* inform_denial: nee
    - utter_product_verhuismelding_kind_inschrijven_uitwonend_meerderjarig
    - action_check_municipality
    - slot{"feature_content": true}
    - slot{"municipality": "Dongen"}
    - slot{"gemeente_website": "http://www.dongen.nl"}
    - slot{"gemeente_telefoonnummer": "14 0162"}
    - slot{"gemeente_adres": "Hoge Ham 62 5104 JJ DONGEN"}
    - slot{"gemeente_postadres": "Postbus 10153 5100 GE DONGEN"}
    - slot{"gemeente_emailadres": "info@dongen.nl"}
    - utter_landingpage_verhuisformulier
    - action_helpmore
* overig_digid_hebgeendigid: ik heb geen digid
    - utter_overig_digid_aanvragen
    - action_helpmore
    - reminder{"intent": "schedule_helpmore", "date_time": "2020-06-17T13:27:58.766742", "entities": null, "name": "3883f34c-b09e-11ea-96c5-0242ac1c0004", "kill_on_user_msg": true}
* overig_digid_wilgeendigid: ik wil geen digid gebruiken
    - action_check_municipality
    - slot{"feature_content": true}
    - slot{"municipality": "Dongen"}
    - slot{"gemeente_website": "http://www.dongen.nl"}
    - slot{"gemeente_telefoonnummer": "14 0162"}
    - slot{"gemeente_adres": "Hoge Ham 62 5104 JJ DONGEN"}
    - slot{"gemeente_postadres": "Postbus 10153 5100 GE DONGEN"}
    - slot{"gemeente_emailadres": "info@dongen.nl"}
    - utter_overig_digid_wilgeendigid
    - action_helpmore
