## Demo testscript op aangesloten gemeente 2528~20200617
* greet: /greet{"municipality":"Dongen"}
    - slot{"municipality": "Dongen"}
    - utter_greet
    - utter_greet2
    - utter_howcanihelp
    - action_back
* chitchat_hoegaathet: Hoi Gem, hoe gaat het met je?
    - utter_chitchat_hoegaathet
* watkangem: Wat kan jij allemaal?
    - utter_watkangem
    - action_helpmore
* hoelang: Wanneer is mijn [paspoort](product) klaar?
    - slot{"product": "paspoort"}
    - action_check_product
    - slot{"product": "paspoort"}
    - utter_productgroep_reisdocumenten_aanvragen_wanneerklaar
    - action_helpmore
* watmeenemen: wat moet ik dan meenemen?
    - utter_productgroep_reisdocumenten_aanvragen_watmeenemen
* out_of_scope: [](product:afvalcontainer)mijn container is niet geleegd
    - slot{"product": "afvalcontainer"}
    - action_check_municipality
    - slot{"feature_content": true}
    - slot{"municipality": "Dongen"}
    - slot{"gemeente_website": "http://www.dongen.nl"}
    - slot{"gemeente_telefoonnummer": "14 0162"}
    - slot{"gemeente_adres": "Hoge Ham 62 5104 JJ DONGEN"}
    - slot{"gemeente_postadres": "Postbus 10153 5100 GE DONGEN"}
    - slot{"gemeente_emailadres": "info@dongen.nl"}
    - action_samenwerkendecatalogi_intro
    - action_samenwerkendecatalogi_toelichting
    - action_samenwerkendecatalogi_resultaat
    - action_helpmore
* out_of_scope: Kan ik mijn [rijbewijs](product) verlengen?
    - slot{"product": "rijbewijs"}
    - action_check_municipality
    - slot{"feature_content": true}
    - slot{"municipality": "Dongen"}
    - slot{"gemeente_website": "http://www.dongen.nl"}
    - slot{"gemeente_telefoonnummer": "14 0162"}
    - slot{"gemeente_adres": "Hoge Ham 62 5104 JJ DONGEN"}
    - slot{"gemeente_postadres": "Postbus 10153 5100 GE DONGEN"}
    - slot{"gemeente_emailadres": "info@dongen.nl"}
    - action_samenwerkendecatalogi_intro
    - action_samenwerkendecatalogi_toelichting
    - action_samenwerkendecatalogi_resultaat
    - action_helpmore
* gemeente_adres: Wat is het adres van de gemeente?
    - action_check_municipality
    - slot{"feature_content": true}
    - slot{"municipality": "Dongen"}
    - slot{"gemeente_website": "http://www.dongen.nl"}
    - slot{"gemeente_telefoonnummer": "14 0162"}
    - slot{"gemeente_adres": "Hoge Ham 62 5104 JJ DONGEN"}
    - slot{"gemeente_postadres": "Postbus 10153 5100 GE DONGEN"}
    - slot{"gemeente_emailadres": "info@dongen.nl"}
    - utter_gemeente_adres_lokaal
    - action_helpmore
* bellen_met_gemeente: kan ik ook bellen?
    - action_check_municipality
    - slot{"feature_content": true}
    - slot{"municipality": "Dongen"}
    - slot{"gemeente_website": "http://www.dongen.nl"}
    - slot{"gemeente_telefoonnummer": "14 0162"}
    - slot{"gemeente_adres": "Hoge Ham 62 5104 JJ DONGEN"}
    - slot{"gemeente_postadres": "Postbus 10153 5100 GE DONGEN"}
    - slot{"gemeente_emailadres": "info@dongen.nl"}
    - utter_gemeente_telefoonnummer_lokaal
    - action_helpmore
* compliment: top!
    - utter_graaggedaan
* wie_is_de_burgemeester_van: Wie is de burgemeester eigenlijk?
    - action_check_municipality
    - slot{"feature_content": true}
    - slot{"municipality": "Dongen"}
    - slot{"gemeente_website": "http://www.dongen.nl"}
    - slot{"gemeente_telefoonnummer": "14 0162"}
    - slot{"gemeente_adres": "Hoge Ham 62 5104 JJ DONGEN"}
    - slot{"gemeente_postadres": "Postbus 10153 5100 GE DONGEN"}
    - slot{"gemeente_emailadres": "info@dongen.nl"}
    - action_wieisdeburgemeester
    - action_helpmore
* envoor: en van [Nieuwegein](municipality)?
    - slot{"municipality": "Nieuwegein"}
    - action_envoor
* wie_is_de_burgemeester_van: /wie_is_de_burgemeester_van
    - slot{"municipality": "Nieuwegein"}
    - action_check_municipality
    - slot{"feature_content": false}
    - slot{"municipality": "Nieuwegein"}
    - slot{"gemeente_website": "http://www.nieuwegein.nl"}
    - slot{"gemeente_telefoonnummer": "14030"}
    - slot{"gemeente_adres": "Stadsplein 1 3431 LZ NIEUWEGEIN"}
    - slot{"gemeente_postadres": "Postbus 1 3430 AA NIEUWEGEIN"}
    - slot{"gemeente_emailadres": "gemeente@nieuwegein.nl"}
    - action_wieisdeburgemeester
    - action_helpmore
* envoor: en van [dongen](municipality:Dongen)?
    - slot{"municipality": "Dongen"}
    - action_envoor
* wie_is_de_burgemeester_van: /wie_is_de_burgemeester_van
    - slot{"municipality": "Dongen"}
    - action_check_municipality
    - slot{"feature_content": true}
    - slot{"municipality": "Dongen"}
    - slot{"gemeente_website": "http://www.dongen.nl"}
    - slot{"gemeente_telefoonnummer": "14 0162"}
    - slot{"gemeente_adres": "Hoge Ham 62 5104 JJ DONGEN"}
    - slot{"gemeente_postadres": "Postbus 10153 5100 GE DONGEN"}
    - slot{"gemeente_emailadres": "info@dongen.nl"}
    - action_wieisdeburgemeester
    - action_helpmore
* watkostdat: wat kost een [paspoort](product)?
    - slot{"product": "paspoort"}
    - action_check_product
    - slot{"product": "paspoort"}
    - action_check_municipality
    - slot{"feature_content": true}
    - slot{"municipality": "Dongen"}
    - slot{"gemeente_website": "http://www.dongen.nl"}
    - slot{"gemeente_telefoonnummer": "14 0162"}
    - slot{"gemeente_adres": "Hoge Ham 62 5104 JJ DONGEN"}
    - slot{"gemeente_postadres": "Postbus 10153 5100 GE DONGEN"}
    - slot{"gemeente_emailadres": "info@dongen.nl"}
    - utter_product_paspoort_kosten
    - action_helpmore
* envoor: en een [id-kaart](product)?
    - slot{"product": "id-kaart"}
    - action_envoor
* watkostdat: /watkostdat
    - slot{"product": "id-kaart"}
    - action_check_product
    - slot{"product": "id-kaart"}
    - action_check_municipality
    - slot{"feature_content": true}
    - slot{"municipality": "Dongen"}
    - slot{"gemeente_website": "http://www.dongen.nl"}
    - slot{"gemeente_telefoonnummer": "14 0162"}
    - slot{"gemeente_adres": "Hoge Ham 62 5104 JJ DONGEN"}
    - slot{"gemeente_postadres": "Postbus 10153 5100 GE DONGEN"}
    - slot{"gemeente_emailadres": "info@dongen.nl"}
    - utter_product_id-kaart_kosten
    - action_helpmore
* productgroep_reisdocumenten_geldig_waar: is een [id-kaart](product) geldig in [Zwitserland](land)?
    - slot{"land": "Zwitserland"}
    - slot{"product": "id-kaart"}
    - utter_productgroep_reisdocumenten_geldig_inditland
* envoor: en in [Portugal](land)?
    - slot{"land": "Portugal"}
    - action_envoor
* productgroep_reisdocumenten_geldig_waar: /productgroep_reisdocumenten_geldig_waar
    - slot{"land": "Portugal"}
    - slot{"product": "id-kaart"}
    - utter_productgroep_reisdocumenten_geldig_inditland
* envoor: en in [China](land)?
    - slot{"land": "China"}
    - action_envoor
* productgroep_reisdocumenten_geldig_waar: /productgroep_reisdocumenten_geldig_waar
    - slot{"land": "China"}
    - slot{"product": "id-kaart"}
    - utter_productgroep_reisdocumenten_geldig_nietinditland
* spoed: ik heb er [met spoed](spoed) één nodig
    - slot{"spoed": "met spoed"}
    - action_check_product
    - slot{"product": "id-kaart"}
    - action_check_municipality
    - slot{"feature_content": true}
    - slot{"municipality": "Dongen"}
    - slot{"gemeente_website": "http://www.dongen.nl"}
    - slot{"gemeente_telefoonnummer": "14 0162"}
    - slot{"gemeente_adres": "Hoge Ham 62 5104 JJ DONGEN"}
    - slot{"gemeente_postadres": "Postbus 10153 5100 GE DONGEN"}
    - slot{"gemeente_emailadres": "info@dongen.nl"}
    - utter_productgroep_reisdocumenten_aanvragen_spoed_vandaagnog
* inform_denial: /inform_denial
    - utter_productgroep_reisdocumenten_aanvragen_spoed_oudeproduct
* inform_affirmative: /inform_affirmative
    - utter_productgroep_reisdocumenten_aanvragen_spoed
    - utter_product_id-kaart_kosten
    - utter_productgroep_reisdocumenten_aanvragen_spoed_watkostdat
    - utter_landingpage_id-kaart_spoed
    - action_helpmore
* openingstijden: Vanaf hoe laat zijn jullie open?
    - action_check_municipality
    - slot{"feature_content": true}
    - slot{"municipality": "Dongen"}
    - slot{"gemeente_website": "http://www.dongen.nl"}
    - slot{"gemeente_telefoonnummer": "14 0162"}
    - slot{"gemeente_adres": "Hoge Ham 62 5104 JJ DONGEN"}
    - slot{"gemeente_postadres": "Postbus 10153 5100 GE DONGEN"}
    - slot{"gemeente_emailadres": "info@dongen.nl"}
    - utter_openingstijden_algemeen
    - action_helpmore
* overig_coronavirus: Mag ik nog wel naar het gemeentehuis komen?
    - action_check_municipality
    - slot{"feature_content": true}
    - slot{"municipality": "Dongen"}
    - slot{"gemeente_website": "http://www.dongen.nl"}
    - slot{"gemeente_telefoonnummer": "14 0162"}
    - slot{"gemeente_adres": "Hoge Ham 62 5104 JJ DONGEN"}
    - slot{"gemeente_postadres": "Postbus 10153 5100 GE DONGEN"}
    - slot{"gemeente_emailadres": "info@dongen.nl"}
    - utter_overig_coronavirus
    - action_helpmore
* product_verhuismelding_verhuizen: wij gaan [verhuizen](product:verhuismelding)
    - slot{"product": "verhuismelding"}
    - action_check_product
    - slot{"product": "verhuismelding"}
    - utter_product_verhuismelding_algemeen
* hoe_doorgeven: /hoe_doorgeven{"product":"verhuismelding"}
    - slot{"product": "verhuismelding"}
    - action_check_product
    - slot{"product": "verhuismelding"}
    - utter_product_verhuismelding_doorgeven_waar
    - action_check_municipality
    - slot{"feature_content": true}
    - slot{"municipality": "Dongen"}
    - slot{"gemeente_website": "http://www.dongen.nl"}
    - slot{"gemeente_telefoonnummer": "14 0162"}
    - slot{"gemeente_adres": "Hoge Ham 62 5104 JJ DONGEN"}
    - slot{"gemeente_postadres": "Postbus 10153 5100 GE DONGEN"}
    - slot{"gemeente_emailadres": "info@dongen.nl"}
    - utter_product_verhuismelding_doorgeven_hoe
    - utter_landingpage_verhuisformulier
    - action_helpmore
* product_verhuismelding_doorgeven_namenswie: kan ik mijn [kind](namens) [meeverhuizen](product:verhuismelding)?
    - slot{"product": "verhuismelding"}
    - action_check_product
    - slot{"product": "verhuismelding"}
    - action_set_namens
    - slot{"namens": "kind"}
    - utter_product_verhuismelding_kind_inschrijven_waarnuingeschreven-q
* inform_affirmative: ja
    - utter_product_verhuismelding_kind_inschrijven_inwonend
    - action_check_municipality
    - slot{"feature_content": true}
    - slot{"municipality": "Dongen"}
    - slot{"gemeente_website": "http://www.dongen.nl"}
    - slot{"gemeente_telefoonnummer": "14 0162"}
    - slot{"gemeente_adres": "Hoge Ham 62 5104 JJ DONGEN"}
    - slot{"gemeente_postadres": "Postbus 10153 5100 GE DONGEN"}
    - slot{"gemeente_emailadres": "info@dongen.nl"}
    - utter_landingpage_verhuisformulier
    - action_helpmore
* product_verhuismelding_doorgeven_namenswie: kan ik mijn [partner](namens) ook [meeverhuizen](product:verhuismelding)?
    - slot{"product": "verhuismelding"}
    - action_check_product
    - slot{"product": "verhuismelding"}
    - action_set_namens
    - slot{"namens": "partner"}
    - utter_product_verhuismelding_doorgeven_namens_partner
    - action_check_municipality
    - slot{"feature_content": true}
    - slot{"municipality": "Dongen"}
    - slot{"gemeente_website": "http://www.dongen.nl"}
    - slot{"gemeente_telefoonnummer": "14 0162"}
    - slot{"gemeente_adres": "Hoge Ham 62 5104 JJ DONGEN"}
    - slot{"gemeente_postadres": "Postbus 10153 5100 GE DONGEN"}
    - slot{"gemeente_emailadres": "info@dongen.nl"}
    - utter_landingpage_verhuizen
    - action_helpmore
* fout_gegaan: Er is iets niet goed gegaan
    - action_check_municipality
    - slot{"feature_content": true}
    - slot{"municipality": "Dongen"}
    - slot{"gemeente_website": "http://www.dongen.nl"}
    - slot{"gemeente_telefoonnummer": "14 0162"}
    - slot{"gemeente_adres": "Hoge Ham 62 5104 JJ DONGEN"}
    - slot{"gemeente_postadres": "Postbus 10153 5100 GE DONGEN"}
    - slot{"gemeente_emailadres": "info@dongen.nl"}
    - utter_medewerker_nodig_metlivechat
* contact: /contact
    - action_check_municipality
    - slot{"feature_content": true}
    - slot{"municipality": "Dongen"}
    - slot{"gemeente_website": "http://www.dongen.nl"}
    - slot{"gemeente_telefoonnummer": "14 0162"}
    - slot{"gemeente_adres": "Hoge Ham 62 5104 JJ DONGEN"}
    - slot{"gemeente_postadres": "Postbus 10153 5100 GE DONGEN"}
    - slot{"gemeente_emailadres": "info@dongen.nl"}
    - utter_gemeente_contact_lokaal
* overig_coronavirus: Hoe vraag ik een TOZO aan?
    - action_check_municipality
    - slot{"feature_content": true}
    - slot{"municipality": "Dongen"}
    - slot{"gemeente_website": "http://www.dongen.nl"}
    - slot{"gemeente_telefoonnummer": "14 0162"}
    - slot{"gemeente_adres": "Hoge Ham 62 5104 JJ DONGEN"}
    - slot{"gemeente_postadres": "Postbus 10153 5100 GE DONGEN"}
    - slot{"gemeente_emailadres": "info@dongen.nl"}
    - utter_overig_coronavirus
    - action_helpmore
* ontevreden: Wat waardeloos
    - utter_feedback_ontevreden_intro
* feedback_chatbot: /feedback_chatbot
    - action_set_feedback_negatief
    - slot{"feedback_geholpen": "Negatief"}
    - form_feedback
    - form{"name": "form_feedback"}
    - slot{"feedback_geholpen": "Negatief"}
    - slot{"requested_slot": "feedback_mis"}
* form: teamtest: test [cobi](name)
    - slot{"name": "cobi"}
    - form: form_feedback
    - slot{"feedback_mis": "test cobi"}
    - slot{"requested_slot": "feedback_antwoordverwacht"}
* form: teamtest: test [cobi](name)
    - slot{"name": "cobi"}
    - form: form_feedback
    - slot{"feedback_antwoordverwacht": "test cobi"}
    - slot{"requested_slot": "feedback_verderverbeteren"}
* form: teamtest: test [cobi](name)
    - slot{"name": "cobi"}
    - form: form_feedback
    - slot{"feedback_verderverbeteren": "test cobi"}
    - slot{"feedback_geholpen": null}
    - slot{"feedback_verderverbeteren": null}
    - slot{"feedback_mis": null}
    - slot{"feedback_antwoordverwacht": null}
    - form{"name": null}
    - slot{"requested_slot": null}
* graaggedaan: graag gedaan
    - utter_fijnedag
    - action_helpmore
* product_verhuismelding_doorgeven_wanneer: Wanneer moet ik mijn [verhuizing](product:verhuismelding) doorgeven?
    - slot{"product": "verhuismelding"}
    - action_check_product
    - slot{"product": "verhuismelding"}
    - form_when_verhuismelding
    - form{"name": "form_when_verhuismelding"}
    - slot{"requested_slot": "verhuismelding_datum"}
* form: oepsikbedoel: [Volgende week donderdag](time:2020-06-25T00:00:00.000-07:00)
    - slot{"time": "2020-06-25T00:00:00.000-07:00"}
    - form: form_when_verhuismelding
    - slot{"verhuismelding_datum": "2020-06-25"}
    - slot{"verhuismelding_datum_jaar": null}
    - slot{"verhuismelding_datum": null}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_helpmore
* watkostdat: Wat kost dat?
    - action_check_product
    - slot{"product": "verhuismelding"}
    - utter_product_verhuismelding_doorgeven_watkostdat
    - action_helpmore
* compliment: Super
    - utter_graaggedaan
