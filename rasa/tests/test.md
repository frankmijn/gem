## DW-4570.20200602
* watkostdat: wat kost een [verhuizing](product:verhuismelding)?
    - slot{"product": "verhuismelding"}
    - action_check_product
    - slot{"product": "verhuismelding"}
    - utter_product_verhuismelding_doorgeven_watkostdat
    - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->
#     - reminder{"intent": "schedule_helpmore", "date_time": "2020-06-02T18:41:32.697983", "entities": null, "name": "8a4b774e-a500-11ea-b344-0242ac180004", "kill_on_user_msg": true}
# * schedule_helpmore: EXTERNAL: schedule_helpmore
#     - utter_helpmore