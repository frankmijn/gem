## overig_foto_eisen 1/1 2543~20200617
* greet: /greet{"municipality":"Dongen"}
    - slot{"municipality": "Dongen"}
    - utter_greet
    - utter_greet2
    - utter_howcanihelp
    - action_back
* overig_foto_eisen: kan ik een schoolfoto gebruiken?
    - utter_overig_foto_eisen
    - action_helpmore

## ontevreden - feedback + ## Chitchat oke  2546~20200617
* greet: /greet{"municipality":"Dongen"}
    - slot{"municipality": "Dongen"}
    - utter_greet
    - utter_greet2
    - utter_howcanihelp
    - action_back
* ontevreden: belachelijk
    - utter_feedback_ontevreden_intro
* feedback_chatbot: feedback
    - action_set_feedback_negatief
    - slot{"feedback_geholpen": "Negatief"}
    - form_feedback
    - form{"name": "form_feedback"}
    - slot{"feedback_geholpen": "Negatief"}
    - slot{"requested_slot": "feedback_mis"}
* form: greet: hier
    - form: form_feedback
    - slot{"feedback_mis": "hier"}
    - slot{"requested_slot": "feedback_antwoordverwacht"}
* form: wie: dat
    - form: form_feedback
    - slot{"feedback_antwoordverwacht": "dat"}
    - slot{"requested_slot": "feedback_verderverbeteren"}
* form: inform_municipality: veel
    - form: form_feedback
    - slot{"feedback_verderverbeteren": "veel"}
    - slot{"feedback_geholpen": null}
    - slot{"feedback_verderverbeteren": null}
    - slot{"feedback_mis": null}
    - slot{"feedback_antwoordverwacht": null}
    - form{"name": null}
    - slot{"requested_slot": null}
* inform_oke: oke
    - action_helpmore
