#### ALLE GENERIEKE VRAGEN (landelijk)


### Welke vraag over verhuizen?

## product_verhuismelding_algemeen 1/1 #tsdemo
* producten_algemeen{"product": "verhuismelding"} OR product_verhuismelding_wonen{"product": "verhuismelding"} OR product_verhuismelding_verhuizen{"product": "verhuismelding"} <!-- Ik verhuis -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_algemeen <!-- Je hebt een vraag over verhuizen. Wat wil je hierover weten? [adreswijziging] [wanneer doorgeven] --> 

## leuke gemeente?
* leuke_gemeente <!-- Is {municipality} een leuke woonplaats? -->
  - utter_leuke_gemeente <!-- {municipality} is fantastisch! Kom eens kijken of kijk op [de website van {municipality}] -->

### Wanneer verhuizing doorgeven? 

## product_verhuismelding_doorgeven_wanneer 1/2 #tsdemo
* wanneer{"product": "verhuismelding"} OR nudoorgeven{"product": "verhuismelding"} OR product_verhuismelding_doorgeven_wanneer{"product": "verhuismelding"} OR product_verhuismelding_doorgeven_wanneer{"product": "verhuismelding","time": "1 augustus"} <!-- Wanneer moet ik mijn verhuizing doorgeven? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - form_when_verhuismelding <!-- Bot: Wat is je verhuisdatum? ___ Klant: [Datum] ___ Bot: [even rekenen... antwoord (te vroeg / op tijd / te laat / algemeen (algemeen als datum onbegrijpelijk is: vanaf 28 dagen voor tot 5 dagen na verhuisdatum)] -->
  - form{"name": "form_when_verhuismelding"} <!-- Formulier activeren -->
  - form{"name": null} <!-- Formulier deactiveren -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_doorgeven_wanneer 2/2
* wanneer{"product": "verhuismelding"} OR nudoorgeven{"product": "verhuismelding"} OR product_verhuismelding_doorgeven_wanneer{"product": "verhuismelding"} OR product_verhuismelding_doorgeven_wanneer{"product": "verhuismelding","time": "1 augustus"} <!-- Wanneer moet ik mijn verhuizing doorgeven? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - form_when_verhuismelding <!-- Bot: Wat is je verhuisdatum? ___ Klant: [Datum] ___ Bot: [even rekenen... antwoord (te vroeg / op tijd / te laat / algemeen (algemeen als datum onbegrijpelijk is: vanaf 28 dagen voor tot 5 dagen na verhuisdatum)] -->
  - form{"name": "form_when_verhuismelding"} <!-- Formulier activeren -->
* inform_dontknow
  - utter_product_verhuismelding_meldperiode_algemeen
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->
  - form{"name": null} <!-- Formulier deactiveren -->

### Is mijn verhuismelding bevestigd?

## product_verhuismelding_bevestiging 1/1
* product_verhuismelding_bevestiging{"product": "verhuismelding"} <!-- Hoe weet ik zeker dat het doorgeven van mijn verhuizing gelukt is? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"} 
  - utter_product_verhuismelding_bevestiging <!-- Het verwerken van een verhuismelding kost meestal 5 dagen. Na je verhuisdatum zie je op www.mijnoverheid.nl je nieuwe adres staan. Je moet hiervoor inloggen met DigiD. -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_doorgeven_hoelang 1/1
* hoelang OR hoelang{"product": "verhuismelding"} <!-- Hoe lang duurt het? Of hoe lang van tevoren kan ik het doorgeven? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"} 
  - utter_product_verhuismelding_doorgeven_hoelang <!-- Je kunt je verhuizing doorgeven vanaf 28 dagen vóór je verhuisdatum, tot uiterlijk 5 dagen erna. \n \nDe gemeente heeft meestal 5 dagen nodig om je verhuizing te verwerken. Na je verhuisdatum zie je op [www.mijnoverheid.nl](https://www.mijnoverheid.nl) je nieuwe adres staan. -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->


 
### Kind verhuizen    #@# Testen: opsplitsing naar aangeslotengemeente vs. nietaangeslotengemeente

## product_verhuismelding_kind_inschrijven_inwonend_aangeslotengemeente 1a/3  #tsdemo
* product_verhuismelding_ander_inschrijven{"namens": "kind", "product":"verhuismelding"} OR namenswie{"namens": "kind"} OR product_verhuismelding_doorgeven_namenswie{"product": "verhuismelding", "namens": "kind"}<!-- Kan ik mijn kind inschrijven / meeverhuizen? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - action_set_namens
  - slot{"namens":"kind"}
  - utter_product_verhuismelding_kind_inschrijven_waarnuingeschreven-q<!-- Staat je kind nu bij jou ingeschreven? -->
* inform_affirmative OR inform_oke <!-- Ja -->
  - utter_product_verhuismelding_kind_inschrijven_inwonend <!-- Dan geef je dit door bij je eigen verhuizing -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":true} <!-- [ik herken een aangesloten gemeente] --> 
  - utter_landingpage_verhuisformulier <!-- Je vindt het online formulier op [url] of je kunt bellen voor een afspraak met [telefoonnummer]. -->
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_kind_inschrijven_inwonend_nietaangeslotengemeente 1b/3  
* product_verhuismelding_ander_inschrijven{"namens": "kind", "product":"verhuismelding"} OR namenswie{"namens": "kind"} OR product_verhuismelding_doorgeven_namenswie{"product": "verhuismelding", "namens": "kind"}<!-- Kan ik mijn kind inschrijven / meeverhuizen? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - action_set_namens
  - slot{"namens":"kind"}
  - utter_product_verhuismelding_kind_inschrijven_waarnuingeschreven-q<!-- Staat je kind nu bij jou ingeschreven? -->
* inform_affirmative OR inform_oke <!-- Ja -->
  - utter_product_verhuismelding_kind_inschrijven_inwonend <!-- Dan geef je dit door bij je eigen verhuizing -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false} <!-- [ik herken een niet-aangesloten gemeente; ik val terug op samenwerkende catalogi] --> 
  - action_samenwerkendecatalogi_toelichting <!-- Hiervoor kan ik je nog niet uitgebreid helpen. Maar ik ga voor je zoeken. -->
  - action_samenwerkendecatalogi_resultaat <!-- Voor [product] in de gemeente [municipality] heb ik het volgende voor je gevonden: [lijst met links] --> 
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_kind_inschrijven_uitwonend_minderjarig_aangeslotengemeente 2a/3
* product_verhuismelding_ander_inschrijven{"namens": "kind", "product":"verhuismelding"} OR namenswie{"namens": "kind"} OR product_verhuismelding_doorgeven_namenswie{"product": "verhuismelding", "namens": "kind"}<!-- Kan ik mijn kind inschrijven / meeverhuizen? --> 
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - action_set_namens
 - slot{"namens":"kind"}
  - utter_product_verhuismelding_kind_inschrijven_waarnuingeschreven-q<!-- Staat je kind nu bij jou ingeschreven? --> 
* inform_denial <!-- Nee -->
  - utter_overig_kindjongerdan18-q<!-- Is je kind jonger dan 18? --> 
* inform_affirmative OR inform_oke <!-- Ja -->
  - utter_product_verhuismelding_kind_inschrijven_uitwonend_minderjarig <!-- Je kind is minderjarig. Dan moet je de verhuizing doorgeven via een formulier of via een afspraak. De andere ouder moet schriftelijk toestemming geven. Ook heb je een kopie van het paspoort, rijbewijs of de id-kaart van de andere ouder nodig.  --> 
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":true} <!-- [ik herken een niet-aangesloten gemeente] --> 
  - utter_landingpage_verhuisformulier <!-- Je vindt het online formulier op [url] of je kunt bellen voor een afspraak met [telefoonnummer]. -->
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_kind_inschrijven_uitwonend_minderjarig_nietaangeslotengemeente 2b/3
* product_verhuismelding_ander_inschrijven{"namens": "kind", "product":"verhuismelding"} OR namenswie{"namens": "kind"} OR product_verhuismelding_doorgeven_namenswie{"product": "verhuismelding", "namens": "kind"}<!-- Kan ik mijn kind inschrijven / meeverhuizen? --> 
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - action_set_namens
 - slot{"namens":"kind"}
  - utter_product_verhuismelding_kind_inschrijven_waarnuingeschreven-q<!-- Staat je kind nu bij jou ingeschreven? --> 
* inform_denial <!-- Nee -->
  - utter_overig_kindjongerdan18-q<!-- Is je kind jonger dan 18? --> 
* inform_affirmative OR inform_oke <!-- Ja -->
  - utter_product_verhuismelding_kind_inschrijven_uitwonend_minderjarig <!-- Je kind is minderjarig. Dan moet je de verhuizing doorgeven via een formulier of via een afspraak. De andere ouder moet schriftelijk toestemming geven. Ook heb je een kopie van het paspoort, rijbewijs of de id-kaart van de andere ouder nodig.  --> 
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false} <!-- [ik herken een niet-aangesloten gemeente; ik val terug op samenwerkende catalogi] --> 
  - action_samenwerkendecatalogi_toelichting <!-- Hiervoor kan ik je nog niet uitgebreid helpen. Maar ik ga voor je zoeken. -->
  - action_samenwerkendecatalogi_resultaat <!-- Voor [product] in de gemeente [municipality] heb ik het volgende voor je gevonden: [lijst met links] --> 
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_kind_inschrijven_uitwonend_meerderjarig_aangeslotengemeente 3a/3
* product_verhuismelding_ander_inschrijven{"namens": "kind", "product":"verhuismelding"} OR namenswie{"namens": "kind"} OR product_verhuismelding_doorgeven_namenswie{"product": "verhuismelding", "namens": "kind"}<!-- Kan ik mijn kind inschrijven / meeverhuizen? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - action_set_namens
  - slot{"namens":"kind"}
  - utter_product_verhuismelding_kind_inschrijven_waarnuingeschreven-q<!-- Staat je kind nu bij jou ingeschreven? -->
* inform_denial <!-- Nee -->
  - utter_overig_kindjongerdan18-q <!-- Is je kind jonger dan 18? --> 
* inform_denial <!-- Nee -->
  - utter_product_verhuismelding_kind_inschrijven_uitwonend_meerderjarig <!-- Je kind is meerderjarig. Dan kan je kind de verhuizing alleen zelf doorgeven. -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":true} <!-- [ik herken een aangesloten gemeente] --> 
  - utter_landingpage_verhuisformulier <!-- Je vindt het online formulier op [url] of je kunt bellen voor een afspraak met [telefoonnummer]. -->
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_kind_inschrijven_uitwonend_meerderjarig_nietaangeslotengemeente 3b/3
* product_verhuismelding_ander_inschrijven{"namens": "kind", "product":"verhuismelding"} OR namenswie{"namens": "kind"} OR product_verhuismelding_doorgeven_namenswie{"product": "verhuismelding", "namens": "kind"}<!-- Kan ik mijn kind inschrijven / meeverhuizen? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - action_set_namens
  - slot{"namens":"kind"}
  - utter_product_verhuismelding_kind_inschrijven_waarnuingeschreven-q<!-- Staat je kind nu bij jou ingeschreven? -->
* inform_denial <!-- Nee -->
  - utter_overig_kindjongerdan18-q <!-- Is je kind jonger dan 18? --> 
* inform_denial <!-- Nee -->
  - utter_product_verhuismelding_kind_inschrijven_uitwonend_meerderjarig <!-- Je kind is meerderjarig. Dan kan je kind de verhuizing alleen zelf doorgeven. -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false} <!-- [ik herken een niet-aangesloten gemeente; ik val terug op samenwerkende catalogi] --> 
  - action_samenwerkendecatalogi_toelichting <!-- Hiervoor kan ik je nog niet uitgebreid helpen. Maar ik ga voor je zoeken. -->
  - action_samenwerkendecatalogi_resultaat <!-- Voor [product] in de gemeente [municipality] heb ik het volgende voor je gevonden: [lijst met links] --> 
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->



### Kind op kamers    

## product_verhuismelding_kind_opkamers_uitwonend_minderjarig_aangeslotengemeente 1a/2
* product_verhuismelding_kind_opkamers{"product":"verhuismelding", "namens":"kind"} OR product_verhuismelding_kind_opkamers{"namens":"kind"} <!-- Mijn kind gaat op kamers, kan ik het nieuwe adres doorgeven? --> 
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - slot{"namens":"kind"}
  - utter_overig_kindjongerdan18-q <!-- Is je kind jonger dan 18? -->
* inform_affirmative OR inform_oke <!--Ja -->
  - utter_product_verhuismelding_kind_inschrijven_uitwonend_minderjarig <!-- Geef dit schriftelijk door of maak een afspraak. De andere ouder moet toestemming geven ( formulier + kopie id) --> 
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":true} <!-- [ik herken een aangesloten gemeente] --> 
  - utter_landingpage_verhuisformulier <!-- Je vindt het online formulier op [url] of je kunt bellen voor een afspraak met [telefoonnummer]. -->
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_kind_opkamers_uitwonend_minderjarig_nietaangeslotengemeente 1b/2
* product_verhuismelding_kind_opkamers{"product":"verhuismelding", "namens":"kind"} OR product_verhuismelding_kind_opkamers{"namens":"kind"} <!-- Mijn kind gaat op kamers, kan ik het nieuwe adres doorgeven? --> 
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - slot{"namens":"kind"}
  - utter_overig_kindjongerdan18-q <!-- Is je kind jonger dan 18? -->
* inform_affirmative OR inform_oke <!--Ja -->
  - utter_product_verhuismelding_kind_inschrijven_uitwonend_minderjarig <!-- Geef dit schriftelijk door of maak een afspraak. De andere ouder moet toestemming geven ( formulier + kopie id) --> 
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false} <!-- [ik herken een niet-aangesloten gemeente; ik val terug op samenwerkende catalogi] --> 
  - action_samenwerkendecatalogi_toelichting <!-- Hiervoor kan ik je nog niet uitgebreid helpen. Maar ik ga voor je zoeken. -->
  - action_samenwerkendecatalogi_resultaat <!-- Voor [product] in de gemeente [municipality] heb ik het volgende voor je gevonden: [lijst met links] --> 
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_kind_opkamers_uitwonend_meerderjarig_aangeslotengemeente 2a/2 #ts
* product_verhuismelding_kind_opkamers{"product":"verhuismelding", "namens":"kind"} OR product_verhuismelding_kind_opkamers{"namens":"kind"} <!-- Mijn kind gaat op kamers, kan ik het nieuwe adres doorgeven? --> 
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - slot{"namens":"kind"}
  - utter_overig_kindjongerdan18-q <!-- Is je kind jonger dan 18? --> 
* inform_denial <!-- Nee --> 
  - utter_product_verhuismelding_kind_inschrijven_uitwonend_meerderjarig <!-- Dan kan je kind de verhuizing alleen zelf doorgeven. --> 
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":true} <!-- [ik herken een aangesloten gemeente] --> 
  - utter_landingpage_verhuisformulier <!-- Je vindt het online formulier op [url] of je kunt bellen voor een afspraak met [telefoonnummer]. -->
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_kind_opkamers_uitwonend_meerderjarig_nietaangeslotengemeente 2b/2
* product_verhuismelding_kind_opkamers{"product":"verhuismelding", "namens":"kind"} OR product_verhuismelding_kind_opkamers{"namens":"kind"} <!-- Mijn kind gaat op kamers, kan ik het nieuwe adres doorgeven? --> 
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - slot{"namens":"kind"}
  - utter_overig_kindjongerdan18-q <!-- Is je kind jonger dan 18? --> 
* inform_denial <!-- Nee --> 
  - utter_product_verhuismelding_kind_inschrijven_uitwonend_meerderjarig <!-- Dan kan je kind de verhuizing alleen zelf doorgeven. --> 
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false} <!-- [ik herken een niet-aangesloten gemeente; ik val terug op samenwerkende catalogi] --> 
  - action_samenwerkendecatalogi_toelichting <!-- Hiervoor kan ik je nog niet uitgebreid helpen. Maar ik ga voor je zoeken. -->
  - action_samenwerkendecatalogi_resultaat <!-- Voor [product] in de gemeente [municipality] heb ik het volgende voor je gevonden: [lijst met links] --> 
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->


### Immigratie

## product_verhuismelding_vanuitbuitenland_hoe 1/1
* product_verhuismelding_vanuitbuitenland_hoe{"product": "verhuismelding"} OR product_verhuismelding_vanuitbuitenland_hoe{"product": "verhuismelding", "land": "Marokko"}<!-- Hoe geef ik mijn verhuizing vanuit het buitenland door? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_vanuitbuitenland_hoe <!-- Dat is afhankelijk van je situatie. Kijk hiervoor op [www.ind.nl](https://www.ind.nl). --> 
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->


#### Emigratie     #@# Testen: opsplitsing naar aangeslotengemeente vs. nietaangeslotengemeente

## product_verhuismelding_naarbuitenland_langerdan8mnd_aangeslotengemeente 1a/2 
* product_verhuismelding_naarbuitenland OR product_verhuismelding_naarbuitenland{"product": "verhuismelding"} OR product_verhuismelding_naarbuitenland{"product": "verhuismelding", "land": "Marokko"} OR product_verhuismelding_naarbuitenland{"land": "Marokko"}<!-- Wanneer moet ik me uitschrijven bij de gemeente? --> 
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_naarbuitenland_langerofkorterdan8mnd-q <!-- Verblijf je in 1 jaar langer dan 8 maanden in het buitenland? --> 
* inform_affirmative OR inform_oke OR inform_maybe OR inform_dontknow <!-- Ja / misschien / weet niet --> 
  - utter_product_verhuismelding_naarbuitenland_langerdan8mnd <!-- Als je 8 maanden of langer in het buitenland verbijft, moet je je uitschrijven bij de gemeente. --> 
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":true} <!-- [ik herken een aangesloten gemeente] --> 
  - utter_landingpage_emigratie <!-- De pagina 'Vertrek naar buitenland' vind je hier: [lokale url] -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_naarbuitenland_langerdan8mnd_nietaangeslotengemeente 1b/2 
* product_verhuismelding_naarbuitenland OR product_verhuismelding_naarbuitenland{"product": "verhuismelding"} OR product_verhuismelding_naarbuitenland{"product": "verhuismelding", "land": "Marokko"} OR product_verhuismelding_naarbuitenland{"land": "Marokko"}<!-- Wanneer moet ik me uitschrijven bij de gemeente? --> 
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_naarbuitenland_langerofkorterdan8mnd-q <!-- Verblijf je in 1 jaar langer dan 8 maanden in het buitenland? --> 
* inform_affirmative OR inform_oke OR inform_maybe OR inform_dontknow <!-- Ja / misschien / weet niet --> 
  - utter_product_verhuismelding_naarbuitenland_langerdan8mnd <!-- Als je 8 maanden of langer in het buitenland verbijft, moet je je uitschrijven bij de gemeente. --> 
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false} <!-- [ik herken een niet-aangesloten gemeente; ik val terug op samenwerkende catalogi] --> 
  - utter_landingpage_emigratie-rijksoverheid <!-- Meer informatie over vertrek naar het buitenland vind je op rijksoverheid.nl/... -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_naarbuitenland_korterdan8mnd 2/2
* product_verhuismelding_naarbuitenland OR product_verhuismelding_naarbuitenland{"product": "verhuismelding"} OR product_verhuismelding_naarbuitenland{"product": "verhuismelding", "land": "Marokko"} OR product_verhuismelding_naarbuitenland{"land": "Marokko"}<!-- Wanneer moet ik me uitschrijven bij de gemeente? --> 
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_naarbuitenland_langerofkorterdan8mnd-q <!-- Verblijf je in 1 jaar langer dan 8 maanden in het buitenland? --> 
* inform_denial <!-- Nee --> 
  - utter_product_verhuismelding_naarbuitenland_korterdan8mnd <!-- Je hoeft je niet uit te schrijven. --> 
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->


### Waarom / verplicht / boete?

## product_verhuismelding_doorgeven_waarom 1/1
* product_verhuismelding_verplicht{"product": "verhuismelding"} OR waarom{"product": "verhuismelding"} OR waarom<!-- (waarom) Moet ik mijn verhuizing doorgeven? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_waarom <!-- "Volgens de wet ben je verplicht om je verhuizing door te geven aan de gemeente waar je gaat wonen. \n \nOok de termijn van 4 weken vóór, tot uiterlijk 5 dagen na de verhuisdatum, is wettelijk verplicht om de administratie zo goed en betrouwbaar mogelijk te laten verlopen." --> 
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_doorgeven_boete 1/1
* product_verhuismelding_boete{"product": "verhuismelding"} <!-- Krijg ik een boete als ik mijn verhuizing te laat doorgeef? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_boete <!-- Als je je verhuizing te laat doorgeeft, houd je je niet aan een wettelijke regel. Daar kan een boete voor gegeven worden. De gemeente besluit of die boete ook daadwerkelijk wordt gegeven.  --> 
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->


#### Wat kost dat

## product_verhuismelding_doorgeven_watkostdat 1/1 #tsdemo
* watkostdat OR watkostdat{"product": "verhuismelding"} <!-- Wat kost dat? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_doorgeven_watkostdat <!-- Een verhuizing doorgeven aan je gemeente is gratis. --> 
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->



### Aan welke instanties

## product_verhuismelding_doorgeven_aanwelkeinstanties 1/1
* product_verhuismelding_doorgeven_aanwelkeinstanties{"product": "verhuismelding"} <!-- Aan welke instanties geven jullie mijn verhuizing door? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_doorgeven_aanwelkeinstanties <!--De gemeente geeft je verhuizing door aan alle instanties die deze gegevens nodig hebben... Zie [wiekrijgtmijngegevens.nl] --> 
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->


#### Verhuismelding > Wie

## product_verhuismelding_doorgeven_wie_aangeslotengemeente 1/2
* wie <!-- Wie moet dat doen? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product": "verhuismelding"} 
  - utter_product_verhuismelding_doorgeven_wiemagdat <!-- Je geeft zelf je verhuizing door in de gemeente waar je gaat wonen. -->
  - utter_product_verhuismelding_doorgeven_namenswie <!-- "Je kunt een verhuizing doorgeven namens:  \n- je samenwonende echtgenoot of geregistreerd partner  \n- je minderjarige kinderen   \n- je meerderjarige inwonende kinderen  \n\nZie de pagina 'verhuizen' voor meer mogelijkheden. \n\nAls er een echtscheiding is, kan het zijn dat de regels anders zijn. Neem dan contact met ons op." -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": true} <!-- [ik herken een aangesloten gemeente] -->
  - utter_landingpage_verhuizen <!-- "De pagina 'verhuizen' vind je hier: [lokale webpagina verhuizen] -->
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_doorgeven_wie_nietaangeslotengemeente 2/2
* wie <!-- Wie moet dat doen? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product": "verhuismelding"} 
  - utter_product_verhuismelding_doorgeven_wiemagdat <!-- Je geeft zelf je verhuizing door in de gemeente waar je gaat wonen. -->
  - utter_product_verhuismelding_doorgeven_namenswie <!-- "Je kunt een verhuizing doorgeven namens:  \n- je samenwonende echtgenoot of geregistreerd partner  \n- je minderjarige kinderen   \n- je meerderjarige inwonende kinderen  \n\nZie de pagina 'verhuizen' voor meer mogelijkheden. \n\nAls er een echtscheiding is, kan het zijn dat de regels anders zijn. Neem dan contact met ons op." -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false} <!-- [ik herken een niet-aangesloten gemeente; ik val terug op samenwerkende catalogi] --> 
  - action_samenwerkendecatalogi_intro <!-- Je hebt een vraag over ee [product] in de gemeente [municipality] -->
  - action_samenwerkendecatalogi_toelichting <!-- Hiervoor kan ik je nog niet uitgebreid helpen. Maar ik ga voor je zoeken. -->
  - action_samenwerkendecatalogi_resultaat <!-- Voor [product] in de gemeente [municipality] heb ik het volgende voor je gevonden: [lijst met links] --> 
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->


#### Verhuismelding > Namens wie (partner/hulpbehoevende/iemand anders)

## product_verhuismelding_doorgeven_namenswie_partner_aangeslotengemeente 1a/3 #tsdemo
* product_verhuismelding_doorgeven_namenswie{"namens": "partner"} OR product_verhuismelding_doorgeven_namenswie{"product": "verhuismelding", "namens": "partner"} OR namenswie{"namens": "partner"} OR product_verhuismelding_ander_inschrijven{"namens": "partner", "product":"verhuismelding"} <!-- Kan ik het voor mijn partner doorgeven? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product": "verhuismelding"} 
  - action_set_namens 
  - slot{"namens": "partner"} 
  - utter_product_verhuismelding_doorgeven_namens_partner <!-- Je kunt een verhuizing doorgeven namens je samenwonende echtgenoot of geregistreerd partner. Dit kun je tegelijk met je eigen verhuizing doorgeven. -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": true} <!-- [ik herken een aangesloten gemeente] -->
  - utter_landingpage_verhuizen <!-- "De pagina 'verhuizen' vind je hier: [lokale webpagina verhuizen] -->
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_doorgeven_namenswie_hulpbehoevende_aangeslotengemeente 1b/3
* product_verhuismelding_doorgeven_namenswie{"namens": "hulpbehoevende"} OR product_verhuismelding_doorgeven_namenswie{"product": "verhuismelding", "namens": "hulpbehoevende"} OR namenswie{"namens": "hulpbehoevende"} <!-- Kan ik het voor mijn parner doorgeven? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product": "verhuismelding"} 
  - action_set_namens 
  - slot{"namens": "hulpbehoevende"} 
  - utter_product_verhuismelding_doorgeven_namens_hulpbehoevende <!-- "Als iemand onder curatele staat, kan de curator de verhuizing doorgeven. \n \n Als een patiënt zelf niet in staat is om de verhuizing door te geven, mag een hoofd van een instelling voor gezondheidszorg dat doen." -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": true} <!-- [ik herken een aangesloten gemeente] -->
  - utter_landingpage_verhuizen <!-- "De pagina 'verhuizen' vind je hier: [lokale webpagina verhuizen] -->
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_doorgeven_namenswie_partner_nietaangeslotengemeente 2a/3
* product_verhuismelding_doorgeven_namenswie{"namens": "partner"} OR product_verhuismelding_doorgeven_namenswie{"product": "verhuismelding", "namens": "partner"} OR namenswie{"namens": "partner"} OR product_verhuismelding_ander_inschrijven{"namens": "partner", "product":"verhuismelding"} <!-- Kan ik het voor mijn parner doorgeven? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product": "verhuismelding"} 
  - action_set_namens 
  - slot{"namens": "partner"} 
  - utter_product_verhuismelding_doorgeven_namens_partner <!-- Je kunt een verhuizing doorgeven namens je samenwonende echtgenoot of geregistreerd partner. Dit kun je tegelijk met je eigen verhuizing doorgeven. -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false} <!-- [ik herken een niet-aangesloten gemeente; ik val terug op samenwerkende catalogi] --> 
  - action_samenwerkendecatalogi_toelichting <!-- Voor de gemeente [municipality] kan ik je nog niet uitgebreid helpen. Maar ik ga voor je zoeken. -->
  - action_samenwerkendecatalogi_resultaat <!-- Voor [product] in de gemeente [municipality] heb ik het volgende voor je gevonden: [lijst met links] --> 
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_doorgeven_namenswie_hulpbehoevende_nietaangeslotengemeente 2b/3
* product_verhuismelding_doorgeven_namenswie{"namens": "hulpbehoevende"} OR product_verhuismelding_doorgeven_namenswie{"product": "verhuismelding", "namens": "hulpbehoevende"} OR namenswie{"namens": "hulpbehoevende"} <!-- Kan ik het voor mijn parner doorgeven? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product": "verhuismelding"} 
  - action_set_namens 
  - slot{"namens": "hulpbehoevende"} 
  - utter_product_verhuismelding_doorgeven_namens_hulpbehoevende <!-- "Als iemand onder curatele staat, kan de curator de verhuizing doorgeven. \n \n Als een patiënt zelf niet in staat is om de verhuizing door te geven, mag een hoofd van een instelling voor gezondheidszorg dat doen." -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false} <!-- [ik herken een niet-aangesloten gemeente; ik val terug op samenwerkende catalogi] --> 
  - action_samenwerkendecatalogi_toelichting <!-- Voor de gemeente [municipality] kan ik je nog niet uitgebreid helpen. Maar ik ga voor je zoeken. -->
  - action_samenwerkendecatalogi_resultaat <!-- Voor [product] in de gemeente [municipality] heb ik het volgende voor je gevonden: [lijst met links] -->
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_doorgeven_namenswie_aangeslotengemeente 3a/3
* product_verhuismelding_doorgeven_namenswie OR product_verhuismelding_doorgeven_namenswie{"product": "verhuismelding"} OR namenswie <!-- Namens wie kan ik het doorgeven? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product": "verhuismelding"} 
  - action_set_namens 
  - utter_product_verhuismelding_doorgeven_namenswie <!-- "Je kunt een verhuizing doorgeven namens:  \n- je samenwonende echtgenoot of geregistreerd partner  \n- je minderjarige kinderen   \n- je meerderjarige inwonende kinderen  \n\nZie de pagina 'verhuizen' voor meer mogelijkheden. \n\nAls er een echtscheiding is, kan het zijn dat de regels anders zijn. Neem dan contact met ons op." -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":true} <!-- [ik herken een aangesloten gemeente] --> 
  - utter_landingpage_verhuizen <!-- "De pagina 'verhuizen' vind je hier: [lokale webpagina verhuizen] -->
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_doorgeven_namenswie_nietaangeslotengemeente 3b/3
* product_verhuismelding_doorgeven_namenswie OR product_verhuismelding_doorgeven_namenswie{"product": "verhuismelding"} OR namenswie <!-- Namens wie kan ik het doorgeven? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product": "verhuismelding"} 
  - action_set_namens 
  - utter_product_verhuismelding_doorgeven_namenswie <!-- "Je kunt een verhuizing doorgeven namens:  \n- je samenwonende echtgenoot of geregistreerd partner  \n- je minderjarige kinderen   \n- je meerderjarige inwonende kinderen  \n\nZie de pagina 'verhuizen' voor meer mogelijkheden. \n\nAls er een echtscheiding is, kan het zijn dat de regels anders zijn. Neem dan contact met ons op." -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false} <!-- [ik herken een niet-aangesloten gemeente; ik val terug op samenwerkende catalogi] --> 
  - action_samenwerkendecatalogi_toelichting <!-- Voor de gemeente [municipality] kan ik je nog niet uitgebreid helpen. Maar ik ga voor je zoeken. -->
  - action_samenwerkendecatalogi_resultaat <!-- Voor [product] in de gemeente [municipality] heb ik het volgende voor je gevonden: [lijst met links] --> 
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->




#### ALLE SPECIFIEKE VRAGEN (lokaal)


### Online doorgeven lukt niet

## product_verhuismelding_onlineluktniet_aangeslotengemeente 1/2
* product_verhuismelding_onlineluktniet OR product_verhuismelding_onlineluktniet{"product": "verhuismelding"} <!-- Online doorgeven lukt niet -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product": "verhuismelding"}
  - utter_watvervelend  <!-- Wat vervelend dat het niet lukt -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":true} <!-- [ik herken een aangesloten gemeente] -->
  - utter_overig_beterevenbellen <!-- Je kunt ons dan het beste even bellen. We zijn tijdens kantoortijden bereikbaar via {gemeente_telefoonnummer}. -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_onlineluktniet_nietaangeslotengemeente 2/2
* product_verhuismelding_onlineluktniet OR product_verhuismelding_onlineluktniet{"product": "verhuismelding"} <!-- Online doorgeven lukt niet -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product": "verhuismelding"}
  - utter_watvervelend <!-- Wat vervelend dat het niet lukt -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false}
  - action_samenwerkendecatalogi_toelichting <!-- Voor de gemeente [municipality] kan ik je nog niet uitgebreid helpen. Maar ik ga voor je zoeken. -->
  - action_samenwerkendecatalogi_resultaat <!-- Voor [product] in de gemeente [municipality] heb ik het volgende voor je gevonden: [lijst met links] -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->


### Wat meesturen

## product_verhuismelding_watmeesturen_aangeslotengemeente 1/2
* product_verhuismelding_doorgeven_watnodig{"product": "verhuismelding"} OR watmeesturen OR watnodig OR watmeenemen OR wat <!-- Wat moet ik dan allemaal meesturen? / Wat heb ik dan allemaal nodig? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product": "verhuismelding"}
  - action_check_municipality         <!-- In welke gemeente ga je wonen? -->
  - slot{"feature_content": true}     <!-- [ik herken een aangesloten gemeente] -->
  - utter_product_verhuismelding_doorgeven_watmeesturen <!-- Meestal niets, maar soms wel iets. Wat, dat kun je lezen op de website. -->
  - utter_landingpage_verhuizen       <!-- De pagina 'verhuizen' vind je hier: [lokale url] -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_watmeesturen_nietaangeslotengemeente 2/2
* product_verhuismelding_doorgeven_watnodig{"product": "verhuismelding"} OR watmeesturen OR watnodig OR watmeenemen OR wat                          <!-- Wat moet ik dan nog meer meesturen? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product": "verhuismelding"}
  - action_check_municipality         <!-- In welke gemeente ga je wonen? -->
  - slot{"feature_content": false}    <!-- [ik herken een niet-aangesloten gemeente; ik val terug op samenwerkende catalogi] --> 
  - action_samenwerkendecatalogi_toelichting <!-- Voor de gemeente [municipality] kan ik je nog niet uitgebreid helpen. Maar ik ga voor je zoeken. -->
  - action_samenwerkendecatalogi_resultaat      <!-- Volgens mij staat je antwoord op...-->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->


### Hoe doorgeven 

## product_verhuismelding_doorgeven_hoe_aangeslotengemeente 1/2 #tsdemo
* hoe_doorgeven{"product": "verhuismelding"} OR hoe OR hoe{"product": "verhuismelding"} OR aangiftedoen OR aangiftedoen{"product": "verhuismelding"} OR waar OR waar{"product": "verhuismelding"} OR waar{"product": "verhuismelding", "municipality": "Utrecht"}<!-- Hoe geef ik mijn verhuizing door? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product": "verhuismelding"} 
  - utter_product_verhuismelding_doorgeven_waar <!-- Je geeft je verhuizing door in de gemeente waar je gaat wonen -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": true} <!-- [ik herken een aangesloten gemeente] -->
  - utter_product_verhuismelding_doorgeven_hoe <!-- Voor {municipality} kun je je verhuizing online doorgeven met DigiD. -->
  - utter_landingpage_verhuisformulier <!-- Je vindt het online formulier op [url] of je kunt bellen voor een afspraak met [telefoonnummer]. -->
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_doorgeven_hoe_nietaangeslotengemeente 2/2
* hoe_doorgeven{"product": "verhuismelding"} OR hoe OR hoe{"product": "verhuismelding"} OR aangiftedoen OR aangiftedoen{"product": "verhuismelding"} OR waar OR waar{"product": "verhuismelding"} OR waar{"product": "verhuismelding", "municipality": "Utrecht"}<!-- Hoe geef ik mijn verhuizing door? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product": "verhuismelding"} 
  - utter_product_verhuismelding_doorgeven_waar <!-- Je geeft je verhuizing door in de gemeente waar je gaat wonen -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false} <!-- [ik herken een niet-aangesloten gemeente; ik val terug op samenwerkende catalogi] --> 
  - action_samenwerkendecatalogi_toelichting <!-- Voor de gemeente [municipality] kan ik je nog niet uitgebreid helpen. Maar ik ga voor je zoeken. -->
  - action_samenwerkendecatalogi_resultaat <!-- Voor [product] in de gemeente [municipality] heb ik het volgende voor je gevonden: [lijst met links] --> 
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->


### Online verhuizing doorgeven 

## product_verhuismelding_doorgeven_online_aangeslotengemeente 1/2
* product_verhuismelding_doorgeven_online{"product": "verhuismelding"} OR digitaal <!-- Kan ik mijn verhuizing online doorgeven? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product": "verhuismelding"} 
  - utter_product_verhuismelding_doorgeven_online <!-- Je kunt je verhuizing online doorgeven in de gemeente waar je gaat wonen.  -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": true} <!-- [ik herken een aangesloten gemeente] -->
  - utter_landingpage_verhuisformulier <!-- Je vindt het online formulier op [url] of je kunt bellen voor een afspraak met [telefoonnummer]. -->
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_doorgeven_online_nietaangeslotengemeente 2/2
* product_verhuismelding_doorgeven_online{"product": "verhuismelding"} OR digitaal <!-- Kan ik mijn verhuizing online doorgeven? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product": "verhuismelding"} 
  - utter_product_verhuismelding_doorgeven_online <!-- Je kunt je verhuizing online doorgeven in de gemeente waar je gaat wonen.  -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false} <!-- [ik herken een niet-aangesloten gemeente; ik val terug op samenwerkende catalogi] --> 
  - action_samenwerkendecatalogi_toelichting <!-- Voor de gemeente [municipality] kan ik je nog niet uitgebreid helpen. Maar ik ga voor je zoeken. -->
  - action_samenwerkendecatalogi_resultaat <!-- Voor [product] in de gemeente [municipality] heb ik het volgende voor je gevonden: [lijst met links] --> 
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->


### Verhuizing naar nieuwbouwwoning 

## product_verhuismelding_nieuwbouw_aangeslotengemeente 1/2
* product_verhuismelding_nieuwbouw{"product": "verhuismelding"} <!-- Hoe kan ik een adreswijziging naar een nieuwbouwwoning doorgeven?  -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product": "verhuismelding"} 
  - utter_product_verhuismelding_doorgeven_nieuwbouw <!-- Een verhuizing naar een nieuwbouwwoning geef je online door in de gemeente waar je gaat wonen.  Als het nieuwe adres niet herkend wordt, neem dan even contact op. -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": true} <!-- [ik herken een aangesloten gemeente] -->
  - utter_landingpage_verhuisformulier <!-- Je vindt het online formulier op [url] of je kunt bellen voor een afspraak met [telefoonnummer]. -->
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_nieuwbouw_nietaangeslotengemeente 2/2
* product_verhuismelding_nieuwbouw{"product": "verhuismelding"} <!-- Hoe kan ik een adreswijziging naar een nieuwbouwwoning doorgeven?  -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product": "verhuismelding"} 
  - utter_product_verhuismelding_doorgeven_nieuwbouw <!-- Een verhuizing naar een nieuwbouwwoning geef je online door in de gemeente waar je gaat wonen.  Als het nieuwe adres niet herkend wordt, neem dan even contact op. -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false} <!-- [ik herken een niet-aangesloten gemeente; ik val terug op samenwerkende catalogi] --> 
  - action_samenwerkendecatalogi_toelichting <!-- Voor de gemeente [municipality] kan ik je nog niet uitgebreid helpen. Maar ik ga voor je zoeken. -->
  - action_samenwerkendecatalogi_resultaat <!-- Voor [product] in de gemeente [municipality] heb ik het volgende voor je gevonden: [lijst met links] --> 
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->


### Wat allemaal regelen?

## product_verhuismelding_watregelen 1/1
* product_verhuismelding_watregelen{"product": "verhuismelding"} <!-- Wat moet ik allemaal regelen als ik ga verhuizen?  -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product": "verhuismelding"} 
  - utter_product_verhuismelding_watregelen <!-- Ik kan je uitleggen hoe je je verhuizing doorgeeft aan je gemeente. Wil je dat? [ja > doorgeven_hoe] [nee > iets_anders] -->
  - action_helpmore  <!-- Kan ik je verder nog ergens mee helpen? -->
  
  
#### Afspraak maken of afspraak nodig?

## product_verhuismelding_doorgeven_afspraak 1/1
* afspraaknodig OR afspraaknodig{"product": "verhuismelding"} OR afspraakmaken OR afspraakmaken{"product": "verhuismelding"} <!-- Heb ik dan een afspraak nodig? -->
  - action_check_product <!-- Check of product al bekend is. Zo niet, vraag hier eerst om. -->
  - slot{"product":"verhuismelding"}
  - utter_product_verhuismelding_doorgeven_afspraak-nodig <!-- "Als je je verhuizing online doorgeeft, hoef je geen afspraak te maken." -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? --> 


#### Verhuizing doorgeven zonder DigiD: zie 'overige_stories.md'

#### Algemene stories verdiepingsvragen: zie verdieping.md
* product_verhuismelding_doorgeven_wanneer{"product": "verhuismelding"}
    - slot{"product": "verhuismelding"}
    - action_check_product
    - slot{"product": "verhuismelding"}
    - form_when_verhuismelding
    - form{"name": "form_when_verhuismelding"}
* form: inform_dontknow
    - form{"name": null}
    - utter_product_verhuismelding_meldperiode_algemeen
    - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? --> 
