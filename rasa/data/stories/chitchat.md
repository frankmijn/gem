# ## chitchat_path
# * chitchat
#   - utter_chitchat

## chitchat_hoegaathet #tsdemo
* chitchat_hoegaathet <!-- Hoe gaat het met je? -->
  - utter_chitchat_hoegaathet <!-- Heel goed, dank je wel. Waar kan ik je mee helpen? -->

## chitchat_benjeerklaarvoor
* benjeerklaarvoor <!-- Ha Gem, ben je er klaar voor? -->
  - utter_ikbenerklaarvoor <!-- Jazeker! Op dit moment heb ik maanden gewacht! -->

## bedankt #tsdemo2
* thanks OR compliment <!-- Bedankt!  of  Super! -->
  - utter_graaggedaan <!-- Graag gedaan -->
  
## greet
* greet <!-- Hallo -->
  - utter_greet <!-- Hallo, ik ben Gem, de virtuele gemeenteassistent.  -->
  - utter_greet2 <!-- Je kunt mij vragen stellen over verhuizen, paspoort en ID-kaart. -->
  - utter_howcanihelp <!-- Waarmee kan ik je helpen? -->
  - action_back

# ## greet_back 
# * greet
#   - utter_greet
#   - utter_greet2
#   - utter_howcanihelp
#   - action_back
#  * greet
#   - utter_greet_response

## unfriendly_path
* chitchat_unfriendly <!-- Stomme bot -->
  - utter_feedback_unfriendly_intro <!-- Als chatbot doe ik mijn uiterste best, maar wellicht kan ik je hier niet mee helpen. Wil je… [Naar een medewerker] [Feedback geven] [Verder met de chatbot] -->
* feedback_chatbot <!-- Feedback op de chatbot geven -->
  - action_set_feedback_negatief <!-- Start feedbackformulier met ontevreden scenario -->
  - form_feedback
  - form{"name": "form_feedback"}
  - form{"name": null}
  
### Wie is de burgemeester

## Wie is de burgemeester van... 1/2 #tsdemo
* wie_is_de_burgemeester_van <!-- Wie is de burgemeester van gemeente? -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": true}
  - action_wieisdeburgemeester <!-- Pik de gemeentenaam op. Herken je die niet, vraag dan naar gemeentenaam -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## Wie is de burgemeester van... 2/2 #tsdemo
* wie_is_de_burgemeester_van <!-- Wie is de burgemeester van gemeente? -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": false}
  - action_wieisdeburgemeester <!-- Pik de gemeentenaam op. Herken je die niet, vraag dan naar gemeentenaam -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->
  
## cbs population
* ask_population
  - action_cbs_population
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## watkangem #tsdemo
* watkangem <!-- Wat kun jij allemaal? -->
  - utter_watkangem <!-- Je kunt mij vragen stellen over verhuizen, paspoort en ID-kaart. -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## Ben jij een mens?
* chitchat_ben_jij_een_mens <!-- Ben jij een mens? -->
  - utter_chitchat_ben_jij_een_mens <!-- Ik ben een robot. Ik ben getraind om je te helpen met vragen aan je gemeente. -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## Wil je met me trouwen?
* chitchat_wil_je_met_me_trouwen <!-- Wil je met me trouwen? -->
  - utter_chitchat_wil_je_met_me_trouwen <!--Ik ben een robot. Van de wet mag ik helaas niet trouwen. -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## Niet trouwen - wat jammer
* chitchat_wil_je_met_me_trouwen <!-- Wil je met me trouwen? -->
  - utter_chitchat_wil_je_met_me_trouwen <!--Ik ben een robot. Van de wet mag ik helaas niet trouwen. -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->
* jammer <!-- Wat jammer -->
  - utter_chitchat_beter_kennen <!-- Laten we elkaar eerst beter leren kennen. Waar kan ik je mee helpen? -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## Mag ik wat vragen
* mag_ik_wat_vragen <!-- Mag ik wat vragen? -->
  - utter_chitchat_mag_ik_wat_vragen <!-- Natuurlijk! -->
  
## Chitchat oke #ts
* inform_oke <!-- Oké -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->
