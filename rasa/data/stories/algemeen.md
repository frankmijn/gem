#### Heb geen / wil geen DigiD

## overig_digid_hebgeendigid 1/3 #ts
* overig_digid_hebgeendigid <!-- Ik heb geen DigiD --> 
  - utter_overig_digid_aanvragen <!-- Je kunt een DigiD aanvragen via... --> 
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## overig_digid_wilgeendigid_aangeslotengemeente 2/3 #ts
* overig_digid_wilgeendigid  <!-- Ik wil geen gebruik maken van DigiD. -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":true}
  - utter_overig_digid_wilgeendigid <!-- Kun je of wil je het niet online regelen, neem dan contact op via mail of telefoon. [contactgegevens] -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## overig_digid_wilgeendigid_nietaangeslotengemeente 3/3
* overig_digid_wilgeendigid <!-- Ik wil geen gebruik maken van DigiD. -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false}
  - utter_overig_digid_wilgeendigid <!-- Kun je of wil je het niet online regelen, neem dan contact op via mail of telefoon. [contactgegevens] -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->


#### Heb geen / wil geen DigiD bij verhuizing

## product_verhuismelding_zonderdigid_aangeslotengemeente 1/2
* overig_digid_hebgeendigid{"product": "verhuismelding"} OR overig_digid_wilgeendigid{"product": "verhuismelding"} <!-- ik heb geen digid  OF  Ik wil geen digid gebruiken -->
  - slot{"product": "verhuismelding"} 
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": true} <!-- [ik herken een aangesloten gemeente] -->
  - utter_overig_digid_aanvragen <!-- Je kunt DigiD aanvragen via [digid.nl]. -->
  - utter_product_verhuismelding_doorgeven_zonderdigid <!-- Kun je of wil je je verhuizing niet online regelen, geef je verhuizing dan schriftelijk door of maak een afspraak. Hoe je dat doet, lees je op de pagina 'verhuizen'. -->
  - utter_landingpage_verhuizen <!-- De pagina 'verhuizen' vind je hier: [lokale url] -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## product_verhuismelding_zonderdigid_nietaangeslotengemeente 2/2
* overig_digid_hebgeendigid{"product": "verhuismelding"} OR overig_digid_wilgeendigid{"product": "verhuismelding"} <!-- ik heb geen digid  OF  Ik wil geen digid gebruiken -->
  - slot{"product": "verhuismelding"} 
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": false} <!-- [ik herken een niet-aangesloten gemeente; ik val terug op samenwerkende catalogi] --> 
  - action_samenwerkendecatalogi_intro <!-- Je hebt een vraag over een [product] in de gemeente [municipality] -->
  - action_samenwerkendecatalogi_toelichting <!-- Hiervoor kan ik je nog niet uitgebreid helpen. Maar ik ga voor je zoeken. -->
  - action_samenwerkendecatalogi_resultaat <!-- Voor [product] in de gemeente [municipality] heb ik het volgende voor je gevonden: [lijst met links] --> 
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->


#### Wat is DigiD

## overig_digid_watisdigid 1/1
* overig_digid_watisdigid <!-- Wat is DigiD? --> 
  - utter_overig_digid_watisdigid <!-- Met DigiD kun je inloggen op websites van de overheid. Je kunt DigiD aanvragen via [digid.nl]({https://digid.nl}). --> 
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->





#### Hoe betalen

## producten_hoebetalen 1/1
* producten_hoebetalen <!-- Kan ik met pinpas betalen? -->
  - utter_producten_hoebetalen <!-- Wij geven de voorkeur aan betalen met pinpas aan de balie. Contant betalen kan natuurlijk ook. -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->


#### Foto-eisen

## overig_foto_eisen 1/1 #ts
* overig_foto_eisen <!--Wat zijn de eisen voor een pasfoto? -->
  - utter_overig_foto_eisen <!-- Een pasfoto in een paspoort, identiteitskaart of rijbewijs moet voldoen aan een aantal eisen. Welke dat zijn vind je op  [rijksoverheid.nl](https://www.rijksoverheid.nl/onderwerpen/paspoort-en-identiteitskaart/eisen-pasfoto-paspoort-id-kaart).  Elke officiële fotograaf is op de hoogte van de pasfoto-eisen. -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->



#### Contact met gemeente

## bellen_met_gemeente (algemeen) 1/2 #tsdemo
* bellen_met_gemeente <!-- Kan ik jullie bellen? -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":true} <!-- Gem herkent een aangesloten gemeente -->
  - utter_gemeente_telefoonnummer_lokaal <!-- Je kunt de gemeente [municipality] binnen kantoortijden bellen op... -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## bellen_met_gemeente (algemeen) 2/2
* bellen_met_gemeente <!-- Kan ik jullie bellen? -->  
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false} <!-- Gem herkent een niet-aangesloten gemeente -->
  - utter_gemeente_telefoonnummer_landelijk <!-- Je kunt de gemeente [municipality] binnen kantoortijden bellen op... -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## gemeente_adres (algemeen) 1/2 #tsdemo
* gemeente_adres <!-- Wat is jullie adres? -->     
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":true} <!-- Gem herkent een aangesloten gemeente -->
  - utter_gemeente_adres_lokaal <!-- Je kunt de gemeente [municipality] bezoeken op het volgende adres... -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## gemeente_adres (algemeen) 2/2
* gemeente_adres <!-- Wat is jullie adres? -->   
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false} <!-- Gem herkent een niet-aangesloten gemeente -->
  - utter_gemeente_adres_landelijk <!-- Je kunt de gemeente [municipality] bezoeken op het volgende adres... -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## gemeente_postadres (algemeen) 1/2
* gemeente_postadres <!-- Wat is jullie postadres? -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":true} <!-- Gem herkent een aangesloten gemeente -->
  - utter_gemeente_postadres_lokaal <!-- Je kunt de gemeente [municipality] post sturen op het volgende adres... -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## gemeente_postadres (algemeen) 2/2
* gemeente_postadres <!-- Wat is jullie postadres? -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false} <!-- Gem herkent een niet-aangesloten gemeente -->
  - utter_gemeente_postadres_landelijk  <!-- Je kunt de gemeente [municipality] post sturen op het volgende adres... -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->



#### Contact met medewerker

## hiervoor_is_een_medewerker_nodig_aangeslotengemeente 1/4 #tsdemo
* medewerker_nodig OR fout_gegaan OR fout_gegaan{"product":"verhuismelding"} OR fout_gegaan{"product":"paspoort"} OR fout_gegaan{"product":"id-kaart"} OR afspraak_wijzigen <!-- Vraag waarbij echt een medewerker nodig is -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":true} <!-- Gem herkent een aangesloten gemeente -->
  - utter_medewerker_nodig_metlivechat  <!-- Voor deze vraag kun je het beste een collega van me spreken. Zal ik voor je kijken of ik een livechat kan starten? [livechat] [contactgegevens] [verder met chatbot] -->

## hiervoor_is_een_medewerker_nodig_aangeslotengemeente_livechat-ja 2/4
* medewerker_nodig OR fout_gegaan OR fout_gegaan{"product":"verhuismelding"} OR fout_gegaan{"product":"paspoort"} OR fout_gegaan{"product":"id-kaart"} OR afspraak_wijzigen <!-- Vraag waarbij echt een medewerker nodig is -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":true} <!-- Gem herkent een aangesloten gemeente -->
  - utter_medewerker_nodig_metlivechat  <!-- Voor deze vraag kun je het beste een collega van me spreken. Zal ik voor je kijken of ik een livechat kan starten? [livechat] [contactgegevens] [verder met chatbot] -->
* inform_affirmative OR inform_oke
  - action_escalatie <!-- Start livechat -->
  - action_gespreksgeschiedenis

## hiervoor_is_een_medewerker_nodig_aangeslotengemeente_livechat-nee 3/4
* medewerker_nodig OR fout_gegaan OR fout_gegaan{"product":"verhuismelding"} OR fout_gegaan{"product":"paspoort"} OR fout_gegaan{"product":"id-kaart"} OR afspraak_wijzigen <!-- Vraag waarbij echt een medewerker nodig is -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":true} <!-- Gem herkent een aangesloten gemeente -->
  - utter_medewerker_nodig_metlivechat  <!-- Voor deze vraag kun je het beste een collega van me spreken. Zal ik voor je kijken of ik een livechat kan starten? [livechat] [contactgegevens] [verder met chatbot] -->
* inform_denial
  - utter_gemeente_contact_lokaal <!--  text: Je kunt de gemeente [municipality] binnen kantoortijden bellen op .... Kijk op de [website] voor meer contactgegevens. -->
  - action_gespreksgeschiedenis
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## hiervoor_is_een_medewerker_nodig_nietaangeslotengemeente 4/4
* medewerker_nodig OR fout_gegaan OR fout_gegaan{"product":"verhuismelding"} OR fout_gegaan{"product":"paspoort"} OR fout_gegaan{"product":"id-kaart"} OR afspraak_wijzigen <!-- Vraag waarbij echt een medewerker nodig is -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false} <!-- Gem herkent een niet-aangesloten gemeente -->
  - utter_medewerker_nodig_zonderlivechat  <!-- Voor deze vraag kun je het beste een collega van me spreken. -->
  - utter_gemeente_contact_landelijk <!-- Je kunt de gemeente {municipality} binnen kantoortijden bellen op {gemeente_telefoonnummer} of een e-mail sturen naar {gemeente_emailadres}. -->
  - action_gespreksgeschiedenis
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->


## je-snapt-het-hiet
* je-snapt-het-niet <!-- Je begrijpt me niet -->
  - utter_verdieping_vast <!-- Ik ben bang dat ik je niet goed begrijp. Wil je... [Naar een medewerker] [Verder met de chatbot] -->


#### klachtindienen en feedback

## klachtindienen_algemeen
* klachtindienen_algemeen{"product":"klacht"} <!-- Ik wil een klacht indienen -->
  - slot{"product":"klacht"}
  - utter_feedback_klacht_waarover-q  <!-- Waarover wil je een klacht indienen? [chatbot] [behandeling door medewerker] -->

## klachtindienen over de bot
* klachtindienen_algemeen{"product":"klacht"} <!-- Ik wil een klacht indienen -->
  - slot{"product":"klacht"}
  - utter_feedback_klacht_waarover-q  <!-- Waarover wil je een klacht indienen? [chatbot] [behandeling door medewerker] -->
* feedback_chatbot                    <!-- Over de chatbot -->
  - action_set_feedback_negatief      <!-- Wat vervelend..., start feedbackformulier -->
  - form_feedback
  - form{"name": "form_feedback"}
  - form{"name": null}

## product_klacht_indienen (feature_content = true)
* product_klacht_indienen{"product":"klacht"} <!-- Klacht over behandeling door medewerker -->
  - slot{"product":"klacht"}
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":true} <!-- Gem herkent een aangesloten gemeente -->
  - utter_klachtindienen_algemeen <!-- Gebruik de klachtenprocedure van {municipality}. Kijk voor meer informatie op de [website van {municipality}]({url}). -->

## product_klacht_indienen (feature_content = false)
* product_klacht_indienen{"product":"klacht"} <!-- Klacht over behandeling door medewerker -->
  - slot{"product":"klacht"}
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false} <!-- Gem herkent een aangesloten gemeente -->
  - utter_klachtindienen_algemeen <!-- Gebruik de klachtenprocedure van {municipality}. Kijk voor meer informatie op de [website van {municipality}]({url}). -->

## feedback_chatbot
* feedback_chatbot <!-- Ik wil feedback geven -->
  - utter_feedback_intro <!-- Ik ben benieuwd naar je mening. Wat vond je van ons chatgesprek?  -->
  - form_feedback <!-- Start feedbackformulier -->
  - form{"name": "form_feedback"}
  - form{"name": null}

## feedback_afsluiting_vriendelijk #tsdemo
  - form_feedback <!-- Start feedbackformulier -->
  - form{"name": "form_feedback"}
  - form{"name": null}
* graaggedaan <!-- Graag gedaan -->
  - utter_fijnedag <!-- Fijne dag! -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## afscheid
* afscheid <!-- Tot ziens! -->
  - utter_fijnedag <!-- Fijne dag! -->

## fijne dag - jij ook
  - utter_fijnedag  <!-- Fijne dag! -->
* jijook <!-- Jij ook -->
  - utter_bedankt <!-- Bedankt -->

## fallback story #tsdemo2
* out_of_scope <!-- Vraag waarvan we al weten dat Gem er niet mee kan helpen -->
  - utter_out_of_scope <!-- Sorry, met deze vraag kan ik je nog niet helpen. Wil je... [naar een medewerker] [verder met chatbot] -->


## contact - aangesloten gemeente 1/2 #tsdemo
* contact <!-- Hoe kan ik jullie bereiken? -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": true} <!-- Gem herkent een aangesloten gemeente -->
  - utter_gemeente_contact_lokaal <!--  text: Je kunt de gemeente [municipality] binnen kantoortijden bellen op .... Kijk op de [website] voor meer contactgegevens. -->

## contact - niet aangesloten gemeente 2/2
* contact <!-- Hoe kan ik jullie bereiken? -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": false} <!-- Gem herkent een niet-aangesloten gemeente -->
  - utter_gemeente_contact_landelijk <!--  text: Je kunt de gemeente [municipality] binnen kantoortijden bellen op .... Kijk op de [website] voor meer contactgegevens. -->

## verder_met_de_chatbot
* ask_for_chatbot <!-- Verder met de chatbot -->
  - utter_howcanihelp <!-- Waarmee kan ik je helpen? -->

## escalate to live chat (True) - Download - 1/3 
* ask_for_human <!-- Ik wil met een medewerker praten -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": true} <!-- Gem herkent een aangesloten gemeente -->
  - action_check_livechat <!-- Check of livechat beschikbaar is -->
  - slot{"feature_livechat":true} <!-- Livechat is beschikbaar -->
  - utter_livechatbeschikbaar_starten-q <!-- "Als je wilt, kun je een livechat starten. Ik stuur dan ons chatgesprek door naar een medewerker die verder met je chat. \n \n Wil je een livechat starten?" -->
* inform_affirmative OR inform_oke <!-- 'Ja'  of 'Oké' -->
  - action_escalatie <!-- Start livechat -->
  - action_gespreksgeschiedenis <!-- Je kunt je gespreksgeschiedenis downloaden -->

## escalate to live chat (True) - Download - 2/3 
* ask_for_human <!-- Ik wil met een medewerker praten -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": true} <!-- Gem herkent een aangesloten gemeente -->
  - action_check_livechat <!-- Check of livechat beschikbaar is -->
  - slot{"feature_livechat":true} <!-- Livechat is beschikbaar -->
  - utter_livechatbeschikbaar_starten-q <!-- "Als je wilt, kun je een livechat starten. Ik stuur dan ons chatgesprek door naar een medewerker die verder met je chat. \n \n Wil je een livechat starten?" -->
* inform_denial  <!-- Nee -->
  - utter_gemeente_contact_lokaal <!-- Je kunt de gemeente {municipality} binnen kantoortijden bellen op {gemeente_telefoonnummer} of een e-mail sturen naar {gemeente_emailadres}. -->

## escalate to live chat (True) - Download - 2/3 
* ask_for_human <!-- Ik wil met een medewerker praten -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": true} <!-- Gem herkent een aangesloten gemeente -->
  - action_check_livechat <!-- Check of livechat beschikbaar is -->
  - slot{"feature_livechat":false} <!-- Livechat is niet beschikbaar -->
  - utter_livechatnietbeschikbaar <!-- De livechat is momenteel helaas niet beschikbaar.  -->
  - utter_gemeente_contact_lokaal <!-- Je kunt de gemeente {municipality} binnen kantoortijden bellen op {gemeente_telefoonnummer} of een e-mail sturen naar {gemeente_emailadres}. -->

## escalate to live chat (False) - Download 3/3 
* ask_for_human <!-- Ik wil met een medewerker praten -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": false} <!-- Gem herkent een niet-aangesloten gemeente -->
  - utter_livechatnietaanwezig <!-- Voor de gemeente {municipality} is geen livechat beschikbaar helaas. -->
  - utter_gemeente_contact_landelijk <!-- Je kunt de gemeente {municipality} binnen kantoortijden bellen op {gemeente_telefoonnummer} of een e-mail sturen naar {gemeente_emailadres}. -->


#### Kan ik je verder nog ergens mee helpen?

## Kan ik je verder nog helpen? - Ja
  - utter_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->
* inform_affirmative OR inform_oke <!-- Ja -->
  - utter_howcanihelp <!-- Waarmee kan ik je helpen? -->
  - action_listen

## Kan ik je verder nog helpen? - Nee
  - utter_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->
* inform_denial <!-- Nee -->
  - utter_bedanktvoorjetijd <!-- Oké, dank voor je tijd.  -->
  - utter_feedback_intro <!-- Ik ben benieuwd naar je mening. Wat vond je van ons chatgesprek?  -->
  - form_feedback <!-- Start feedbackformulier -->
  - form{"name": "form_feedback"}
  - form{"name": null}
  - action_listen


#### Emoties

## ontevreden - feedback #ts #tsdemo
* ontevreden <!-- Ik ben ontevreden -->
  - utter_feedback_ontevreden_intro <!-- Zo te horen gaat er iets niet goed. Wil je [feedback geven] [verder met de chatbot] -->
* feedback_chatbot <!-- Feedback geven -->
  - action_set_feedback_negatief <!-- Wat vervelend..., start feedbackformulier -->
  - form_feedback
  - form{"name": "form_feedback"}
  - form{"name": null}
  - action_listen

#### Openingstijden

## openingstijden_aangeslotengemeenten 1/2 #tsdemo
* openingstijden <!-- Wat zijn jullie openingstijden? -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": true}
  - utter_openingstijden_algemeen <!-- Onze openingstijden vind je op [webpagina] -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## openingstijden_nietaangeslotengemeenten 2/2
* openingstijden
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": false}
  - utter_openingstijden_algemeen_nietaangeslotengemeenten <!-- Je hebt een vraag over de openingstijden van de gemeente [municipality]. Voor deze gemeente kan ik je nog niet uitgebreid helpen. Kijk voor meer informatie op [de website]. -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

#### Corona

## coronavirus aangesloten gemeente #tsdemo2
* overig_coronavirus OR overig_coronavirus{"product": "verhuismelding"} OR overig_coronavirus{"product": "paspoort"}  OR overig_coronavirus{"product": "id-kaart"}<!-- Een corona - covid - tozo - togs - lockdown - virus gerelateerde vraag -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": true}
  - utter_overig_coronavirus <!-- Je hebt een vraag over de situatie rondom het coronavirus. Alle informatie van de gemeente Dongen vind je op de webpagina [url] -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## coronavirus niet aangesloten gemeente
* overig_coronavirus OR overig_coronavirus{"product": "verhuismelding"} OR overig_coronavirus{"product": "paspoort"}  OR overig_coronavirus{"product": "id-kaart"}<!-- Een corona - covid - tozo - togs - lockdown - virus gerelateerde vraag -->
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": false}
  - utter_overig_coronavirus_landelijk <!-- Je hebt een vraag over de situatie rondom het coronavirus. Alle informatie vind je op de [website van je gemeente] -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->


#### Teamlid

## teamtest_laat_weten_dat_je_een_teamlid_bent
* teamtest {"name": "Cobi"} <!-- Test [naam] -->
  - utter_teamtest <!-- Welkom! Wat kan ik voor je doen? -->

#### Download chat

## download chat
* download_chatgeschiedenis <!-- Download chatlog -->
  - action_gespreksgeschiedenis <!-- Je kunt onze [chat downloaden]. Kijk bij de downloads in je browser, daar vind je 'm als tekstbestand. -->

#### Privacy

## privacy
* privacy <!-- Wat gaan jullie met mijn gegevens doen? -->
  - utter_privacy <!-- Je hebt een vraag over privacy. In ons [privacy statement](pdf) kun je lezen wat we met jouw gegevens doen -->
