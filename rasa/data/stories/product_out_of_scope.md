#### ALLE SPECIFIEKE VRAGEN (LOKAAL)

## out_of_scope (feature_content = true)
* out_of_scope{"product": "bijstand"}
  - slot{"product": "bijstand"}
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": true}
  - action_samenwerkendecatalogi_intro
  - action_samenwerkendecatalogi_toelichting
  - action_samenwerkendecatalogi_resultaat
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->

## out_of_scope (feature_content = false)
* out_of_scope{"product": "bijstand"}
  - slot{"product": "bijstand"}
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content": false}
  - action_samenwerkendecatalogi_intro
  - action_samenwerkendecatalogi_toelichting
  - action_samenwerkendecatalogi_resultaat
  - action_helpmore