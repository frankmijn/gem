## escalatie 1/3
* escalatie <!-- te vervangen niet gebruiken --> 
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":true} <!-- Gem herkent een aangesloten gemeente -->
  - action_check_livechat
  - slot{"feature_livechat":true}
  - utter_livechatbeschikbaar
  - action_escalatie

## escalatie 2/3
* escalatie <!-- te vervangen niet gebruiken --> 
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":true} <!-- Gem herkent een aangesloten gemeente -->
  - action_check_livechat
  - slot{"feature_livechat":false}
  - utter_gemeente_contact_lokaal
  - action_gespreksgeschiedenis

## escalatie 3/3
* escalatie <!-- te vervangen niet gebruiken --> 
  - action_check_municipality <!-- Check of gemeente bekend is. Zo niet, eerst vragen -->
  - slot{"feature_content":false} <!-- Gem herkent een niet-aangesloten gemeente -->
  - utter_gemeente_contact_landelijk <!-- Je kunt de gemeente {municipality} binnen kantoortijden bellen op {gemeente_telefoonnummer} of een e-mail sturen naar {gemeente_emailadres}. -->
  - action_helpmore <!-- Kan ik je verder nog ergens mee helpen? -->


