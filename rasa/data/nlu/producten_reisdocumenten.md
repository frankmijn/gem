#### ALLE GENERIEKE VRAGEN (landelijk)


## intent:productgroep_reisdocumenten_aanvragen_waar 
- Waar kan ik een [paspoort](product) aanvragen?
- Waar kan ik een [id-kaart](product) aanvragen?
- waar kan ik mijn [paspoort](product) aanvragen?
- Waar vraag ik een nieuwe [id-kaart](product) aan?
- Ik heb een nieuw [paspoort](product) nodig, waar moet ik dan zijn? 
- waar moet ik mijn [paspoort](product) aanvragen?
<!-- // Samengevoegd met intent 'hoelang', wordt getest. // -->
<!-- ## intent:productgroep_reisdocumenten_wanneerklaar
- Kunt u kijken of mijn [id-kaart](product) al klaar ligt?
- Hoe snel kan ik een nieuwe [id-kaart](product) hebben? 
- Wanneer is het [id-kaart](product) klaar?
- Wanneer kan ik het [id-kaart](product) ophalen?
- Hoe lang duurt het voor mijn [id-kaart](product) klaar is?
- Kan ik mijn [id-kaart](product) al ophalen? 
- Is mijn [id-kaart](product) al klaar?
- Hoe snel kan ik een nieuw [id-kaart](product) hebben? 
- Hoe lang duurt het voordat ik een nieuw [id-kaart](product) heb?
- Hoe lang duurt het aanvragen van een [id-kaart](product)? 
- Duurt het lang voordat ik een nieuw [id-kaart](product) heb?
- Ligt mijn [ID-kaart](product:id-kaart) al klaar?
- Hoe snel kan ik een nieuw [id-kaart](product) hebben? 
- Hoe lang duurt het voordat ik een nieuw [id-kaart](product) krijg?
- Hoe lang duurt het voordat ik een nieuw [id-kaart](product) heb?
- Hoe lang duurt het aanvragen van een [id-kaart](product)? 
- Duurt het lang voordat ik een nieuw [id-kaart](product) heb?
- Kunt u kijken of mijn [paspoort](product) al klaar ligt?
- Wanneer is het [paspoort](product) klaar?
- Wanneer kan ik het [paspoort](product) ophalen?
- Hoe lang duurt het voor mijn [paspoort](product) klaar is?
- Kan ik mijn [paspoort](product) al ophalen? 
- Is mijn [paspoort](product) al klaar?
- Ligt mijn [paspoort](product) al klaar?
- Hoe snel kan ik een nieuw [paspoort](product) hebben? 
- Hoe lang duurt het voordat ik een nieuw [paspoort](product) krijg?
- Hoe lang duurt het voordat ik een nieuw [paspoort](product) heb?
- Hoe lang duurt het aanvragen van een [paspoort](product)? 
- Duurt het lang voordat ik een nieuw [paspoort](product) heb? -->

## intent:productgroep_reisdocumenten_aanvragen_watmeenemen
- Wat moet ik meenemen bij de aanvraag van een [id-kaart](product)?
- Moet ik iets meenemen als ik een [id-kaart](product) aanvraag? 
- Wat ik heb ik nodig als ik een [id-kaart](product) aan wil vragen?
- Wat zijn de benodigdheden voor als ik een [id-kaart](product) aan wil vragen?
- Wat moet ik bij me hebben als ik een [id-kaart](product) aan ga vragen?
- Welke dingen moet ik meenemen bij de aanvraag van een [id-kaart](product)?
- Wat moet ik bij me hebben als ik een [IDkaart](product:id-kaart) aan ga vragen?
- Moet ik mijn oude [IDkaart](product:id-kaart) meenemen als ik een nieuwe aanvraag?
- Wat moet ik meenemen bij de aanvraag van een [paspoort](product)?
- Moet ik iets meenemen als ik een [paspoort](product) aanvraag? 
- Wat ik heb ik nodig als ik een [paspoort](product) aan wil vragen?
- Wat zijn de benodigdheden voor als ik een [paspoort](product) aan wil vragen?
- Wat moet ik bij me hebben als ik een [paspoort](product) aan ga vragen?
- Welke dingen moet ik meenemen bij de aanvraag van een [paspoort](product)?
- Moet ik mijn oude [paspoort](product) meenemen als ik een nieuwe aanvraag?

## intent:productgroep_reisdocumenten_afhalen_bewijskwijt
- Kan ik het [id-kaart](product) afhalen als ik het afhaalbewijs kwijt ben?
- Kan ik het [id-kaart](product) ophalen zonder afhaalbewijs?
- Wat als ik mijn afhaalbewijs kwijt ben? 
- Ik ben mijn afhaalbewijs kwijt, wat nu? 
- Ik ben mijn bonnetje kwijt, wat nu?
- Ik ben het afhaalbewijs voor het [id-kaart](product) kwijt, wat nu? 
- Kan ik mijn [id-kaart](product) zonder bewijs ophalen? 
- Is het afhalen van mijn [id-kaart](product) mogelijk zonder afhaalbewijs?
- Kan ik het [idkaart](product:id-kaart) afhalen als ik het afhaalbewijs kwijt ben?
- Kan ik het [idkaart](product:id-kaart) ophalen zonder afhaalbewijs?
- Ik ben het afhaalbewijs voor het [identiteitskaart](product:id-kaart) kwijt, wat nu? 
- Kan ik mijn [id](product:id-kaart) zonder bewijs ophalen? 
- Is het afhalen van mijn [identiteitskaart](product:id-kaart) mogelijk zonder afhaalbewijs?
- Kan ik het [paspoort](product) afhalen als ik het afhaalbewijs kwijt ben?
- Kan ik het [paspoort](product) ophalen zonder afhaalbewijs?
- Ik ben het afhaalbewijs voor het [paspoort](product) kwijt, wat nu? 
- Kan ik mijn [paspoort](product) zonder bewijs ophalen? 
- Is het afhalen van mijn [paspoort](product) mogelijk zonder afhaalbewijs?

## intent:productgroep_reisdocumenten_kwijt_inbuitenland
- Ik heb mijn [id-kaart](product) in het buitenland verloren. Wat moet ik doen?
- Mijn [id-kaart](product) is gestolen in het buitenland. Wat nu?
- Ik ben mijn [id-kaart](product) kwijtgeraakt in het buitenland. Hoe vraag ik een nieuwe aan?
- Waar vraag ik een tijdelijk [id-kaart](product) in het buitenland aan?
- Ik ben mijn [id-kaart](product) verloren op vakantie. Wat moet ik doen?
- Ik heb mijn [paspoort](product) in het buitenland verloren. Wat moet ik doen?
- Mijn [paspoort](product) is gestolen in het buitenland. Wat nu?
- Ik ben mijn [paspoort](product) kwijtgeraakt in het buitenland. Hoe vraag ik een nieuwe aan?
- Waar vraag ik een tijdelijk [paspoort](product) in het buitenland aan?
- Ik ben mijn [paspoort](product) verloren op vakantie. Wat moet ik doen?
- Ik zit in [Marokko](land) en ik ben mijn [paspoort](product) kwijt. 
- Ik kan mijn [identiteitskaart](product:id-kaart) nergens meer vinden en ik zit in [italie](land). 
- Ik ben op vakantie in [griekenland](land) en ik ben mijn [paspoort](product) kwijt. Wat moet ik nu doen? 
- Ik ben mijn [paspoort](product) in het buitenland verloren. Wat moet ik doen?

## intent:productgroep_reisdocumenten_aanvragen_inbuitenland_hoe 
- Hoe vraag ik een [paspoort](product) aan vanuit het buitenland?
- Hoe vraag ik een [paspoort](product) aan vanuit [Hongarije](land)?
- Hoe kom ik aan een [id-kaart](product) als ik in het buitenland woon?
- Waar haal ik mijn [id-kaart](product) in het buitenland?
- Kan ik een [id-kaart](product) ook in het buitenland aanvragen?
- Ik woon in het buitenland, hoe kom ik aan mijn [id-kaart](product)?
- Ik woon in [China](land), kan ik hier een Nederlands [paspoort](product) aanvragen?
- Hoe vraag ik een [id-kaart](product) in het buitenland aan?
- Hoe vraag ik een [id-kaart](product) in [Frankrijk](land) aan?
- Hoe kom ik aan een [paspoort](product) als ik in het buitenland woon?
- Waar haal ik mijn [paspoort](product) in het buitenland?
- Kan ik een [paspoort](product) ook in het buitenland aanvragen?
- Hoe vraag ik een [paspoort](product) in het buitenland aan?

## intent:productgroep_reisdocumenten_aanvragen_zonderinschrijving
- Hoe moet ik een Nederlands [paspoort](product) aanvragen als ik nergens sta ingeschreven?
- Ik wil een [paspoort](product) aanvragen, maar ik sta niet ingeschreven. Wat nu?
- Ik wil me inschrijven en een [paspoort](product) aanvragen, kan dat? 
- Ik sta nergens ingeschreven, hoe kom ik aan een [paspoort](product)? 
- Hoe vraag je een [paspoort](product) aan als je niet ingeschreven staat?
- Hoe moet ik een Nederlands [id-kaart](product) aanvragen als ik nergens sta ingeschreven?
- Ik wil een [id-kaart](product) aanvragen, maar ik sta niet ingeschreven. Wat nu?
- Ik wil me inschrijven en een [id-kaart](product) aanvragen, kan dat? 
- Ik sta nergens ingeschreven, hoe kom ik aan een [id-kaart](product)? 
- Hoe vraag je een [id-kaart](product) aan als je niet ingeschreven staat?

## intent:productgroep_reisdocumenten_aanvragen_wiemagdat
- Wie kan een [id-kaart](product) aanvragen? 
- Hoe oud moet je zijn om een [id-kaart](product) aan te vragen? 
- Mag iedereen een [id-kaart](product) aanvragen?
- Wanneer mag je een [id-kaart](product) aanvragen? 
- Wanneer mag ik een [paspoort](product) aanvragen?
- Vanaf welke leeftijd mag je een [id-kaart](product) aanvragen?
- Wie kan een [paspoort](product) aanvragen? 
- Hoe oud moet je zijn om een [paspoort](product) aan te vragen? 
- Mag iedereen een [paspoort](product) aanvragen?
- Wanneer mag je een [paspoort](product) aanvragen? 
- Vanaf welke leeftijd mag je een [paspoort](product) aanvragen?

## intent:productgroep_reisdocumenten_aanvragen_vooriemandanders 
- Kan iemand anders mijn [id-kaart](product) aanvragen of ophalen?
- Hoe kan ik een [id-kaart](product) aanvragen als ik niet langs kan komen?
- Ik kan niet naar de balie komen. Hoe vraag ik een [id-kaart](product) aan? 
- Ik ben slecht ter been en kan mijn [id-kaart](product) daardoor niet ophalen. 
- Ik heb last van mijn gezondheid en kan daardoor mijn [id-kaart](product) niet ophalen.
- Kan iemand anders mijn [paspoort](product) aanvragen of ophalen?
- Hoe kan ik een [paspoort](product) aanvragen als ik niet langs kan komen?
- Ik kan niet naar de balie komen. Hoe vraag ik een [paspoort](product) aan? 
- Ik ben slecht ter been en kan mijn [paspoort](product) daardoor niet ophalen. 
- Ik heb last van mijn gezondheid en kan daardoor mijn [paspoort](product) niet ophalen.
- Kan ik iemand anders mijn [id-kaart](product) laten aanvragen?
- Kan ik iemand anders mijn [paspoort](product) laten afhalen?

## intent:productgroep_reisdocumenten_vingerafdruk
- Is een vingerafdruk verplicht voor een [id-kaart](product)?
- Moet ik een vingerafdruk zetten voor een [id-kaart](product)?
- Is het nodig dat ik een vingerafdruk laat maken voor een [id-kaart](product)? 
- Wat doen jullie met mijn vingerafdrukken voor mijn [id-kaart](product)? 
- Waarvoor hebben jullie mijn vingerafdrukken nodig in mijn [id-kaart](product)? 
- Waarom zijn mijn vingerafdrukken nodig voor een [id-kaart](product)?
- Is een vingerafdruk verplicht voor een [paspoort](product)?
- Moet ik een vingerafdruk zetten voor een [paspoort](product)?
- Is het nodig dat ik een vingerafdruk laat maken voor een [paspoort](product)? 
- Wat doen jullie met mijn vingerafdrukken? 
- Waarvoor hebben jullie mijn vingerafdrukken nodig? 
- Waarom zijn mijn vingerafdrukken nodig?

## intent:productgroep_reisdocumenten_teruggevonden 
- Ik heb mijn [id-kaart](product) teruggevonden, mag ik die weer gebruiken?
- Mijn verloren [id-kaart](product) is weer opgedoken. Wat doe ik daarmee?
- Ik vond mijn [id-kaart](product) weer, wat nu?
- Wat moet ik doen als ik mijn [id-kaart](product) weer heb teruggevonden?
- Mijn [id-kaart](product) vond ik zojuist weer terug.
- Ik heb mijn [paspoort](product) teruggevonden, mag ik die weer gebruiken?
- Mijn verloren [paspoort](product) is weer opgedoken. Wat doe ik daarmee?
- Ik vond mijn [paspoort](product) weer, wat nu?
- Wat moet ik doen als ik mijn [paspoort](product) weer heb teruggevonden?
- Mijn [paspoort](product) vond ik zojuist weer terug.

## intent:productgroep_reisdocumenten_kind_kindmee
- Moet mijn [dochter](namens:kind) mee als ik een [paspoort](product) voor haar aanvraag?
- Moet mijn [kind](namens:kind) mee als ik zijn [idkaart](product:id-kaart) kom ophalen?
- Moet mijn [kind](namens:kind) er per se bij zijn als we zijn [paspoort](product) ophalen?
- Is aanwezigheid van mijn [kind](namens:kind) verplicht bij het ophalen van haar [identiteitskaart](product:id-kaart)
- Moet mijn [zoon](namens:kind) ook mee als ik zijn nieuwe [paspoort](product) kom ophalen?
- Moet mijn [kind](namens:kind) aanwezig zijn bij het ophalen van zijn [idkaart](product:id-kaart)?
- moet ik mijn [kind](namens) nemen voor [paspoort](product)

## intent:productgroep_reisdocumenten_kind_watmeenemen
- Wat moet ik meenemen als ik een [id-kaart](product) aan vraag voor mijn [kind](namens:kind)?
- Wat moet ik meenemen als ik een [paspoort](product) aanvraag voor mijn [kind](namens:kind)?
- Wat heb ik nodig als ik voor mijn [kind](namens:kind) een [id-kaart](product) aan wil vragen?
- Wat moet ik doen als ik voor mijn [dochter](namens:kind) een [id-kaart](product) wil aanvragen?
- Wat heb ik nodig als ik voor mijn [zoon](namens:kind) een [id-kaart](product) ga aanvragen?
- Wat moet ik hebben als ik een [id-kaart](product) voor mijn [oudste](namens:kind) wil halen?
- Moet mijn [pleegkind](namens:kind) meekomen als ik een [id-kaart](product) voor hem aanvraag?
- Wat heb ik nodig als ik voor mijn [kind](namens:kind) een [paspoort](product) aan wil vragen?
- Wat moet ik doen als ik voor mijn [dochter](namens:kind) een [paspoort](product) wil aanvragen?
- Wat heb ik nodig als ik voor mijn [zoon](namens:kind) een [paspoort](product) ga aanvragen?
- Wat moet ik hebben als ik een [paspoort](product) voor mijn [kind](namens:kind) wil halen?
- Moet mijn [zoontje](namens:kind) meekomen als ik een [paspoort](product) voor haar aanvraag?
- wat moet ik meenemen bij aanvraag [paspoort](product) voor mijn [kind](namens:kind)

## intent:productgroep_reisdocumenten_kind_aanvragen_zelf
- Kan mijn [kind](namens:kind) zelf een [idkaart](product:id-kaart) aanvragen?
- Mijn [dochter](namens:kind) wil zelf een [paspoort](product:paspoort) aanvragen, kan dat?
- Kan mijn [zoon](namens:kind) zelf een nieuwe [ID-kaart](product:id-kaart) aanvragen? 
- Moet mijn [kind](namens:kind) zijn [identiteitskaart](product:id-kaart) zelf regelen?
- Moet mijn [kind](namens:kind) een [paspoort](product) zelf aanvragen
- Moet mijn [dochter](namens:kind) haar [paspoort](product) zelf aanvragen
- Moet mijn [zoon](namens:kind) zelf zijn [idkaart](product) aanvragen?
- Mijn [jongste](namens:kind) wil zelf een [paspoort](product:paspoort) aanvragen, mag dat?
- kan mijn [jongste](namens:kind) zelf een [paspoort](product) aanvragen?
- mijn [jongste](namens:kind) [dochter](namens:kind) wil zelf een [paspoort](product) aanvragen, mag dat?
- kan mijn [kind](namens) zelf een [ID kaart](product:id-kaart) aanvragen
- kan mijn [oudste](namens) zelf een [paspoort](product) aanvragen?
- moet mijn [kind](namens) zelf een [idkaart](product:id-kaart) aanvragen?
- mijn [kind](namens) wil zelf een [paspoort](product) aanvragen mag dat
- mijn [kind](namens) wil zelf haar [paspoort](product) aanvragen
- Moet mijn [kind](namens) zijn [paspoort](product) zelf aanvragen?
- Mijn [dochter](namens:kind) is 15. Moet ik als vader toestemming geven als zij een [paspoort](product) aanvraagt?

## intent:productgroep_reisdocumenten_kind_eigen_reisdocument_nodig
- Heeft mijn [kind](namens:kind) een eigen [idkaart](product:id-kaart) nodig?
- Moet mijn [dochter](namens:kind) een eigen [paspoort](product) hebben? 
- We gaan op vakantie. Heeft mijn [zoon](namens:kind) dan een [id](product:id-kaart) nodig?
- Hebben we een [pasport](product:paspoort) voor onze [jongste](namens:kind) nodig als we naar [Frankrijk](land) gaan?
- Heeft mijn [baby](namens:kind) van 3 maanden ook ee neigen [paspoort](product) nodig?

## intent:productgroep_reisdocumenten_kwijt
- mijn [paspoort](product) is kwijt. wat moet ik doen?
- Hoi ik ben mijn [paspoort](product) kwijt
- Hoi ik ben mijn [id-kaart](product) kwijt
- ik ben mijn [paspoort](product) verloren. wat moet ik doen?
- mijn [paspoort](product) is weg.
- mijn [paspoort](product) is weg. wat moet ik doen?
- mijn [id-kaart](product) is gestolen. 
- mijn [paspoort](product) is gestolen. Wat moet ik doen?
- Mijn [paspoort](product) is gestolen. Hoe kan ik een nieuwe aanvragen?
- Mijn [paspoort](product) is weg. Wat moet ik doen?
- Mijn [paspoort](product) is kwijt. Wat moet ik doen?
- Mijn [paspoort](product) is verdwenen. Wat moet ik doen?
- Wat moet ik doen als mijn [paspoort](product) kwijt, vermist of gestolen is?
- Wat moet ik doen als mijn [id-kaart](product:id-kaart) kwijt, vermist of gestolen is?
- Moet ik het doorgeven als mijn [paspoort](product) kwijt is?
- Het [paspoort](product) van mijn [kind](namens:kind) is kwijt, wat moet ik doen?
- Ik kan mijn [paspoort](product) niet meer vinden, wat nu?
- Ik ben mijn [paspoort](product) verloren?
- ik ben mijn [id-kaart](product) verloren. wat moet ik doen?
- ik ben mijn [paspoort](product) kwijt?
- mijn [identiteitskaart](product:id-kaart) is weg. wat moet ik doen?
- mijn [id kaart](product:id-kaart) is kwijt in [Amsterdam](municipality). wat moet ik doen?
- mijn [id kaart](product:id-kaart) is weg.
- mijn [ID](product:id-kaart) is weg in [Ede](municipality).
- mijn [ID kaart](product:id-kaart) is gestolen. Wat moet ik doen?
- Mijn [id](product:id-kaart) is weg/kwijt/verdwenen/gestolen. Wat moet ik doen?
- Mijn [id-kaart](product:id-kaart) is weg/kwijt/verdwenen/gestolen in [Dongen](municipality). Wat moet ik doen?
- Moet ik het doorgeven als mijn [id-kaart](product:id-kaart) kwijt is?
- Het [id-kaart](product:id-kaart) van mijn [kind](namens:kind) is kwijt, wat moet ik doen?
- Ik kan mijn [id-kaart](product) niet meer vinden, wat nu?
- Mijn [paspoort](product) is gestolen. Hoe kan ik een nieuw [paspoort](product) aanvragen?
- Mijn [id-kaart](product) is gestolen. Hoe kan ik een nieuw [id-kaart](product) aanvragen?
- Mijn [paspoort](product) is gestolen.
- Mijn [id-kaart](product) is gestolen.
- Mijn [paspoort](product) is zoek	

## intent:productgroep_reisdocumenten_kwijt_vermissingsverklaring
- Moet ik geen aangifte bij de politie doen?
- Moet ik aangifte bij de politie doen als ik mijn [paspoort](product) ben verloren?
- Moet ik het ook aan de politie doorgeven?
- Moet ik ook naar de politie als ik mijn [paspoort](product) ben verloren?
- Is het verplicht om naar de politie te gaan?
- Moet ik naar de politie als ik mijn [id-kaart](product) ben kwijtgeraakt?
- Is het nodig om bij de politie aangifte te doen?

## intent:productgroep_reisdocumenten_kapot
- mijn [paspoort](product) is kapot. wat moet ik doen?
- Hoi mijn [id-kaart](product) is kaduuk
- Er zit een scheurtje in mijn [identiteitskaart](product:id-kaart) is dat erg?
- Ik heb een scheur in mijn [id-kaart](product)
- Wat als mijn [pasport](product:paspoort) beschadigd is?
- Ik heb koffie over mijn [id-kaart](product) gegooid, is hij nog geldig?
- Me [passport](product:paspoort) is kapot
- Mijn [paspoort](product) is nat geworden, is dat erg?
- Mijn [paspoort](product) is in het water gevallen, kan ik hem nog gebruiken?
- Ik heb mijn [idkaart](product) met mijn broek meegewassen, is dat erg?
- er zit een scheur in me [paspoort](product)
- me [paspoort](product) is kaduuk
- [paspoort](product) kapot
- [paspoort](product) in de wasmachine
- [paspoort](product) verbrand

## intent:productgroep_reisdocumenten_geldig_waar
- Waar is een [paspoort](product) geldig?
- Waar ter wereld is mijn [id-kaart](product) geldig?
- Waar kan ik reizen met een [id-kaart](product)?
- In welke landen is een [paspoort](product) geldig?
- Naar welke landen mag ik met mijn [id-kaart](product)?
- Welke landen erkennen mijn [paspoort](product)?
- Mag ik met mijn [identiteitskaart](product:id-kaart) naar [Noord-Korea](land) reizen?
- Mag ik naar [Amerika](land) met mijn [ID-kaart](product:id-kaart)?
- Kan ik met een [idkaart](product:id-kaart) naar [Marokko](land)?
- Kan ik met mijn [id kaart](product:id-kaart) naar [China](land)?
- Kan ik op vakantie naar [Brazilie](land) met een [idkaart](product:id-kaart)?
- Accepteert [Duitsland](land) een [ID-kaart](product:id-kaart)?
- Is een [id_-_kaart](product) geldig in [Zwitserland](land)
- Is een [ID kaart](product:id-kaart) geldig in [Portugal](land)
- Is mijn [id kaart](product:id-kaart) geldig in [Frankrijk](land)?
- Is mijn [idkaart](product:id-kaart) geldig in [Iran](land)?
- Is mijn [ID kaart](product:id-kaart) geldig in [Frankrijk](land)?
- Is mijn [paspoort](product:paspoort) geldig in [Rusland](land)?
- Is een [idkaart](product:id-kaart) een geldig document in [Griekenland](land)?
- Is een [idkaart](product:id-kaart) als ik op vakantie ga naar [Rusland](land)?
- Is het mogelijk om met een [id-kaart](product:id-kaart) naar [Polen](land) te reizen?
- Wordt mijn [idkaart](product:id-kaart) geaccepteerd in [Italië](land)?
- Geldt mijn [idkaart](product:id-kaart) in [Irak](land)?
- Geldt mijn [paspoort](product:paspoort) in [Irak](land)?
- heb ik een speciaal [paspoort](product:paspoort) nodig voor reizen naar [uk](land:Verenigd Koninkrijk)?
- Welke landen?

## intent:productgroep_reisdocumenten_geldig_hoelang
- Hoe lang is een [id-kaart](product) geldig?
- Hoeveel jaar is mijn [paspoort](product) geldig?
- Tot wanneer is een [id-kaart](product) geldig
- Na hoeveel jaar is mijn [paspoort](product) verlopen?
- Wanneer verloopt mijn [id-kaart](product)?
- Wanneer vervalt mijn [paspoort](product)?
- hoe lang is mijn [paspoort](product) geldig
- hoe lang is een [idkaart](product:id-kaart) geldig?
- hoe lang is [paspoort](product) geldig
- hoe lang is [id-kaart](product) geldig
- hoelang is mijn [paspoort](product) nog geldig
- geldig
- geldigheid
- voor hoe lang geldig
- en hoe lang is het geldig
- geldigheidsduur [id-kaart](product)
- En hoe lang is het dan geldig?

## intent:productgroep_reisdocumenten_verlopen
- Mijn [paspoort](product) is verlopen
- Mijn [id-kaart](product) is verlopen
- [paspoort](product) verlopen
- Wat moet ik doen als mijn [id-kaart](product) verlopen is?
- mijn [identiteitskaart](product:id-kaart) is niet meer geldig
- me [pasport](product:paspoort) is oud, kan ik hem nog gebruiken?
- kan ik met een verlopen [paspoort](product) reizen
- m'n [idkaart](product:id-kaart) is ongeldig. Moet ik nu een nieuwe aanvragen?
- Kan ik met een verlopen [identiteitskaart](product:id-kaart) reizen?
- Mijn [paspoort](product) is verlopen, moet ik hem nu verplicht verlengen?

<!-- ## productgroep_reisdocumenten_aanvragen_hoe    ## verhuisd naar hoe_aanvragen -->

## intent:productgroep_reisdocumenten_spoed_kosten
- Wat zijn de kosten van een [spoedaanvraag](spoed) voor een [id-kaart](product)?
- Wat zijn de kosten van een [spoedaanvraag](spoed) voor een [paspoort](product) in [Tilburg](municipality)?
- Wat kost een [spoedaanvraag](spoed) [id-kaart](product)?
- Wat kost een [spoedaanvraag](spoed) [paspoort](product) in [Utrecht](municipality)?
- Wat betaal ik als ik het [id-kaart](product) met [spoed](spoed) aanvraag?
- Wat betaal ik als ik het [paspoort](product) met [spoed](spoed) aanvraag in [Ede](municipality)?
- Wat kost het als ik het [paspoort](product) [sneller](spoed) nodig heb?
- Wat kost het als ik het [id-kaart](product) [sneller](spoed) nodig heb in [Ede](municipality)?
- Wat is de prijs van een [spoedaanvraag](spoed) voor een [id-kaart](product)?
- Wat is de prijs van een [spoedaanvraag](spoed) voor een [paspoort](product) in [Amsterdam](municipality)?
- Wat zijn de kosten als ik een [id-kaart](product) met [spoed](spoed) nodig heb?
- Wat zijn de kosten als ik een [paspoort](product) met [spoed](spoed) nodig heb in [Eindhoven](municipality)?
- Ik moet [snel](spoed) weg en heb een [IDkaart](product:id-kaart) nodig. Kan dat?
- Ik moet deze week nog weg en heb [snel](spoed) een [IDkaart](product:id-kaart) nodig. Is dat duurder?
- Ik moet deze week weg en heb een nieuw [paspoort](product) nodig. Komen er dan extra kosten bij?
- Ik heb deze week nog een [ID-kaart](product:id-kaart) nodig. Betaal ik dan meer?
- Ik heb [heel snel](spoed:spoed) een [paspoort](product) nodig? Wat kost het dan? 
- Ik heb deze week nog een [paspoort](product) nodig. Betaal ik dan meer?
- Is een [idkaart](product:id-kaart) met [spoed](spoed) aanvragen duurder?
- Is een [spoedaanvraag](spoed) voor een [paspoort](product) duurder?
- Zijn er extra kosten verbonden aan een [spoedaanvraag](spoed) [idkaart](product:id-kaart)?
- Zijn er extra kosten verbonden aan een [spoedaanvraag](spoed) [paspoort](product:paspoort) in [Tilburg](municipality)?
- Hoeveel meer kost een [spoedaanvraag](spoed) [paspoort](product)?
- Hoeveel meer kost een [spoedaanvraag](spoed) [paspoort](product) in [De Bilt](municipality)?
- Moet ik meer betalen voor een [spoed](spoed) [ID-kaart](product:id-kaart)?
 

<!-- ## intent:productgroep_reisdocumenten_aanvragen_spoed  # verhuisd naar algemeen.md - hoe_aanvragen -->

# De intent productgroep_reisdocumenten_afspraakmaken is verhuisd naar algemeen.md, afspraakmaken. 

## intent:productgroep_reisdocumenten_afhalen_hoe
- Hoe kan ik een [paspoort](product) ophalen in [Amsterdam](municipality)? 
- Waar kan ik mijn [paspoort](product) afhalen?
- Waar kan ik mijn [paspoort](product) afhalen in [Amsterdam](municipality)?
- Hoe kan ik een [id-kaart](product) ophalen? 
- Waar kan ik mijn [ik-kaart](product:id-kaart) afhalen?
- Waar kan ik mijn [id-kaart](product) afhalen in [Amsterdam](municipality)?
- ik moet mijn [id](product:id-kaart) ophalen

## intent:productgroep_reisdocumenten_bezorgmogelijkheid
- Wordt een [id-kaart](product) thuisbezorgd? 
- Wordt een [id-kaart](product) thuisbezorgd in [Ede](municipality)? 
- Ik kan mijn [id-kaart](product) niet ophalen, kan die opgestuurd worden?
- Kan de [ik kaart](product:id-kaart) opgestuurd worden?
- Kan het [passpoort](product:paspoort) opgestuurd worden?
- Kan het [id kaart](product:id-kaart) opgestuurd worden in [Dongen](municipality)?
- Hoe duur is het om een [id-kaart](product) thuis te laten bezorgen?
- Versturen jullie [id-kaart](product) ook per post?
- Wordt een [paspoort](product) thuisbezorgd in [De Bilt](municipality)? 
- Ik kan mijn [paspoort](product) niet ophalen, kan die opgestuurd worden?
- Ik kan mijn [paspoort](product) niet ophalen, kan die opgestuurd worden in [Dongen](municipality)?
- Kan het [paspoort](product) opgestuurd worden?
- Kan het [paspoort](product) opgestuurd worden in [Tilburg](municipality)?
- Hoe duur is het om een [paspoort](product) thuis te laten bezorgen in [Dongen](municipality)?
- Versturen jullie [paspoort](product) ook per post in [Tilburg](municipality)?
- Kan ik 'm thuis laten bezorgen? 
- Kan ik die ook thuis laten bezorgen?
- Kan ik die bij mij laten bezorgen?
- Kunnen jullie die ook thuis bezorgen?
- Kunnen jullie die ook thuisbezorgen?
- Kunnen jullie 'm ook opsturen?
- Kan-ie ook opgestuurd worden?
- Is het mogelijk om 'm per post te versturen?
- wordt die ook thuisbezorgd?

#### Paspoort specifieke vragen

## intent:product_paspoort_aanvragen_noodpaspoort
- Wat doe ik als mijn [paspoort](product) kwijt is en ik moet vandaag nog weg?
- Ik heb een [noodpaspoort](product:paspoort) nodig, hoe regel ik dat?
- Waar vraag ik een [noodpaspoort](product:paspoort) aan?
- Hoe vraag ik een [noodpaspoort](product:paspoort) aan?
- Ik wil een [noodpaspoort](product:paspoort) aanvragen.

## intent:product_paspoort_kind_bijschrijven
- Kan ik mijn [kind](namens:kind) nog laten bijschrijven in mijn [paspoort](product)?
- Kan ik mijn [kind](namens:kind) van 21 op mijn [paspoort](product) bijschrijven?
- Mag mijn [zoon](namens:kind) in mijn [paspoort](product) bijgeschreven worden?
- Ik wil mijn [dochter](namens:kind) in mijn [paspoort](product) bijschrijven, kan dat?
- Hoe kan ik mijn [middelste](namens:kind) laten bijschrijven in mijn [paspoort](product)?
- Ik wil graag mijn [baby](namens:kind) laten bijschrijven in m'n [paspoort](product).
- kan ik mijn [kinderen](namens:kind) op het [paspoort](product) laten bijschtijven
- kan ik mijn [kind](namens:kind) bijschrijven in mijn [paspoort](product)?
- kan ik mijn [kinderen](namens:kind) op het [paspoort](product) laten bijschtijven
- Kan ik mijn [kind](namens:kind) van 21 on mijn [paspoort](product) bijschrijven?


#### Met kind naar buitenland

## intent:productgroep_reisdocumenten_kind_naarbuitenland
- Wat heb ik nodig als eenoudergezin om mijn [kind](namens:kind) mee naar het buitenland te nemen?
- Heb ik iets nodig om met mijn [kind](namens:kind) naar het buitenland te reizen?
- Ik wil op vakantie met mijn [kind](namens:kind), wat moet ik regelen?
- ik wil met mijn [kind](namens:kind) naar het buitenland reizen
- Ik wil met mijn [kind](namens:kind) naar het buitenland.
- Kan ik mijn [kind](namens:kind) meenemen naar het buitenland?

## intent:productgroep_reisdocumenten_kind_geentoestemming
- Wat moet ik doen als de andere ouder geen toestemming wil geven om te reizen?
- Mijn ex geeft geen toestemming om met mijn [kind](namens:kind) te reizen.
- Ik krijg geen toestemming van mijn ex partner om met mijn [kind](namens:kind) op reis te gaan.
- Mijn vorige partner geeft geen toestemming om met mijn [kind](namens:kind) te reizen.
- Ik mag van mijn ex partner niet met mijn [kind](namens:kind) op vakantie.
- Ik mag van mijn ex niet met mijn [kind](namens:kind) op vakantie.
- Ik mag van mijn ex niet met onze [zoon](namens:kind) op vakantie.
- moet toestemming van vader ook hebben als je gescheiden ben
- waar kan ik toestemming krijgen

#### Paspoort en ID-kaart tegelijk

## intent:productgroep_reisdocumenten_tweede_aanvragen
- Mag ik mijn [id-kaart](product) behouden als ik een [paspoort](product) aanvraag? 
- Kan ik een [paspoort](product) aanvragen als ik al een [id-kaart](product) heb? 
- Kan ik mijn [id-kaart](product) behouden als ik een nieuw [paspoort](product) aanvraag?
- Ik ga een nieuwe [id-kaart](product) aanvragen, kan ik mijn [paspoort](product) dan houden?
- Kan ik mijn [id-kaart](product) dan behouden?

## intent:productgroep_reisdocumenten_twee_tegelijk_aanvragen
- Kan ik tegelijkertijd een [id-kaart](product) en een [paspoort](product) aanvragen? 
- Mag ik een [paspoort](product) en een [id-kaart](product) aanvragen? 
- Kan ik een [id-kaart](product) en een [paspoort](product) tegelijk aanvragen?
- Ik wil een [paspoort](product) en een [id-kaart](product) aanvragen, mag dat? 
- Kan ik dan ook tegelijk een [id-kaart](product) aanvragen?

## intent:productgroep_reisdocumenten_verschil
- Wat is het verschil tussen een [id-kaart](product) en een [paspoort](product)?
- Kan ik beter een [id-kaart](product) of een [paspoort](product) aanvragen?
- Wat is het verschil met een [id-kaart](product)?
- Hoe verschilt dat van een [paspoort](product)?
- Wat is nou het verschil tussen een [paspoort](product) en een [idkaart](product:id-kaart)?
- Maar wat is het verschil met een [paspoort](product)?
