# ## intent:chitchat
# - wanneer is de borrel
# - ik wil een biertje bestellen
# - mag ik een biertje
# - mag ik een pilsje
# - een vaasje aub
# - een vaasje
# - een fluitje
# - een La Trappe blond aub
# - een Hertog Jan
# - ik heb zin in bier
# - ik wil een biertje

## intent:thanks
- Bedankt!
- bedankt
- dank je
- dankje
- thanks
- dank u
- dank u vriendelijk
- dakjewel
- enorm bedankt!
- fijn, dankje
- bedankt he
- bedankt voor de hulp
- ik weet voldoende dank je wel
- ik weet genoeg bedankt
- ik wil je bedanken
- mag ik je bedanken
- Super. Dank je wel
- oké dank je wel
- oke bedankt
- oke bedakt
- Maar dank je wel dat het kan
- Ok tnx
- thnx
- Dankjewel
- Goede tip. Dankjewel gem!

## intent:graaggedaan
- Graag gedaan
- Met plezier
- Met genoegen
- Heel graag gedaan
- Met alle plezier
- graag gedaan hoor
- graag gedaan gem
- met plezier

## intent:compliment
- Super!
- top!
- toppie
- mooi
- heel goed
- hartstikke goed
- fijn
- Goed antwoord
- dat is mooi
- je bent de liefste
- je bent mooi
- je bent geweldig
- wat goed!
- Geweldig Gem! Ga zo door!
- Wat ben je toch slim
- goed bezig
- mooi
- goed gedaan
- super goed
- geweldig
- fantastisch
- buitengewoon
- formidabel
- magnifiek
- prachtig
- subliem
- briljant
- te gek

## intent:jammer
- jammer
- wat jammer
- he wat jammer
- da's nou jammer
- wat vervelend
- wat een pech
- da's balen
- balen zeg
- bah
- wat rot
- wat naar
- helaas
- geen goed antwoord

## intent:chitchat_unfriendly
- klote bot
- stomme bot
- stomme chatbot
- klote chatbot
- rot op
- Rot op
- ROT OP
- FUCK YOU
- fuck you
- stomme gem
- ROT OP GEM
- ga toch weg robot
- ben je dom ofzo
- wat ben jij dom
- Doe niet zo dom.
- domme bot
- Domme Gem
- ik haat chatbots

## intent:wie_is_de_burgemeester_van
- Wie is de burgemeester in [Tilburg](municipality)?
- Burgemeerster [Tilburg](municipality)?
- wie is de burgermeester van [Tilburg](municipality)?
- wie is de burgermeester van [tilburg](municipality)?
- wie is de burgervader van [Tilburg](municipality)?
- wie is de voorzitter van de gemeenteraad in [tilburg](municipality)?
- Wie is de baas van de politie in [Tilburg](municipality)?
- Wie is de burgemeester in [Dongen](municipality)?
- Wie is de burgemeester van [Dongen](municipality)?
- burgemeester [Dongen](municipality)?
- Weet jij wie de burgemeester is in [Dongen](municipality)
- wie is de burgervader van [dongen](municipality)
- wie is de voorzitter van de gemeenteraad in [Dongen](municipality)?
- wie zit de gemeenteraad voor in [Dongen](municipality)?
- Wie is de burgemeester in [Utrecht](municipality)?
- Wie is de burgemeester in [utrecht](municipality)
- Wie is de burgemeester van [utrecht](municipality)?
- burgemeester van [utrecht](municipality)?
- Weet jij wie de burgemeester is in [Utrecht](municipality)?
- wie is de burgervader van [utrecht](municipality)?
- wie is de voorzitter van de gemeenteraad in [utrecht](municipality)?
- Wie is de baas van de politie in [Utrecht](municipality)?

## intent:ask_population
- Hoeveel inwoners heeft nederland?
- weet jij hoeveel mensen er in Nederland wonen?
- hoeveel nederlanders zijn er
- inwoners nederland
- hoeveel inwoners heeft nederland
- Hoeveel inwoners hebben we in Nederland
- hoeveel mensen wonen er in Nederland

## intent:ask_weather
- wat voor weer is het?
- gaat het regenen?
- ik wil graag weten of het gaat regenen?
- Lekker weertje hè?
- Mooi weer buiten
- weet je wat voor weer het wordt?
- wat is de weersvoorspelling?
- wat voor weer is het daar?
- regent het?
- moet ik mijn paraplu pakken?
- is het droog buiten?
- is het droog?
- lekker weer he?
- valt er regen?
- is het droog buiten?
- wordt het mooi weer vandaag?

## intent:greet
- hallo [Tilburg](municipality)!
- Hallo [Utrecht](municipality)! Mijn naam is [Piet](name)
- hey gem, mijn naam is [jason](name)
- Hoi mijn naam is [Wim](name)
- Hi, ik ben [Harvey](name)
- Hallo hier [Ali](name)
- Hey, ik ben [Jeroen](name)
- Hoi ik ben [Fatima](name)
- Hallo, [Emma](name) hier!
- Hallo [Ede](municipality), ik ben [Wendy](name)
- Hallo
- hoi
- hi
- hey
- heey
- heuy
- goede morgen
- goede middag
- goedenavond
- goedenavond [Utrecht](municipality)
- Hoi [Dongen](municipality)
- Hoi [Uden](municipality)
- Hallo Gem, welkom bij de gemeente [Dongen](municipality)
- [Stijn](name) hier
- Hallo [Suzanne](name) hier

## intent:benjeerklaarvoor
- Ben je er klaar voor? 
- Hoi Gem, ben je er klaar voor?
- Ha Gem, ben je er klaar voor? 
- Gem, ben je er klaar voor? 
- Gem, heb je er zin in? 

## intent:afscheid
- Tot ziens
- tot spreeks
- tot kijk
- doei
- doeg
- aju
- Fijne dag verder
- Houdoe

## intent:chitchat_hoegaathet
- alles goed?
- hoe gaat het met je?
- Hoi Gem, hoe gaat het met je?
- hoe was je dag?
- hoe is je dag?
- gaat alles goed vandaag?
- how are you doing?
- how are you?
- all good?
- are you doing good?
- hoe is het met je?
- hoe gaat het?
- hoe is het?

## intent:chitchat_ben_jij_een_mens
- ben jij een mens?
- ben je een mens?
- ben jij een echt mens? 
- ben je een echt mens?
- ben jij een robot?
- ben je een robot?
- ben jij een chatbot?
- ben je een chatbot?
- ben jij een mens of een robot?
- ben je een mens of een robot?

## intent:chitchat_wil_je_met_me_trouwen
- wil je met mij trouwen?
- wil jij met me trouwen? 
- wil je met me trouwen?
- wil jij met mij trouwen?
- wilt u met mij trouwen?
- wilt gij met mij trouwen?

## intent:mag_ik_wat_vragen
- Mag ik wat vragen?
- Ik heb een vraag
- ik wil iets vragen
- mag ik iets vragen?
- ik heb nog een vraag
- Gem, kan ik je nog iets vragen?
- kan ik nog wat vragen stellen?
- ik wil een vraag stellen
- Ik wil nog een vraag stellen	