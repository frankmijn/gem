## intent:inform_municipality
- [tilburg](municipality)
- [breda](municipality)
- [utrecht](municipality)
- [amsterdam](municipality)
- [dongen](municipality)
- Gemeente [Tilburg](municipality)
- Gemeente [Rotterdam](municipality)
- [Rotterdam](municipality)
- Dat is [Dongen](municipality)
- Voor [Amsterdam](municipality)
- In [Ede](municipality)
- [almere](municipality)
- [udenhout](municipality)
- [Berkel-Enschot](municipality)
- [biezenmortel](municipality)
- [Udenhout](municipality)
- [dongen](municipality)
- [Vaart](municipality)
- [klein dongen](municipality)
- [vaart](municipality)
- [sgravenmoer](municipality)
- ['sgravenmoer](municipality)
- [Arnhem](municipality)
- [De bilt](municipality)
- [buren](municipality)
- [best](municipality)
- [Ede](municipality)
- [eindhoven](municipality)
- [enschede](municipality)
- [Bergen op Zoom](municipality)
- [Aa en Hunze](municipality)
- [Alphen aan den Rijn](municipality)
- [Berg en Dal](municipality)
- [Capelle aan den IJssel](municipality)
- [De Bilt](municipality)
- [De Fryske Marren](municipality)
- [De Ronde Venen](municipality)
- [Krimpen aan den IJssel](municipality)
- [Castricum](municipality)
-  [weurt](municipality)
-  [bennebroek](municipality)
-  [molenaarsgraaf](municipality)
-  [maastricht](municipality)
-  [stein](municipality)
-  [de wolden](municipality)
-  [oudendijk](municipality)
-  [idskenhuizen](municipality)
-  [kaard](municipality)
-  [wijk aan zee](municipality)
-  [loenen](municipality)
-  [philippine](municipality)
-  [loosdrecht](municipality)
-  [sumar](municipality)
-  [it heidenskip](municipality)
-  [hooge mierde](municipality)
-  [beetsterzwaag](municipality)
-  [raard](municipality)
-  [geijsteren](municipality)
-  [beringe](municipality)
-  [heesbeen](municipality)
-  [esbeek](municipality)
-  [zoutkamp](municipality)
-  [stokkum](municipality)
-  [ravenstein](municipality)
-  [nieuwkoop](municipality)
-  [valthe](municipality)
-  [bemelen](municipality)
-  [heijenrath](municipality)
-  [oud ootmarsum](municipality)
-  [teroele](municipality)
-  [noord-sleen](municipality)
-  ['s-graveland](municipality)
-  [doetinchem](municipality)
-  [waarland](municipality)
-  [wijckel](municipality)
-  [neerkant](municipality)
-  [oudemolen](municipality)
-  [broekland](municipality)
-  [nieuw-lekkerland](municipality)
-  [almelo](municipality)
-  [uithuizen](municipality)
-  [tuk](municipality)
-  [koggenland](municipality)
-  [schermerhorn](municipality)
-  [langedijk](municipality)
-  [loon op zand](municipality)
-  [bussum](municipality)
-  [baexem](municipality)
-  [tuil](municipality)
-  [gasselternijveenschemond](municipality)
-  [veenhuizen](municipality)
-  [kelpen-oler](municipality)
-  [sebaldeburen](municipality)
-  [beets](municipality)
-  [nijensleek](municipality)
-  [wernhout](municipality)
-  [ruinerwold](municipality)
-  [idzega](municipality)
-  [den hout](municipality)
-  ['s hertogenbosch](municipality)
-  [s-gravenhage](municipality)
-  [Terschelling](municipality)
-  [Nijmegen](municipality)
-  Ik woon in [Dongen](municipality)


## synonym:Aa en Hunze
- aa en hunze
- amen
- anderen
- anloo
- annen
- annerveenschekanaal
- balloërveld
- balloo
- deurze
- eext
- eexterveen
- eexterveenschekanaal
- eexterzandvoort
- ekehaar
- eldersloo
- eleveld
- gasselte
- gasselternijveen
- gasselternijveenschemond
- gasteren
- geelbroek
- gieten
- gieterveen
- grolloo
- marwijksoord
- nieuw annerveen
- nieuwediep
- nijlande
- nooitgedacht
- oud annerveen
- papenvoort
- rolde
- schipborg
- schoonloo
- vredenheim

## synonym:Aalsmeer                           
- aalsmeer
- kudelstaart

## synonym:Aalten                             
- aalten
- bredevoort
- de heurne
- dinxperlo

## synonym:Achtkarspelen
- achtkarspelen
- augustinusga
- boelenslaan
- buitenpost
- drogeham
- gerkesklooster
- harkema
- kootstertille
- stroobos
- surhuisterveen
- surhuizum
- twijzel
- twijzelerheide

## synonym:Alblasserdam                       
- alblasserdam

## synonym:Albrandswaard
- poortugaal
- rhoon
- albrandswaard

## synonym:Alkmaar                            
- alkmaar
- de rijp
- driehuizen
- graft
- grootschermer
- markenbinnen
- noordeinde
- oost-graftdijk
- oterleek
- oudorp
- schermerhorn
- starnmeer
- stompetoren
- west-graftdijk
- zuidschermer

## synonym:Almelo                             
- aadorp
- almelo
- bornerbroek

## synonym:Almere                             
- almere

## synonym:Alphen aan den Rijn                
- aarlanderveen
- alphen aan den rijn
- benthuizen
- boskoop
- hazerswoude-dorp
- hazerswoude-rijndijk
- koudekerk aan den rijn
- zwammerdam

## synonym:Alphen-Chaam 
- alphen-chaam                      
- chaam
- galder
- strijbeek

## synonym:Altena
- altena
- almkerk
- andel
- babyloniënbroek
- drongelen
- dussen
- eethen
- genderen
- giessen
- hank
- meeuwen
- nieuwendijk
- sleeuwijk
- uitwijk
- veen
- waardhuizen
- werkendam
- wijk en aalburg
- woudrichem

## synonym:Ameland       
- ameland                     
- ballum
- hollum

## synonym:Amersfoort                         
- amersfoort
- hoogland
- hooglanderveen
- stoutenburg noord

## synonym:Amstelveen                         
- amstelveen

## synonym:Amsterdam                          
- amsterdam

## synonym:Apeldoorn                          
- apeldoorn
- beekbergen
- beemte broekland
- hoog soeren
- lieren
- loenen
- radio kootwijk
- uddel
- ugchelen
- wenum wiesel

## synonym:Appingedam                         
- appingedam

## synonym:Arnhem                             
- arnhem

## synonym:Assen                              
- assen
- loon
- rhee
- ter aard
- ubbena
- zeijerveen
- zeijerveld

## synonym:Asten                              
- asten
- ommel

## synonym:Baarle-Nassau                      
- baarle-nassau
- castelre
- ulicoten

## synonym:Baarn                              
- baarn
- lage vuursche

## synonym:Barendrecht                        
- barendrecht

## synonym:Barneveld                 
- barneveld
- de glind
- garderen
- kootwijk
- kootwijkerbroek
- stroe
- terschuur
- voorthuizen
- zwartebroek

## synonym:Beek  
- beek                             
- baek
- sjpaubik
- spaubeek

## synonym:Beekdaelen
- beekdaelen
- amstenrade
- bingelrade
- doenrade
- hulsberg
- jabeek
- merkelbeek
- nuth
- oirsbeek
- puth
- schimmert
- schinnen
- schinveld
- sweikhuizen
- wijnandsrade

## synonym:Beemster      
- beemster                     
- middenbeemster
- noordbeemster
- westbeemster
- zuidoostbeemster

## synonym:Beesel                             
- beesel
- reuver

## synonym:Berg en Dal                        
- berg en dal
- erlecom
- groesbeek
- heilig landstichting
- kekerdom
- leuth
- millingen aan de rijn
- ooij
- persingen
- ubbergen

## synonym:Bergeijk                           
- bergeijk
- luyksgestel
- riethoven
- westerhoven

## synonym:Bergen (L.)                        
- siebengewald
- wellerlooi

## synonym:Bergen (NH.)                       
- bergen aan zee
- egmond aan den hoef
- egmond aan zee
- egmond-binnen
- groet
- schoorl

## synonym:Bergen op Zoom                     
- bergen op zoom
- halsteren
- lepelstraat

## synonym:Berkelland
- berkelland                         
- beltrum
- borculo
- eibergen
- gelselaar
- haarlo
- neede
- rekken
- rietmolen
- ruurlo

## synonym:Bernheze
- bernheze                          
- heesch
- heeswijk-dinther
- loosbroek
- nistelrode
- vorstenbosch

## synonym:Best                               
- best

## synonym:Beuningen  
- beuningen                        
- ewijk
- weurt
- winssen

## synonym:Beverwijk                          
- beverwijk
- wijk aan zee

## synonym:Bladel                             
- bladel
- casteren
- hapert
- hoogeloon
- netersel

## synonym:Blaricum                           
- blaricum

## synonym:Bloemendaal                        
- aerdenhout
- bennebroek
- bloemendaal
- overveen
- vogelenzang

## synonym:Bodegraven-Reeuwijk  
- bodegraven-reeuwijk              
- bodegraven
- driebruggen
- nieuwerbrug aan den rijn
- reeuwijk
- waarder

## synonym:Boekel                             
- boekel
- venhorst

## synonym:Borger-Odoorn
- 1e exloërmond
- 2e exloërmond
- 2e valthermond                        
- borger-odoorn
- borger
- bronneger
- bronnegerveen
- buinen
- buinerveen
- drouwen
- drouwenermond
- drouwenerveen
- ees
- eesergroen
- eeserveen
- ellertshaar
- exloërveen
- exloo
- klijndijk
- nieuw-buinen
- odoorn
- odoornerveen
- valthe
- valthermond
- westdorp
- zandberg

## synonym:Borne                              
- borne
- hertme
- zenderen

## synonym:Borsele   
- borsele                         
- baarland
- borssele
- driewegen
- ellewoutsdijk
- heinkenszand
- hoedekenskerke
- kwadendamme
- lewedorp
- nieuwdorp
- nisse
- oudelande
- ovezande
- 's-gravenpolder
- sgravenpolder
- 's-heer abtskerke
- 's-heerenhoek
- heer abtskerke
- heerenhoek

## synonym:Boxmeer                        
- beugen
- boxmeer
- groeningen
- holthees
- maashees
- oeffelt
- overloon
- rijkevoort
- sambeek
- vierlingsbeek
- vortum-mullem

## synonym:Boxtel                             
- boxtel
- liempde

## synonym:Breda                              
- breda
- prinsenbeek
- teteringen

## synonym:Brielle                            
- brielle
- vierpolders
- zwartewaal

## synonym:Bronckhorst   
- bronckhorst                     
- baak
- bronkhorst
- drempt
- halle
- hoog-keppel
- hummelo
- keijenborg
- laag-keppel
- olburgen
- rha
- steenderen
- toldijk
- vierakker
- vorden
- wichmond
- zelhem

## synonym:Brummen                            
- brummen
- eerbeek
- empe
- hall
- leuvenheim
- tonden

## synonym:Brunssum                           
- brunssum

## synonym:Bunnik                             
- bunnik
- odijk
- werkhoven

## synonym:Bunschoten
- bunschoten                         
- bunschoten-spakenburg
- eemdijk

## synonym:Buren
- buren                              
- asch
- beusichem
- eck en wiel
- erichem
- ingen
- kapel-avezaath
- kerk-avezaath
- lienden
- maurik
- ommeren
- ravenswaaij
- zoelen
- zoelmond

## synonym:Capelle aan den IJssel             
- capelle aan den ijssel

## synonym:Castricum                          
- akersloot
- castricum
- de woude
- limmen

## synonym:Coevorden                          
- aalden
- benneveld
- coevorden
- dalen
- dalerpeel
- dalerveen
- de kiel
- diphoorn
- erm
- gees
- geesbrug
- holsloot
- meppen
- nieuwlande coevorden
- noord-sleen
- oosterhesselen
- schoonoord
- sleen
- stieltjeskanaal
- 't haantje
- wachtum
- wezup
- wezuperbrug
- zweeloo
- zwinderen

## synonym:Cranendonck                        
- cranendonck
- budel
- budel-dorplein
- budel-schoot
- gastel
- maarheeze
- soerendonk

## synonym:Cuijk                              
- beers
- cuijk
- haps
- linden
- sint agatha
- vianen nb

## synonym:Culemborg                          
- culemborg

## synonym:Dalfsen                            
- dalfsen
- lemelerveld
- nieuwleusen

## synonym:Dantumadiel
- dantumadiel                        
- broeksterwâld
- damwâld
- de falom
- de westereen
- driezum
- feanwâlden
- readtsjerk
- rinsumageast
- sibrandahûs
- wâlterswâld

## synonym:De Bilt                            
- bilthoven
- de bilt
- groenekan
- hollandsche rading
- maartensdijk
- westbroek

## synonym:De Fryske Marren
- de fryske marren                   
- akmarijp
- bakhuizen
- balk
- bantega
- boornzwaag
- broek
- delfstrahuizen
- dijken
- doniaga
- echtenerbrug
- eesterga
- elahuizen
- follega
- goingarijp
- harich
- haskerhorne
- idskenhuizen
- joure
- kolderwolde
- langweer
- legemeer
- lemmer
- mirns
- nijehaske
- nijemirdum
- oldeouwer
- oosterzee
- oudehaske
- oudemirdum
- ouwsterhaule
- ouwster-nijega
- rijs
- rohel
- rotstergaast
- rotsterhaule
- ruigahuizen
- scharsterbrug
- sint nicolaasga
- sintjohannesga
- sloten
- snikzwaag
- sondel
- terherne
- terkaple
- teroele
- tjerkgaast
- vegelinsoord
- wijckel

## synonym:De Ronde Venen
- de ronde venen                     
- abcoude
- amstelhoek
- baambrugge
- de hoef
- mijdrecht
- vinkeveen
- waverveen
- wilnis

## synonym:De Wolden
- de wolden                          
- de wijk
- drogteropslagen
- kerkenveld
- koekange
- linde
- ruinerwold
- veeningen

## synonym:Delft                              
- delft

## synonym:Delfzijl                           
- bierum
- borgsweer
- delfzijl
- farmsum
- godlinze
- holwierde
- krewerd
- losdorp
- meedhuizen
- termunten
- termunterzijl
- wagenborgen
- woldendorp

## synonym:Den Helder                         
- den helder
- huisduinen
- julianadorp

## synonym:Deurne                             
- deurne
- helenaveen
- liessel
- neerkant
- vlierden

## synonym:Deventer                           
- bathmen
- colmschate
- deventer
- diepenveen
- lettele
- okkenbroek
- schalkhaar

## synonym:Diemen                             
- diemen

## synonym:Dinkelland
- dinkelland                         
- agelo
- denekamp
- lattrop-breklenkamp
- nutter
- ootmarsum
- oud ootmarsum
- saasveld
- tilligte
- weerselo

## synonym:Doesburg                           
- doesburg

## synonym:Doetinchem                         
- doetinchem
- gaanderen
- wehl

## synonym:Dongen                             
- dongen
- 's gravenmoer
- klein dongen
- vaart
- sgravenmoer
- 'sgravenmoer
- gravemoer
- gravenmoer
- sgravemoer
- kleindongen


## synonym:Dordrecht                          
- dordrecht

## synonym:Drechterland
- drechterland                       
- hem
- hoogkarspel
- oosterblokker
- oosterleek
- schellinkhout
- venhuizen
- westwoud
- wijdenes

## synonym:Drimmelen                          
- drimmelen
- hooge zwaluwe
- lage zwaluwe
- made
- terheijden
- wagenberg

## synonym:Dronten                            
- biddinghuizen
- dronten
- swifterbant

## synonym:Druten                             
- deest
- druten
- horssen
- puiflijk

## synonym:Duiven                             
- duiven
- groessen

## synonym:Echt-Susteren
- echt-susteren                      
- echt
- koningsbosch
- maria hoop
- nieuwstadt
- roosteren
- sint joost
- susteren

## synonym:Edam-Volendam
- edam-volendam                      
- beets
- edam
- hobrede
- kwadijk
- middelie
- oosthuizen
- schardam
- volendam
- warder

## synonym:Ede                                
- bennekom
- de klomp
- deelen
- ede
- ederveen
- harskamp
- lunteren
- otterlo
- wekerom

## synonym:Eemnes                             
- eemnes

## synonym:Eersel                             
- duizel
- eersel
- knegsel
- steensel
- vessem
- wintelre

## synonym:Eijsden-Margraten
- eijsden-margraten                  
- banholt
- bemelen
- cadier en keer
- eckelrade
- eijsden
- gronsveld
- margraten
- mheer
- noorbeek
- scheulder
- sint geertruid

## synonym:Eindhoven                          
- eindhoven

## synonym:Elburg                             
- doornspijk
- elburg
- 't harde

## synonym:Emmen                              
- barger-compascuum
- emmen
- emmer-compascuum
- erica
- klazienaveen
- klazienaveen-noord
- nieuw-amsterdam
- nieuw-dordrecht
- nieuw-schoonebeek
- nieuw-weerdinge
- roswinkel
- schoonebeek
- veenoord
- weiteveen
- zandpol
- zwartemeer

## synonym:Enkhuizen                          
- enkhuizen

## synonym:Enschede                           
- enschede

## synonym:Epe                                
- emst
- epe
- oene
- vaassen

## synonym:Ermelo                             
- ermelo

## synonym:Etten-Leur                         
- etten-leur

## synonym:Geertruidenberg                    
- geertruidenberg
- raamsdonk
- raamsdonksveer

## synonym:Geldrop-Mierlo
- geldrop-mierlo                     
- geldrop
- mierlo

## synonym:Gemert-Bakel
- gemert-bakel                       
- bakel
- de mortel
- de rips
- elsendorp
- gemert
- handel
- milheeze

## synonym:Gennep                             
- gennep
- heijen
- milsbeek
- ottersum
- ven-zelderheide

## synonym:Gilze en Rijen
- gilze en rijen                     
- gilze
- hulten
- molenschot
- rijen

## synonym:Goeree-Overflakkee
- goeree-overflakkee                 
- achthuizen
- den bommel
- dirksland
- goedereede
- herkingen
- melissant
- middelharnis
- nieuwe-tonge
- ooltgensplaat
- ouddorp
- oude-tonge
- sommelsdijk
- stad aan 't haringvliet
- stellendam

## synonym:Goes                               
- goes
- kattendijke
- 's-heer arendskerke
- 's-heer hendrikskinderen
- wilhelminadorp
- wolphaartsdijk

## synonym:Goirle                             
- goirle
- riel

## synonym:Gooise Meren
- gooise meren                       
- bussum
- muiden
- muiderberg
- naarden

## synonym:Gorinchem                          
- dalem
- gorinchem

## synonym:Gouda                              
- gouda

## synonym:Grave                              
- escharen
- gassel
- grave

## synonym:Groningen                          
- garmerwolde
- glimmen
- groningen
- lellens
- meerstad
- noordlaren
- onnen
- sint annen
- ten boer
- ten post
- thesinge
- winneweer
- woltersum

## synonym:Gulpen-Wittem
- gulpen-wittem                      
- beutenaken
- elkenrade
- epen
- eys
- gulpen
- heijenrath
- ingber
- mechelen
- reijmerstok
- slenaken
- wijlre
- wittem

## synonym:Haaksbergen                        
- haaksbergen

## synonym:Haaren                             
- biezenmortel
- esch
- haaren
- helvoirt

## synonym:Haarlem                            
- haarlem

## synonym:Haarlemmermeer
- haarlemmermeer                  
- aalsmeerderbrug
- abbenes
- badhoevedorp
- beinsdorp
- boesingheliede
- buitenkaag
- burgerveen
- cruquius
- haarlemmerliede
- halfweg
- hoofddorp
- leimuiderbrug
- lijnden
- lisserbroek
- nieuw-vennep
- oude meer
- rijsenhout
- schiphol
- schiphol-rijk
- vijfhuizen
- weteringbrug
- zwaanshoek
- zwanenburg

## synonym:Halderberge
- halderberge                        
- bosschenhoofd
- hoeven
- oud gastel
- oudenbosch
- stampersgat

## synonym:Hardenberg                         
- ane
- anerveen
- anevelde
- balkbrug
- bergentheim
- brucht
- bruchterveld
- collendoorn
- de krim
- dedemsvaart
- den velde
- diffelen
- gramsbergen
- hardenberg
- heemserveen
- holtheme
- holthone
- hoogenweg
- loozen
- lutten
- mariënberg
- radewijk
- rheeze
- rheezerveen
- schuinesloot
- slagharen
- venebrugge

## synonym:Harderwijk                         
- harderwijk
- hierden

## synonym:Hardinxveld-Giessendam             
- hardinxveld-giessendam

## synonym:Harlingen                          
- harlingen
- midlum
- wijnaldum

## synonym:Hattem                             
- hattem

## synonym:Heemskerk                          
- heemskerk

## synonym:Heemstede                          
- heemstede

## synonym:Heerde                             
- heerde
- veessen
- vorchten
- wapenveld

## synonym:Heerenveen                         
- akkrum
- aldeboarn
- bontebok
- de knipe
- gersloot
- haskerdijken
- heerenveen
- hoornsterzwaag
- jubbega
- katlijk
- luinjeberd
- mildam
- nieuwebrug
- nieuwehorne
- nieuweschoot
- oranjewoud
- oudehorne
- oudeschoot
- terband
- tjalleberd

## synonym:Heerhugowaard                      
- heerhugowaard

## synonym:Heerlen                            
- heerlen
- hoensbroek

## synonym:Heeze-Leende
- heeze-leende                       
- heeze
- leende
- sterksel

## synonym:Heiloo                             
- heiloo

## synonym:Hellendoorn                        
- daarle
- daarlerveen
- hellendoorn
- nijverdal

## synonym:Hellevoetsluis                     
- hellevoetsluis
- oudenhoorn

## synonym:Helmond                            
- helmond

## synonym:Hendrik-Ido-Ambacht                
- hendrik-ido-ambacht

## synonym:Het Hogeland
- het hogeland                       
- adorp
- baflo
- bedum
- den andel
- eemshaven
- eenrum
- eppenhuizen
- hornhuizen
- houwerzijl
- kantens
- kloosterburen
- lauwersoog
- leens
- mensingeweer
- oldenzijl
- onderdendam
- oosternieland
- oudeschip
- pieterburen
- rasquert
- roodeschool
- saaxumhuizen
- sauwerd
- schouwerzijl
- stitswerd
- tinallinge
- uithuizen
- uithuizermeeden
- ulrum
- usquert
- vierhuizen
- warffum
- warfhuizen
- wehe-den hoorn
- westernieland
- wetsinge
- zandeweer
- zoutkamp
- zuurdijk

## synonym:Heumen                             
- heumen
- malden
- nederasselt
- overasselt

## synonym:Heusden
- heusden                            
- doeveren
- drunen
- elshout
- haarsteeg
- hedikhuizen
- heesbeen
- herpt
- nieuwkuijk
- oudheusden
- vlijmen

## synonym:Hillegom                           
- hillegom

## synonym:Hilvarenbeek                       
- biest-houtakker
- diessen
- esbeek
- haghorst
- hilvarenbeek

## synonym:Hilversum                          
- hilversum

## synonym:Hoeksche Waard
- hoeksche waard                     
- goudswaard
- heinenoord
- klaaswaal
- maasdam
- mijnsheerenland
- mookhoek
- nieuw-beijerland
- numansdorp
- oud-beijerland
- piershil
- puttershoek
- 's-gravendeel
- sgravendeel
- strijen
- strijensas
- westmaas
- zuid-beijerland

## synonym:Hof van Twente
- hof van twente                     
- ambt delden
- bentelo
- delden
- diepenheim
- goor
- hengevelde
- markelo

## synonym:Hollands Kroon
- hollandse kroon                     
- anna paulowna
- barsingerhorn
- breezand
- den oever
- haringhuizen
- hippolytushoef
- kolhorn
- kreileroord
- lutjewinkel
- middenmeer
- nieuwe niedorp
- oude niedorp
- slootdorp
- 't veld
- westerland
- wieringerwaard
- wieringerwerf
- winkel
- zijdewind

## synonym:Hoogeveen                          
- alteveer gem hoogeveen
- elim
- fluitenberg
- hollandscheveld
- hoogeveen
- nieuwlande
- noordscheschut

## synonym:Hoorn
- hoorn                              
- blokker
- zwaag

## synonym:Horst aan de Maas
- horst aan de maas                  
- america
- broekhuizenvorst
- evertsoord
- griendtsveen
- grubbenvorst
- hegelsom
- horst
- kronenberg
- lottum
- meerlo
- melderslo
- meterik
- sevenum
- swolgen
- tienray

## synonym:Houten                             
- houten
- schalkwijk
- 't goy
- tull en 't waal

## synonym:Huizen                             
- huizen

## synonym:Hulst                              
- clinge
- graauw
- heikant
- hengstdijk
- hulst
- kapellebrug
- kloosterzande
- kuitaart
- lamswaarde
- nieuw namen
- ossenisse
- sint jansteen
- terhole
- vogelwaarde
- walsoorden

## synonym:IJsselstein                        
- ijsselstein

## synonym:Kaag en Braassem
- kaag en braassem                   
- hoogmade
- kaag
- leimuiden
- nieuwe wetering
- oud ade
- oude wetering
- rijnsaterwoude
- rijpwetering
- roelofarendsveen
- woubrugge

## synonym:Kampen                             
- grafhorst
- ijsselmuiden
- kampen
- kamperveen
- 's-heerenbroek
- heerenbroek
- wilsum
- zalk

## synonym:Kapelle                            
- kapelle
- schore
- wemeldinge

## synonym:Katwijk
- katwijk                            
- rijnsburg

## synonym:Kerkrade                           
- eygelshoven
- kerkrade

## synonym:Koggenland
- koggenland                         
- avenhorn
- berkhout
- de goorn
- hensbroek
- obdam
- oudendijk
- scharwoude
- spierdijk
- zuidermeer

## synonym:Krimpen aan den IJssel             
- krimpen aan den ijssel

## synonym:Krimpenerwaard
- krimpenerwaard                     
- ammerstol
- bergambacht
- berkenwoude
- gouderak
- haastrecht
- krimpen aan de lek
- lekkerkerk
- ouderkerk aan den ijssel
- schoonhoven
- stolwijk
- vlist

## synonym:Laarbeek
- laarbeek                           
- aarle-rixtel
- beek en donk
- lieshout
- mariahout

## synonym:Landerd
- landerd                            
- reek
- schaijk
- zeeland

## synonym:Landgraaf                          
- landgraaf

## synonym:Landsmeer                          
- den ilp
- landsmeer
- purmerland

## synonym:Langedijk
- langedijk                          
- broek op langedijk
- noord-scharwoude
- sint pancras
- zuid-scharwoude

## synonym:Lansingerland
- lansingerland                      
- bergschenhoek
- berkel en rodenrijs
- bleiswijk

## synonym:Leeuwarden                         
- alde leie
- baard
- bears
- britsum
- eagum
- easterlittens
- feinsum
- friens
- goutum
- grou
- hempens
- hijum
- hilaard
- húns
- idaerd
- jellum
- jelsum
- jirnsum
- jorwert
- koarnjum
- leeuwarden
- lekkum
- leons
- mantgum
- miedum
- reduzum
- snakkerburen
- stiens
- swichum
- teerns
- warstiens
- warten
- weidum
- wergea
- wytgaard

## synonym:Leiden                             
- leiden

## synonym:Leiderdorp                         
- leiderdorp

## synonym:Leidschendam-Voorburg
- leidschendam-voorburg              
- leidschendam
- voorburg

## synonym:Lelystad                           
- lelystad

## synonym:Leudal
- leudal                             
- baexem
- buggenum
- ell
- grathem
- haelen
- haler
- heibloem
- heythuysen
- horn
- hunsel
- ittervoort
- kelpen-oler
- neer
- neeritter
- nunhem
- roggel

## synonym:Leusden                            
- leusden
- stoutenburg

## synonym:Lingewaard
- lingewaard
- hulhuizen                         
- angeren
- bemmel
- doornenburg
- gendt
- haalderen
- huissen
- ressen

## synonym:Lisse                              
- lisse

## synonym:Lochem                             
- almen
- barchem
- eefde
- epse
- gorssel
- harfsen
- joppe
- kring van dorth
- lochem

## synonym:Loon op Zand                       
- de moer
- kaatsheuvel
- loon op zand

## synonym:Lopik                              
- benschop
- jaarsveld
- lopik
- lopikerkapel
- polsbroek

## synonym:Loppersum                          
- eenum
- garrelsweer
- garsthuizen
- huizinge
- leermens
- loppersum
- middelstum
- oosterwijtwerd
- stedum
- 't zandt
- toornwerd
- westeremden
- westerwijtwerd
- zeerijp
- zijldijk

## synonym:Losser                             
- de lutte
- glane
- losser
- overdinkel

## synonym:Maasdriel
- maasdriel                          
- alem
- ammerzoden
- hedel
- heerewaarden
- hoenzadriel
- hurwenen
- kerkdriel
- velddriel

## synonym:Maasgouw
- maasgouw                           
- beegden
- heel
- linne
- maasbracht
- ohé en laak
- stevensweert
- thorn
- wessem

## synonym:Maassluis                          
- maassluis

## synonym:Maastricht                         
- maastricht

## synonym:Medemblik                          
- abbekerk
- andijk
- benningbroek
- hauwert
- lambertschaag
- medemblik
- midwoud
- nibbixwoud
- oostwoud
- opperdoes
- sijbekarspel
- twisk
- wervershoof
- wognum
- zwaagdijk-oost
- zwaagdijk-west

## synonym:Meerssen                           
- bunde
- geulle
- meerssen
- moorveld
- ulestraten

## synonym:Meierijstad
- meierijstad                        
- erp
- schijndel
- sint-oedenrode
- veghel

## synonym:Meppel                             
- de schiphorst
- meppel
- nijeveen
- rogat

## synonym:Middelburg                         
- arnemuiden
- middelburg
- nieuw- en sint joosland

## synonym:Midden-Delfland
- midden-delfland                    
- maasland
- schipluiden

## synonym:Midden-Drenthe
- midden-drenthe                     
- balinge
- beilen
- bovensmilde
- bruntinge
- drijber
- elp
- garminge
- hijken
- hooghalen
- mantinge
- nieuw-balinge
- oranje
- orvelte
- smilde
- westerbork
- wijster
- witteveen
- zuidveld
- zwiggelte

## synonym:Midden-Groningen
- midden-groningen                   
- foxhol
- froombosch
- hellum
- hoogezand
- kiel-windeweer
- kolham
- kropswolde
- luddeweer
- meeden
- muntendam
- noordbroek
- overschild
- sappemeer
- scharmer
- schildwolde
- siddeburen
- slochteren
- steendam
- tjuchem
- tripscompagnie
- waterhuizen
- westerbroek
- woudbloem
- zuidbroek

## synonym:Mill en Sint Hubert
- mill en sint hubert                
- langenboom
- mill
- sint hubert
- wilbertoord

## synonym:Moerdijk                           
- fijnaart
- heijningen
- klundert
- langeweg
- moerdijk
- noordhoek
- standdaarbuiten
- willemstad
- zevenbergen

## synonym:Molenlanden
- molenlanden                        
- arkel
- bleskensgraaf
- brandwijk
- giessenburg
- goudriaan
- groot-ammers
- hoogblokland
- hoornaar
- kinderdijk
- langerak
- molenaarsgraaf
- nieuw-lekkerland
- nieuwpoort
- noordeloos
- ottoland
- oud-alblas
- schelluinen
- streefkerk
- waal
- wijngaarden

## synonym:Montferland 
- montferland                       
- azewijn
- braamt
- didam
- kilder
- lengel
- loerbeek
- 's-heerenberg
- heerenberg
- stokkum
- vethuizen
- wijnbergen
- zeddam

## synonym:Montfoort                          
- linschoten
- montfoort

## synonym:Mook en Middelaar
- mook en middelaar                  
- middelaar
- molenhoek
- mook
- plasmolen

## synonym:Neder-Betuwe
- neder-betuwe                       
- dodewaard
- echteld
- ijzendoorn
- kesteren
- ochten
- opheusden

## synonym:Nederweert                         
- leveroy
- nederweert
- nederweert-eind
- ospel

## synonym:Nieuwegein                         
- nieuwegein

## synonym:Nieuwkoop                          
- nieuwkoop
- nieuwveen
- noorden
- ter aar
- vrouwenakker
- woerdense verlaat
- zevenhoven

## synonym:Nijkerk                            
- hoevelaken
- nijkerk
- nijkerkerveen

## synonym:Nijmegen                           
- lent
- nijmegen

## synonym:Nissewaard
- nissewaard                         
- abbenbroek
- geervliet
- heenvliet
- hekelingen
- simonshaven
- spijkenisse
- zuidland

## synonym:Noardeast-Fryslân
- noardeast-fryslân                  
- aalsum
- anjum
- augsbuurt
- blije
- bornwird
- brantgum
- burdaard
- burum
- dokkum
- ee
- engwierum
- ferwert
- foudgum
- ginnum
- hallum
- hantum
- hantumeruitburen
- hantumhuizen
- hegebeintum
- hiaure
- holwerd
- jannum
- jislum
- jouswier
- kollum
- kollumerpomp
- kollumerzwaag
- lichtaard
- lioessens
- marrum
- metslawier
- moddergat
- morra
- munnekezijl
- niawier
- oosternijkerk
- oudwoude
- paesens
- raard
- reitsum
- ternaard
- triemen
- veenklooster
- waaxens
- wânswert
- warfstermolen
- westergeest
- wetsens
- wierum
- zwagerbosch

## synonym:Noord-Beveland
- noord-beveland                     
- colijnsplaat
- geersdijk
- kamperland
- kats
- kortgene
- wissenkerke

## synonym:Noordenveld
- noordenveld                        
- een
- een-west
- foxwolde
- langelo
- leutingewolde
- lieveren
- matsloot
- nietap
- nieuw-roden
- norg
- peest
- peize
- roden
- roderesch
- roderwolde
- veenhuizen
- westervelde
- zuidvelde

## synonym:Noordoostpolder
- noordoostpolder                    
- bant
- creil
- emmeloord
- ens
- espel
- kraggenburg
- luttelgeest
- marknesse
- nagele
- rutten
- schokland
- tollebeek

## synonym:Noordwijk
- noordwijk                          
- de zilk
- noordwijkerhout

## synonym:Nuenen, Gerwen en Nederwetten
- nuenen, gerwen en nederwetten
- gerwen
- nederwetten      
- nuenen

## synonym:Nunspeet                           
- elspeet
- hulshorst
- nunspeet
- vierhouten

## synonym:Oegstgeest                         
- oegstgeest

## synonym:Oirschot                           
- oirschot
- oost west en middelbeers

## synonym:Oisterwijk                         
- heukelom
- moergestel
- oisterwijk

## synonym:Oldambt
- oldambt                            
- bad nieuweschans
- beerta
- blauwestad
- drieborg
- finsterwolde
- heiligerlee
- midwolda
- nieuw beerta
- nieuw scheemda
- nieuwolda
- oudezijl
- scheemda
- 't waar
- westerlee
- winschoten

## synonym:Oldebroek                          
- hattemerbroek
- noordeinde gld
- oldebroek
- oosterwolde gld
- 't loo oldebroek
- wezep

## synonym:Oldenzaal                          
- oldenzaal

## synonym:Olst-Wijhe
- olst-wijhe                         
- marle
- olst
- welsum
- wesepe
- wijhe

## synonym:Ommen                              
- arriën
- beerze
- beerzerveld
- dalmsholte
- giethmen
- lemele
- ommen
- stegeren
- vilsteren
- vinkenbuurt
- witharen

## synonym:Oost Gelre
- oost gelre
- oost-gelre                         
- groenlo
- harreveld
- lichtenvoorde
- lievelde
- mariënvelde
- vragender
- zieuwent

## synonym:Oosterhout
- oosterhout                         
- den hout
- dorst
- oosteind

## synonym:Ooststellingwerf
- oostellingwerf                   
- appelscha
- donkerbroek
- fochteloo
- haule
- haulerwijk
- langedijke
- makkinga
- nijeberkoop
- oldeberkoop
- oosterwolde
- ravenswoud
- waskemeer

## synonym:Oostzaan                           
- oostzaan

## synonym:Opmeer                             
- aartswoud
- de weere
- hoogwoud
- opmeer
- spanbroek

## synonym:Opsterland
- opsterland                         
- bakkeveen
- beetsterzwaag
- drachten-azeven
- frieschepalen
- gorredijk
- hemrik
- jonkerslân
- langezwaag
- lippenhuizen
- luxwoude
- nij beets
- olterterp
- siegerswoude
- terwispel
- tijnje
- ureterp
- wijnjewoude

## synonym:Oss                                
- berghem
- demen
- deursen-dennenburg
- dieden
- geffen
- herpen
- huisseling
- keent
- lith
- lithoijen
- macharen
- maren-kessel
- megen
- neerlangel
- neerloon
- oijen
- oss
- overlangel
- ravenstein
- teeffelen

## synonym:Oude IJsselstreek
- oude ijsselstreek                  
- breedenbroek
- etten
- gendringen
- heelweg
- megchelen
- netterden
- silvolde
- sinderen
- terborg
- ulft
- varsselder
- varsseveld
- westendorp

## synonym:Ouder-Amstel
- ouder-amstel                       
- amsterdam-duivendrecht
- duivendrecht
- ouderkerk aan de amstel

## synonym:Oudewater                          
- hekendorp
- oudewater
- papekop
- snelrewaard

## synonym:Overbetuwe 
- overbetuwe                        
- andelst
- driel
- elst
- hemmen
- herveld
- heteren
- homoet
- randwijk
- slijk-ewijk
- valburg
- zetten

## synonym:Papendrecht                        
- papendrecht

## synonym:Peel en Maas
- peel en maas                       
- beringe
- egchel
- grashoek
- helden
- kessel
- koningslust
- maasbree
- meijel
- panningen

## synonym:Pekela
- pekela                             
- nieuwe pekela
- oude pekela

## synonym:Pijnacker-Nootdorp
- pijnacker-nootdorp                 
- delfgauw
- nootdorp
- pijnacker

## synonym:Purmerend                          
- purmerend

## synonym:Putten                             
- putten

## synonym:Raalte                             
- broekland
- heeten
- heino
- laag zuthem
- lierderholthuis
- luttenberg
- mariënheem
- nieuw heeten
- raalte

## synonym:Reimerswaal
- reimerswaal                        
- hansweert
- krabbendijke
- kruiningen
- oostdijk
- rilland
- waarde
- yerseke

## synonym:Renkum                             
- doorwerth
- heelsum
- heveadorp
- oosterbeek
- renkum
- wolfheze

## synonym:Renswoude                          
- renswoude

## synonym:Reusel-De Mierden
- reusel-de mierden                  
- hooge mierde
- hulsel
- lage mierde
- reusel

## synonym:Rheden                             
- de steeg
- dieren
- ellecom
- laag-soeren
- rheden
- spankeren

## synonym:Rhenen                             
- elst ut
- rhenen

## synonym:Ridderkerk                         
- ridderkerk

## synonym:Rijssen-Holten
- rijssen-holten                     
- holten
- rijssen

## synonym:Roerdalen
- roerdalen                          
- herkenbosch
- melick
- montfort
- posterholt
- sint odiliënberg
- vlodrop

## synonym:Roermond                           
- herten
- roermond
- swalmen

## synonym:Roosendaal                         
- heerle
- moerstraten
- nispen
- roosendaal
- wouw
- wouwse plantage

## synonym:Rotterdam                          
- botlek rotterdam
- europoort
- hoek van holland
- hoogvliet
- maasvlakte
- pernis
- rotterdam
- vondelingenplaat

## synonym:Rozendaal                          
- rozendaal

## synonym:Rucphen                            
- rucphen
- schijf
- sprundel
- st. willebrord
- zegge

## synonym:Schagen                            
- burgerbrug
- callantsoog
- dirkshorn
- oudesluis
- petten
- schagen
- schagerbrug
- sint maarten
- sint maartensbrug
- sint maartensvlotbrug
- 't zand
- tuitjenhorn
- waarland
- warmenhuizen

## synonym:Schiedam                           
- schiedam

## synonym:Schiermonnikoog                    
- schiermonnikoog

## synonym:Schouwen-Duiveland
- schouwen-duiveland                 
- brouwershaven
- bruinisse
- burgh-haamstede
- dreischor
- ellemeet
- kerkwerve
- nieuwerkerk
- noordgouwe
- noordwelle
- oosterland
- ouwerkerk
- renesse
- scharendijke
- sirjansland
- zierikzee
- zonnemaire

## synonym:'s-Gravenhage                      
- 's-gravenhage
- gravenhage
- sgravenhage
- den haag
- denhaag

## synonym:'s-Hertogenbosch                   
- nuland
- rosmalen
- 's-hertogenbosch
- hertogenbosch
- shertogenbosch
- den bosch
- denbosch

## synonym:Simpelveld                         
- baneheide
- bocholtz
- simpelveld

## synonym:Sint Anthonis                      
- landhorst
- ledeacker
- oploo
- rijkevoort-de walsert
- sint anthonis
- stevensbeek
- wanroij
- westerbeek

## synonym:Sint-Michielsgestel                
- berlicum
- den dungen
- gemonde
- sint-michielsgestel

## synonym:Sittard-Geleen
- sittard-geleen                     
- born
- buchten
- einighausen
- geleen
- grevenbicht
- guttecoven
- holtum
- limbricht
- munstergeleen
- obbicht
- papenhoven
- sittard
- windraak

## synonym:Sliedrecht                         
- sliedrecht

## synonym:Sluis                              
- aardenburg
- breskens
- cadzand
- eede
- groede
- hoofdplaat
- ijzendijke
- nieuwvliet
- oostburg
- retranchement
- schoondijke
- sint kruis
- sluis
- waterlandkerkje
- zuidzande

## synonym:Smallingerland
- smallingerland                     
- boornbergum
- de tike
- de veenhoop
- de wilgen
- drachten
- drachtstercompagnie
- goëngahuizen
- houtigehage
- kortehemmen
- nijega
- opeinde
- rottevalle
- smalle ee

## synonym:Soest                              
- soest
- soesterberg

## synonym:Someren                            
- lierop
- someren

## synonym:Son en Breugel                     
- son en breugel

## synonym:Stadskanaal                        
- mussel
- musselkanaal
- onstwedde
- stadskanaal

## synonym:Staphorst                          
- ijhorst
- punthorst
- rouveen
- staphorst

## synonym:Stede Broec
- stede broec                        
- bovenkarspel
- grootebroek
- lutjebroek

## synonym:Steenbergen
- steenbergen                        
- de heen
- dinteloord
- kruisland
- nieuw-vossemeer

## synonym:Steenwijkerland
- steenwijkerland                    
- baars
- basse
- belt-schutsloot
- blankenham
- blokzijl
- de bult
- de pol
- eesveen
- giethoorn
- ijsselham
- kalenberg
- kallenkote
- kuinre
- marijenkampen
- nederland
- oldemarkt
- onna
- ossenzijl
- paasloo
- scheerwolde
- sint jansklooster
- steenwijk
- steenwijkerwold
- tuk
- vollenhove
- wanneperveen
- wetering
- willemsoord
- witte paarden
- zuidveen

## synonym:Stein                              
- stein
- urmond

## synonym:Stichtse Vecht
- stichtse vecht                     
- breukelen
- kockengen
- loenen aan de vecht
- loenersloot
- maarssen
- nieuwer ter aa
- nieuwersluis
- nigtevecht
- oud zuilen
- tienhoven
- vreeland

## synonym:Súdwest-Fryslân 
- súdwest-fryslân                   
- abbega
- allingawier
- arum
- blauwhuis
- boazum
- bolsward
- breezanddijk
- britswert
- burgwerd
- cornwerd
- dearsum
- dedgum
- easterein
- easterwierrum
- exmorra
- ferwoude
- folsgare
- gaast
- gaastmeer
- gauw
- goënga
- greonterp
- hartwerd
- heeg
- hemelum
- hichtum
- hidaard
- hieslum
- hindeloopen
- hinnaard
- hommerts
- idsegahuizum
- idzega
- iens
- ijlst
- indijk
- it heidenskip
- itens
- jutrijp
- kimswerd
- kornwerderzand
- koudum
- koufurderrige
- kûbaard
- loënga
- lollum
- longerhouw
- lytsewierrum
- makkum
- molkwerum
- nijhuizum
- nijland
- offingawier
- oosthem
- oppenhuizen
- parrega
- piaam
- pingjum
- poppenwier
- raerd
- reahûs
- rien
- sandfirden
- scharnegoutum
- schettens
- schraard
- sibrandabuorren
- smallebrugge
- sneek
- stavoren
- súdwest-fryslân
- tersoal
- tirns
- tjalhuizum
- tjerkwerd
- uitwellingerga
- waaksens
- warns
- westhem
- witmarsum
- wiuwert
- wolsum
- wommels
- wons
- workum
- woudsend
- ypecolsga
- ysbrechtum
- zurich

## synonym:Terneuzen                          
- axel
- hoek
- koewacht
- overslag
- philippine
- sas van gent
- sluiskil
- spui
- terneuzen
- westdorpe
- zaamslag
- zuiddorpe

## synonym:Terschelling
- terschelling                       
- baaiduinen
- formerum
- hee
- kaard
- kinnum
- landerum
- lies
- midsland
- striep
- west-terschelling

## synonym:Texel
- texel                              
- de cocksdorp
- de koog
- de waal
- den burg
- oudeschild

## synonym:Teylingen
- teylingen                          
- sassenheim
- voorhout
- warmond

## synonym:Tholen                             
- oud-vossemeer
- poortvliet
- scherpenisse
- sint philipsland
- sint-annaland
- sint-maartensdijk
- stavenisse
- tholen

## synonym:Tiel                               
- kapel avezaath
- kerk avezaath
- tiel
- wadenoijen

## synonym:Tilburg                            
- berkel-enschot
- tilburg
- udenhout

## synonym:Tubbergen                          
- albergen
- fleringen
- harbrinkhoek
- hezingen
- langeveen
- mander
- manderveen
- mariaparochie
- reutum
- tubbergen
- vasse

## synonym:Twenterand
- twenterand                         
- bruinehaar
- geerdijk
- vriezenveen
- vroomshoop
- westerhaar-vriezenveensewijk

## synonym:Tynaarlo                           
- bunne
- de groeve
- de punt
- donderen
- eelde
- eelderwolde
- midlaren
- paterswolde
- taarlo
- tynaarlo
- vries
- winde
- yde
- zeegse
- zeijen
- zuidlaarderveen
- zuidlaren

## synonym:Tytsjerksteradiel
- tytsjerksteradiel                  
- aldtsjerk
- burgum
- earnewâld
- eastermar
- garyp
- gytsjerk
- hurdegaryp
- jistrum
- mûnein
- noardburgum
- oentsjerk
- ryptsjerk
- sumar
- suwâld
- tytsjerk
- wyns

## synonym:Uden                               
- odiliapeel
- uden
- volkel

## synonym:Uitgeest                           
- uitgeest

## synonym:Uithoorn                           
- de kwakel
- uithoorn

## synonym:Urk                                
- urk

## synonym:Utrecht                            
- de meern
- haarzuilens
- utrecht
- vleuten

## synonym:Utrechtse Heuvelrug
- utrechtse heuvelrug                
- amerongen
- doorn
- driebergen-rijsenburg
- leersum
- maarn
- maarsbergen
- overberg

## synonym:Vaals                              
- lemiers
- vaals
- vijlen

## synonym:Valkenburg aan de Geul
- valkenburg aan de geul             
- berg en terblijt
- schin op geul
- walem

## synonym:Valkenswaard                       
- valkenswaard

## synonym:Veendam                            
- veendam
- wildervank

## synonym:Veenendaal                         
- veenendaal

## synonym:Veere                              
- aagtekerke
- biggekerke
- domburg
- gapinge
- grijpskerke
- koudekerke
- meliskerke
- oostkapelle
- veere
- vrouwenpolder
- westkapelle
- zoutelande

## synonym:Veldhoven                          
- veldhoven

## synonym:Velsen                             
- driehuis nh
- ijmuiden
- santpoort-noord
- santpoort-zuid
- velsen-noord
- velsen-zuid
- velserbroek

## synonym:Venlo                              
- arcen
- belfeld
- lomm
- steyl
- tegelen
- velden
- venlo

## synonym:Venray                             
- blitterswijck
- castenray
- geijsteren
- heide
- leunen
- merselo
- oirlo
- smakt
- venray
- veulen
- vredepeel
- wanssum
- ysselsteyn

## synonym:Vijfheerenlanden
- vijfheerenlanden                   
- ameide
- everdingen
- hagestein
- hei- en boeicop
- hoef en haag
- kedichem
- leerbroek
- leerdam
- lexmond
- meerkerk
- nieuwland
- oosterwijk
- ossenwaard
- schoonrewoerd
- tienhoven aan de lek
- vianen
- zijderveld

## synonym:Vlaardingen                        
- vlaardingen

## synonym:Vlieland                           
- vlieland

## synonym:Vlissingen                         
- oost-souburg
- ritthem
- vlissingen

## synonym:Voerendaal                         
- klimmen
- ransdaal
- voerendaal

## synonym:Voorschoten                        
- voorschoten

## synonym:Voorst
- voorst                             
- nijbroek
- steenenkamer
- terwolde
- teuge
- twello
- wilp

## synonym:Vught                              
- cromvoirt
- vught

## synonym:Waadhoeke
- waadhoeke                          
- achlum
- baaium
- berltsum
- bitgum
- bitgummole
- blessum
- boer
- boksum
- deinum
- dongjum
- dronryp
- firdgum
- franeker
- herbaijum
- hitzum
- ingelum
- klooster lidlum
- marsum
- menaam
- minnertsga
- nij altoenae
- oosterbierum
- oudebildtzijl
- peins
- pietersbierum
- ried
- schalsum
- sexbierum
- skingen
- slappeterp
- spannum
- st.-annaparochie
- st.-jacobiparochie
- tzum
- tzummarum
- vrouwenparochie
- westhoek
- wier
- wjelsryp
- zweins

## synonym:Waalre                             
- waalre

## synonym:Waalwijk                           
- sprang-capelle
- waalwijk
- waspik

## synonym:Waddinxveen                        
- waddinxveen

## synonym:Wageningen                         
- wageningen

## synonym:Wassenaar                          
- wassenaar

## synonym:Waterland
- waterland                          
- broek in waterland
- ilpendam
- katwoude
- marken
- monnickendam
- uitdam
- watergang
- zuiderwoude

## synonym:Weert                              
- stramproy
- weert

## synonym:Weesp                              
- weesp

## synonym:West Betuwe
- west betuwe                        
- acquoy
- asperen
- beesd
- deil
- enspijk
- est
- geldermalsen
- gellicum
- haaften
- heesselt
- hellouw
- herwijnen
- heukelum
- meteren
- neerijnen
- ophemert
- opijnen
- rhenoy
- rumpt
- tricht
- tuil
- varik
- vuren
- waardenburg

## synonym:West Maas en Waal
- west maas en waal                  
- altforst
- appeltern
- beneden-leeuwen
- boven-leeuwen
- dreumel
- maasbommel
- wamel

## synonym:Westerkwartier 
- westerkwartier                    
- aduard
- boerakker
- briltil
- de wilp
- den horn
- doezum
- enumatil
- ezinge
- feerwerd
- garnwerd
- grijpskerk
- grootegast
- jonkersvaart
- kommerzijl
- kornhorn
- lauwerzijl
- leek
- lettelbert
- lucaswolde
- lutjegast
- marum
- midwolde
- niebert
- niehove
- niezijl
- noordhorn
- nuis
- oldehove
- oldekerk
- opende
- pieterzijl
- saaksum
- sebaldeburen
- tolbert
- visvliet
- zuidhorn

## synonym:Westerveld
- westerveld                         
- boschoord
- darp
- diever
- dieverbrug
- doldersum
- dwingeloo
- frederiksoord
- geeuwenbrug
- havelte
- havelterberg
- nijensleek
- oude willem
- uffelte
- vledder
- wapse
- wapserveen
- wateren
- wilhelminaoord
- wittelte
- zorgvlied

## synonym:Westervoort                        
- westervoort

## synonym:Westerwolde
- westerwolde                        
- bellingwolde
- blijham
- bourtange
- oudeschans
- sellingen
- ter apel
- ter apelkanaal
- veelerveen
- vlagtwedde
- vriescheloo
- wedde

## synonym:Westland
- westland                           
- de lier
- honselersdijk
- kwintsheul
- maasdijk
- monster
- naaldwijk
- poeldijk
- 's-gravenzande
- sgravenzande
- ter heijde
- wateringen

## synonym:Weststellingwerf
- weststellingwerf                 
- blesdijke
- boijl
- de blesse
- de hoeve
- langelille
- munnekeburen
- nijeholtpade
- nijeholtwolde
- nijelamer
- nijetrijne
- oldeholtpade
- oldeholtwolde
- oldelamer
- oldetrijne
- oosterstreek
- peperga
- slijkenburg
- sonnega
- spanga
- steggerda
- ter idzard
- vinkega
- wolvega
- zandhuizen

## synonym:Westvoorne
- westvoorne                         
- oostvoorne
- rockanje
- tinte

## synonym:Wierden                            
- enter
- hoge hexel
- notter
- wierden
- zuna

## synonym:Wijchen                            
- balgoij
- batenburg
- bergharen
- hernen
- leur
- niftrik
- wijchen

## synonym:Wijdemeren
- wijdemeren                         
- ankeveen
- breukeleveen
- kortenhoef
- loosdrecht
- nederhorst den berg
- 's-graveland
- sgraveland

## synonym:Wijk bij Duurstede                 
- cothen
- langbroek
- wijk bij duurstede

## synonym:Winterswijk                        
- winterswijk
- winterswijk brinkheurne
- winterswijk corle
- winterswijk henxel
- winterswijk huppel
- winterswijk kotten
- winterswijk meddo
- winterswijk miste
- winterswijk ratum
- winterswijk woold

## synonym:Woensdrecht                        
- hoogerheide
- huijbergen
- ossendrecht
- putte
- woensdrecht

## synonym:Woerden                            
- harmelen
- kamerik
- woerden
- zegveld

## synonym:Wormerland
- wormerland                         
- jisp
- oostknollendam
- wijdewormer
- wormer

## synonym:Woudenberg                         
- woudenberg

## synonym:Zaanstad
- zaanstad                           
- assendelft
- koog aan de zaan
- krommenie
- westknollendam
- westzaan
- wormerveer
- zaandam
- zaandijk

## synonym:Zaltbommel                         
- aalst
- bern
- brakel
- bruchem
- delwijnen
- gameren
- kerkwijk
- nederhemert
- nieuwaal
- poederoijen
- zaltbommel
- zuilichem

## synonym:Zandvoort                          
- bentveld
- zandvoort

## synonym:Zeewolde                           
- zeewolde

## synonym:Zeist                              
- austerlitz
- bosch en duin
- den dolder
- zeist

## synonym:Zevenaar                           
- aerdt
- angerlo
- babberich
- giesbeek
- herwen
- lathum
- lobith
- pannerden
- tolkamer
- zevenaar

## synonym:Zoetermeer                         
- zoetermeer

## synonym:Zoeterwoude                        
- gelderswoude
- zoeterwoude

## synonym:Zuidplas
- zuidplas                           
- moerkapelle
- moordrecht
- nieuwerkerk aan den ijssel

## synonym:Zundert                            
- achtmaal
- klein zundert
- rijsbergen
- wernhout
- zundert

## synonym:Zutphen                            
- warnsveld
- zutphen

## synonym:Zwartewaterland
- zwartewaterland                    
- genemuiden
- hasselt
- zwartsluis

## synonym:Zwijndrecht                        
- heerjansdam
- zwijndrecht

## synonym:Zwolle                             
- zwolle
