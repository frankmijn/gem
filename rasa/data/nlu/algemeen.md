## intent:overig_digid_hebgeendigid
- Ik heb geen DigiD. 
- DigiD, dat heb ik niet. 
- Dat heb ik niet. 
- Heb ik niet. 
- Heb geen DigiD.
- ik heb geen digid
- DigiD heb ik niet.

## intent:overig_digid_watisdigid
- Wat is DigiD?
- Ik weet niet wat DigiD is.
- Ik heb geen idee wat DigiD is. 
- Wat is dat? 
- DigiD? Nog nooit van gehoord.
- DigiD? Nooit van gehoord.
- DigiD? geen idee wat dat is.

## intent:overig_digid_wilgeendigid
- Ik wil geen gebruik maken van DigiD.
- Ik vertrouw DigiD niet. 
- Ik wil geen DigiD gebruiken.
- Ik vind DigiD stom. 
- DigiD wil ik niet. 
- Ik ben tegen digid. 
- Ik wil DigiD niet gebruiken.
- DigiD is stom

## intent:producten_hoebetalen
- Kan ik met pinpas betalen?
- Kan ik met pin betalen?
- Kan ik met cash betalen?
- Kan ik met contant geld betalen?
- Hoe kan ik betalen bij de balie?
- Kan ik pinnen?
- Welke betaalmethodes bieden jullie aan?
- Hoe kan ik betalen?
- Kan ik met briefgeld betalen?
- Kan ik met briefgeld betalen?
- Is het mogelijk om met creditcard te betalen?
- Kan ik ook contant betalen?
- Kan ik met creditcard betalen?
- Nee ik bedoel kan ik met geld betalen?
- Kan ik contant afrekenen?	

## intent:overig_foto_eisen
- Waar kan ik pasfoto's laten maken voor een [IDkaart](product:id-kaart)?
- Waar kan ik pasfoto's laten maken voor een [ID-kaart](product:id-kaart) in [Utrecht](municipality)?
- Wat voor pasfoto moet ik meenemen voor [ID-kaart](product:id-kaart) ?
- Wat zijn de regels voor een foto voor een [ID-kaart](product:id-kaart)?
- Aan welke eisen moet een pasfoto voor [ID-kaart](product:id-kaart)  voldoen?
- Wat zijn de eisen van een pasfoto voor mijn [id-kaart](product)?
- Waar moet ik een pasfoto laten maken voor mijn [ID-kaart](product:id-kaart)?
- Welke pasfoto moet er op mijn [id-kaart](product)?
- Waar kan ik pasfoto's laten maken voor een [paspoort](product)?
- Waar kan ik pasfoto's laten maken voor een [paspoort](product) in [Utrecht](municipality)?
- Wat voor pasfoto moet ik meenemen voor [paspoort](product)?
- Wat zijn de regels voor een foto voor een [paspoort](product)?
- Aan welke eisen moet een pasfoto voor [paspoort](product) voldoen?
- Wat zijn de eisen van een pasfoto voor mijn [paspoort](product)?
- Waar moet ik een pasfoto laten maken voor mijn [paspoort](product)?
- Welke pasfoto moet er op mijn [paspoort](product)?
- Hoeveel pasfoto's heb ik nodig voor mijn [paspoort](product)?
- En een pasfoto?
- hoe recent moet de pasfoto zijn?
- kan ik een schoolfoto gebruiken?
- mag een zwartwit foto?
- foto
- hoeveel pasfoto's heb ik nodig

# Dit is een test
### product_verhuismelding_hebikdanmijnpaspoortnodig
# - Heb ik dan mijn paspoort nodig? 
# - Moet ik dat met mijn paspoort doen? 
# - Moet dat met een paspoort?

## intent:klachtindienen_algemeen  
- ik wil een [klacht](product) indienen
- kan ik hier een [klacht](product) indienen?
- Ik heb een [klacht](product)
- [Klacht](product)
- Ik ben niet tevreden. Ik wil een [klacht](product) indienen
- Ik ben niet goed geholpen. Ik wil een [klacht](product) indienen
- ik wil in deze chat mijn [klacht](product) indienen
- ik wil gewoon hier mijn [klacht](product) indienen

## intent:product_klacht_indienen
- [Klacht](product) indienen bij de gemeente
- Ik wil een [klacht](product) indienen bij de gemeente
- [Klacht](product) indienen bij gemeente [Dongen](municipality)
- Ik wil een [klacht](product) indienen bij gemeente [Dongen](municipality)
- Ik ben niet tevreden. Ik wil een [klacht](product) indienen bij gemeente [Dongen](municipality)
- Ik ben niet tevreden. Ik wil een [klacht](product) indienen bij [Dongen](municipality)
- Ik wil een [klacht](product) indienen bij de gemeente

## intent:feedback_chatbot
- feedback
- ik heb feedback
- mag ik feedback geven?
- mag ik tips geven?
- Ik wil graag feedback geven over de bot. Hoe kan ik dat doen?
- Ik wil graag feedback geven over de bot
- Ik wil graag feedback geven over de chatbot
- Ik wil graag feedback geven over de chatbot. Hoe kan ik dat doen?
- Ik heb een verbetervoorstel voor de chatbot
- Dat kan beter
- Ik weet hoe het beter kan
- Ik heb suggesties
- Ik heb een suggestie voor jou
- Ik heb een idee
- In de keuze opties zie ik technische termen geen begrijpelijke keuze

## intent:openingstijden
- Wat zijn de openingstijden van het stadhuis? 
- Wat zijn de openingstijden van de zwembaden?
- Wanneer zijn jullie open? 
- Wanneer is het stadskantoor open? 
- Wanneer is het Servicepunt open?
- Wanneer kan ik terecht? 
- Tot hoe laat zijn jullie open? 
- Tot hoe laat kan dat?
- Tot hoe laat?
- Vanaf hoe laat zijn jullie open?
- Vanaf hoe laat kan dat?
- Vanaf hoe laat? 
- Zijn jullie nog open? 
- Hoe vroeg is de balie open? 
- Hoe laat kan ik bij de Stadswinkel terecht? 
- Tot hoe laat is het afvalscheidingsstation open? 
- Hoe laat is het wijkbureau open?
- Hoe laat is het zwembad open?
- Hoe laat dan?
- Hoe laat kan dat?
- Hoe laat?
- openingstijden
- wat zijn de openingstijden van het gemeentehuis?
- Hoelang zijn jullie morgen open?


## intent:afspraakmaken
- Kan ik een afspraak maken om aan een [id-kaart](product) te komen?
- Kan ik een afspraak maken om aan een [id-kaart](product) te komen in [Amsterdam](municipality)?
- Kan ik een afspraak maken om mijn [verhuizing](product:verhuismelding) door te geven?
- Kan ik een afspraak maken om mijn [nieuwe adres](product:verhuismelding) in [Amsterdam](municipality) door te geven?
- Hoe maak ik een afspraak om [id-kaart](product) aan te vragen?
- Ik wil een [id-kaart](product) aanvragen aan de balie, hoe maak ik een afspraak?
- Ik wil een [id-kaart](product) aanvragen aan de balie, hoe maak ik een afspraak in [Dongen](municipality)?
- Kan ik online een afspraak maken voor het aanvragen van een [id-kaart](product)? 
- Kan ik een afspraak maken om aan een [paspoort](product) te komen in [Dongen](municipality)?
- Hoe maak ik een afspraak om [paspoort](product) aan te vragen?
- Ik wil een [paspoort](product) aanvragen aan de balie, hoe maak ik een afspraak in [Amsterdam](municipality)?
- Ik wil een afspraak om mijn [rijbewijs](product) aan te vragen
- Ik wil een afspraak maken in [Tilburg](municipality) om mijn [rijbewijs](product) aan te vragen
- Kan ik online een afspraak maken voor het aanvragen van een [paspoort](product)? 
- Ik wil graag een afspraak maken voor het aanvragen van een [paspoort](product).
- Kan ik een afspraak maken voor een nieuw [rijbewijs](product)?
- Kan ik daarvoor een afspraak maken?
- Kan ik daar een afspraak voor maken? 
- Hoe kan ik daarvoor een afspraak maken?
- Hoe maak ik daar een afspraak voor?
- Afspraak maken

## intent:afspraaknodig
- Moet ik een afspraak maken om mijn [verhuizing](product:verhuismelding) door te geven?
- Moet ik een afspraak maken voor nieuwe [identiteitskaart](product:id-kaart) in [Apeldoorn](municipality)?
- Moet ik een afspraak maken voor mijn [verhuizing](product:verhuismelding) naar [Dongen](municipality)?
- Moet ik een afspraak maken voor het afhalen van mijn [id-kaart](product)?
- Moet ik dan een afspraak maken? 
- Heb ik een afspraak nodig voor een nieuw [paspoort](product)?
- Heb ik een afspraak nodig voor een nieuwe [id-kaart](product)?
- Heb ik dan een afspraak nodig? 
- Heeft mijn [zoon](namens:kind) dan een afspraak nodig?
- Kan ik mijn [id-kaart](product) ophalen zonder afspraak? 
- Kan ik mijn [id-kaart](product) ophalen zonder afspraak in [Rotterdam](municipality)? 
- Kan ik een [id-kaart](product) zonder afspraak op komen halen?
- Is het mogelijk om een [id-kaart](product) zonder afspraak op te halen?
- Afspraak nodig
- Kan ik een [paspoort](product) zonder afspraak op komen halen?
- Kan ik een [paspoort](product) zonder afspraak op komen halen in [Dongen](municipality)?
- Is het mogelijk om een [paspoort](product) zonder afspraak op te halen?
- Is het mogelijk om een [paspoort](product) zonder afspraak op te halen in [Tilburg](municipality)? 

## intent:watkangem
- Wat kun jij allemaal?
- Wat kun je allemaal
- Wat kan je voor me doen? 
- Wat kan ik van jou verwachten? 
- Waar kun je me mee helpen? 
- wat kun je allemaal?
- wat kan jij allemaal?
- wat kan jij?
- wat weet je allemaal
- waar kan je me wel mee helpen?
- welke vragen kan je dan beantwoorden
- welke vragen kan ik stellen
- welke vragen kan ik aan jou stellen
- wat kun jij zoal?
- Wat kan je allemaal?
- Wat kan ik nog meer
- Werkt de chatbot?

## intent:hoe_aanvragen 
- Hoe kom ik aan een [id-kaart](product)? 
- Hoe kom ik aan een [paspoort](product) in [Utrecht](municipality)? 
- Hoe kom ik aan een nieuw [paspoort](product)?
- Hoe krijg ik een [id-kaart](product)?
- Hoe krijg ik een [paspoort](product) in [Amsterdam](municipality)?
- Hoe vraag ik een [id-kaart](product) aan?
- Hoe vraag ik dat aan?
- Hoe moet ik dat aanvragen?
- Ik wil een [paspoort](product) aanvragen, hoe doe ik dat?
- Ik wil een [id-kaart](product) aanvragen, hoe doe ik dat in [Utrecht](municipality)?
- Ik wil een nieuwe [passeport](product:paspoort) aanvragen, hoe doe ik dat?
- Ik wil een nieuw [paspoort](product) aanvragen, hoe doe ik dat in [Tilburg](municipality)?
- Ik wil een nieuw [paspoort](product)
- Ik wil een nieuwe [id-kaart](product)
- Ik wil mijn [paspoort](product) verlengen, hoe moet dat?
- Ik wil een [paspoort](product) aanvragen
- Kan ik een [id-kaart](product) aanvragen?
- Kan ik een [paspoort](product) aanvragen in [Utrecht](municipality)?
- kan ik mijn [paspoort](product) verlengen?
- Kan ik online een [id-kaart](product) aanvragen?
- Kan ik online een [id-kaart](product) aanvragen in [Ede](municipality)?
- Ik moet mijn [paspoort](product) verlengen
- Hi Gem, weet je hoe ik mijn [paspoort](product) kan verlengen?
- met [paspoort](product) aanvragen
- met het aanvragen van een nieuwe [paspoort](product)
- Wat moet ik doen om een [pasoort](product:paspoort) aan te vragen?
- Wat moet ik doen om een [id-kaart](product) [met spoed](spoed) aan te vragen??
- Wat moet ik doen om een [id](product:id-kaart) aan te vragen in [Zeist](municipality)
- Kan ik een [id-kaart](product) [met spoed](spoed) aanvragen?
- Kan ik een [id-kaart](product) [met spoed](spoed) aanvragen in [Dongen](municipality)?
- Kan ik [versneld](spoed) een [paspoort](product) aanvragen?
- Kan ik [versneld](spoed) een [id-kaart](product) aanvragen in [Rotterdam](municipality)?
- Kan ik een [paspoort](product) [met spoed](spoed) aanvragen?
- Kan ik een [paspoort](product) [met spoed](spoed) aanvragen in [Tilburg](municipality)?
- Hoe kan ik een [id-kaart](product) [met spoed](spoed) aanvragen?
- Hoe kan ik een [paspoort](product) [met spoed](spoed) aanvragen in [Dongen](municipality)?
- Hoe kan ik een [spoed](spoed) [paspoort](product) aanvragen?
- Hoe kan ik [met spoed](spoed) een [id-kaart](product) aanvragen?
- Ik wil mijn [id-kaart](product) [eerder](spoed) hebben, kan dat? 
- Ik wil mijn [paspoort](product) [eerder](spoed) hebben, kan dat in [Tilburg](municipality)?
- Ik wil [snel](spoed) een [paspoort](product) aanvragen.
- Ik wil [snel](spoed) een [paspoort](product) aanvragen in [Tilburg](municipality)
- Ik heb deze week nog heel [snel](spoed) een [paspoort](product) nodig. Kan dat?
- Ik heb heel [snel](spoed) een [paspoort](product) nodig. 
- Ik heb heel [snel](spoed) een nieuw [paspoort](product) nodig. 
- Ik heb heel [snel](spoed) een nieuwe [id-kaart](product) nodig. 
- Ik heb [met spoed](spoed) een nieuw [paspoort](product) nodig.
- Ik heb [met spoed](spoed) een [id-kaart](product) nodig
- Ik heb [snel](spoed) een nieuw [paspoort](product) nodig.
- mijn [kind](namens) heeft een nieuw [paspoort](product) nodig
- hoe kan ik een nieuwe [id-kaart](product) voor mijn [kind](namens) aanvragen
- ik wil een [paspoort](product) aanvragen voor mijn [kind](namens)
- ik wil een [IDkaart](product:id-kaart) voor mijn [zoon](namens:kind)
- Hoe vraag ik dat aan voor mijn [kind](namens)?
- Hoe moet ik dat aanvragen voor mijn [kind](namens)?
- hoe vraag ik een [ID-kaart](product:id-kaart) aan voor mijn [zoon](namens:kind)
- Kan ik voor mijn [kind](namens) een [paspoort](product) aanvragen?
- Ik wil een [id-kaart](product) voor mijn [dochter](namens:kind) aanvragen?
- Ik wil graag een nieuw [paspoort](product) aanvragen
- hoe kun je helpen met [paspoort](product) aanvragen
- Wat heb ik nodig voor het aanvragen van mijn [paspoort](product)?
- ik wil een [paspoort](product) regelen voor mijn [zoontje](namens:kind)
- Ik wil graag een Nederlands [paspoort](product) aanvragen?
- Ik wil een Nederlands [paspoort](product) aanvragen?	
- ik heb een [paspoort](product) nodig voor zaterdag
- [paspoort](product) aanvragen
- Ik moet een [paspoort](product)
- hoe regel ik een nieuw [paspoort](product)
- ik wil [snel](spoed) een [paspoort](product) hebben
- Nieuwe [id](product:id-kaart) bewijs
- Ik wil een nieuwe [id](product:id-kaart) aanvragen
- [paspoort](product) aanvragen
- wil graag een nieuw [paspoort](product) aanvragen
- wil graag een nieuw [paspoort](product)
- Mag ik zelf een [paspoort](product) aanvragen?
- Ik wil er een kopen
- Ik wil een [paspoort](product) kopen
- graag wil ik een [id](product:id-kaart)
- ik wil graag een [id](product:id-kaart)

## intent:spoed
- Ik heb 'm [met spoed](spoed) nodig
- Ik heb het [snel](spoed) nodig
- Kan dat ook heel [snel](spoed)?
- Ik heb hem morgen al nodig
- Ik heb er [met spoed](spoed) één nodig
- Ik heb [met spoed](spoed) nodig
- Ik heb hem zaterdag al nodig
- Ik heb hem aanstaande vrijdag al nodig
- Kan dat met spoed?

## intent:vandaag
- Ik heb het vandaag nog nodig
- Kan het vandaag nog?
- Ik ga vandaag op reis
- We vertrekken vandaag
- Kan het vandaag klaar zijn?
- Ik heb het vandaag nodig 
- is vandaag oké? 
- Vertrekken vandaag
- vertrekken over een paar uurtjes
- ik vertrek later op de dag

## intent:hoe_doorgeven
- [Verhuizing](product:verhuismelding) doorgeven
- [adreswijziging](product:verhuismelding)
- Hoe geef ik mijn [verhuizing](product:verhuismelding) door in [Amsterdam](municipality)?
- Hoe geef ik mijn [verhuizing](product:verhuismelding) door?
- Hoe kan ik mijn [verhuizing](product:verhuismelding) doorgeven?
- Hoe moet ik een [verhuizing](product:verhuismelding) doorgeven?
- Hoe moet ik een [verhuizing](product:verhuismelding) doorgeven in [Dongen](municipality)?
- Hoe moet ik mijn [verhuizing](product:verhuismelding) doorgeven?
- Hoe moet ik mijn [verhuizing](product:verhuismelding) doorgeven in [Dongen](municipality)?
- Hoe [verhuizen](product:verhuismelding)
- Hoe verander ik mijn adres?
- Hoe wijzig ik mijn adres
- Hoe geef ik dat door?
- Hoe geef ik dat dan door?
- Hoe moet ik dat doorgeven?
- Hoe geef ik mijn [nieuwe adres](product:verhuismelding) door?
- Hoe geef ik mijn [nieuwe adres](product:verhuismelding) door in [Dongen](municipality)?
- Hoe kan ik mijn [nieuwe adres](product:verhuismelding) doorgeven?
- Hoe geef ik mijn [adreswijziging](product:verhuismelding) door?
- Hoe geef ik mijn [adreswijziging](product:verhuismelding) door in [Dongen](municipality)?
- Kan ik ook naar [Eindhoven](municipality) [verhuizen](product:verhuismelding)?
- Kan ik naar [De Bilt](municipality) [verhuizen](product:verhuismelding)?
- Kan ik mijn [nieuwe adres](product:verhuismelding) doorgeven?
- Kan ik met mijn digid [verhuizen](product:verhuismelding)
- Ik ga [verhuizen](product:verhuismelding). Hoe kan ik dit aan de gemeente doorgeven?
- Ik moet een [nieuwe adres](product:verhuismelding) doorgeven
- Ik heb een [adreswijziging](product:verhuismelding)
- Ik wil mijn [adres wijzigen](product:verhuismelding)
- Ik wil mijn [nieuwe adres](product:verhuismelding) doorgeven aan gemeente [Tilburg](municipality)
- Ik ga [verhuizen](product:verhuismelding). Hoe kan ik dit aan de gemeente doorgeven?
- Ik ga [verhuizen](product:verhuismelding) naar [Dongen](municipality). Wat moet ik doorgeven?
- En zou graag willen [verhuizen](product:verhuismelding)
- IK [verhuis](product:verhuismelding) binnen [dongen](municipality), wat moet ik doen?
- Hoe vraag ik een [verhuismelding](product) aan?
- Moet ik mijn [verhuizing](product:verhuismelding) doorgeven aan mijn huidige gemeente?
- Moet ik mijn [verhuizing](product:verhuismelding) doorgeven aan de gemeente waar ik ga wonen?
- Ik wil gewoon ver weg uit dit stomme dorp!

## intent:ontevreden
- Belachelijk
- Ik ben ontevreden
- Ik ben niet blij
- Ik ben niet happy
- Ik ben niet tevreden
- Ik ben niet tevreden met de bot
- Wat waardeloos
- Wat ontzettend waardeloos
- Dat is waardeloos

## intent:je-snapt-het-niet
- Je begrijpt me niet
- Je snapt er geen bal van
- Je snapt er niets van
- je snapt het niet
- je snapte mn vraag niet
- je snapt ook niets
- Snap je dat nou niet
- Je begrijpt me verkeerd
- Je begrijpt er niets van
- Je hebt het verkeerd begrepen
- waarom snap je niets

## intent:contact
- ik wil contactgegevens
- hoe kan ik jullie bereiken?
- op welk mailadres kan ik jullie bereiken?
- hoe kan ik een e-mail sturen?
- wat is het juiste e-mailadres? 

## intent:producten_algemeen
- ik wil meer informatie over [idkaart](product:id-kaart)
- ik wil meer informatie over [paspoort](product)
- mijn vraag gaat over [idkaart](product:id-kaart)
- ik heb een vraag over mijn [idkaart](product:id-kaart)
- kan je mij informatie geven over [paspoort](product)
- ik heb je hulp nodig. het gaat over mijn [idkaart](product:id-kaart)
- mijn vraag gaat over [paspoort](product)
- ik heb een vraag over mijn [paspoort](product)
- Ik heb een vraag over [verhuizen](product:verhuismelding)
- Mijn vraag gaat over een [verhuizing](product:verhuismelding)
- Ik heb een vraag over [verhuizen](product:verhuismelding)
- Mijn vraag gaat over een [verhuizing](product:verhuismelding)
- Ik heb een vraag over [verhuizen](product:verhuismelding)
- ik heb een vraag over een [paspoort](product)
- ik heb een vraag over een [ID kaart](product:id-kaart)
- ik wil iets weten over een [paspoort](product)
- Nee, ik heb een vraag over [verhuizing](product:verhuismelding)
- wil iets weten over een [paspoort](product)

## intent:overig_coronavirus
- corona
- Corona
- carona
- korona
- virus
- Hoe lang blijven de corona-maatregelen nog gelden?
- Ik wil informatie over corona
- covid
- covid19
- CoVid-19
- Tozo
- Hoe kan ik me aanmelden voor de Tozo?
- Hoe kan ik een tozo aanvragen?
- Hoe vraag ik een tozo aan?
- Hoe kan ik me aanmelden voor financiële compensatie? 
- Togs
- Kom ik in aanmerking voor de Togs?
- Hoe kan ik een togs aanvragen?
- Waar kan ik de informatie voor ondersteuning aan ZZP’ers vinden?
- NOW
- Kan ik gebruik maken van de NOW-regeling?
- Hoe vraag ik een NOW aan?
- lockdown
- Hoe lang blijft de lockdown nog duren? 
- Zijn jullie nog open met het virus? 
- Hoe lang blijven de scholen nog dicht?
- Wanneer mogen de sportscholen weer open? 
- Hoe druk is het in de ziekenhuizen?
- Zijn er nog genoeg IC-bedden? 
- Hoe weet ik of ik het corona-virus heb?
- Mogen we nog gaan bidden in de moskee?
- Kunnen we nog naar de kerk?
- Wordt vanwege het virus alles uitgesteld?
- Mag ik nog open blijven met mijn winkel?
- Er wordt nog gevoetbald met groepjes van meer dan 3, waar kan ik dat melden?
- wat zijn de maatregelen rondom corona bij de gemeente
- Kan ik mijn [id-kaart](product) nu nog wel ophalen vanwege corona?
- Kunnen we ons [paspoort](product) nog wel ophalen vanwege het virus?
- Kunnen we onze [bruiloft](product:trouwen) verplaatsen? Kost dat extra geld?
- Gaat ons [geregistreerd partnerschap](product:trouwen) nog door ivm het corona virus?
- Kan ik nog wel naar het gemeentehuis komen? 
- Mag ik nog wel op het gemeentehuis komen? 
- Mag ik nog wel naar het gemeentehuis komen? 
- Mag ik nog wel naar het gemeentehuis komen met het virus? 
- moet ik ergens rekening mee houden met corona?

## intent:teamtest
- Test [Harvey](name)
- test [Steven](name)
- Test [Dylan](name)
- test [Jason](name)
- Test [Frank](name)
- test [Stijn](name)
- Test [Margon](name)
- Test [Yvonne](name)
- Test [Sandra](name)
- Test [Cobi](name)
- Gewoon even testen
- dit is een test

## intent:download_chatgeschiedenis
- Ik wil dit gesprek bewaren
- Download chatlog
- Download chatgeschiedenis
- Download gesprek
- Chatlog
- Gespreksgeschiedenis bewaren

## intent:digitaal
- Kan ik dat ook digitaal doen?
- Kan ik het digitaal doorgeven?
- Digitaal
- Ik wil dat digitaal doen
- Ik wil dat online doen
- Kan ik dat ook online doorgeven?
- Kan dat ook op jullie website?
- En kan dat digitaal?
- Kan ik dit online regelen
- Kan ik niet online aanvragen?

## intent:privacy
- Wat gaan jullie met mijn gegevens doen
- Hoe zit het met de privacy?
- Wat gebeurt er met mijn gegevens?
- Wat doen jullie met mijn data?
- Hoe ga jij met mijn gegevens om?
- Met welke partijen worden mijn gegevens gedeeld?
- Hoe lang bewaren jullie mijn gegevens? 
- Met wie worden mijn gegevens gedeeld?
- Hoe gaan jullie om met mijn data
- Wordt deze chat opgeslagen?
- Kunnen jullie deze chat ook verwijderen?
- Ik wil mijn gegevens bekijken.
- Ik wil graag weten wat je met mijn gegevens doet
- wat gebeurt er met de gegevens die ik hier invul?

## intent:verschil
- Wat is het verschil?
- Wat is het verschil dan?
- Wat is het verschil tussen die twee? 
- Waarin verschillen ze van elkaar?
- Wat is het verschil tussen de twee? 
- Wat is het verschil tussen beide?
- waar verschillen ze in?
- wat maakt het verschil?
