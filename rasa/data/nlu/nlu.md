## intent:inform
- vandaag
- morgen
- overmorgen
- nu
- 1-07-2020
- 12 januari
- 18 februari
- 24 maart
- 4 april 
- 27 mei 
- 2 juni
- 23 juli
- 10 augustus 
- 5 september
- 19 oktober
- 13 november
- 25 december
- 18 jaar
- 9 juni 2020
- over twee weken

## intent:inform_affirmative
- ja
- ja, graag
- graag
- aub
- yes please!
- akkoord
- prima
- dat zou mooi zijn
- helemaal geweldig
- alsjeblieft
- ja doe maar
- vooruit dan maar
- yup

## intent:inform_oke
- ok
- oke
- okee
- okay
- oké
- okidoki
- okay dan

## intent:inform_denial
- nee
- Nee
- nee gem
- nee thnx
- nee bedankt
- nee, bedankt
- nee dank je
- nope
- nope thanks
- nope thnx
- neen
- alsjeblieft niet
- zeker niet
- echt niet
- nee hoor hoeft niet
- nee danke
- nee hoor bedankt
- euhh nee
- Nee laat maar

## intent:inform_maybe
- misschien
- wie weet
- maybe
- wellicht
- misschien wel

## intent:inform_dontknow
- dat weet ik niet
- geen idee
- ?
- zou het niet weten
- daar vraag je me wat
- Joost mag het weten
- dat weet ik nog niet
- weet ik niet
- weet ik nog niet

## intent:inform_redelijk
- mwa
- redelijk
- deels 
- ja, maar niet hellemaal
- bij sommige vragen wel en sommige vragen niet
- matig

## intent:jijook
- jij ook
- insgelijks
- jij ook Gem
- van hetzelfde
- hetzelfde
- u ook

## intent:cancel
- annuleren
- stop
- ik wil stoppen
- ik wil afbreken aub
- ik wil afbreken alstublieft
- kunnen we stoppen?
- stop aub
- stop alstublieft
- exit

## intent:restart
- Opnieuw
- opnieuw
- restart
- Herstart
- Reboot
- Ik wil opnieuw beginnen
- Hoe start ik opnieuw?
- opnieuw invoeren
- opnieuw beginnen

## intent:ask_for_human
- Ik wil met een medewerker spreken
- ik wil een mens spreken
- kan ik een mensen spreken
- kan ik een echt gesprek hebben
- Ik wil een mens aan de lijn
- Verbind me even door met een echt mens
- echt mens aub
- ik praat niet met chatbots
- Ik praat niet met bots
- Ik praat niet met robots
- praat met medewerker
- ik wil iemand spreken van burgerzaken
- Ik wil met een mens chatten
- ik wil toch maar met een medewerker overleggen
- Ik wil toch liever een echt mens
- medewerker
- Ik wil iemand persoonlijk spreken. Geen Bot
- Ik wil contact met een medewerker
- Ik wil met een persoon praten
- Kan ik praten met een persoon?

## intent:ask_for_chatbot
- ik wil met de chatbot chatten
- ik wil verder met de chatbot
- Chat met de chatbot
- Ga verder met de bot
- Ga door met de chatbot
- Verder praten met de chatbot

## intent:waaromvraagjedat
- Waarom wil je dat weten? 
- Waarom vraag je dat? 
- Waarom moet ik dat vertellen? 
- Waarom zou ik dat zeggen?
- Hoezo wil je dat weten?

## intent:oepsikbedoel
- Oeps ik bedoel 20 september <!--HvdM time niet taggen, dan pakt DIET/CRF deze op en dat zit in de weg voor Duckling-->
- Oeps ik bedoel [Dongen](municipality)
- Oeps, ik bedoelde eigenlijk [Utrecht](municipality)
- oeps [idkaart](product:id-kaart)
- Ik bedoelde [Tilburg](municipality)
- Ik bedoel een [paspoort](product)
- Ik bedoel [Iran](land)?
- Ik bedoel mijn [dochter](namens:kind)
- Nee [Dongen](municipality)
- Nee ik bedoel 1 april
- Nee ik bedoel [Tilburg](municipality)
- Nee ik bedoelde [Frankrijk](land)?
- Nee volgende week zaterdag
- Het moest eigenlijk [Dongen](municipality) zijn.
- O nee [Tilburg](municipality)
- Oh nee volgende week zondag
- Oh nee voor mijn [vader](namens:hulpbehoevende)
- och nee een [paspoort](product)
- Och voor mijn [zoon](namens:kind) natuurlijk
- Sorry, [Spanje](land)?
- Verhip, [Utrecht](municipality)
- Grutjes het moet volgend jaar zijn
- Excuses, een [paspoort](product)
- Sorry, [Hongarije](land)

## intent:bedoel_iets_anders
- Ik bedoelde iets anders
- nee dat bedoel ik niet
- ik wil mijn vraag anders stellen
- bedoelde wat anders
- bedoelde eigenlijk wat anders
- iets anders
- sorry, bedoelde wat anders
- Ik bedoelde iets anders
- Ik heb een fout gemaakt
- Nee, dat bedoel ik niet
- Nee, dat vroeg ik niet

## intent:contact
- Ik wil contactgegevens
- Ik wil contact met de gemeente
- Wat zijn de contactgegevens van de gemeente [Tilburg](municipality)?
- Hoe krijg ik contact met de gemeente?
- Wat zijn de contactgegevens?

## intent:bellen_met_gemeente
- Kan ik jullie bellen?
- Kan ik ook bellen?
- Op welk telefoonnummer kan ik gemeente [Dongen](municipality) bereiken?
- Op welk telefoonnummer zijn jullie te bereiken?
- op welk telefoonnummer zijn jullie bereikbaar?
- Hoe kan ik jullie bellen?
- Wat is het telefoonnummer?
- wat is jullie telefoonnummer?
- Wat is het telefoonnummer van de gemeente [De Bilt](municipality)?
- Wat is het juiste telefoonnummer?
- op welke telefoonnummer kan ik jullie bereiken?


## intent:gemeente_adres
- Wat is het adres van gemeente [Dongen](municipality)?
- Wat is het bezoekadres van gemeente [Dongen](municipality)?
- Wat is jullie bezoekadres?
- kan ik alstublieft het bezoekadres van gemeente [Tilburg](municipality) krijgen?
- wat is het adres van het gemeentehuis in [dongen](municipality)?
- Wat is het adres?
- Wat is het adres van de gemeente?
- wat is jullie bezoekadres?
- Ik wil weten wat jullie bezoekadres is
- Ik wil weten wat het bezoekadres van gemeente [Bergen op Zoom](municipality)?
- wat is jullie adres
- Wat is het bezoekadres van gemeente [Rotterdam](municipality)?
- Wat is het bezoekadres van [Amsterdam](municipality)?
- Wat is het adres van het stadhuis
- Wat is het adres van [Utrecht](municipality)

## intent:gemeente_postadres
- Wat is het postadres van gemeente [Dongen](municipality)?
- Wat is jullie postadres?
- kan ik alstublieft het postadres van gemeente [Tilburg](municipality) krijgen?
- Wat is het postadres?
- wat is jullie postadres?
- Ik wil weten wat jullie postadres is
- Ik wil weten wat het postadres van gemeente [Alphen aan den Rijn](municipality)?
- Hoe kan ik de gemeente post sturen?

<!-- Voor als bezoeker alleen product invoert, óf voor als Gem heeft moeten vragen "voor welk product vraag je dit" -->
## intent:inform_product  
- [id-kaart](product)
- [idkaart](product:id-kaart)
- [ID-kaart](product:id-kaart)
- [IDkaart](product:id-kaart)
- [paspoort](product)
- [Paspoort](product:paspoort)
- [id-kaart](product:id-kaart)
- [paspoort](product)?
- [verhuismelding](product)
- [verhuizing](product:verhuismelding)
- [Verhuizen](product:verhuismelding)


## intent:envoor
- en voor [Dongen](municipality)?
- En voor [Dongen](municipality) dan?
- en voor [kinderen](namens:kind)?
- en voor een [paspoort](product)?
- En voor een [paspoort](product) in [Amsterdam](municipality) dan?
- en voor een [verhuizing](product:verhuismelding)?
- en voor een [verhuizing](product:verhuismelding) naar [Breda](municipality)?
- En voor mijn [kinderen](namens:kind)?
- En voor mijn [opa](namens:hulpbehoevende)?
- En voor mijn [vriend](namens:partner)?
- en van een [id_kaart](product:id-kaart)?
- en van een [paspoort](product)?
- en bij een [verhuizing](product:verhuismelding)
- En mijn [pleegkind](namens:kind)
- En mijn [vriendin](namens:partner) dan?
- en naar [Maastricht](municipality)?
- En naar [Noorwegen](land)?
- En hoe zit het dan voor een [ID-kaart](product:id-kaart)?
- en wat is dat voor een [paspoort](product)?
- En in [België](land)?
- En in [Portugal](land)?
- en in [Afghanistan](land)?
- en in [China](land)?
- en voor [China](land)?
- en [Canada](land)?
- En [Finland](land)?
- En ook in [Cuba](land)?
- En voor mijn [kind](namens)?
- En voor mijn [baby](namens:kind)?
- En voor mijn [partner](namens)?
- En ook voor mijn [ouders](namens:hulpbehoevende)?
- En ook in [Alkmaar](municipality)?
- En ook voor een [paspoort](product)?
- en ook bij een [verhuizing](product:verhuismelding)
- en een [verhuismelding](product)?
- En een [id-kaart](product) dan?
- en mijn [middelste](namens:kind)?
- en [dongen](municipality) dan?
- en [Rotterdam](municipality)?
- en in [Den Haag](municipality)?
- en van [Nieuwegein](municipality)?
- En van [Tilburg](municipality)?
- en vann [Den Haag](municipality)?
- enn van [Tilburg](municipality)?
- En als ik naar [Hattem](municipality) [verhuis](product:verhuismelding)?


#### Verdiepingsvragen

## intent:hoe
- Hoe moet ik dat doen?
- Hoe moet dat dan?
- Hoe doe ik dat?
- hoe kan ik dat doen?
- hoe werkt dat?
- hoe werkt zoiets?
- Leg me uit hoe ik dat kan doen.
- Ik weet niet hoe
- Ik weet niet hoe ik dat moet doen
- hoe?
- Hoe dan?
- en hoe kan ik dat doen?
- en hoe dan?
- ja maar hoe dan?
- en hoe doe ik dat?
- hoe dan?
- hoe

## intent:wie
- Wie mag dat doen?
- Wie moet dat doen?
- Wie kan dat doen?
- Wie zal dat doen?
- Wie doet dat?
- Wie dan?
- Wie doet dat dan?
- Wie kan dat doen dan?

## intent:wanneer
- Wanneer?
- wanneer moet ik dat doen?
- Wanneer gebeurt dat?
- Wanneer kan ik dat doen?
- Wanneer dan?

## intent:hoelang 
- Hoe lang duurt de procedure? 
- Hoe lang duurt het voor hij klaar is? 
- Hoe lang duurt de procedure voor mijn [id-kaart](product) klaar is? 
- Hoe lang duurt het voordat mijn [paspoort](product) klaar is? 
- Hoe lang duurt het voordat mijn [verhuizing](product:verhuismelding) is verwerkt? 
- Hoe lang duurt zoiets?
- Hoe lang gaat dat duren?
- Hoe lang van tevoren kan dit?
- Duurt het lang voordat ik een nieuw [id-kaart](product) heb?
- Wanneer is het klaar? 
- Wanneer is het [paspoort](product) klaar?
- Wanneer kan ik de [id-kaart](product) ophalen?
- Wanneer kan ik mijn [paspoort](product) ophalen?
- Hoe lang duurt het aanvragen van een [id-kaart](product)? 
- Hoe lang duurt het om een [paspoort](product) aan te vragen? 
- Hoe lang duurt het voordat ik een nieuwe [id-kaart](product) krijg?
- Hoe lang duurt het voordat ik een nieuw [paspoort](product) heb?
- Hoe lang duurt het voor mijn [id-kaart](product) klaar is?
- Hoe lang duurt het voordat ik een nieuw [paspoort](product) krijg?
- Hoe lang duurt het voordat ik een nieuwe [id-kaart](product) heb?
- Hoe lang duurt het voordat de [verhuizing](product:verhuismelding) binnen de gemeente is bevestigd?
- Hoe lang moet ik wachten als ik een [paspoort](product) aanvraag?
- Hoe snel kan ik een nieuwe [id-kaart](product) hebben? 
- Hoe snel kan ik een nieuw [paspoort](product) hebben? 
- Hoeveel tijd heb ik om een [verhuizing](product:verhuismelding) door te geven?
- Kan ik mijn [id-kaart](product) al ophalen? 
- Kunt u kijken of mijn [id-kaart](product) al klaar ligt?
- Kunt u kijken of mijn [paspoort](product) al klaar is?
- Ligt mijn [ID-kaart](product:id-kaart) al klaar?
- Is mijn [paspoort](product) al klaar?


## intent:wat
- Wat dan?
- Wat?
- Wat moet ik dan doen?
- wat kan ik doen?
- Kun je me uitleggen wat?
- Ik snap niet wat
- Wat wil je?

## intent:waar
- Waar kan dat?
- Waar kan ik dat doen?
- Waar zal ik dat doen?
- Waar dan?
- Waar?
- Waar moet ik mijn [verhuizing](product:verhuismelding) doorgeven?
- Waar moet ik mijn [verhuizing](product:verhuismelding) opgeven?
- Waar kan ik mijn [verhuizing](product:verhuismelding) doorgeven aan [Utrecht](municipality)?
- Waar geef ik mijn [verhuizing](product:verhuismelding) door?
- Hallo, ik [verhuis](product:verhuismelding) van [Dongen](municipality) naar [Breda](municipality). Waar moet ik dit doorgeven?
- Waar is dat?
- Waar doe ik dat?
- waar moet ik dat doen

## intent:waarom
- Waarom moet dat?
- Waarom moet ik dat doen?
- Waarom dan?
- Waarom
- Waarom in hemelsnaam?
- Waarom is dat zo?
- waarom niet?

<!-- ## watkostdat
 - Wat moet dat kosten?
 - Wat kost zoiets?
 - Wat kost dat?
 - Hoeveel kost dat?
 - Hoeveel gaat dat kosten?
 - Hoeveel geld kost dat?
 - Hoe duur is dat? 
 - Wat zijn de kosten?  -->

## intent:nieuwe
- Ik heb een nieuwe nodig
- Hoe kan ik een nieuwe krijgen? 
- Waar vraag ik een nieuwe aan? 
- Kan ik die nieuw krijgen? 
- ik wil een nieuwe
- nieuwe nodig

## intent:kwijt
- Ik ben 'm kwijt
- Ik kan het nergens meer vinden
- verloren
- Ik ben het verloren
- foetsie
- gestolen
- Hij is gestolen

## intent:kapot
- Het is kapot
- Hij is kaduuk
- Wat als-ie gebroken is?
- Hij is kapot, wat moet ik nu doen? 
- kapot
- gescheurd
- gebroken

## intent:watkostdat
- Hoeveel kost het om mijn [verhuizing](product:verhuismelding) door te geven?
- Ik heb een [nieuw adres](product:verhuismelding), moet ik geld betalen om dat door te geven? 
- Hoeveel kost het om een [verhuismelding](product:verhuismelding) te doen?
- Ik wil mijn [nieuwe adres](product:verhuismelding) doorgeven, zijn daar kosten aan verbonden? 
- Wat kost een [idkaart](product:id-kaart)?
- Wat kost een [idkaart](product:id-kaart) in [Tilburg](municipality)?
- Wat kost een [id-kaart](product)?
- Hoeveel kost een [idkaart](product:id-kaart)?
- Hoeveel kost een [idkaart](product:id-kaart) in [Amsterdam](municipality)?
- Wat betaal ik voor een [idkaart](product:id-kaart)?
- Hoe duur is een [id-kaart](product)?
- Hoeveel moet ik voor een [id-kaart](product) betalen?
- Hoeveel moet ik voor een [id-kaart](product) betalen in [Utrecht](municipality)?
- Wat is de prijs van een [id-kaart](product)?
- Wat betaalt men voor een [id-kaart](product)?
- Wat kost een [paspoort](product)?
- Wat kost een [paspoort](product) in [Tilburg](municipality)?
- Wat kost een [paspoort](product) in [Loon op Zand](municipality)?
- Hoeveel kost een [paspoort](product)?
- Hoeveel kost een [paspoort](product) in [Utrecht](municipality)?
- Wat betaal ik voor een [paspoort](product)?
- Wat betaal ik voor een [paspoort](product) in [Tilburg](municipality)?
- Hoe duur is een [paspoort](product)?
- Hoe duur is een [paspoort](product) in [Dongen](municipality)?
- Hoeveel moet ik voor een [paspoort](product) betalen?
- Hoeveel moet ik voor een [paspoort](product) betalen in [Tilburg](municipality)?
- Wat is de prijs van een [paspoort](product)?
- Wat is de prijs van een [paspoort](product) in [dongen](municipality)
- Wat betaalt men voor een [paspoort](product)?
- Wat betaalt men voor een [paspoort](product) in [Tilburg](municipality)?
- Wat moet dat kosten?
- Wat kost zoiets?
- Wat kost dat?
- Hoeveel kost dat?
- Hoeveel gaat dat kosten?
- Hoeveel geld kost dat?
- Hoe duur is dat? 
- Wat zijn de kosten?
- en wat kost een [id-kaart](product)?
- hoe duur is mijn [paspoort](product)
- wat kost een [verhuizing](product:verhuismelding)
- wat kost het om te [verhuizen](product:verhuismelding)
- Zijn er kosten als ik naar [dongen](municipality:Dongen) ga [verhuizen](product:verhuismelding)

## intent:watnodig
- Wat heb ik dan nodig? 
- Waar moet ik dan allemaal aan denken? 
- Wat is daar allemaal voor nodig? 
- Welke documenten heb ik nodig
- Wat heb ik allemaal nodig?
- Welke documenten moet ik dan allemaal aan denken?
- Wat heb ik ervoor nodgi?

## intent:watmeenemen
- Wat moet ik dan meenemen? 
- wat moet ik meenemen
- Wat moet ik dan allemaal meebrengen? 
- Wat moet ik dan allemaal meenemen?
- Moet ik dan mijn koopcontract meenemen? 
- Moet ik iets meenemen? 
- Welke documenten moet ik meenemen?
- moet ik iets meenemen wanneer ik aan de balie kom?
- wat moet ik meebrengen
- wat moet ik dan allemaal meenemen

## intent:watmeesturen
- Moet ik dan mijn huurcontract meesturen? 
- Wat moet ik nog meer opsturen?
- Wat moet ik dan nog meer meesturen?
- Welke documenten moet ik meesturen?
- wat moet ik dan allemaal meesturen
- wat moet ik dan meesturen?

## intent:kindmee
- Moet mijn [kind](namens:kind) dan mee?  
- Moet mijn [jongste](namens:kind) dan mee?
- Kan dat zonder mijn [kind](namens:kind)? 
- Moet mijn [baby](namens:kind) dan mee?
- Moet mijn [dochter](namens:kind) dan mee? 
- Moet mijn [zoon](namens:kind) dan mee? 

## intent:aangiftedoen
- Moet ik dan aangifte doen? 
- Moet ik dat dan aangeven? 
- Moet ik dan naar de politie?
- Moet ik aangifte doen dan?
- Moet ik dat melden bij de politie?
- Moet ik dat doorgeven aan de politie?

## intent:nudoorgeven
- Kan ik het nu doorgeven?
- Kan ik dat doorgeven?
- Dan wil ik het nu doorgeven
- Ik wil het doorgeven
- Nu doorgeven graag
- Hoe geef ik dat nu door?

## intent:namenswie
- Kan ik dat ook voor mijn [partner](namens:partner) doen?
- Kan ik dat ook voor mijn [kind](namens:kind) doorgeven? 
- Ik wil dat graag voor [opa](namens:hulpbehoevende) doen. 
- Kan ik dat voor mijn [kinderen](namens:kind) doen? 
- Ik wil dat voor een ander doen
- Kan dat ook voor iemand anders
- Hoe regel ik dat voor mijn [vrouw](namens:partner)
- Kunnen jullie dat gelijk voor mijn [man](namens:partner) regelen?
- namens wie?
- namens?
- kan iemand anders dat doen?
- kan ik dat ook voor mijn [kinderen](namens:kind) doen?


## lookup:municipality
  data/woonplaatsen.txt
  
## lookup:land
  data/landen.txt



## lookup:products
- inzamelkalender
- ophaalschema
- bijstandsuitkering
- BRP-uittreksel
- uittreksel
- uittreksel van de BRP
- GBA-uittreksel
- bewijs van inschrijving
- geboorte aangeven
- geboorte melden
- ID kaart
- ID-kaart
- id_kaart
- id_ _kaart
- ID
- IDkaart
- ik kaart
- identiteitskaart
- spoedID-kaart
- noodpaspoort
- paspoort
- spoedpaspoort
- passeport
- passpoort
- pasport
- passport
- pasoort
- autorijbewijs
- beginnersrijbewijs
- brommerrijbewijs
- jongerenrijbewijs
- motorrijbewijs
- puntenrijbewijs
- rijbewijscategorieën
- rijbewijsregels
- scooterrijbewijs
- vrachtwagenrijbewijs
- Verhuizen
- Verhuizing doorgeven
- adres wijzigen
- adreswijziging
- gaan wonen
- meeverhuizen
- meeverhuisd
- mijn nieuwe adres
- nieuw adres
- nieuwe adres
- nieuwe address
- nieuw huis
- nieuwe woning
- verhiuzin
- verhuis
- verhuisd
- verhuist
- verhuist mee
- verhuizing
- afvalcontainer
- afvalbak
- kliko
- groene kliko
- bruine kliko
- afvalkalender
- belasting kwijtschelding
- bijstand
- geboorteaangifte
- rijbewijs
- trouwen
- geregistreerd partnerschap
- bruiloft
- verhuismelding
- parkeervergunning
- parkeren
- parkeer
- overlijdensakte
- uitkering
- hondenbelasting
- belasting-kwijtschelding
- afspraak
- melding-openbare-ruimte
- klacht
- grofvuil

## lookup:spoed
- met spoed
- versneld
- eerder
- snel
- spoed
- spoedpaspoort
- spoedaanvraag
- heel snel
- sneller

## lookup:namenswie
- kind
- partner
- hulpbehoevende
- kind
- kinderen
- kindje
- kids
- dochter
- dochters
- dochtertje
- dochtertjes
- zoon
- zoons
- zoontje
- zoontjes
- jongste
- oudste
- middelste
- pleegkind
- pleegkinderen
- mijn vriend
- me vriend
- mijn vriendin
- me vriendin
- partner
- mijn man
- me man
- mijn vrouw
- me vrouw
- mijn lief
- vader
- moeder
- opa
- oma
- oom
- tante
- buurman
- buurvrouw

<!-- NB: alle items in deze lijst óók opnemen in ## lookup:namenswie -->
## synonym:kind
- kind
- kinderen
- kindje
- kids
- dochter
- dochters
- dochtertje
- dochtertjes
- zoon
- zoons
- zoontje
- zoontjes
- jongste
- oudste
- middelste
- pleegkind
- pleegkinderen

<!-- NB: alle items in deze lijst óók opnemen in ## lookup:namenswie -->
## synonym:partner
- mijn vriend
- me vriend
- mijn vriendin
- me vriendin
- partner
- mijn man
- me man
- mijn vrouw
- me vrouw
- mijn lief

<!-- NB: alle items in deze lijst óók opnemen in ## lookup:namenswie -->
## synonym:hulpbehoevende
- vader
- moeder
- opa
- oma
- oom
- tante
- buurman
- buurvrouw