#!/bin/bash

# clear model directory
FIRST_ARGUMENT="$1"
if [ "$FIRST_ARGUMENT" = "clean" ] ; then
    echo "Cleaning Model directory"
    rm models/web/latest.tar.gz
else
    echo "To clean the model directory:"
    echo "$0 clean"
fi

docker-compose run rasa train --debug --out models/web --fixed-model-name latest

echo "Training done"

echo "Testing..."

docker-compose run rasa test

echo "Testing done"
