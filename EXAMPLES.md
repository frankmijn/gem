Chatbot prototype Voorbeelden
=============================

# Voorbeelden DEMO Sprint 5 Gemeente Utrecht (November)

LET OP!: Bij schakelen van gemeente moet je ff reseten

---->>>>> Open localhost

## hoeveel inwoners heeft nederland?
    Nederland heeft 17282163 inwoners in 2019 🦄
    
## wie is de burgemeester van Utrecht
    Dat is Jan van Zanen.
   
   
## hoe kom ik aan een id-kaart
    Aanvragen en ophalen van een id-kaart moet je zelf doen, aan de balie. De kosten betaal je meteen bij de aanvraag. Na 5 werkdagen ligt het id-kaart klaar. Je maakt een afspraak voor de aanvraag eenvoudig op onze website.
## wat kost een paspoort?
    In welke gemeente woon je?
## Dongen
    Een paspoort kost €71,30. Heb je nog een andere vraag voor me?
## kan ik pinnen?
    Wij geven de voorkeur aan betalen met pinpas aan de balie. Contant betalen kan natuurlijk ook.


---->>>> Switch to chatbot.tilburg.io


## wanneer moet ik mijn verhuizing doorgeven?
    Wat is je verhuisdatum?
## de 7de dinsdag in 2020
    Dat moet dan tussen 21-01-2020 en 23-02-2020
    
## Kan ik mijn verhuizing al eerder doorgeven in Dongen?
    Dat kan vanaf 40 weken voor de verhuisdatum.
    
## * Schakel over naar Gemcontent
## * https://content.tilburg.io/admin
## * utter_product_verhuismelding_when_info
## * Edit utter: pas 40 aan in 4

## Kan ik mijn verhuizing al eerder doorgeven?
    Dat kan vanaf 4 weken voor de verhuisdatum.
  

----->> Switch to localhost 


## Hoe verander ik mijn adres -----> LINKS NIET OPENEN
    In welke gemeente ga je wonen?
## Utrecht
    Verhuis je binnen Nederland? Dan kun je dit online doorgeven op deze webpagina. Hier heb je DigiD voor nodig. Verhuis je vanuit het buitenland? Dan kun je dit niet online doorgeven. Kijk op deze webpagina.


## waarom moet ik dat dan doorgeven?
    Sorry, ik begrijp je niet. Wil je misschien met een medewerker chatten?
    /Chat verder met de chatbot
    /Chat met een medewerker
## /Chat met een medewerker
    Een moment, ik schakel je door naar een medewerker...🦄
    Klik hier om met een medewerker verder te praten 🦄
## *Schakel over naar OBI4wan

    
-------

# Voorbeelden DEMO Fieldlab Common Ground (September)

LET OP!: Bij schakelen van gemeente moet je ff reseten


## Ik ga trouwen
    Ik weet nog niet zo veel over trouwen.
    In welke gemeente woon je?
## Dongen
    Misschien staat je antwoord op deze [webpagina](https://www.dongen.nl/trouwenindongen.html).


## ik heb last van ongediert
    Ik weet nog niet zo veel over melding-openbare-ruimte.
    Misschien staat je antwoord op deze [webpagina](https://www.dongen.nl/meldpunt.html).
    

## verhuizen
    Verhuis je binnen Nederland? Dan kun je dit online doorgeven. Verhuis je vanuit het buitenland? Neem dan contact op met de gemeente waar je gaat wonen.
    /Ik wil mijn verhuizing doorgeven
    /Wanneer moet ik mijn verhuizing doorgeven?
## /Wanneer moet ik mijn verhuizing doorgeven?
    Wat is je verhuisdatum?
## Volgende week donderdag
    Dat moet dan voor 08-10-2019

## ik wil mijn verhuizing doorgeven
    Binnenkort kan ik je helpen met de verwerking van je verhuizing!
    [afbeelding]
      
      
## Hoe vaak wordt de afvalcontainer opgehaald
    Ik weet nog niet zo veel over afvalkalender.
    In welke gemeente woon je?
## Eindhoven
    Helaas kan ik geen informatie vinden over dit product. Wil je misschien met een medewerker chatten?
    /Chat met een medewerker
    /Chat verder met de chatbot
## /Chat met een medewerker
    Een moment, ik schakel je door naar een medewerker...
    
    
## Bedankt
    Graag gedaan
    
--------

## gaat het regenen?
## lekker weertje he?
    [afbeelding]
 
 
## verhuizen
    Verhuis je binnen Nederland? Dan kun je dit online doorgeven. Verhuis je vanuit het buitenland? Neem dan contact op met de gemeente waar je gaat wonen.
    /Ik wil mijn verhuizing doorgeven
    /Wanneer moet ik mijn verhuizing doorgeven?
    /Meer informatie
## /Wanneer moet ik mijn verhuizing doorgeven?
    Volgende week donderdag


## kan ik de geboorte van mijn zoon aangeven?
    Ik weet nog niet zo veel over geboorteaangifte.
    In welke gemeente woon je?
## Utrecht
    Misschien staat je antwoord op deze [webpagina](https://pki.utrecht.nl/Loket/prodcat/products/getProductDetailsAction.do?name=Geboorteaangifte).


## ik ga verhuizen naar Utrecht
    Bedoel je ... 
    /product_verhuismelding_apply{"product": "verhuismelding"}
    /Iets anders
## /product_verhuismelding_apply{"product": "verhuismelding"}
    Binnenkort kan ik je helpen met de verwerking van je verhuizing!
    [afbeelding]
    

## ik heb last van ongedierte
    Ik weet nog niet zo veel over melding-openbare-ruimte.
    In welke gemeente woon je?
## Dongen
    Misschien staat je antwoord op deze [webpagina](https://www.dongen.nl/meldpunt.html).
    
    
## wat kost een paspoort?
    Ik weet nog niet zo veel over paspoort.
    In welke gemeente woon je?
## Buren
    Misschien staat je antwoord op deze (webpagina)[Misschien staat je antwoord op deze webpagina.].




## Hoe vaak wordt de afvalcontainer opgehaald
    Ik weet nog niet zo veel over afvalkalender.
    In welke gemeente woon je?
##? Dongen
    Helaas kan ik geen informatie vinden over dit product. Wil je misschien met een medewerker chatten?
    /Chat met een medewerker
    /Chat verder met de chatbot
## Chat met een medewerker
    Een moment, ik schakel je door naar een medewerker...


# NIET GEBRUIKEN

## ik wil een biertje
## doe mij een vaasje
    Ohhjee, dit gaat heel lang duren.
    Wat een humor pantalon.
    
## wie is de directeur?
    Dat is Wishal Gokoel
    [afbeelding]
## dankje
    Graag gedaan!


-------

# Voorbeelden 

Onderstaande lijst bevat vragen en de verwachte antwoorden van de Chatbot, bij het invullen gemeente: Gemeente Utrecht

## Wanneer kan ik mijn verhuizing doorgeven?
    De aanvraag voor een paspoort heeft een doorlooptijd van vijf werkdagen

## Waar vraag ik mijn verhuizing aan? (WHERE)
    Je kan je verhuizing regelen bij je gemeente, daarvoor moet je een afspraak maken.
    Je kan je verhuizing online regelen via de website van je gemeente.

## Hoe lang is mijn paspoort geldig?
    Voor personen vanaf 18 jaar is het paspoort of de ...

## Wat kost een paspoort?  (geeft ook een link naar SC indien van toepassing)
    In Tilburg kost een paspoort €71,35 euro

## Hoe kan ik mijn hond aanmelden?  ('mijn hond' is een synoniem voor 'hondenbelasting') (HOW)
    Start je aanvraag op ... 
    
## Hoe kan ik mijn hond aanmelden in Breda? (HOW, SHOP)
    Start je aanvraag op ... 
    
## Hoe kan ik mijn hond aanmelden in Eindhoven? (HOW, !SC)
    Sorry, geen informatie gevonden.

## Hoe kan ik mijn hond aanmelden in Oosterhout? (HOW, !SHOP)
    Je kan dit bij jou gemeente niet online doen.

## Is mijn verhuizing al verwerkt?
    Je krijgt een seintje via MijnOverheid wanneer je verhuizing is verwerkt...
    
## Waar vraag ik een paspoort aan?
    Je kan je paspoort regelen bij je gemeente, daarvoor moet je een afspraak maken.
    
## iets met een vraag
Chatbot vraag om verduidelijking aan de gebruiker
    
### KEUZE: optie 1 of 2
Story gaat door...


### KEUZE: optie "Iets anders" 
    ... [moet nog]
    
## ga weg gem
Chatbot escaleerd naar de live chat van de betreffende gemeente
    
    Hoi Jan hier, waar kan ik je mee helpen?
    
    