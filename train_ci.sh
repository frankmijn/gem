#!/bin/bash

# This is a modified version of train_sandbox.sh for our Gitlab pipeline
# to ensure the model output filename remains consistent

# clear model directory
FIRST_ARGUMENT="$1"
if [ "$FIRST_ARGUMENT" = "clean" ] ; then
    echo "Cleaning Model directory"
    rm models/*
else
    echo "To clean the model directory:"
    echo "$0 clean"
fi

# Load up .env
set -o allexport; source .env; set +o allexport 
# Forced retraining of Rasa NLU + Core

mkdir $(pwd)/models
chmod -R 777 $(pwd)/models
docker run \
  -v $(pwd)/models:/builds/virtuele-gemeente-assistent/gem/rasa/models \
  -v $(pwd)/rasa:/builds/virtuele-gemeente-assistent/gem/rasa \
  rasa_ci \
  train \
    -vv \
    --data data \
    --force \
    --fixed-model-name gitlab-model

exit $?