(function () {
  'use strict';
  var headerElements = document.getElementsByTagName("h1"); 
  var headerElement = headerElements.item(0);
  var spanElements = headerElement.getElementsByTagName("span");
  var spanElement = spanElements.item(0);
  spanElement.addEventListener('click', function(){if (window.localStorage) {window.localStorage.clear();} if (window.sessionStorage) {window.sessionStorage.clear();}window.location = window.location;
}, false);
  
  // Hide/show correct 
  var bodyElements = document.getElementsByTagName("body");
  var bodyElement = bodyElements.item(0);
  var url = window.location.hostname;
  bodyElement.className = (url.indexOf("local") >= 0) ? "local" : "remote";
})();