import requests
import json
import os
import logging
import random
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import ActionExecutor, CollectingDispatcher
from rasa_sdk.events import UserUtteranceReverted, SlotSet

logger = logging.getLogger(__name__)

class ActionGreet(Action):
  def name(self):
    return "action_greet"

  def run(self, dispatcher, tracker, domain):
    events = [UserUtteranceReverted()]
    logger.info("sender_id: "+tracker.sender_id)

    name = next(tracker.get_latest_entity_values("name"), None) 
    
    if name == None :
      dispatcher.utter_message(template="utter_greet")
      dispatcher.utter_message(template="utter_greet2")
    else:
      dispatcher.utter_message(template="utter_greet_personal",name=name)
    
    dispatcher.utter_message(template="utter_howcanihelp")
   
    return events
