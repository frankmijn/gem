import requests
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, FollowupAction, UserUtteranceReverted, ActionReverted, ConversationPaused
from rasa_sdk import Tracker
import logging
from .allmanak_api import allmanak_api_url
from .utils import getsuggestionbuttonforintent, transform_entities, transform_suggestions, process_event_text


logger = logging.getLogger(__name__)

class ActionEscalatie(Action):

    availability_uri = "https://cloudstatic.obi4wan.com/api/v1.0/chat/availability/{0}"

    def name(self):
        return "action_escalatie"

    def run(self, dispatcher, tracker, domain):
        livechat = False

        user_event_metadata = tracker.get_last_event_for("user").get("metadata", {})

        municipality_match = tracker.get_slot("municipality") == user_event_metadata.get("municipality")
        livechat_type_match = user_event_metadata.get("livechat") == "obi4wan"

        livechat_available = False
        guid = user_event_metadata.get("guid")
        if guid:
            try:
                availability_resp = requests.get(
                    self.availability_uri.format(guid), headers={"accept": "application/json"}
                ).json()
            except requests.exceptions.RequestException as e:
                dispatcher.utter_message(template="utter_no_human_available")
                logger.error(f"Error occurred while requesting availability API: {e}")
            else:
                availability = availability_resp['available']
                if availability:
                    livechat_available = True
                    logger.info("OBI4wan: Agent available") # DEBUG
                else:
                    logger.info("OBI4wan: Agent NOT available") # DEBUG
        
        if municipality_match and livechat_type_match and livechat_available:
            livechat = True

        if livechat:
            entities = transform_entities(tracker)
            suggestions = transform_suggestions(domain)

            trackerlog = tracker.current_state()
            chat_history = []
            previous_id = None
            for event in trackerlog["events"]:
                if 'text' not in event:
                    continue
                
                if event['event'] == 'user':
                    if not event.get("message_id"):
                        continue

                    if event['text'].startswith("/greet"):
                        continue

                    # Workaround for duplicate user events after clicking button
                    # from ask_affirmation action, see: https://forum.rasa.com/t/user-messages-duplication/23746/29
                    if event.get("message_id") == previous_id:
                        logger.info(f"Duplicate user event encountered, skipping {event}")
                        continue
                    previous_id = event.get("message_id")

                    processed_text = process_event_text(event, entities, suggestions)

                    chat_history.append({"event": event["event"], "text": processed_text})
                elif event['event'] == 'bot':
                    processed_text = process_event_text(event, entities, suggestions)

                    chat_history.append({"event": event["event"], "text": processed_text})

            dispatcher.utter_message(template="utter_livechatbeschikbaar")
            logger.info(str(tracker.latest_message))

            # Send the municipality to determine the avatar used for livechat
            dispatcher.utter_message(
                json_message={'history': chat_history, 'municipality': tracker.get_slot("municipality")}
            )
            return[ConversationPaused()]
        else:
            municipality_status = tracker.get_slot("feature_content") #checkt of de betreffende gemeente is aangesloten
            gemeente_telefoonnummer = tracker.get_slot("gemeente_telefoonnummer")
            municipality = tracker.get_slot("municipality")

            if municipality == None:
                user_event_metadata = tracker.get_last_event_for("user").get("metadata", {})
                municipality = user_event_metadata.get("municipality")

            if municipality_status == False:
                allmanakinfo = allmanak_api_url(municipality)
                logger.info(allmanakinfo)

                #hieronder wordt er eerst geprobeerd om een email-adres op te halen uit de Allmanak API. Indien die niet beschikbaar is (except) dan wordt er een utter verstuurd met uitsluitend een telefoonnummer (die is altijd beschikbaar).
                try: 
                    gemeente_emailadres = allmanakinfo[0]["contact"]["emailadres"]["value"]
                    dispatcher.utter_template('utter_gemeente_telefoonnummer_email', tracker, municipality="{0}".format(municipality), gemeente_emailadres="{0}".format(gemeente_emailadres), gemeente_telefoonnummer = "{0}".format(gemeente_telefoonnummer))
                    return []

                except:
                    dispatcher.utter_template('utter_gemeente_telefoonnummer', tracker, municipality="{0}".format(municipality), gemeente_telefoonnummer = "{0}".format(gemeente_telefoonnummer))
                    return []


            if municipality_status == True:

                allmanakinfo = allmanak_api_url(municipality)
                logger.info(allmanakinfo)
                dispatcher.utter_template('utter_livechatnietbeschikbaar', tracker)

                # Aangesloten gemeenten kunnen de utter_gemeente_contact naar wens aanpassen.
                gemeente_emailadres = allmanakinfo[0]["contact"]["emailadres"]["value"]
                dispatcher.utter_template('utter_gemeente_contact', tracker, municipality="{0}".format(municipality), gemeente_emailadres="{0}".format(gemeente_emailadres), gemeente_telefoonnummer = "{0}".format(gemeente_telefoonnummer))
                return []





