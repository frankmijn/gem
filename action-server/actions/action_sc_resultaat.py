import requests
import logging
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import ParseError
from rasa_sdk import Action

from .utils import ignore_dh_request

class action_samenwerkendecatalogi_resultaat(Action):
  def name(self):
    return "action_samenwerkendecatalogi_resultaat"

  def run(self, dispatcher, tracker, domain):
    # TODO: add checks for input slots => change CustomAction to a ActionForm

    municipality = tracker.get_slot("municipality")
    gemeente_website = tracker.get_slot("gemeente_website")
    product = tracker.get_slot("product")
    if municipality == None:
      municipality = tracker.get_slot("municipality")
    if municipality == None:
      user_event_metadata = tracker.get_last_event_for("user").get("metadata", {})
      municipality = user_event_metadata.get("municipality")

    logging.info("Slot: %s", municipality)
    logging.info("Slot: %s", product)

    product_xml = [product]

    if product == "paspoort":             
      product_xml = ["paspoort", "identiteitsbewijs"]

    elif product == "trouwen":             
      product_xml = ["trouwen", "trouwlocatie"]

    elif product == "parkeervergunning":             
      product_xml = ["parkeervergunning", "parkeren", "verkeer"]

    elif product == "geboorteaangifte":
      product_xml = ["geboorteaangifte", "geboorte aangifte", "geboorte aangeven", "geboorte"]       

    elif product == "grofvuil":
      product_xml = ["afvalinzameling grofvuil", "grofvuil", "afvalinzameling", "afval"]                         

    elif product == "afvalkalender":
      product_xml = ["afvalkalender", "afval ophalen", "afvalinzameling", "afval"] 

    elif product == "afvalcontainer":
      product_xml = ["afvalcontainer", "afvalinzameling", "afval"]                              

    elif product == "verhuismelding":             # Product heet anders in SC
      product_xml = ["verhuizing", "verhuizen"]        

    elif product == "melding-openbare-ruimte":  # Product heet anders in SC
      product_xml = ["melding openbare ruimte"]     
      
    elif product == "id-kaart":                 # Product heet anders in SC
      product_xml = ["identiteitskaart", "id-kaart", "identiteitsbewijs"] 
      
    elif product == "brp-uittreksel":           # Product heet anders in SC
      product_xml = ["uittreksel"]

    elif product == "uittreksel-verhuismelding":# Product heet anders in SC
      product_xml = ["uittreksel"]
    
    elif product == "belasting-kwijtschelding": # Product heet anders in SC
      product_xml = ["kwijtschelding"]

    #if municipality != None and product != None:
    # Option: replace keyword with uniformeProductnaam to switch to us
    for x in product_xml:
          response = ignore_dh_request('https://zoekdienst.overheid.nl/SRUServices/SRUServices.asmx/Search',
                params={'version':'1.2', 
                    'operation':'searchRetrieve',
                    'x-connection':'sc',
                    'recordSchema':'sc4.0',
                    'startRecord':'1',
                    'maximumRecords':'10',
                    'query':'((organisatie="{0}") and (organisatietype="''Gemeente''")) and (keyword="{1}")'.format(municipality,x)}
          ) 
          # DEBUG 
          logging.info('SC.XML response request url: %s',response.request.url)
          logging.info('SC.XML response: %s',response.text)
          # DEBUG END
          try:
          # In case of double encoded responses
            root = ET.fromstring(response.text.encode('raw_unicode_escape'))
          except ParseError as e:
          # Regular encoding
            logging.warning(f"Exception occurred while parsing SC response, assuming regular encoding: {e}")
            root = ET.fromstring(response.content)
          ns = {'test': 'http://www.loc.gov/zing/srw/',
                'dcterms': 'http://purl.org/dc/terms/',
                'overheid': 'http://standaarden.overheid.nl/owms/terms/',
                'overheidproduct': 'http://standaarden.overheid.nl/product/terms/',
                'sru': 'http://standaarden.overheid.nl/sru',
                'xsi': 'http://www.w3.org/2001/XMLSchema-instance'}
          
          recordcount = root.find("test:numberOfRecords", ns)

          if int(recordcount.text) > 0:
            logging.info('WEL resultaten in SC gevonden voor zoekwoord: %s', x)
            logging.info('in combinatie met gemeente: %s', municipality)
            break
          else:
            logging.info('GEEN resultaten in SC gevonden voor zoekwoord: %s', x)
            logging.info('in combinatie met gemeente: %s', municipality)

    sc_template = "utter_samenwerkendecatalogi_resultaat"
    

    urls = []
    if int(recordcount.text) > 0:
      for record in root.findall("test:records/test:record", ns):
        identifier = record.find(".//overheidproduct:scproduct//dcterms:identifier", ns)
        title = record.find(".//overheidproduct:scproduct//dcterms:title", ns)
        if identifier is not None and title is not None:
          urls.append((title.text, identifier.text))

    # Format as bulleted list in markdown
    formatted_urls = '\n'.join(f"- [{url[0]}]({url[1]})" for url in urls)
    
    if int(recordcount.text) <= 0:
      website = gemeente_website
      dispatcher.utter_template("utter_samenwerkendecatalogi_website", tracker, url=website, product=product, municipality=municipality)
      return []


        #if int(recordcount.text) > 1:
          #url_sc = "https://www.overheid.nl/zoekresultaat/diensten-per-organisatie/1/10/_overheid.authority={0}".format(municipality)
          #dispatcher.utter_template("utter_sc_more_found", tracker, sc_url="{0}".format(url_sc)
          #dispatcher.utter_template("utter_sc_more_found", tracker, items="{0}".format(int(recordcount.text)-1))
          

    if product is not None:
        product_template = sc_template + '_' + product
        responses = domain.get('responses',domain.get('templates'))
        if product_template in responses.keys():
            sc_template = product_template

    dispatcher.utter_template(sc_template, tracker, results=formatted_urls, product=product, municipality=municipality)

      
    return []