from typing import Dict, Text, Any, List, Union, Optional
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, FollowupAction, UserUtteranceReverted, ActionReverted
from rasa_sdk.interfaces import ActionExecutionRejection
from rasa_sdk import Tracker
import logging
import numpy
from .allmanak_api import allmanak_api_url
logger = logging.getLogger(__name__)



class ActionCheckMunicipality(Action):

    def name(self):
        return "action_check_municipality"

    def levenshteinDistanceDP(self, token1, token2):
        distances = numpy.zeros((len(token1) + 1, len(token2) + 1))

        for t1 in range(len(token1) + 1):
            distances[t1][0] = t1

        for t2 in range(len(token2) + 1):
            distances[0][t2] = t2
            
        a = 0
        b = 0
        c = 0
        
        for t1 in range(1, len(token1) + 1):
            for t2 in range(1, len(token2) + 1):
                if (token1[t1-1] == token2[t2-1]):
                    distances[t1][t2] = distances[t1 - 1][t2 - 1]
                else:
                    a = distances[t1][t2 - 1]
                    b = distances[t1 - 1][t2]
                    c = distances[t1 - 1][t2 - 1]
                    
                    if (a <= b and a <= c):
                        distances[t1][t2] = a + 1
                    elif (b <= a and b <= c):
                        distances[t1][t2] = b + 1
                    else:
                        distances[t1][t2] = c + 1

        # printDistances(distances, len(token1), len(token2))
        return distances[len(token1)][len(token2)]

    def run(self, dispatcher, tracker, domain):
        intent = tracker.latest_message.get('intent')['name']
        logger.info(intent)
        
        if intent == "inform_municipality":
            logger.info("inform_municipality")
            if tracker.active_form.get("name") is not None:
                raise ActionExecutionRejection(self.name(),f"currently in form, skip action")
            municipality = next(tracker.get_latest_entity_values("municipality"), None)
            events = []
            last_action = tracker.get_last_event_for("action", exclude=["action_listen"])
            logger.info(str(last_action))
            logger.info(self.name())
            logger.info(str(tracker.latest_message))


            if (last_action.get('name') == self.name()):
                entity = next((entity for entity in tracker.latest_message.get("entities", []) if entity["entity"] == "municipality"),None)
                logger.info(str(entity))
                if entity:
                    processors = entity.get('processors',[])
                    if tracker.latest_message['text'].startswith('/'):
                        return [UserUtteranceReverted(),ActionReverted(),SlotSet("municipality",municipality)]
                    elif 'EntitySynonymMapper' not in processors:
                        # was not processed by EntitySynonymMapper (= no known direct municipality mapping)
                        suggestions = self.get_suggestions().get(municipality.lower())
                        # maybe the municipality is one of the doubles
                        if suggestions:
                            buttons = []
                            for item in suggestions: 
                                suggestion = item.get("gemeente")
                                payload = "/inform_municipality{{\"municipality\":\"{0}\"}}".format(suggestion)
                                buttons.append({"title": municipality +", Gemeente "+suggestion, "payload": payload})
                            dispatcher.utter_button_template("utter_ask_affirmation", buttons, tracker)
                            return [UserUtteranceReverted()]
                        else:
                            # municpality does not exsist
                            municipality = None
                    else:
                        # if levenshteinDistanceDP > 3 probably a woonplaats TODO
                        orig_val = tracker.latest_message.get("text")[entity["start"] : entity["end"]]
                        entity_val = municipality.lower()
                        distance = self.levenshteinDistanceDP(orig_val, entity_val)
                        logger.info("{0} vs {1} = {2}".format(orig_val,entity_val,distance))
                        #if distance > 3:
                            # dispatcher.utter_message(template="utter_info_woonplaatsinmunicipality",woonplaats="{0}".format(orig_val),municipality="{0}".format(municipality))
                else:
                    dispatcher.utter_message(template="utter_warning_municipalitynietherkend")
                return [UserUtteranceReverted(),ActionReverted(),SlotSet("municipality",municipality)]
            else: 
                # last_intent = tracker.get_last_event_for("user",exclude=["inform_municipality"])
                # logger.debug(str(tracker.applied_events()))
                raise ActionExecutionRejection(self.name(),f"inform_municipality came at an unexpected moment, fallback to other policies")


        else:
            # logger.debug(str(tracker.applied_events()))
            lastevent = tracker.events[-1]
            municipality = None
            if lastevent["event"] == "slot" and lastevent["name"] == "municipality":
                municipality = lastevent["value"]
            else:
                municipality = next(tracker.get_latest_entity_values("municipality"), None)
                entity = next((entity for entity in tracker.latest_message.get("entities", []) if entity["entity"] == "municipality"),None)
                logger.info(str(entity))
                if entity:
                    processors = entity.get('processors',[])
                    if tracker.latest_message['text'].startswith('/'):
                        pass
                    elif 'EntitySynonymMapper' not in processors:
                        # maybe the municipality is one of the doubles
                        suggestions = self.get_suggestions().get(municipality.lower())
                        if suggestions:
                            buttons = []
                            for item in suggestions: 
                                suggestion = item.get("gemeente")
                                payload = "/inform_municipality{{\"municipality\":\"{0}\"}}".format(suggestion)
                                buttons.append({"title": municipality +", Gemeente "+suggestion, "payload": payload})
                            dispatcher.utter_button_template("utter_ask_affirmation", buttons, tracker)
                            return [FollowupAction('action_listen')]
                        else:
                            # municpality  does not exsist
                            municipality = None
                    else:
                        # if levenshteinDistanceDP > 3 probably a woonplaats TODO
                        orig_val = tracker.latest_message.get("text")[entity["start"] : entity["end"]]
                        entity_val = municipality.lower()
                        distance = self.levenshteinDistanceDP(orig_val, entity_val)
                        logger.info("{0} vs {1} = {2}".format(orig_val,entity_val,distance))
                        # if distance > 3:
                            # dispatcher.utter_message(template="utter_info_woonplaatsinmunicipality",woonplaats="{0}".format(orig_val),municipality="{0}".format(municipality))

            if municipality == None:
                municipality = tracker.get_slot("municipality")
            if municipality == None or not self.validate(municipality):
                user_event_metadata = tracker.get_last_event_for("user").get("metadata", {})

                municipality = user_event_metadata.get("municipality")
             
            if municipality == None:
                product = tracker.get_slot("product")
                ask_template = "utter_ask_municipality"
                if product is not None:
                    product_template = ask_template + '_' + product
                    responses = domain.get('responses',domain.get('templates'))
                    if product_template in responses.keys():
                        ask_template = product_template
                       
                dispatcher.utter_message(template=ask_template)
                return [FollowupAction('action_listen')]
                
            else:
                feature_content = SlotSet("feature_content",False)
                if municipality in ["Dongen"]:
                    feature_content = SlotSet("feature_content",True)

            #gets municipality info from overheid_allmanak
            allmanakinfo = allmanak_api_url(municipality)
            returnlist = [feature_content, SlotSet("municipality",municipality)]



            try:
                website = allmanakinfo[0]["contact"]["internet"]["value"]
                returnlist.append(SlotSet("gemeente_website",website))
            except:
                website = None


            try:
                telefoonnummer = allmanakinfo[0]["contact"]["telefoon"]["value"]

                # Voor Dongen staat momenteel niet het juiste tel. nr. in de Allmanak. Vandaar hier even hardcoded erin. 
                if municipality == 'Dongen':
                    telefoonnummer = '14 0162'

                returnlist.append(SlotSet("gemeente_telefoonnummer",telefoonnummer))
            except:
                telefoonnummer = None

            try:
                adres = list(allmanakinfo[0]["contact"]["bezoekAdres"].values())
                adres = adres[1]+" "+adres[3]+" "+adres[2]+" "+adres[0]
                returnlist.append(SlotSet("gemeente_adres",adres))
            except:
                adres = None

            try:
                postadres = list(allmanakinfo[0]["contact"]["postAdres"].values())
                postadres = postadres[1]+" "+postadres[2]+" "+postadres[0]
                returnlist.append(SlotSet("gemeente_postadres",postadres))

            except:
                postadres = None

            try:
                emailadres = allmanakinfo[0]["contact"]["emailadres"]["value"]
                returnlist.append(SlotSet("gemeente_emailadres",emailadres))

            except:
                emailadres = None
                
            return returnlist
        
        return []

    def validate(self,value):
        municipalities = ["Aa en Hunze", "Aalsmeer", "Aalten,", "Achtkarspelen", "Alblasserdam", "Albrandswaard", "Alkmaar", "Almelo", "Almere", "Alphen aan den Rijn", "Alphen-Chaam", "Altena", "Ameland", "Amersfoort",
            "Amstelveen", "Amsterdam", "Apeldoorn", "Appingedam", "Arnhem", "Assen", "Asten", "Baarle-Nassau", "Baarn", "Barendrecht", "Barneveld", "Beek", "Beekdaelen", "Beemster", "Beesel", "Berg en Dal",
            "Bergeijk", "Bergen (L.)", "Bergen (NH.)", "Bergen op Zoom", "Berkelland", "Bernheze", "Best", "Beuningen", "Beverwijk", "Bladel", "Blaricum", "Bloemendaal", "Bodegraven-Reeuwijk", "Boekel",
            "Borger-Odoorn", "Borne", "Borsele", "Boxmeer", "Boxtel", "Breda", "Brielle", "Bronckhorst", "Brummen", "Brunssum", "Bunnik", "Bunschoten", "Buren", "Capelle aan den IJssel", "Castricum", "Coevorden",
            "Cranendonck", "Cuijk", "Culemborg", "Dalfsen", "Dantumadiel", "De Bilt", "De Fryske Marren", "De Ronde Venen", "De Wolden", "Delft", "Delfzijl", "Den Helder", "Deurne", "Deventer", "Diemen", "Dinkelland",
            "Doesburg", "Doetinchem", "Dongen", "Dordrecht", "Drechterland", "Drimmelen", "Dronten", "Druten", "Duiven", "Echt-Susteren", "Edam-Volendam", "Ede", "Eemnes", "Eersel", "Eijsden-Margraten", "Eindhoven",
            "Elburg", "Emmen", "Enkhuizen", "Enschede", "Epe", "Ermelo", "Etten-Leur", "Geertruidenberg", "Geldrop-Mierlo", "Gemert-Bakel", "Gennep", "Gilze en Rijen", "Goeree-Overflakkee", "Goes", "Goirle",
            "Gooise Meren", "Gorinchem", "Gouda", "Grave", "Groningen", "Gulpen-Wittem", "Haaksbergen", "Haaren", "Haarlem", "Haarlemmermeer", "Halderberge", "Hardenberg", "Harderwijk", "Hardinxveld-Giessendam",
            "Harlingen", "Hattem", "Heemskerk", "Heemstede", "Heerde", "Heerenveen", "Heerhugowaard", "Heerlen", "Heeze-Leende", "Heiloo", "Hellendoorn", "Hellevoetsluis", "Helmond", "Hendrik-Ido-Ambacht", "Hengelo",
            "Het Hogeland", "Heumen", "Heusden", "Hillegom", "Hilvarenbeek", "Hilversum", "Hoeksche Waard", "Hof van Twente", "Hollands Kroon", "Hoogeveen", "Hoorn", "Horst aan de Maas", "Houten", "Huizen", "Hulst",
            "IJsselstein", "Kaag en Braassem", "Kampen", "Kapelle", "Katwijk", "Kerkrade", "Koggenland", "Krimpen aan den IJssel", "Krimpenerwaard", "Laarbeek", "Landerd", "Landgraaf", "Landsmeer", "Langedijk",
            "Lansingerland", "Laren", "Leeuwarden", "Leiden", "Leiderdorp", "Leidschendam-Voorburg", "Lelystad", "Leudal", "Leusden", "Lingewaard", "Lisse", "Lochem", "Loon op Zand", "Lopik", "Loppersum", "Losser",
            "Maasdriel", "Maasgouw", "Maassluis", "Maastricht", "Medemblik", "Meerssen", "Meierijstad", "Meppel", "Middelburg", "Midden-Delfland", "Midden-Drenthe", "Midden-Groningen", "Mill en Sint Hubert",
            "Moerdijk", "Molenlanden", "Montferland", "Montfoort", "Mook en Middelaar", "Neder-Betuwe", "Nederweert", "Nieuwegein", "Nieuwkoop", "Nijkerk", "Nijmegen", "Nissewaard", "Noardeast-Fryslân",
            "Noord-Beveland", "Noordenveld", "Noordoostpolder", "Noordwijk", "Nuenen", "Gerwen en Nederwetten", "Nunspeet", "Oegstgeest", "Oirschot", "Oisterwijk", "Oldambt", "Oldebroek", "Oldenzaal", "Olst-Wijhe",
            "Ommen", "Oost Gelre", "Oosterhout", "Ooststellingwerf", "Oostzaan", "Opmeer", "Opsterland", "Oss", "Oude IJsselstreek", "Ouder-Amstel", "Oudewater", "Overbetuwe", "Papendrecht", "Peel en Maas", "Pekela",
            "Pijnacker-Nootdorp", "Purmerend", "Putten", "Raalte", "Reimerswaal", "Renkum", "Renswoude", "Reusel-De Mierden", "Rheden", "Rhenen", "Ridderkerk", "Rijssen-Holten", "Rijswijk", "Roerdalen", "Roermond",
            "Roosendaal", "Rotterdam", "Rozendaal", "Rucphen", "Schagen", "Scherpenzeel", "Schiedam", "Schiermonnikoog", "Schouwen-Duiveland", "'s-Gravenhage", "'s-Hertogenbosch", "Simpelveld", "Sint Anthonis",
            "Sint-Michielsgestel", "Sittard-Geleen", "Sliedrecht", "Sluis", "Smallingerland", "Soest", "Someren", "Son en Breugel", "Stadskanaal", "Staphorst", "Stede Broec", "Steenbergen", "Steenwijkerland",
            "Stein", "Stichtse Vecht", "Súdwest-Fryslân", "Terneuzen", "Terschelling", "Texel", "Teylingen", "Tholen", "Tiel", "Tilburg", "Tubbergen", "Twenterand", "Tynaarlo", "Tytsjerksteradiel", "Uden", "Uitgeest",
            "Uithoorn", "Urk", "Utrecht", "Utrechtse Heuvelrug", "Vaals", "Valkenburg aan de Geul", "Valkenswaard", "Veendam", "Veenendaal", "Veere", "Veldhoven", "Velsen", "Venlo", "Venray", "Vijfheerenlanden",
            "Vlaardingen", "Vlieland", "Vlissingen", "Voerendaal", "Voorschoten", "Voorst", "Vught", "Waadhoeke", "Waalre", "Waalwijk", "Waddinxveen", "Wageningen", "Wassenaar", "Waterland", "Weert", "Weesp",
            "West Betuwe", "West Maas en Waal", "Westerkwartier", "Westerveld", "Westervoort", "Westerwolde", "Westland", "Weststellingwerf", "Westvoorne", "Wierden", "Wijchen", "Wijdemeren", "Wijk bij Duurstede",
            "Winterswijk", "Woensdrecht", "Woerden", "Wormerland", "Woudenberg", "Zaanstad", "Zaltbommel", "Zandvoort", "Zeewolde", "Zeist", "Zevenaar", "Zoetermeer", "Zoeterwoude", "Zuidplas", "Zundert", "Zutphen",
            "Zwartewaterland", "Zwijndrecht", "Zwolle"]
        return value in municipalities

    def get_suggestions(self) -> Dict:
        data = {
            "achterveld": [{"gemeente": "Barneveld"},{"gemeente": "Leusden"}],
            "afferden": [{"gemeente": "Druten"},{"gemeente": "Bergen (L.)"}],
            "alphen": [{"gemeente": "Alphen-Chaam"},{"gemeente": "West Maas en Waal"}],
            "alteveer": [{"gemeente": "De Wolden"},{"gemeente": "Noordenveld"},{"gemeente": "Stadskanaal"}],
            "ansen": [{"gemeente": "De Wolden"},{"gemeente": "Westerveld"}],
            "baarlo": [{"gemeente": "Peel en Maas"},{"gemeente": "Steenwijkerland"}],
            "bavel": [{"gemeente": "Alphen-Chaam"},{"gemeente": "Breda"}],
            "beek": [{"gemeente": "Beek"},{"gemeente": "Berg en Dal"},{"gemeente": "Montferland"}],
            "bergen": [{"gemeente": "Bergen (NH.)"},{"gemeente": "Bergen (L.)"}],
            "beuningen": [{"gemeente": "Beuningen"},{"gemeente": "Losser"}],
            "biervliet": [{"gemeente": "Sluis"},{"gemeente": "Terneuzen"}],
            "borgercompagnie": [{"gemeente": "Midden-Groningen"},{"gemeente": "Veendam"}],
            "broekhuizen": [{"gemeente": "Horst aan de Maas"},{"gemeente": "Meppel"}],
            "buren": [{"gemeente": "Ameland"},{"gemeente": "Buren"}],
            "buurmalsen": [{"gemeente": "Buren"},{"gemeente": "West Betuwe"}],
            "den ham": [{"gemeente": "Twenterand"},{"gemeente": "Westerkwartier"}],
            "den hoorn": [{"gemeente": "Midden-Delfland"},{"gemeente": "Texel"}],
            "deurningen": [{"gemeente": "Dinkelland"},{"gemeente": "Oldenzaal"}],
            "echten": [{"gemeente": "De Fryske Marren"},{"gemeente": "De Wolden"}],
            "elsloo": [{"gemeente": "Ooststellingwerf"},{"gemeente": "Stein"}],
            "eursinge": [{"gemeente": "De Wolden"},{"gemeente": "Midden-Drenthe"}],
            "geesteren": [{"gemeente": "Berkelland"},{"gemeente": "Tubbergen"}],
            "haarle": [{"gemeente": "Hellendoorn"},{"gemeente": "Tubbergen"}],
            "haren": [{"gemeente": "Groningen"},{"gemeente": "Oss"}],
            "harkstede": [{"gemeente": "Groningen"},{"gemeente": "Midden-Groningen"}],
            "hengelo": [{"gemeente": "Bronckhorst"},{"gemeente": "Hengelo"}],
            "heusden": [{"gemeente": "Asten"},{"gemeente": "Heusden"}],
            "hoenderloo": [{"gemeente": "Apeldoorn"},{"gemeente": "Ede"}],
            "hoogersmilde": [{"gemeente": "Midden-Drenthe"},{"gemeente": "Westerveld"}],
            "hoorn": [{"gemeente": "Hoorn"},{"gemeente": "Terschelling"}],
            "huis ter heide": [{"gemeente": "Noordenveld"},{"gemeente": "Zeist"}],
            "katwijk": [{"gemeente": "Cuijk"},{"gemeente": "Katwijk"}],
            "klarenbeek": [{"gemeente": "Apeldoorn"},{"gemeente": "Voorst"}],
            "kloetinge": [{"gemeente": "Goes"},{"gemeente": "Kapelle"}],
            "kloosterhaar": [{"gemeente": "Hardenberg"},{"gemeente": "Twenterand"}],
            "koedijk": [{"gemeente": "Alkmaar"},{"gemeente": "Langedijk"}],
            "lageland": [{"gemeente": "Groningen"},{"gemeente": "Midden-Groningen"}],
            "laren": [{"gemeente": "Laren"},{"gemeente": "Lochem"}],
            "loo gld": [{"gemeente": "Duiven"},{"gemeente": "Lingewaard"}],
            "mastenbroek": [{"gemeente": "Kampen"},{"gemeente": "Zwartewaterland"}],
            "nes": [{"gemeente": "Ameland"},{"gemeente": "Heerenveen"},{"gemeente": "Noardeast-Fryslân"}],
            "niekerk": [{"gemeente": "Het Hogeland"},{"gemeente": "Westerkwartier"}],
            "nieuweroord": [{"gemeente": "Hoogeveen"},{"gemeente": "Midden-Drenthe"}],
            "noordwijk": [{"gemeente": "Noordwijk"},{"gemeente": "Westerkwartier"}],
            "noordwolde": [{"gemeente": "Het Hogeland"},{"gemeente": "Weststellingwerf"}],
            "oosterend": [{"gemeente": "Terschelling"},{"gemeente": "Texel"}],
            "oosterhout": [{"gemeente": "Oosterhout"},{"gemeente": "Overbetuwe"}],
            "oostrum": [{"gemeente": "Noardeast-Fryslân"},{"gemeente": "Venray"}],
            "oostwold": [{"gemeente": "Oldambt"},{"gemeente": "Westerkwartier"}],
            "oudega": [{"gemeente": "De Fryske Marren"},{"gemeente": "Smallingerland"},{"gemeente": "Súdwest-Fryslân"}],
            "oudemolen": [{"gemeente": "Moerdijk"},{"gemeente": "Tynaarlo"}],
            "oudkarspel": [{"gemeente": "Langedijk"},{"gemeente": "Schagen"}],
            "pesse": [{"gemeente": "Hoogeveen"},{"gemeente": "Westerveld"}],
            "purmer": [{"gemeente": "Edam-Volendam"},{"gemeente": "Waterland"}],
            "rijswijk": [{"gemeente": "Altena"},{"gemeente": "Buren"},{"gemeente": "Rijswijk"}],
            "rossum": [{"gemeente": "Dinkelland"},{"gemeente": "Maasdriel"}],
            "rottum": [{"gemeente": "De Fryske Marren"},{"gemeente": "Het Hogeland"}],
            "rozenburg": [{"gemeente": "Haarlemmermeer"},{"gemeente": "Rotterdam"}],
            "ruinen": [{"gemeente": "De Wolden"},{"gemeente": "Westerveld"}],
            "scherpenzeel": [{"gemeente": "Scherpenzeel"},{"gemeente": "Weststellingwerf"}],
            "serooskerke": [{"gemeente": "Schouwen-Duiveland"},{"gemeente": "Veere"}],
            "sibculo": [{"gemeente": "Hardenberg"},{"gemeente": "Twenterand"}],
            "spaarndam": [{"gemeente": "Haarlem"},{"gemeente": "Haarlemmermeer"}],
            "spier": [{"gemeente": "Midden-Drenthe"},{"gemeente": "Westerveld"}],
            "spijk": [{"gemeente": "Delfzijl"},{"gemeente": "West Betuwe"},{"gemeente": "Zevenaar"}],
            "spijkerboor": [{"gemeente": "Aa en Hunze"},{"gemeente": "Wormerland"}],
            "startenhuizen": [{"gemeente": "Het Hogeland"},{"gemeente": "Loppersum"}],
            "steenbergen": [{"gemeente": "Noordenveld"},{"gemeente": "Steenbergen"}],
            "stuifzand": [{"gemeente": "Hoogeveen"},{"gemeente": "Midden-Drenthe"}],
            "tiendeveen": [{"gemeente": "Hoogeveen"},{"gemeente": "Midden-Drenthe"}],
            "ulvenhout": [{"gemeente": "Alphen-Chaam"},{"gemeente": "Breda"}],
            "ursem": [{"gemeente": "Alkmaar"},{"gemeente": "Koggenland"}],
            "valkenburg": [{"gemeente": "Katwijk"},{"gemeente": "Valkenburg aan de Geul"}],
            "velp": [{"gemeente": "Grave"},{"gemeente": "Rheden"}],
            "vinkel": [{"gemeente": "Bernheze"},{"gemeente": "'s-Hertogenbosch"}],
            "vledderveen": [{"gemeente": "Stadskanaal"},{"gemeente": "Westerveld"}],
            "voorst": [{"gemeente": "Oude IJsselstreek"},{"gemeente": "Voorst"}],
            "well": [{"gemeente": "Maasdriel"},{"gemeente": "Bergen (L.)"}],
            "winsum": [{"gemeente": "Het Hogeland"},{"gemeente": "Waadhoeke"}],
            "wirdum": [{"gemeente": "Leeuwarden"},{"gemeente": "Loppersum"}],
            "zennewijnen": [{"gemeente": "Tiel"},{"gemeente": "West Betuwe"}],
            "zevenbergschen hoek": [{"gemeente": "Drimmelen"},{"gemeente": "Moerdijk"}],
            "zevenhuizen": [{"gemeente": "Westerkwartier"},{"gemeente": "Zuidplas"}],
            "zuidwolde": [{"gemeente": "De Wolden"},{"gemeente": "Het Hogeland"}]
        }
        return data
