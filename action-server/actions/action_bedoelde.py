from rasa_sdk import Action
from rasa_sdk.events import SlotSet, FollowupAction, UserUtteranceReverted, ActionReverted, UserUttered, ActionExecuted
from rasa_sdk import Tracker
import json
from rasa_sdk.interfaces import ActionExecutionRejection
from typing import Text, Dict, Any, List
import copy
import logging
logger = logging.getLogger(__name__)

class ActionBedoelde(Action):
    
    def name(self):
        return "action_bedoelde"

    def run(self, dispatcher, tracker, domain):

        entities = tracker.latest_message.get("entities", [])

        logger.info("start action_bedoelde")

        
        return_events = [UserUtteranceReverted(),UserUtteranceReverted()] #undo 'ikbedoelde' utter and the utter before that
        
        if len(entities) > 0:
            def filter_function(e: Dict[Text, Any]) -> bool:
                has_instance = e["event"] == "user"
                excluded = e["event"] == "user" and e["parse_data"]['intent']['name'] in ['oepsikbedoel','bedoel_iets_anders', 'inform_municipality', 'inform_product', 'inform_affirmative', 'inform_denial','inform_dontknow', 'inform_maybe','inform','inform_redelijk', 'schedule_helpmore']
                return has_instance and not excluded

            filtered = filter(filter_function, reversed(tracker.applied_events()))
            previousutterevent = next(filtered, None)

            previousutterevent = copy.deepcopy(previousutterevent)
            logger.info(str(previousutterevent))
            parsedata = previousutterevent.get("parse_data")
            parsedata.pop("intent_ranking",[])
            parsedata.pop("text",None)
            previousintent = parsedata.get("intent")
            previousentities = parsedata.get("entities",[])
            for entity in entities:
                c_entity = next((item for item in previousentities if item["entity"] == entity["entity"]),None)
                if c_entity:
                    #TODO copy all properties from entity
                    c_entity["value"] = entity["value"]
                else:
                    #insert entity 
                    previousentities.append(entity)
            
            entities = {e["entity"]: e["value"] for e in previousentities}
            entities_json = json.dumps(entities)
            text = "/{}{}".format(previousintent, entities_json)
            parsedata["text"] = text
            logger.info(str(previousentities))
            logger.info(str(parsedata))
            
            # tempo hack TODO Retrieve intent that it is being replaced with out_of_scope? @ Harvey @ Jason @ Dylan
            try:
                product_slot = domain.get("slots")
                product_slot = product_slot["product"]["values"]
                previousentity = tracker.latest_message.get("entities", [])
                product_entity = [entity_dicts["value"] for entity_dicts in previousentity if entity_dicts["entity"] == "product"][0]

                if previousintent["name"] == "out_of_scope" and product_entity in product_slot:
                    previousutterevent["parse_data"]["intent"]["name"] = "inform_product"
                    for index,entity_dicts in enumerate(previousutterevent["parse_data"]["entities"]):
                        if entity_dicts["entity"] != "product":
                            del previousutterevent["parse_data"]["entities"][index]
            except:
                pass

            return_events.extend([ActionExecuted("action_listen"),previousutterevent])
        else: #no entities found, user typed: Ik bedoelde iets anders... => wat dan?
            dispatcher.utter_message(template="utter_bedoel_iets_anders") #special case check_municipality/check_product TODO?
            return_events.extend([FollowupAction('action_listen')])

            
        # [ActionExecuted("action_listen")] + [UserUttered("/" + "producten_algemeen", {
        #             "intent": {"name": "producten_algemeen", "confidence": 1.0},
        #             "entities": [{'entity': 'product', 'start': 0, 'end': 1, 'extractor': 'DIETClassifier', 'value': product, 'processors': ['EntitySynonymMapper']}]
        #             })]


        # entities = tracker.latest_message.get("entities", [])
        # entities = {e["entity"]: e["value"] for e in entities}
        # logger.info(entities)

        # if "municipality" in entities.keys():
        #     logger.info('oepsikbedoelmunicipality')
        #     municipality = entities["municipality"]
        #     logger.info(municipality)
        #     return [ActionReverted(), SlotSet("municipality",municipality)]

        # else:
        #     logger.info('oepsikbedoelgeenmunicipality')
        #     dispatcher.utter_message(template="inform_municipality")
        #     return [UserUtteranceReverted(), ActionReverted()]
        return return_events






