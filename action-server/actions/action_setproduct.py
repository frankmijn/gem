import requests
import os
import logging
import datetime
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import ActionExecutor, CollectingDispatcher
from rasa_sdk.events import ReminderScheduled, SlotSet, ActionReverted

logger = logging.getLogger(__name__)

class ActionSetProduct(Action):
  def name(self):
    return "action_setproduct"

  def run(self, dispatcher, tracker, domain):
      product = tracker.get_slot("product")
      return [ActionReverted(), SlotSet("product",product)]