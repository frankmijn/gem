import random
from rasa_sdk import Action, Tracker
from rasa_sdk.events import SlotSet
from rasa_sdk.executor import ActionExecutor, CollectingDispatcher

import logging
import requests
import json

logger = logging.getLogger(__name__)


class ActionWieIsDeBurgemeester(Action):
  def name(self):
    return "action_wieisdeburgemeester"

  def run(self, dispatcher, tracker, domain):
    municipality = tracker.get_slot("municipality")
    if municipality == "'s-Gravenhage":
        municipality = "Den Haag"    
    logger.info("Burgemeester Allmanak")
    url = "https://rest-api.allmanak.nl/v1/overheidsorganisatie?afkorting=eq.{municipality}&types=cs.{{Gemeente}}&einddatum=is.null&select=afkorting,naam,functies(functie:functieid(naam,medewerkers(persoon:persoonid(naam))))&functies.functie.naam=eq.Burgemeester".format(municipality=municipality)
    logger.info(url)
    burgemeester = None
    try:
      response = requests.get(url).json() #make an api call
      logger.info(str(response))
      burgemeester = response[0]['functies'][0]['functie']['medewerkers'][0]['persoon']['naam']
    except Exception as e:
      logger.warning(e)
      dispatcher.utter_message(template="utter_burgemeester_nietgevonden",gemeente="{0}".format(municipality))
      return []
    logger.info(burgemeester)
    dispatcher.utter_message(template="utter_burgemeester",gemeente="{0}".format(municipality),burgemeester="{0}".format(burgemeester))
    return []
