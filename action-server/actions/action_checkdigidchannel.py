import logging
from rasa_sdk import Action, Tracker
from rasa_sdk.events import SlotSet
from rasa_sdk.executor import ActionExecutor, CollectingDispatcher

class ActionCheckDigidChannel(Action):

    def name(self):
        # type: () -> Text
        return "action_isapplieable"

    def run(self, dispatcher, tracker, domain):
        # type: (CollectingDispatcher, Tracker, Dict[Text, Any]) -> List[Dict[Text, Any]]
        return [SlotSet("channel_is_digid_channel", tracker.get_latest_input_channel() in ["facebook_referrer"])]
