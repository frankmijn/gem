import datetime
import logging
import os

from zgw_api import ZGWAPI
from bag_api import BagAPI

logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))

bag_api = BagAPI()
bag_response = bag_api.get("1015CJ", "117")

api = ZGWAPI()


today = datetime.date.today()

api.get_zaaktype()
data = api.zaak_toevoegen("Mijn verhuizing", "test-verhuizing chatbot overheid", today, geo=bag_response["geo"])

if "url" in data:
    uuid = data["url"].split("/")[-1]
    zac = "https://ref.tst.vng.cloud/demo/apps/zaakbeheer/{}".format(uuid)
    print(zac)
    mijn_gemeente = "https://ref.tst.vng.cloud/demo/apps/mijngemeente/"
    print(mijn_gemeente)
    
