import logging
import os
import requests

def allmanak_api_url(municipality):
    if municipality == "'s-Gravenhage":
        municipality = "Den Haag"
    allmanak_api_url = 'https://rest-api.allmanak.nl/v1/overheidsorganisatie?naam=eq.Gemeente%20'
        
    url = "{}{}".format(allmanak_api_url,municipality)
    #logger.info("ALLMANAK API URL: {}".format(url))
    try:
        response = requests.get(url).json() #make an api call

    except Exception as e:
        logger.warning(e)
        return None
    #logger.info(response)
    return response