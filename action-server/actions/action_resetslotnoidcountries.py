from rasa_sdk import Action
from rasa_sdk.events import SlotSet
from rasa_sdk import Tracker
import logging
logger = logging.getLogger(__name__)

class resetslotnoidcountries(Action):

    def name(self):
        return "action_resetslotnoidcountries"

    def run(self, dispatcher, tracker, domain):
        return [
            SlotSet('noidcountries', None)
            ]