import requests
import os
import logging
import datetime
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import ActionExecutor, CollectingDispatcher
from rasa_sdk.events import ReminderScheduled, SlotSet, ActionReverted

logger = logging.getLogger(__name__)

class ActionSetNamens(Action):
  def name(self):
    return "action_set_namens"

  def run(self, dispatcher, tracker, domain):
      namens = next(tracker.get_latest_entity_values("namens"), None)
      return [SlotSet("namens",namens)]