from typing import Dict, Text, Any, List, Union, Optional
from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker
from rasa_sdk import Action
from rasa_sdk.events import SlotSet
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction, REQUESTED_SLOT
import logging
import os
import json
from urllib.parse import urlencode
from .content_api import ContentAPI
logger = logging.getLogger(__name__)

class Formfeedback(FormAction):
    """Example of a custom form action"""

    def name(self):
        # type: () -> Text
        """Unique identifier of the form"""

        return "form_feedback"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:

        """A list of required slots that the form has to fill"""
        required_slots = ["feedback_geholpen"]
        geholpen = tracker.get_slot("feedback_geholpen")

        if geholpen == "Ja":
            required_slots += ["feedback_verderverbeteren"]
        if geholpen in ["Nee","Redelijk","Negatief"]:
            required_slots += ["feedback_mis","feedback_antwoordverwacht","feedback_verderverbeteren"]

        return required_slots

    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
         """A dictionary to map required slots to
             - an extracted entity
             - intent: value pairs
             - a whole message
             or a list of them, where a first match will be picked"""

         return {
             "feedback_geholpen": [self.from_intent(intent="inform_affirmative", value= "Ja"),
             self.from_intent(intent="inform_redelijk", value = "Redelijk"),
             self.from_intent(intent="inform_dontknow", value = "Redelijk"),
             self.from_intent(intent="inform_denial", value="Nee"),self.from_text()],
             "feedback_mis": [self.from_text()],
             "feedback_antwoordverwacht": [self.from_text()],
             "feedback_verderverbeteren": [self.from_text()]}

    def validate_feedback_geholpen(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Optional[Text]:
        """Validate date"""

        if value == "Ja":
            dispatcher.utter_template("utter_feedback_top", tracker)
        if value == "Redelijk":
            dispatcher.utter_template("utter_feedback_verbeteren", tracker)
        elif value == "Nee":
            dispatcher.utter_template("utter_feedback_jammer", tracker)

        return {"feedback_geholpen": value}

    def submit(self, dispatcher, tracker, domain):
        """Define what the form has to do
            after all required slots are filled"""

        GITLAB_ACCESS_TOKEN = os.getenv('GITLAB_ACCESS_TOKEN', 'A-E23VRRYLxB2zhxXeo5')
        trackerlog = tracker.current_state()
        
        # // Code below sends user chatlog to gitlab issue

        # list containing user and bot responses
        # loglist = []
        # for conversation in trackerlog["events"]:
        #     if "form_feedback" in conversation.values():
        #         break
        #     else:
        #         if conversation["event"] == "bot":
        #             loglist.append("**Gem**: " + conversation["text"]+"  ")
        #             if conversation['data']['buttons']:
        #                 x = 0
        #                 while x < len(conversation['data']['buttons']):
        #                     loglist.append("**Gem**: " + '|'+ json.dumps(conversation['data']['buttons'][x]['title']) + '|' + "  ")
        #                     x += 1
        #         if conversation["event"] == "user" and conversation["text"] != "/greet":
        #             if conversation["text"] :
        #                 loglist.append("**Gebruiker**: " + conversation["text"]+"  ")
        #             else:
        #                 loglist.append("**Gebruiker**:  ")

        # list containing user and bot responses until form_feedback
        # logger.info("LOGLIST")
        # logger.info(loglist)

        # Concatenate question and answer combinations
        question_answer = ""
        responses = domain.get('responses',domain.get('templates'))
        for slot in Formfeedback.required_slots(tracker):
            # a bit hacky, actions should not access templates directly, requires template without variables, no variations, just text element. TODO NLG implementation
            test = "**" + responses["utter_ask_" +slot][0]["text"] + "**  \n" + "*" + tracker.get_slot(slot) + "*  \n"
            question_answer += test

        import requests
        url = "https://gitlab.com/api/v4/projects/12344345/issues"
        # retrieve unique chat ID
        title = "Gebruikers feedback " + tracker.sender_id.split("/")[0].strip()
        # gem content dialog url
        gem_content_url = os.getenv("GEMCONTENT_URL")
        query_params = urlencode({"sender_id": tracker.sender_id})
        gem_content = f'{gem_content_url}/admin/tracker/event/?{query_params}'
        # markdown for description
        gem_content_markdown = "["+title+"]" + "(" + gem_content + ")"
        description = question_answer + "  \n" + gem_content_markdown 
        # + "  \n\n" + "**Chatlog**" + "  \n\n" + "\n".join(loglist).replace(",","").replace("'","").replace("[","").replace("]","").replace("€",":euro:")
        description = description.replace("🦄", ":unicorn:")
        logger.info("description")
        logger.info(description)

        geholpen = tracker.get_slot("feedback_geholpen")

        label = ""
        if geholpen in ["Ja","Nee","Redelijk","Negatief"]:
            label = "Geholpen_"+geholpen
        # payload message
        data = {
           "id": "12344345",
            "title": title,
            "description": description,
            "labels": label,
            "state": "opened",
            "confidential": True}
        
        headers = {'Authorization': 'Bearer '+ GITLAB_ACCESS_TOKEN, 'Content-Type': 'application/json'}
        try:
            # POST issue to gitlab
            response = requests.post(url, json = data, headers=headers).json()
            # utter_bedanktvoorjefeedback
            dispatcher.utter_message(template="utter_feedback_bedankt")
            # do something with feedback
        
        except Exception as e:
            logger.error(e)
            
        return [SlotSet("feedback_geholpen", None),SlotSet("feedback_verderverbeteren", None),SlotSet("feedback_mis", None),SlotSet("feedback_antwoordverwacht", None)]

