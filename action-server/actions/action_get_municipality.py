from .bag_api import BagAPI
from rasa_sdk import Action
from rasa_sdk.events import SlotSet
import logging
logger = logging.getLogger(__name__)

# gets municpality

class ActionGetMunicipality(Action):
    def name(self):
        return "action_get_municipality"

    def run(self, dispatcher, tracker, domain):
        municipality = tracker.get_slot("municipality")
        return [SlotSet('municipality',municipality)]
