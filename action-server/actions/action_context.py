from rasa_sdk import Action
from rasa_sdk.events import SlotSet, FollowupAction, UserUtteranceReverted, ActionReverted
from rasa_sdk import Tracker
import logging
logger = logging.getLogger(__name__)

class action_context(Action):

    def name(self):
        return "action_context"

    def run(self, dispatcher, tracker, domain):
        intent = tracker.latest_message.get('intent')['name']
        productslot = tracker.get_slot("product")

        logger.info('KIJK HIER VOOR INTENT')
        logger.info(intent)

        if intent == "hoe_doorgeven" or intent == "hoe_doorgeven" or intent == "hoe":
            keyword = '_hoe'
        # elif intent == "wie":
        #     keyword = '_wie'
        # elif intent == "wanneer":
        #     keyword = '_wanneer'
        # elif intent == "wat":
        #     keyword = '_wat'
        # elif intent == "waar":
        #     keyword = '_waar'
        # elif intent == "waarom":
        #     keyword = '_waarom'
        # elif intent == "watkostdat":
        #     keyword = '_watkostdat'
        # elif intent == "nieuwe":
        #     keyword = '_nieuwe'
        # elif intent == "kwijt":
        #     keyword = '_kwijt'
        # elif intent == "kapot":
        #     keyword = '_kapot'
        # elif intent == "watnodig":
        #     keyword = '_watnodig'
        # elif intent == "watmeebrengen":
        #     keyword = '_watmeebrengen'
        # elif intent == "watmeesturen":
        #     keyword = '_watmeesturen'
        # elif intent == "kindmee":
        #     keyword = '_kindmee'
        # elif intent == "aangiftedoen":
        #     keyword = '_aangiftedoen'
        # elif intent == "nudoorgeven":
        #     keyword = '_nudoorgeven'
        # elif intent == "namenswie":
        #     keyword = '_namenswie'

        tracker1 = tracker.events
            # logger.info(tracker1)
            # the variable name "last_utter" loops in a reversed list containing dictionaries with all events of a setion
            # selects all dictionaries that contain .keys() == "name"
            # returns value of "name" if it starts with "utter" or "action" and is not equal to "action_listen"
            
        last_utter = ""
        for dicts in tracker1[::-1]:
            if "name" in dicts.keys() and (dicts["name"][0:5] == "utter" or dicts["name"][0:6] == "action" and not dicts["name"] == "action_listen" and not dicts["name"] == "action_helpmore"):
                last_utter += dicts["name"]
                break

        logger.info('last_utter')
        logger.info(last_utter)

        utter_template = last_utter + keyword

        utter_template_algemeen = 'utter_algemeen' + keyword

        responses = domain.get('responses',domain.get('templates'))   
        if utter_template in responses.keys():
            logger.info("verdieping: specifiek ") 
            dispatcher.utter_template(utter_template, tracker)
            return [UserUtteranceReverted()]
            
        else:
            product = tracker.get_slot("product")
            return [SlotSet("product",product)]














            # try:
            #     utter_product_template = 'utter_product_' + productslot + keyword
            #     if utter_product_template in domain.get('templates').keys():
            #         logger.info("utter_product_template (general)")
            #         dispatcher.utter_template(utter_product_template, tracker)
            #         return [UserUtteranceReverted()]
                
            #     else:
            #         logger.info("geen utter gevonden, algemene intent")
            #         dispatcher.utter_template(utter_template_algemeen, tracker)
            #         return [UserUtteranceReverted()]

            # except:
            #     logger.info("Algemene intent")
            #     dispatcher.utter_template(utter_template_algemeen, tracker)
            #     return [UserUtteranceReverted()]