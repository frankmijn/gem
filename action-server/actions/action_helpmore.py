import requests
import os
import logging
import datetime
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import ActionExecutor, CollectingDispatcher
from rasa_sdk.events import ReminderScheduled

logger = logging.getLogger(__name__)

class ActionHelpMore(Action):
  def name(self):
    return "action_helpmore"

  def run(self, dispatcher, tracker, domain):
    now = datetime.datetime.now()
    now_plus_wait = now + datetime.timedelta(seconds = 60)
    return [ReminderScheduled('schedule_helpmore',now_plus_wait)]