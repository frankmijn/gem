import logging
import os
import requests

logger = logging.getLogger(__name__)


class BRPAPI(object):

    def get(self, bsn):
        logger.info("In BRP API GET")
        bsn = bsn.lower().replace(" ", "")
        API_URL = os.environ.get('BRP_API_URL', 'http://webservices.tilburg.io:8089/')
        url = API_URL + "GetPersoongegevens/{}".format(bsn)
        logger.info(url)
        try:
            response = requests.get(url).json()
        except Exception as e:
            logger.warning(e)
            return None
        if 'Status' in response and response['Status'] == 'Niet gevonden':
            return None
        logger.info(response)
        return response
