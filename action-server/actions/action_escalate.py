from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

from rasa_sdk import Action, Tracker, ActionExecutionRejection
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import (
    SlotSet,
    UserUtteranceReverted,
    ConversationPaused,
    FollowupAction,
    AllSlotsReset,
    ConversationPaused,
    ConversationResumed
)

import logging
import os
import requests
import json
import pprint
import websockets
import asyncio
import datetime
import uuid

logger = logging.getLogger(__name__)

class ActionTalkToHuman(Action):
    """
	human in the loop action
	"""
    
    sender_id = ""
    socket_id = ""
    channel_name = ""
    activity_timeout = 0 # default value
    appid = "" #"8da830a258993eaa5deb" # unique for each municipality
    guid = "" #"acd3da2b-4906-45da-8373-96e632c899de" # unique for each municipality
    #ws_uri = "ws://ws-eu.pusher.com/app/{0}?protocol=7&client=js&version=4.3.1&flash=false:8765".format(appid)
    ws_uri = "ws://ws-eu.pusher.com/app/{0}?protocol=7&client=js&version=4.3.1&flash=false:8765"
    #availability_uri = "https://cloudstatic.obi4wan.com/api/v1.0/chat/availability/{0}".format(guid)
    availability_uri = "https://cloudstatic.obi4wan.com/api/v1.0/chat/availability/{0}"
    chatapi_uri = "https://chatapi.obi4wan.com/api/v1.0/token"
    domain_address = "" #'http://localhost/'
    #websocket = None
    auth_token = ""
    user_id = ""
    clientname = "ChatBotGem"
    origin = "widget" # can we change this?
    username = "widgetUser"
    time_machine = None
    
    ## These templates should probably be moved to NLG and retreived in some way so they can be managed outside of this code.
    caption_chathistory = "Chat History from ChatBotGem 🦄"
    caption_chatstarted = "Chat started 🦄"
    caption_chatlinkshared = "Link to Livechat shared with user 🦄"
    
    def generateTimeStampFull(self):
        datetime_now = datetime.datetime.now()
        datetime_stamp = datetime_now.strftime('%Y-%m-%dT%H:%M:%S.%f')
        return datetime_stamp
      
    def generateTimeStamp(self):
        datetime_now = datetime.datetime.now()
        datetime_stamp = datetime_now.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3]
        logger.info("OBI4wan: generateTimeStamp: {0}".format(datetime_stamp))
        datetime_stamp += "Z"
        return datetime_stamp
      
    def convertTimeStamp(self, timestamp):
        datetime_stamp = datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3]
        logger.info("OBI4wan: convertTimeStamp: datetime_stamp: {0}".format(datetime_stamp))
        datetime_stamp += "Z"
        return datetime_stamp
      
    def generateContextMessageJSON(self, message, timestamp):
        if timestamp == None:
            contextmessage_datetime = self.generateTimeStamp()
        else:
            contextmessage_datetime = self.convertTimeStamp(timestamp)
        contextmessage_data_message_author_dict = {'url':None,'name':self.clientname}                                                                              
        contextmessage_data_message_dict = {'date':contextmessage_datetime,'content': message,'type':'incoming','author':contextmessage_data_message_author_dict,'attachment':None,'displayInWidget':False,'eventType':'client-context-message','url':None}
        contextmessage_data_message_json = json.dumps(contextmessage_data_message_dict,separators=(",", ":"))
        contextmessage_dict = {'event':'client-context-message','data':{'message':contextmessage_data_message_json},'channel':self.channel_name}
        contextmessage_json = json.dumps(contextmessage_dict,separators=(",", ":")) 
        return contextmessage_json
    
    def generateMessageJSON(self, message, timestamp):
        message_datetime = self.convertTimeStamp(timestamp)
        message_data_message_author_dict = {'url':None,'name':self.username}                                                                                 
        message_data_message_dict = {'date':message_datetime,'content': message,'type':'incoming','author':message_data_message_author_dict,'attachment':None,'displayInWidget':True,'eventType':'client-message','url':self.domain_address}
        message_data_message_json = json.dumps(message_data_message_dict,separators=(",", ":"))
        message_dict = { 'event':'client-message','data':{'message':message_data_message_json},'channel':self.channel_name}                                                      
        message_json = json.dumps(message_dict,separators=(",", ":"))     
        return message_json
    
    def generateBotMessageJSON(self, message, timestamp):
        message_datetime = self.convertTimeStamp(timestamp)
        message_data_message_author_dict = {'url':None,'name':self.clientname}                                                                           
        message_data_message_dict = {'date':message_datetime,'content': message,'type':'incoming','author':message_data_message_author_dict,'attachment':None,'displayInWidget':False,'eventType':'client-message','url':self.domain_address}
        message_data_message_json = json.dumps(message_data_message_dict,separators=(",", ":"))
        message_dict = { 'event':'client-context-message','data':{'message':message_data_message_json},'channel':self.channel_name}                                                      
        message_json = json.dumps(message_dict,separators=(",", ":"))     
        return message_json
    
    # Function to handle Websocket connection with OBI4wan
    async def darth2solo(self, dispatcher, tracker):
        logger.info("OBI4wan: darth2solo(): Executing...") #DEBUG
        async with websockets.connect(self.ws_uri) as websocket:
            ### get socket_id from ws
            ## {"event":"pusher:connection_established","data":"{\"socket_id\":\"126593.6138224\",\"activity_timeout\":120}"}
            ## {"event":"pusher:error","data":{"code":4005,"message":"Path not found"}}
            logger.info("OBI4wan darth2solo(): Receiving first message from websocket...") #DEBUG
            connection_resp = await websocket.recv()
            #dispatcher.utter_message("Receiving message 1: {}".format(connection_resp)) #DEBUG
            connection_resp_dict = json.loads(connection_resp)
            connection_resp_dict_event = connection_resp_dict['event']
            connection_event_name = connection_resp_dict_event.split(':')[1]
            
            logger.info("OBI4wan darth2solo(): Pusher Event: " + connection_event_name) #DEBUG
            #dispatcher.utter_message("Pusher event: {}".format(connection_event_name)) #DEBUG
            if connection_event_name == "connection_established":
                connection_resp_data_dict = json.loads(connection_resp_dict['data']) #DEBUG
                self.socket_id = connection_resp_data_dict['socket_id']
                self.activity_timeout = connection_resp_data_dict['activity_timeout']
                logger.info("OBI4wan darth2solo(): socket_id: " + self.socket_id)
                logger.info("OBI4wan darth2solo(): activity_timeout: " + str(self.activity_timeout))
                ## MOVE ALL CODE FROM BELOW HERE 
                ### Get auth token, etc. using socket_id from ChatAPI
                ## params {"Form data":{"socket_id":"126607.5046277","channel_name":"presence-acd3da2b-4906-45da-8373-96e632c899de_c9190c36-5bd6-4fc9-8b1e-767e87e8b0b4","domain_address":"http://localhost/livechat/demo/","origin":"widget","username":"widgetUser"}}
                ## response {"auth":"8da830a258993eaa5deb:96b859be81cd5832cb6f91c21d77e6b22819f1e2c40273ab8f81893bcdaa8796","channel_data":"{\"user_id\":\"Widget_c9190c36-5bd6-4fc9-8b1e-767e87e8b0b4\",\"user_info\":{\"name\":\"widgetUser\"}}"}
                self.sender_id = str(uuid.uuid4())
                self.channel_name = 'presence-' + self.guid + '_' + self.sender_id
                token_req_params = { 'socket_id': self.socket_id, 'channel_name': self.channel_name, 'domain_address': self.domain_address, 'origin': self.origin, 'username' : self.username }
                token_req_headers = { 'accept' : 'application/json'}

                try:
                    token_resp = requests.post(self.chatapi_uri, data=token_req_params, headers=token_req_headers).json()
                except requests.exceptions.RequestException as e:  # This is the correct syntax
                    logger.error("OBI4wan darth2solo(): ChatAPI Error: {}".format(e))
                    dispatcher.utter_template("utter_no_human_available", tracker)
                    return
                else:
                    ### MOVE ALL CODE FROM BELOW HERE
                    logger.info("OBI4wan darth2solo(): token_resp: " + pprint.pformat(token_resp))   # DEBUG         
                    #dispatcher.utter_message("Receiving token: {}".format(token_resp)) #DEBUG

                    self.auth_token = token_resp['auth']
                    logger.info('OBI4wan darth2solo(): self.auth_token: '+self.auth_token) # DEBUG

                    token_channeldata = json.loads(token_resp['channel_data'])
                    logger.info('OBI4wan darth2solo(): token_channeldata: ' +pprint.pformat(token_channeldata))  # DEBUG
                    self.user_id = token_channeldata['user_id'] # "Widget_" + sender_id
                    logger.info('OBI4wan darth2solo(): self.user_id: '+self.user_id) # DEBUG

                    token_channeldata_userinfo = token_channeldata['user_info']
                    token_channeldata_userinfo_name = token_channeldata_userinfo['name'] # "widgetUser"
                    logger.info('OBI4wan darth2solo(): token_channeldata_userinfo_name: '+token_channeldata_userinfo_name) # DEBUG

                    ### Pusher API subscribe
                    ## {"event":"pusher:subscribe","data":{"auth":"8da830a258993eaa5deb:96b859be81cd5832cb6f91c21d77e6b22819f1e2c40273ab8f81893bcdaa8796","channel_data":"{\"user_id\":\"Widget_c9190c36-5bd6-4fc9-8b1e-767e87e8b0b4\",\"user_info\":{\"name\":\"widgetUser\"}}","channel":"presence-acd3da2b-4906-45da-8373-96e632c899de_c9190c36-5bd6-4fc9-8b1e-767e87e8b0b4"}}
                    ## {"event":"pusher_internal:subscription_succeeded","data":"{\"presence\":{\"count\":1,\"ids\":[\"Widget_dae28d48-9cdf-45ca-a857-313a1be77a1d\"],\"hash\":{\"Widget_dae28d48-9cdf-45ca-a857-313a1be77a1d\":{\"name\":\"widgetUser\"}}}}","channel":"presence-acd3da2b-4906-45da-8373-96e632c899de_dae28d48-9cdf-45ca-a857-313a1be77a1d"}
                    logger.info("OBI4wan darth2solo(): Sending event:subscribe...")
                    subscribe_channeldata_dict = {'user_id':self.user_id,'user_info':{'name':self.username}}
                    subscribe_channeldata_json = json.dumps(subscribe_channeldata_dict,separators=(",", ":"))
                    subscribe_dict = {'event':'pusher:subscribe','data':{'auth':self.auth_token,'channel_data':subscribe_channeldata_json,'channel':self.channel_name}}
                    subscribe_json = json.dumps(subscribe_dict,separators=(",", ":"))
                    await websocket.send(subscribe_json)
                    logger.info("OBI4wan darth2solo(): Sending event:subscribe (JSON): " + subscribe_json) # DEBUG
                    #dispatcher.utter_message("Sending: {}".format(subscribe_json)) # DEBUG

                    #logger.info("OBI4wan darth2solo(): Receiving response for event:subscribe...")
                    subscribe_resp = await websocket.recv()
                    #dispatcher.utter_message("Response: {}".format(subscribe_resp)) # DEBUG
                    logger.info("OBI4wan darth2solo(): Receiving event:pusher_internal:subscription_succeeded: " + subscribe_resp) # DEBUG

                    subscribe_resp_dict = json.loads(subscribe_resp)
                    subscribe_resp_dict_event = subscribe_resp_dict['event']
                    subscribe_resp_event_name = subscribe_resp_dict_event.split(':')[0]
                    logger.info("OBI4wan darth2solo(): subscribe_resp_event_name: {0}".format(subscribe_resp_event_name)) # DEBUG
                    if subscribe_resp_event_name == "pusher_internal":
                        subscribe_resp_event_name_sub = subscribe_resp_dict_event.split(':')[1]
                        if subscribe_resp_event_name_sub == "subscription_succeeded":
                            logger.info('OBI4wan darth2solo(): subscribe_resp_event_name: {0}:{1}'.format(subscribe_resp_event_name,subscribe_resp_event_name_sub)) #DEBUG
                            # MOVE ALL CODE FROM BELOW HERE
                        else: 
                            logger.info('OBI4wan darth2solo(): subscribe_resp_event_name: {0}'.format(subscribe_resp_event_name)) #DEBUG
                            dispatcher.utter_template("utter_no_human_available", tracker)
                            return
                    else:
                        dispatcher.utter_template("utter_no_human_available", tracker)
                        return  

                    ### Sending chat history to Webcare 
                    ## {"event":"client-message","data":{"message":"{\"date\":\"2019-10-15T08:26:32.313Z\",\"content\":\"hoi\",\"type\":\"incoming\",\"author\":{\"url\":null,\"name\":\"ObiEngageDemo\"},\"attachment\":null,\"displayInWidget\":true,\"eventType\":\"client-message\",\"url\":\"http://localhost/livechat/demo/\"}"},"channel":"presence-acd3da2b-4906-45da-8373-96e632c899de_c9190c36-5bd6-4fc9-8b1e-767e87e8b0b4"}
                    ## This context message should be send with a timestamp older then the first message from tracker.events

                    ### Get dialog history and transfer to OBI4wan
                    event_index = 0
                    for event in tracker.events:
                        if event_index == 0:
                            await websocket.send(self.generateContextMessageJSON(self.caption_chathistory, event.get("timestamp") -1))
                        if event.get("event") == "user":
                            #dispatcher.utter_message("{}".format(event.get("text"))) # DEBUG
                            logger.info("tracker.event[]: {} (User)".format(event.get("text"))) # DEBUG
                            await websocket.send(self.generateMessageJSON(event.get("text"), event.get("timestamp")))
                        elif event.get("event") == "bot":
                            logger.info("tracker.event[]: {0}: {1} (Bot)".format(event.get("event"),event.get("text"))) # DEBUG 
                            await websocket.send(self.generateBotMessageJSON("{0}: {1}".format(event.get("event").upper(),event.get("text")),event.get("timestamp")))
                        elif event.get("event") == "action":
                            logger.info("tracker.event[]: {0}: {1} (Action)".format(event.get("event"),event.get("text"))) # DEBUG
                        else:
                            logger.info("tracker.event[]: {0}: {1} (Other)".format(event.get("event"),event.get("text"))) # DEBUG
                            #await websocket.send(self.generateContextMessageJSON("tracker.event[]: {0}: {1} (Other)".format(event.get("event"),event.get("text"))))
                        event_index += 1
                        #await asyncio.sleep(1)

                    ### LiveChat  
                    ## {"event":"client-context-message","data":{"message":"{\"date\":\"2019-10-15T08:26:29.873Z\",\"content\":\"Chat started\",\"type\":\"incoming\",\"author\":{\"url\":null,\"name\":\"ObiEngageDemo\"},\"attachment\":null,\"displayInWidget\":false,\"eventType\":\"client-context-message\",\"url\":null}"},"channel":"presence-acd3da2b-4906-45da-8373-96e632c899de_c9190c36-5bd6-4fc9-8b1e-767e87e8b0b4"}
                    ## {"event":"pusher_internal:member_added","data":"{\"user_id\":\"Agent_b20bfbc5-cfb6-4c85-a1a5-06d69c5bc848\",\"user_info\":{\"name\":\"agent\"}}","channel":"presence-acd3da2b-4906-45da-8373-96e632c899de_b20bfbc5-cfb6-4c85-a1a5-06d69c5bc848"}
                    ## {"event":"pusher_internal:member_removed","data":"{\"user_id\":\"Agent_9597a112-723e-40e0-aa7a-db73948d6535\"}","channel":"presence-acd3da2b-4906-45da-8373-96e632c899de_9597a112-723e-40e0-aa7a-db73948d6535"}
                    ## {"event":"client-typing","data":"{\"typing\":true}","channel":"presence-acd3da2b-4906-45da-8373-96e632c899de_b20bfbc5-cfb6-4c85-a1a5-06d69c5bc848","user_id":"Agent_b20bfbc5-cfb6-4c85-a1a5-06d69c5bc848"}
                    ## {"event":"client-message","data":"{\"message\":\"{\\\"date\\\":\\\"2019-10-22T13:19:05.0090725Z\\\",\\\"content\\\":\\\"BOOEEEE\\\",\\\"type\\\":\\\"outgoing\\\",\\\"author\\\":{\\\"name\\\":\\\"Wim\\\",\\\"url\\\":null}}\"}","channel":"presence-acd3da2b-4906-45da-8373-96e632c899de_b20bfbc5-cfb6-4c85-a1a5-06d69c5bc848"}
                    ## {"event":"client-typing","data":"{\"typing\":false}","channel":"presence-acd3da2b-4906-45da-8373-96e632c899de_b20bfbc5-cfb6-4c85-a1a5-06d69c5bc848","user_id":"Agent_b20bfbc5-cfb6-4c85-a1a5-06d69c5bc848"}
                    ## loop until escalation ends "/unpause" || ...
                    #await websocket.send(self.generateContextMessageJSON(self.caption_chatstarted), None)
                    await websocket.send(self.generateContextMessageJSON(self.caption_chatlinkshared, None))

                    #message = ""
                    #await asyncio.sleep(1)
                    """
                    while message != '/unpause':
                        ### Receive message from Webcare
                        try:
                            logger.info("OBI4wan darth2solo(): loop: trying to read websocket")
                            message_resp = await websocket.recv()
                            # timeout=self.activity_timeout
                            #message_resp await asyncio.wait_for(websocket.recv(), timeout=1)
                        except websockets.exceptions.ConnectionClosedError:
                            logger.error("OBI4wan darth2solo(): websocket Error: {0}".format(e))
                            #return
                        else:
                            logger.info("OBI4wan darth2solo(): message_resp: {0}".format(message_resp))

                            message_dict = json.loads(message_resp)
                            message_data_dict = json.loads(message_dict['data'])
                            #message_json = message_resp.json()
                            event = message_dict['event']
                            #logger.info("loop: event: {0}".format(event))
                            if event == "client-typing":
                                typing = message_data_dict['typing']
                                if typing:
                                    logger.info("OBI4wan darth2solo(): loop: {0}".format(event)) #DEBUG
                                    dispatcher.utter_message("Agent is typing...🦄")
                                else:
                                    logger.info("OBI4wan darth2solo(): loop: {0}".format(event)) # DEBUG
                                    dispatcher.utter_message("Agent stopped typing...🦄")
                            elif event == "pusher_internal:member_added": 
                                logger.info("OBI4wan darth2solo(): loop: {0}".format(event)) #DEBUG
                                dispatcher.utter_message("Agent joined chat...🦄")
                            elif event == "pusher_internal:member_removed": 
                                logger.info("OBI4wan darth2solo(): loop: {0}".format(event)) #DEBUG
                                dispatcher.utter_message("Agent left chat...🦄")
                            elif event == 'client-message':
                                #logger.info("OBI4wan darth2solo(): loop: {0}".format(event)) #DEBUG
                                message_data_message_dict = json.loads(message_data_dict['message'])
                                message = message_data_message_dict['content']
                                #message_agent_name = message_data_message_dict['content']
                                if message != '/unpause':
                                    dispatcher.utter_message(message)
                                    #tracker.update(BotUttered("message"))
                                #message = "/unpause"
                                #break
                            else:
                                logger.warning("OBI4wan darth2solo(): loop: Unknown event type received: {0}".format(event))

                            #await asyncio.sleep(1)
                            logger.info("OBI4wan darth2solo() message loop: looping...")
                            #dispatcher.utter_message("looping...🦄")
                            #await asyncio.sleep(0.1)

                        ### Send new messages to Webcare
                        ## Skip already send message, use event[index] instead of for loop
                        logger.info("OBI4wan darth2solo(): loop: trying to send new messages to websocket")
                        logger.info("loop tracker.event[]: len(tracker.events) :{0}".format(len(tracker.events)))
                        logger.info("loop tracker.event[]: event_index :{0}".format(event_index))
                        for event in tracker.events[event_index:len(tracker.events)]:
                        #for event in tracker.events:
                            logger.info("loop tracker.event[]: event_index :{0}".format(event_index))
                            #logger.info("loop tracker.event[]: {0}:{1}".format(event.get("event"),event.get("text")))
                            if event.get("event") == "user":
                                #dispatcher.utter_message("{}".format(event.get("text"))) # DEBUG
                                logger.info("loop tracker.event[]: {0} (User)".format(event.get("text"))) # DEBUG
                                await websocket.send(self.generateMessageJSON(event.get("text")))
                            elif event.get("event") == "bot":
                                logger.info("loop tracker.event[]: {0}: {1} (Bot)".format(event.get("event"),event.get("text"))) # DEBUG 
                                await websocket.send(self.generateBotMessageJSON("{0}: {1}".format(event.get("event"),event.get("text"))))
                            elif event.get("event") == "action":
                                logger.info("loop tracker.event[]: {0}: {1} (Action)".format(event.get("event"),event.get("text"))) # DEBUG
                            else:
                                logger.info("loop tracker.event[]: {0}: {1} (Other)".format(event.get("event"),event.get("text"))) # DEBUG
                                #await websocket.send(self.generateContextMessageJSON("tracker.event[]: {0}: {1} (Other)".format(event.get("event"),event.get("text"))))
                            event_index += 1
                    """
                    #dispatcher.utter_message("[Klik hier](http://localhost/livechat/demo/?session={0}#{0}) om met een medewerker verder te praten 🦄".format(self.sender_id))
                    dispatcher.utter_message("[Klik hier](livechat/?session={0}#{0}) om met een medewerker verder te praten 🦄".format(self.sender_id))
                    await websocket.close()  
                    ### MOVE ALL CODE FROM BELOW HERE END
                ## MOVE ALL CODE FROM BELOW HERE END
            elif connection_event_name == "error":
                #connection_resp_data_dict = json.loads(connection_resp_dict['data']) #DEBUG
                logger.info("OBI4wan darth2solo(): Pusher Error: {0}: {1}".format(connection_resp_dict['data']['code'], connection_resp_dict['data']['message']))
                dispatcher.utter_template("utter_no_human_available", tracker) 
            else:
                logger.info("OBI4wan darth2solo(): Unknown Pusher event: " + connection_event_name) #DEBUG
                dispatcher.utter_template("utter_no_human_available", tracker)
                return
        logger.info("OBI4wan darth2solo(): Closing function call...") # DEBUG
      
  
    def name(self):
        return "action_talk_to_human"
  
    def run(self, dispatcher, tracker: Tracker, domain):
        logger.info("OBI4wan: ...") # DEBUG 
       
        ### OBI4wan collecting environment variables
        logger.info("OBI4wan: collecting environment variables") # DEBUG
        APPID = os.environ.get('OBI4WAN_APPID', None)
        GUID = os.environ.get('OBI4WAN_GUID', None)
        DOMAINADDRESS = os.environ.get('OBI4WAN_DOMAINADDRESS', None) 
        if not APPID or not GUID or not DOMAINADDRESS:
            logger.error("OBI4WAN_[APPID|GUID|DOMAINADDRESS] aren't configured, please set environment variables to connect to the OBI4wan API")
            return None
        else:
            self.appid = APPID
            self.ws_uri = self.ws_uri.format(self.appid)
            logger.info("self.ws_uri: {0}".format(self.ws_uri)) # DEBUG
            self.guid = GUID
            self.availability_uri = self.availability_uri.format(self.guid)
            logger.info("self.availability_uri: {0}".format(self.availability_uri)) # DEBUG
            self.domain_address = DOMAINADDRESS
        
        ### Checking availibility
        ## TODO check what happens to availablity when all agents have claimed max number of chats in Webcare?
        logger.info("OBI4wan: Checking Agent availability...")
        availability_req_headers = { 'accept' : 'application/json'}
        try:
            #logger.info("OBI4wan: availability_uri: {0}".format(self.availability_uri)) # DEBUG
            availability_resp = requests.get(self.availability_uri, headers=availability_req_headers).json()
            #logger.info("OBI4wan: availability_resp: {0}".format(availability_resp)) # DEBUG
        except requests.exceptions.RequestException as e:
            dispatcher.utter_template("utter_no_human_available", tracker) 
            logger.error("action_talk_to_human.run(): AvailibalityAPI Error: {}".format(e))
            return None
        else: 
            availability = availability_resp['available']
            if availability:
                dispatcher.utter_template("utter_talk_to_human", tracker)
                logger.info("OBI4wan: Agent available") # DEBUG
            else:
                dispatcher.utter_template("utter_no_human_available", tracker) 
                logger.info("OBI4wan: Agent NOT available") # DEBUG
                return None
    
            ### Pause Rasa
            self.time_machine = self.generateTimeStampFull()
            #tracker._paused = True

            ### Connecting to OBI4wan API's using websockets
            ## TODO: Probably need to split setting up the connecting from the loop, more knowledge required about asyncio and websockets 
            logger.info("OBI4wan: switching to websockets") # DEBUG
            
            #asyncio.sleep(3)
            asyncio.get_event_loop().run_until_complete(self.darth2solo(dispatcher,tracker))   
            #self.darth2solo(dispatcher,tracker)   

            ### ====================================================================================================================
            ### Below you find the old code that faked a live chat reply, this was kept in place to see if code was running properly
            """
            seems like rasa will stop listening once conversation
            is paused, which means no actions are attempted, therefore
            preventing triggering ConversationResumed() in a straightforward way.
            """

            """
            message = ""
          
            municipality = tracker.get_slot("municipality")
            logger.info("OBI4wan: handoff municipality: {0}".format(municipality))
            if municipality is None:
                url = "http://web:80/handoff/django.json?sender={}".format(tracker.sender_id)
            else:
                url = "http://web:80/handoff/django-{}.json?sender={}".format(municipality, tracker.sender_id)  
            
            try:
                logger.info("OBI4wan: handoff url: {0}".format(url))
                req = requests.get(url)
                req.encoding = 'utf-8'
                logger.info("OBI4wan: handoff req: {0}".format(req))
            except requests.exceptions.RequestException as e:
                logger.error("OBI4wan: Handoff Error: {0}".format(e))
            else:
                logger.info("OBI4wan: handoff request handling")
                resp = json.loads(req.text)
                logger.info("OBI4wan: handoff resp: {0}".format(resp))
                if "error" in resp:
                    raise Exception("Error fetching message: " + repr(resp["error"]))
                    return
                else:
                    message = resp["message"]
                    if message != "/unpause":
                        dispatcher.utter_message("{}".format(message))
                    else:
                        # municipality "Goirle" can be used to trigger this.
                        dispatcher.utter_template("utter_talk_to_bot_again", tracker)
            ### ====================================================================================================================
            """
            
            ### Enable chatbot again.
            #tracker.travel_back_in_time(self.time_machine)
            #tracker._paused = False
            
        return[]