from typing import Dict, Text, Any, List, Union, Optional
from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker
from rasa_sdk import Action
from rasa_sdk.events import SlotSet
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction, REQUESTED_SLOT

from .content_api import ContentAPI

class action_price(FormAction):

    def name(self):

        return "action_price"

    def run(self,dispatcher: CollectingDispatcher,tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict]:
        """Define what the form has to do
            after all required slots are filled"""

        content_api = ContentAPI()
        municipality = tracker.get_slot("municipality")
        product = tracker.get_slot("product")
        response = content_api.get(product, municipality)
        if not response:
            dispatcher.utter_message(template="utter_product_not_found")
            return []

        #response = {"price":"71,35🦄","id_price":"35🦄"} #tijdelijk oplossing voor verlopen licentie (CONTENT API)
        
        if product == "paspoort":
            dispatcher.utter_message(template="utter_product_paspoort_costs", price="{0}".format(response["price"]))

        if product == "id-kaart":
            response = {"id_price_child":"30,70 euro🦄","id_price_adult":"58,30 euro🦄"} 
            dispatcher.utter_message(template="utter_product_id-kaart_costs", id_price_child="{0}".format(response["id_price_child"]),
            id_price_adult="{0}".format(response["id_price_adult"]))

        return []
        #return [SlotSet("price", response["price"])]
