import logging
import os
import requests

logger = logging.getLogger(__name__)

class ContentAPI(object):

    def required_slots(self, tracker, product_type):
        product = tracker.get_slot("product")
        if product:
            response = self.get(product)
            if response["generic"][product_type] == True:
                return ["product"]
        return ["product", "municipality"]

    def get(self, product, municipality=None):
        logger.info("In Content API GET")
        if not municipality:
            municipality = "AZ"
        logger.info(municipality)
        logger.info(product)
        municipality = municipality.lower().replace(" ", "_")
        product = product.lower().replace(" ", "_")
        content_api_url = os.environ.get('CONTENT_API')
        url = "{}{}/content/{}".format(
            content_api_url, municipality, product)
        logger.info("Content API URL: {}".format(url))
        try:
            response = requests.get(url).json() #make an api call
        except Exception as e:
            logger.warning(e)
            return None
        logger.info(response)
        return response
