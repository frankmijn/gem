import requests
from typing import Dict, Text, Any, List, Union, Optional
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, FollowupAction, UserUtteranceReverted, ActionReverted
from rasa_sdk.interfaces import ActionExecutionRejection
from rasa_sdk import Tracker
import logging
from .allmanak_api import allmanak_api_url
logger = logging.getLogger(__name__)



class ActionCheckLivechat(Action):

    availability_uri = "https://cloudstatic.obi4wan.com/api/v1.0/chat/availability/{0}"

    def name(self):
        return "action_check_livechat"

    def run(self, dispatcher, tracker, domain):
        # precondition municipality set
        municipality = tracker.get_slot("municipality")

        if not municipality:
            raise ActionExecutionRejection(self.name(),f"no municipality set, use check_municipality in story / prediction first")

        user_event_metadata = tracker.get_last_event_for("user").get("metadata", {})

        widget_livechat_type = user_event_metadata.get("livechat")
        widget_livechat_guid = user_event_metadata.get("guid")
        widget_municipality = user_event_metadata.get("municipality")

        if widget_livechat_type == "obi4wan" and widget_municipality == municipality and widget_livechat_guid:
            try:
                availability_resp = requests.get(self.availability_uri.format(widget_livechat_guid), headers={"accept": "application/json"}).json()
            except requests.exceptions.RequestException as e:
                logger.error(f"Error occurred while requesting availability API: {e}")
            else:
                availability = availability_resp['available']
                if availability:
                    logger.info("OBI4wan: Agent available") # DEBUG
                    return [SlotSet("feature_livechat",True)]
                else:
                    logger.info("OBI4wan: Agent NOT available") # DEBUG
        return [SlotSet("feature_livechat",False)]

