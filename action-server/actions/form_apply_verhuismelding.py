import datetime
import isodate
import requests
import logging
import random
import os
from typing import Dict, Text, Any, List, Union, Optional
from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker
from rasa_sdk import Action
from rasa_sdk.events import SlotSet
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction, REQUESTED_SLOT

from .content_api import ContentAPI
from .bag_api import BagAPI
from .form_apply import ApplyFormAction
from .zgw_api import ZGWAPI
from .tools import Tools

logger = logging.getLogger(__name__)

class VerhuismeldingApplyFormAction(ApplyFormAction):

    def name(self):
        # type: () -> Text
        """Unique identifier of the form"""

        return "form_apply_verhuismelding"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill
        content_api = ContentAPI()"""
        required_slots = ApplyFormAction.required_slots(tracker)
        isloggedin = tracker.get_slot("apply_is_digid_loggedin")

        if isloggedin:
            required_slots += ["verhuismelding_datum","verhuismelding_postcode", "verhuismelding_huisnummer", "verhuismelding_check",  "verhuismelding_confirm"]

        return required_slots 

    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
        """A dictionary to map required slots to
            - an extracted entity
            - intent: value pairs
            - a whole message
            or a list of them, where a first match will be picked"""

        return {
            **super(VerhuismeldingApplyFormAction,self).slot_mappings(),
            "verhuismelding_postcode": [self.from_entity(entity="postcode", not_intent="chitchat"),self.from_text()],
            "verhuismelding_huisnummer": [self.from_entity(entity="huisnummer", not_intent="chitchat"),self.from_text()],
            "verhuismelding_datum": [self.from_entity(entity="time"),self.from_intent(intent="inform_dontknow", value="unknown"),self.from_text()],
            "verhuismelding_check": [
                self.from_intent(intent="inform_affirmative", value=True),
                self.from_intent(intent="inform_denial", value=False),
            ],
            "verhuismelding_confirm": [
                self.from_intent(intent="inform_affirmative", value=True),
                self.from_intent(intent="inform_denial", value=False),
            ]
        }
        
    def validate_verhuismelding_datum(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Optional[Text]:
        """Validate date"""
        if value == "unknown":
            return {"verhuismelding_datum": value}
        else:
            datum = None
            ducktime = next(tracker.get_latest_entity_values("time"), None)
            if ducktime is not None:
                # If detected: Iso-formatted datetime: 2019-09-03T20:56:35.450686 + timezone stuff
                try:
                    datum = datetime.datetime.strptime(ducktime[:10], "%Y-%m-%d").date()
                except Exception as e:
                    logger.exception(e)
                    return {"verhuismelding_datum": None} #Should never happen since ducktime is detected
            else:
                try:
                    datum = datetime.datetime.strptime(value, "%Y-%m-%d").date()
                except Exception as e:
                    try:
                        datum = datetime.datetime.strptime(value, "%d-%m-Yd").date()
                    except Exception as e:
                        logger.exception(e)
                        dispatcher.utter_template("utter_warning_datum_invalid", tracker)
                        return {"verhuismelding_datum": None}
            return {"verhuismelding_datum": datum.__str__()}

    def validate_verhuismelding_huisnummer(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Optional[Text]:
        """Validate postcode/huisnummer value."""
        bag_api = BagAPI()
        postcode = tracker.get_slot("verhuismelding_postcode")
        huisnummer = str(int(value)) # Huisnummer is only set to slot after validation
        logger.info(postcode)
        logger.info(huisnummer)
        data = bag_api.get(postcode, huisnummer)

        if not data or 'street' not in data or 'city' not in data:
            dispatcher.utter_template("utter_warning_adres_invalid", tracker)
            return {"verhuismelding_huisnummer": None,
                "verhuismelding_postcode": None,
                "verhuismelding_straat": None,
                "verhuismelding_woonplaats": None}
        return {"verhuismelding_huisnummer": huisnummer,
                "verhuismelding_straat": data["street"],
                "verhuismelding_woonplaats": data["city"]}

    def validate_verhuismelding_postcode(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Optional[Text]:
        """Validate postcode/huisnummer value."""
        postcode = value.replace(" ", "").upper()
        return {"verhuismelding_postcode": postcode}

    """"def validate_apply_is_digid_channel(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Optional[Text]:
        #Validate digid authentication and get BRP info.
        product = tracker.get_slot("product") #must be "verhuismelding"
        isdigidchannel = tracker.get_slot("apply_is_digid_channel")
        isloggedin = tracker.get_slot("apply_is_digid_loggedin")
        if product == "verhuismelding" and isdigidchannel and isloggedin:
            return {}
        else:
            self.inform_channels_deactivate(dispatcher,tracker)
            self.deactivate()"""
           
    def validate_verhuismelding_check(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Optional[Text]:
        if value:
            # validation succeeded, do nothing, continue
            return {"verhuismelding_check": True}
        else:
            return {"verhuismelding_straat": None, "verhuismelding_woonplaats": None, "verhuismelding_postcode": None, "verhuismelding_huisnummer": None,"verhuismelding_check": None}


    def submit(self,
               dispatcher: CollectingDispatcher,
               tracker: Tracker,
               domain: Dict[Text, Any]) -> List[Dict]:
        """Define what the form has to do
            after all required slots are filled -->
            
            Create Zaak in ZGW-API"""
        isdigidchannel = Tools.isdigidchannel(tracker)
        if (isdigidchannel):
            confirm = tracker.get_slot("verhuismelding_confirm")
            if confirm:
                api = ZGWAPI()
                zaaknummer = str(random.randint(15248,99999))
                toelichting = "Verhuizing {} via chatbot overheid. {} {}, {} {}".format(
                    zaaknummer,
                    tracker.get_slot("verhuismelding_straat"),
                    tracker.get_slot("verhuismelding_huisnummer"),
                    tracker.get_slot("verhuismelding_postcode"),
                    tracker.get_slot("verhuismelding_woonplaats"))
                logger.info(toelichting)
                bag_api = BagAPI()
                data = bag_api.get(tracker.get_slot("verhuismelding_postcode"), tracker.get_slot("verhuismelding_huisnummer"))
                response = api.zaak_toevoegen("Mijn verhuizing", toelichting, datetime.date.today(),
                                              geo=data["geo"])
                if response:
                    uuid = response["url"].split("/")[-1]
                    dispatcher.utter_message("Ik heb je verhuizing voor je aangemeld met zaaknummer: {}".format(zaaknummer))
                    dispatcher.utter_message("Voor meer informatie zie https://ref.tst.vng.cloud/demo/apps/zaakbeheer/{}/".format(uuid))
                    return [SlotSet("verhuismelding_zaaknummer", zaaknummer)]
                else:
                    dispatcher.utter_message("Ik kon helaas je verhuizing niet aanmelden. Probeer het later nogmaals")
                    return []
            else:
                return []
        else:
            self.inform_channels_deactivate(dispatcher,tracker)
            return []


