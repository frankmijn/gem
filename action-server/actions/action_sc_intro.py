import requests
import logging
from rasa_sdk import Action

from .utils import ignore_dh_request

import logging
logger = logging.getLogger(__name__)


class action_samenwerkendecatalogi_intro(Action):
  def name(self):
    return "action_samenwerkendecatalogi_intro"

  def run(self, dispatcher, tracker, domain):

    municipality = tracker.get_slot("municipality")
    gemeente_website = tracker.get_slot("gemeente_website")
    product = tracker.get_slot("product")

    # input_channel = tracker.get_latest_input_channel()
    # logging.info("input_channel")
    # logging.info(input_channel)


    
    if municipality == None:
      municipality = tracker.get_slot("municipality")
    if municipality == None:
      user_event_metadata = tracker.get_last_event_for("user").get("metadata", {})
      municipality = user_event_metadata.get("municipality")

    logging.info("Slot: %s", municipality)
    logging.info("Slot: %s", product)

    sc_template_a = 'utter_samenwerkendecatalogi_intro'
    if product is not None:
        product_template = sc_template_a + '_' + product
        responses = domain.get('responses',domain.get('templates'))
        if product_template in responses.keys():
            sc_template_a = product_template
            dispatcher.utter_template(sc_template_a, tracker, municipality="{0}".format(municipality))
            return []
    else:
      dispatcher.utter_template(sc_template_a, tracker, product = "{0}".format(product), municipality="{0}".format(municipality))
      return []
