from rasa_sdk import Action
from rasa_sdk.events import SlotSet, FollowupAction, UserUtteranceReverted, ActionReverted, UserUttered, ActionExecuted
from rasa_sdk import Tracker
from rasa_sdk.interfaces import ActionExecutionRejection
from typing import Text, Dict, Any, List
import copy
import logging
import json

logger = logging.getLogger(__name__)

class ActionInformEntity(Action):
    
    def name(self):
        return "action_envoor"

    def run(self, dispatcher, tracker, domain):

        entities = tracker.latest_message.get("entities", [])
        logger.info(entities)
        logger.info("start action 'envoor' ")
        return_events = [] #undo 'ikbedoelde' utter and the utter before that
        exclude_list = ['inform_municipality', 'inform_product', 'inform_affirmative', 'inform_denial','inform_dontknow', 'inform_maybe','inform','inform_redelijk','envoor', 'schedule_helpmore']

        tracker1 = tracker.events    

        def filter_function(e: Dict[Text, Any]) -> bool:
            # returns true or false bool
            has_instance = e["event"] == "user"
            # returns true or false bool
            excluded = e["event"] == "user" and e["parse_data"]['intent']['name'] in exclude_list
            logger.info("CHECK filter function")
            logger.info(has_instance and not excluded)
            # returns true or false bool
            return has_instance and not excluded

        filtered = filter(filter_function, reversed(tracker.applied_events()))
        logger.info("filtered")
        logger.info(filtered)
        previousutterevent = next(filtered, None)
        parsedata = previousutterevent.get("parse_data")
        logger.info("PARSEDATA")
        logger.info(parsedata)
        # get previous intent event
        previous_intent = parsedata.get("intent")["name"]
        logger.info('previous_intent')
        logger.info(previous_intent)
        
        previousentities = parsedata.get("entities",[])
        for entity in entities:
            c_entity = next((item for item in previousentities if item["entity"] == entity["entity"]),None)
            if c_entity:
                #TODO copy all properties from entity
                c_entity["value"] = entity["value"]
            else:
                #insert entity 
                previousentities.append(entity)

        logger.info('previousentities')
        logger.info(previousentities)

        newentities = {e["entity"]: e["value"] for e in previousentities}
        entities_json = json.dumps(newentities)
        text = "/{}{}".format(previous_intent, entities_json)
        logger.info('text')
        logger.info(text)
        
        # tempo hack TODO Retrieve intent that it is being replaced with out_of_scope? @ Harvey @ Jason @ Dylan
        try:
            product_slot = domain.get("slots")
            product_slot = product_slot["product"]["values"]
            product_entity = [entity_dicts["value"] for entity_dicts in entities if entity_dicts["entity"] == "product"][0]

            if previous_intent == "out_of_scope" and product_entity in product_slot:
                previous_intent = "inform_product"
                previousentities = [entity_dicts for entity_dicts in entities if entity_dicts["entity"] == "product"]
                text = "/{}{}".format(previous_intent, previousentities)
        
        except:
            pass

        if len(entities) > 0:
            logger.info(entities)

            return [ActionExecuted("action_listen")] + [UserUttered("/" + previous_intent, {"text": text,
                "intent": {"name": previous_intent, "confidence": 1.0},
                "entities": previousentities
                })]

        else: #no entities found, user typed: Ik bedoelde iets anders... => wat dan?
            dispatcher.utter_message(template="utter_bedoel_iets_anders") #special case check_municipality/check_product TODO?
            return_events.extend([UserUtteranceReverted(), FollowupAction('action_listen')])
            logger.info(return_events)

        return return_events