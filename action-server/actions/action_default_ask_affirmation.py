import logging
from datetime import datetime
from typing import Text, Dict, Any, List
import json
import pandas as pd
import os
import requests

from rasa_sdk import Action, Tracker, ActionExecutionRejection
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction, REQUESTED_SLOT
from rasa_sdk.events import (
    SlotSet,
    UserUtteranceReverted,
    ConversationPaused,
    FollowupAction,
    Form,
)

from .utils import getsuggestionbuttonforintent, transform_suggestions, transform_entities

logger = logging.getLogger(__name__)


class ActionDefaultAskAffirmation(Action):
    """Default implementation which asks the user to affirm his intent.
       It is suggested to overwrite this default action with a custom action
       to have more meaningful prompts for the affirmations. E.g. have a
       description of the intent instead of its identifier name.
    """

    def name(self) -> Text:
        return "action_default_ask_affirmation"
        # return ACTION_DEFAULT_ASK_AFFIRMATION_NAME

    def __init__(self) -> None:
        return
            
    def getsuggestionbuttons(
        self,
        dispatcher: CollectingDispatcher,
        suggestions: Dict[Text, Any],
        intents: List[Dict[Text,Any]],
        entities: Dict[Text,Any],
        count: int
    ) -> List:

        buttons = []

        for intent in intents:
            intent_name = intent.get('name')
            suggestionbuttonforintent = getsuggestionbuttonforintent(suggestions,intent_name,entities)
            if suggestionbuttonforintent:
                buttons.append(suggestionbuttonforintent)
        top = count - 1
        buttons = buttons[:top]
        buttons.append({"title": "Iets anders", "payload": "/{}".format("bedoel_iets_anders")})

        return buttons

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List["Event"]:
      
        intent_ranking = tracker.latest_message.get("intent_ranking", [])

        entities = transform_entities(tracker)

        # If True, Gem content will be used to retrieve the suggestion buttons
        gemcontent_suggestions = os.getenv("GEMCONTENT_SUGGESTIONS") in ["1", "yes", "True"]

        if gemcontent_suggestions:
            logger.info("Using Gem Content to retrieve suggestion content")
            base_url = os.getenv("GEMCONTENT_URL")
            response = requests.get(f"{base_url}/api/intents/")
            logger.info(f"Suggestion response status from Gem Content: {response.status_code}")
            suggestions = response.json()
        else:
            # Retrieve suggestions from domain
            suggestions = transform_suggestions(domain)

        if len(intent_ranking) == 0:

            dispatcher.utter_template("utter_ask_rephrase", tracker)
            return []
        else:
            #filtered_intent_ranking = [ranking for ranking in intent_ranking if ranking.get('confidence') > 0.2]
            filtered_intent_ranking = intent_ranking
            logger.info(filtered_intent_ranking)

            suggestionbuttons = self.getsuggestionbuttons(dispatcher,suggestions,filtered_intent_ranking,entities,4)

            logger.info(suggestionbuttons)
            # if len(intent_ranking) > 1:
            #     diff_intent_confidence = intent_ranking[0].get(
            #         "confidence"
            #     ) - intent_ranking[1].get("confidence")
            #     if diff_intent_confidence < 0.2:
            #         intent_ranking = intent_ranking[:2]
            #     else:
            #         intent_ranking = intent_ranking[:1]

            dispatcher.utter_button_template("utter_ask_affirmation", suggestionbuttons, tracker)
            
            return []
  
