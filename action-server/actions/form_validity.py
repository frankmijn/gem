import requests
import logging
from typing import Dict, Text, Any, List, Union, Optional
from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker
from rasa_sdk import Action
from rasa_sdk.events import SlotSet
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction, REQUESTED_SLOT

from .content_api import ContentAPI

class ValidityForm(FormAction):

    def name(self):
        """Unique identifier of the form"""

        return "validity_form"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""
        content_api = ContentAPI()
        return content_api.required_slots(tracker, "validity_duration")

    def submit(self,
               dispatcher: CollectingDispatcher,
               tracker: Tracker,
               domain: Dict[Text, Any]) -> List[Dict]:
        """Define what the form has to do
            after all required slots are filled"""
        content_api = ContentAPI()
        municipality = tracker.get_slot("municipality")
        product = tracker.get_slot("product")
        response = content_api.get(product, municipality)
        if not response:
            dispatcher.utter_template("utter_product_not_found", tracker)
            return []
        dispatcher.utter_message(response["validity_duration"]["info"])
        return []
