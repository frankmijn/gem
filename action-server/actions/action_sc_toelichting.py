import requests
import logging
from rasa_sdk import Action

from .utils import ignore_dh_request

import logging
logger = logging.getLogger(__name__)


class action_samenwerkendecatalogi_toelichting(Action):
  def name(self):
    return "action_samenwerkendecatalogi_toelichting"

  def run(self, dispatcher, tracker, domain):

    municipality = tracker.get_slot("municipality")
    gemeente_website = tracker.get_slot("gemeente_website")
    product = tracker.get_slot("product")
    
    if municipality == None:
      municipality = tracker.get_slot("municipality")
    if municipality == None:
      user_event_metadata = tracker.get_last_event_for("user").get("metadata", {})
      municipality = user_event_metadata.get("municipality")

    logging.info("Slot: %s", municipality)
    logging.info("Slot: %s", product)

    sc_template_b = 'utter_samenwerkendecatalogi'
    if product is not None:
        product_template = sc_template_b + '_' + product
        responses = domain.get('responses',domain.get('templates'))
        if product_template in responses.keys():
            sc_template_b = product_template
            dispatcher.utter_template(sc_template_b, tracker)
            return []
    else:
      dispatcher.utter_template(sc_template_b, tracker,  municipality="{0}".format(municipality))
      return []
