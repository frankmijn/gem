import datetime
import isodate
import logging
import os
import requests
import re
import json

from rasa_sdk.events import SlotSet, FollowupAction, UserUtteranceReverted, ActionReverted, ActionExecuted, UserUttered
from typing import Dict, Text, Any, List, Union, Optional
from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker
from rasa_sdk import Action
from rasa_sdk.events import SlotSet
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction, REQUESTED_SLOT
from datetime import date
from .content_api import ContentAPI
from .form_when import WhenFormAction

logger = logging.getLogger(__name__)

class VerhuismeldingWhenFormAction(WhenFormAction):

    def name(self):
        # type: () -> Text
        """Unique identifier of the form"""

        return "form_when_verhuismelding"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill
        content_api = ContentAPI()
        required_slots = content_api.required_slots(tracker, "channels_register")"""
        required_slots = []

        ducktime = next(tracker.get_latest_entity_values("time"), None)
        # check if time is already given
        tracker.slots["verhuismelding_datum"] = ducktime
        if ducktime is None:
            required_slots.append("verhuismelding_datum")

        elif ducktime is not None:
            try:
                datum = datetime.datetime.strptime(ducktime[:10], "%Y-%m-%d").date()
                today = date.today()
                difference_day = (today - datum).days + 365
                difference_year = (today.year - datum.year)
                logger.info("difference day")
                logger.info(difference_day)
                logger.info("difference year")
                logger.info(difference_year)
                entities = tracker.latest_message.get("entities")
                grain = entities[-1]["additional_info"]["grain"]
            
            except Exception as e:
                logger.info("No time detected")
                logger.exception(e)
                
            if difference_year <= -1 and difference_day >= 30 and grain == "day":
                required_slots.append("verhuismelding_datum_jaar")
                     
        return required_slots

    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
        """A dictionary to map required slots to
            - an extracted entity
            - intent: value pairs
            - a whole message
            or a list of them, where a first match will be picked"""

        return {"verhuismelding_datum": [self.from_entity(entity="time"),self.from_intent(intent="inform_affirmative", value="unknown"),self.from_intent(intent="inform_oke", value="unknown"),self.from_intent(intent="inform_denial", value="unknown"),self.from_intent(intent="inform_maybe", value="unknown"),self.from_intent(intent="inform_redelijk", value="unknown")],
                "verhuismelding_datum_jaar": [self.from_entity(entity="time"),self.from_intent(intent="inform_affirmative", value="unknown"),self.from_intent(intent="inform_oke", value="unknown"),self.from_intent(intent="inform_denial", value="unknown"),self.from_intent(intent="inform_maybe", value="unknown"),self.from_intent(intent="inform_redelijk", value="unknown")]}
 
    def validate(self, dispatcher, tracker, domain):
        # type: (CollectingDispatcher, Tracker, Dict[Text, Any]) -> List[Dict]
        """Validate extracted value of requested slot
            else reject execution of the form action
            Subclass this method to add custom validation and rejection logic
        """
        # extract other slots that were not requested
        # but set by corresponding entity
        slot_values = self.extract_other_slots(dispatcher, tracker, domain)

        # extract requested slot
        slot_to_fill = tracker.get_slot(REQUESTED_SLOT)
        if slot_to_fill:
            slot_values.update(self.extract_requested_slot(dispatcher,
                                                           tracker, domain))
            if not slot_values:
                # reject to execute the form action
                # if some slot was requested but nothing was extracted
                # it will allow other policies to predict another action
                intent = tracker.latest_message.get("intent", {}).get("name")
                entities2 = tracker.latest_message.get("entities", [])
                logger.info(intent)
                logger.info(entities2)
                logger.info(tracker.latest_message)
                return self.deactivate() + [ActionExecuted("action_listen")] + [UserUttered(tracker.latest_message.get('text'), tracker.latest_message)]

        # validation succeed, set slots to extracted values
        return [SlotSet(slot, value) for slot, value in slot_values.items()]
       
    def validate_verhuismelding_datum(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Optional[Text]:
        """Validate date"""
        if value == "unknown":
            return {"verhuismelding_datum": value}

        else:
            datum = None
            ducktime = next(tracker.get_latest_entity_values("time"), None)
            if ducktime is not None:
                # If detected: Iso-formatted datetime: 2019-09-03T20:56:35.450686 + timezone stuff
                try:
                    datum = datetime.datetime.strptime(ducktime[:10], "%Y-%m-%d").date()
                except Exception as e:
                    logger.exception(e)
                    return {"verhuismelding_datum": None} #Should never happen since ducktime is detected
            else:
                try:
                    datum = datetime.datetime.strptime(value, "%Y-%m-%d").date()
                except Exception as e:
                    try:
                        datum = datetime.datetime.strptime(value, "%d-%m-Yd").date()
                    except Exception as e:
                        logger.exception(e)
                        dispatcher.utter_template("utter_warning_datumnietherkend", tracker)
                        return {"verhuismelding_datum": None}

            return {"verhuismelding_datum": datum.__str__()}


    def validate_verhuismelding_datum_jaar(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Optional[Text]:
        """Validate date"""

        if value == "unknown":
            return {"verhuismelding_datum_jaar": value}

        try:
            entities = tracker.latest_message.get("entities")
            grain = entities[-1]["additional_info"]["grain"]
            user_input = tracker.latest_message.get("text")
            year = user_input
        except:
            year = None

        time_extractor = []
        if year != None:
            for index, event in enumerate(tracker.current_state()["events"]):
                try:
                    if event["value"] and grain == "year" and bool(re.search(r'.*([01]?[0-9]|2[0-3]):[0-5][0-9].*', event["value"])) == True:
                        logger.info("EVENT VALUE")
                        logger.info(event["value"])
                        time_extractor.append(event["value"])
                    elif event["value"] and grain == "day":
                        if bool(re.search(r'.*([1-3][0-9]{3})', event["value"])) == True:
                            time_extractor.append(event["value"])   
                        else:
                            return{"verhuismelding_datum_jaar": None}
                except:
                    "error"

            previous_date = time_extractor[0] # old date
            newest_date = time_extractor[-1] # newest date

            #logger.info("previous_date")
            #logger.info(previous_date)
            new_date = newest_date[:4] + previous_date[4:]
            return new_date
        else:
            return{"verhuismelding_datum_jaar": None}

    def submit(self,
               dispatcher: CollectingDispatcher,
               tracker: Tracker,
               domain: Dict[Text, Any]) -> List[Dict]:
        """Define what the form has to do
            after all required slots are filled -->
            Bereken de afstand"""

        content_api = ContentAPI()
        response = content_api.get("verhuizing")
        
        if tracker.get_slot("verhuismelding_datum_jaar") != None:
            datum = tracker.get_slot("verhuismelding_datum_jaar")

        elif tracker.get_slot("verhuismelding_datum_jaar") == None:
            datum = next(tracker.get_latest_entity_values("time"), None)

        if datum == "unknown":
            dispatcher.utter_message(response["term"]["info"])
        else:
            try:
                datum = datetime.datetime.strptime(datum[:10], "%Y-%m-%d").date()
                given_date = datum
                today = date.today()
                difference_day = (today - datum).days + 365
                difference_year = (given_date.year - today.year)
                entities = tracker.latest_message.get("entities")
                grain = entities[-1]["additional_info"]["grain"]
                if difference_year >= 1 and grain == "day":
                    logger.info(str(today.year) + str(datum)[4:])
                    new_datum = str(today.year) + str(datum)[4:]
                    datum = datetime.datetime.strptime(new_datum,"%Y-%m-%d").date()
            except Exception as e:
                logger.exception(e)
                return {"verhuismelding_datum": None} #Should never happen since ducktime is detected

            # Determine the valid date-range based on the duration-fields for the product via
            # the Content API
            within_before = response["term"]["within_before"]
            within_after = response["term"]["within_after"]

            today = datetime.date.today()

            before_date = datum - isodate.parse_duration(within_before)
            after_date = datum + isodate.parse_duration(within_after)
            five_days_ago = today - isodate.parse_duration(within_after)
            logger.info("Aangegeven datum: {}".format(datum))
            logger.info("Dit mag tussen {} en {}".format(before_date, after_date))

            entities = tracker.latest_message.get("entities")

            try:
                logger.info("Grain: {}".format(entities[-1]["additional_info"]["grain"]))
                grain = entities[-1]["additional_info"]["grain"]

                if tracker.get_slot("verhuismelding_datum_jaar") != None:
                    grain = 'day'

            except Exception as e:
                logger.exception(e)
                grain = None

            if grain == 'month' or grain == 'year' or grain == 'week':
                dispatcher.utter_message(template="utter_product_verhuismelding_meldperiode_algemeen")
  
            elif before_date < today < after_date:
                dispatcher.utter_message(template="utter_product_verhuismelding_meldperiode_deadline",after_date="{0}".format(after_date.strftime("%d-%m-%Y")))
                #dispatcher.utter_message("Dat moet dan voor {0}".format(after_date.strftime("%d-%m-%Y")))
                
            elif before_date < today:
                dispatcher.utter_message(template="utter_product_verhuismelding_meldperiode_telaat",after_date="{0}".format(after_date.strftime("%d-%m-%Y")),
                five_days_ago="{0}".format(five_days_ago.strftime("%d-%m-%Y")))
                #dispatcher.utter_message("Oei, dat moest uiterlijk {0}. Geef het dan vandaag nog door. Je gebruikt als verhuisdatum {1}.".format(after_date.strftime("%d-%m-%Y"),five_days_ago.strftime("%d-%m-%Y")))

            else:
                dispatcher.utter_message(template="utter_product_verhuismelding_meldperiode_berekend",before_date="{0}".format(before_date.strftime("%d-%m-%Y")),
                after_date="{0}".format(after_date.strftime("%d-%m-%Y")))
                #dispatcher.utter_message("Dat moet dan tussen {0} en {1}".format(before_date.strftime("%d-%m-%Y"),after_date.strftime("%d-%m-%Y")))
        return [SlotSet('verhuismelding_datum_jaar', None), SlotSet('verhuismelding_datum', None)]
