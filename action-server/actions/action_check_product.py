from rasa_sdk import Action
from rasa_sdk.events import SlotSet, FollowupAction, UserUtteranceReverted, ActionReverted, ActionExecuted, UserUttered
from rasa_sdk import Tracker
from typing import Text, Dict, Any, List
from rasa_sdk.interfaces import ActionExecutionRejection
import logging
logger = logging.getLogger(__name__)

class action_check_product(Action):

    def name(self):
        return "action_check_product"

    def run(self, dispatcher, tracker, domain):
        intent = tracker.latest_message.get('intent')['name']
        logger.info('intent:')
        logger.info(intent)
        
        product_slot = domain.get("slots")
        product_values = product_slot["product"]["values"]

        if intent == "inform_product":
            if tracker.active_form.get("name") is not None:
                raise ActionExecutionRejection(self.name(),f"currently in form, skip action")
            
            product = next(tracker.get_latest_entity_values("product"),None)
            if product not in product_values:
                return [ActionExecuted("action_listen")] + [UserUttered("/" + "out_of_scope", {
                    "intent": {"name": "out_of_scope", "confidence": 1.0},
                    "entities": [{'entity': 'product', 'start': 0, 'end': 1, 'value': product}]
                    })]

            last_action = tracker.get_last_event_for("action", exclude=["action_listen"])

            if (last_action.get('name') == self.name()):

                exclude_list = ['inform_municipality', 'inform_product', 'inform_affirmative', 'inform_denial','inform_dontknow', 'inform_maybe','inform','inform_redelijk','envoor', 'schedule_helpmore']
                def filter_function(e: Dict[Text, Any]) -> bool:
                    # returns true or false bool
                    has_instance = e["event"] == "user"
                    # returns true or false bool
                    excluded = e["event"] == "user" and e["parse_data"]['intent']['name'] in exclude_list
                    # returns true or false bool
                    return has_instance and not excluded

                filtered = filter(filter_function, reversed(tracker.applied_events()))
                previousutterevent = next(filtered, None)
                parsedata = previousutterevent.get("parse_data")
                # get previous intent event
                previous_intent = parsedata.get("intent")["name"]
                logger.info('previous_intent')
                logger.info(previous_intent)

                logger.info('last action:')
                logger.info(str(last_action))
                logger.info('self.name:')
                logger.info(self.name())

                logger.info('laatste return:')
                logger.info(previous_intent)
                logger.info(product)

                return [UserUtteranceReverted(),ActionReverted()] + [ActionExecuted("action_listen")] + [UserUttered("/" + previous_intent, {
                    "intent": {"name": previous_intent, "confidence": 1.0},
                    "entities": [{'entity': 'product', 'start': 0, 'end': 1, 'extractor': 'DIETClassifier', 'value': product, 'processors': ['EntitySynonymMapper']}]
                    })]
            else: 
                last_intent = tracker.get_last_event_for("user",exclude=["inform_product"])
                logger.info(str(tracker.applied_events()))
                logger.info(str(tracker.slots))
                return [ActionExecuted("action_listen")] + [UserUttered("/" + "producten_algemeen", {
                    "intent": {"name": "producten_algemeen", "confidence": 1.0},
                    "entities": [{'entity': 'product', 'start': 0, 'end': 1, 'extractor': 'DIETClassifier', 'value': product, 'processors': ['EntitySynonymMapper']}]
                    })]


        else:
            product = next(tracker.get_latest_entity_values("product"), None)

            if product == None:
                product = tracker.get_slot("product")
             
            if product == None:
                product = tracker.get_slot("product")
                ask_template = "utter_ask_product"
                if intent is not None:
                    intent_template = ask_template + '_' + intent
                    responses = domain.get('responses',domain.get('templates'))
                    if intent_template in responses.keys():
                        ask_template = intent_template
                       
                dispatcher.utter_message(template=ask_template)
                return [FollowupAction('action_listen')]
                
            else:
                if product not in product_values:
                    return [ActionExecuted("action_listen")] + [UserUttered("/" + "out_of_scope", {
                        "intent": {"name": "out_of_scope", "confidence": 1.0},
                        "entities": [{'entity': 'product', 'start': 0, 'end': 1, 'value': product}]
                        })]
                else:
                    return [SlotSet("product",product)]
        return []
