from rasa_sdk import Action
from rasa_sdk.events import SlotSet, FollowupAction, UserUtteranceReverted, ActionReverted
from rasa_sdk import Tracker
import logging
logger = logging.getLogger(__name__)

class action_set_feedback_negatief(Action):

    def name(self):
        return "action_set_feedback_negatief"

    def run(self, dispatcher, tracker, domain):

        return [SlotSet("feedback_geholpen", "Negatief")]