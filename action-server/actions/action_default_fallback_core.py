from rasa_sdk import Action
from rasa_sdk.events import SlotSet, FollowupAction, UserUtteranceReverted, ActionReverted, UserUttered, ActionExecuted
from rasa_sdk import Tracker

class ActionDefaultFallbackCore(Action):
    def name(self):
        return "action_default_fallback_core"

    def run(self, dispatcher, tracker, domain):
        product_slot = domain.get("slots")
        product_values = product_slot["product"]["values"]

        product = next(tracker.get_latest_entity_values("product"),None)
        if not product:
            dispatcher.utter_template("utter_out_of_scope", tracker)
            return []
        elif product not in product_values:
            return [ActionExecuted("action_listen")] + [UserUttered("/" + "out_of_scope", {
                "intent": {"name": "out_of_scope", "confidence": 1.0},
                "entities": [{'entity': 'product', 'start': 0, 'end': 1, 'value': product}]
                })]
