import json
import requests
from typing import Text, Dict, Any

from requests.adapters import HTTPAdapter
from urllib3.util.ssl_ import create_urllib3_context


CIPHERS = (
    'ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+HIGH:'
    'DH+HIGH:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+HIGH:RSA+3DES:!aNULL:'
    '!eNULL:!MD5HIGH:!DH:!aNULL'
)

class IgnoreDHKeyAdapter(HTTPAdapter):
    def init_poolmanager(self, *args, **pool_kwargs):
        context = create_urllib3_context(ciphers=CIPHERS)
        pool_kwargs['ssl_context'] = context
        return super().init_poolmanager(*args, **pool_kwargs)

    def proxy_manager_for(self, *args, **proxy_kwargs):
        context = create_urllib3_context(ciphers=CIPHERS)
        proxy_kwargs['ssl_context'] = context
        return super().proxy_manager_for(*args, **proxy_kwargs)


# Ignore SSLError thrown when accessing zoekdienst.overheid.nl
def ignore_dh_request(url, **params):
    session = requests.Session()
    session.mount('https://', IgnoreDHKeyAdapter())
    return session.get(url, allow_redirects=True, **params)


def getsuggestionbuttonforintent(
    suggestions: Dict[Text,Any],
    intent: Text,
    entities: Dict[Text,Any],
):
    candidates = suggestions.get(intent)
    if not candidates:
        return None
    bestmatch = None
    bestscore = -1
    for candidate in candidates:
        entities_in_suggestion = set(candidate.get('entities', []))
        intent_entity_names = set(entities.keys())
        if entities_in_suggestion.issubset(intent_entity_names):
            score = len(entities_in_suggestion)
            if score > bestscore:
                bestscore = score
                bestmatch = candidate
    entities_json = json.dumps(entities)

    if bestmatch is None:
        return {
            "title": "/{}{}".format(intent, entities_json),
            "payload": "/{}{}".format(intent, entities_json),
        }
    else:
        return {
            "title": "{}".format(bestmatch.get('text').format(**entities)),
            "payload": "/{}{}".format(intent, entities_json),
        }


def transform_entities(tracker):
    entities = tracker.latest_message.get("entities", [])
    entities = {e["entity"]: e["value"] for e in entities}
    return entities


def transform_suggestions(domain):
    suggestions = {}
    domain_intents = domain.get('intents')
    for intentdict in domain_intents:
        domain_intent_name = next(iter(intentdict))
        dictvalue = intentdict.get(domain_intent_name)
        domainsuggestions = dictvalue.get("suggestions",None)
        if domainsuggestions is not None and len(domainsuggestions) == 0:
            continue
        if domainsuggestions is None:
            domainsuggestions = []
        suggestions[domain_intent_name] = domainsuggestions
    return suggestions


def process_event_text(event, entities, suggestions):
    """Retrieve human-readable text from a tracker Event"""
    text = event["text"]
    if event['text'].startswith("/"):
        intent = event['text'].split("/")[-1].split("{")[0]
        button = getsuggestionbuttonforintent(
            suggestions, intent, entities
        )
        if button:
            text = button['title']

    if event.get("data", {}).get("attachment", ""):
        text += " " + event.get("data", {}).get("attachment", "")

    return text