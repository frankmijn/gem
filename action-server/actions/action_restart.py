import requests
import logging
from rasa_sdk import Action, Tracker
from rasa_sdk.events import Restarted, SlotSet, FollowupAction
from rasa_sdk.executor import ActionExecutor, CollectingDispatcher

logger = logging.getLogger(__name__)

class ActionRestart(Action):
  def name(self):
    return "action_restart"

  def run(self, dispatcher, tracker, domain):
    # ID = tracker.get_slot("context_chatid")
    # name = tracker.get_slot("context_name")
    # municipality = tracker.get_slot("context_municipality")

    events = [Restarted()]
    
    # if municipality:
    #   events.append(SlotSet('context_municipality',municipality))
    # if name:
    #   events.append(SlotSet('context_name',name))
    # if ID:
    #   events.append(SlotSet('context_chatid',ID))

    dispatcher.utter_message(template="utter_restart")
    dispatcher.utter_message(template="utter_howcanihelp")
        
    return events
