import requests
import logging
import xml.etree.ElementTree as ET
from typing import Dict, Text, Any, List, Union, Optional
from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker
from rasa_sdk import Action
from rasa_sdk.events import SlotSet
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction, REQUESTED_SLOT

from .utils import ignore_dh_request

class HowForm(FormAction):

    def name(self):
        # type: () -> Text
        """Unique identifier of the form"""

        return "how_form"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""

        return ["product", "municipality"]


    def submit(self,
               dispatcher: CollectingDispatcher,
               tracker: Tracker,
               domain: Dict[Text, Any]) -> List[Dict]:
        """Define what the form has to do
            after all required slots are filled"""

        municipality = tracker.get_slot("municipality")
        product = tracker.get_slot("product")

        if product == "verhuismelding": # Product heet anders in SC
            product = "verhuizing"
        logging.info("Slot: %s", municipality)
        logging.info("Slot: %s", product)
        #if municipality != None and product != None:
        response = ignore_dh_request('https://zoekdienst.overheid.nl/SRUServices/SRUServices.asmx/Search',
          params={'version':'1.2', 
                  'operation':'searchRetrieve',
                  'x-connection':'sc',
                  'recordSchema':'sc4.0',
                  'startRecord':'1',
                  'maximumRecords':'10',
                  'query':'((organisatie="{0}") and (organisatietype="''Gemeente''")) and (keyword="{1}")'.format(municipality,product)}
        ) #make an api call
        logging.info('SC.XML response request url: %s',response.request.url)
        logging.info('SC.XML response: %s',response.text)
        root = ET.fromstring(response.content)
        #logging.info('SC.XML response tag: %s',root.tag)
        ns = {'test': 'http://www.loc.gov/zing/srw/',
              'dcterms': 'http://purl.org/dc/terms/',
              'overheid': 'http://standaarden.overheid.nl/owms/terms/',
              'overheidproduct': 'http://standaarden.overheid.nl/product/terms/',
              'sru': 'http://standaarden.overheid.nl/sru',
              'xsi': 'http://www.w3.org/2001/XMLSchema-instance'}

        recordcount = root.find("test:numberOfRecords", ns)
        if int(recordcount.text) > 0:      
          element = root.find("test:records", ns)
          element = element.find("test:record", ns)
          element = element.find("test:recordData", ns)
          element = element.find("sru:gzd", ns)
          element = element.find("sru:originalData", ns)
          element = element.find("overheidproduct:scproduct", ns)
          element = element.find("overheidproduct:meta", ns)
          elementSCMeta = element.find("overheidproduct:scmeta", ns)
          elementOnlineAanvragen = elementSCMeta.find("overheidproduct:onlineAanvragen", ns)
          if elementOnlineAanvragen.text in ["ja", "digid"]:
            elementSCMetaAanvraagURL = elementSCMeta.find("overheidproduct:aanvraagURL", ns)
            elementSCMetaAanvraagURL = elementSCMetaAanvraagURL.attrib
            elementSCMetaAanvraagURL = elementSCMetaAanvraagURL.get("resourceIdentifier", None)

            if elementSCMetaAanvraagURL is None:
              dispatcher.utter_template("utter_sc_shop_url_not_found", tracker)
            else:
              dispatcher.utter_template("utter_shop_url", tracker, url="{0}".format(elementSCMetaAanvraagURL))
              
          else: 
            dispatcher.utter_template("utter_sc_shop_url_not_found", tracker)
            # Fallback to overheidproduct:meta/overheidproduct:owmskern/dcterms:identifier
            elementOWMSKern = element.find("overheidproduct:owmskern", ns)
            elementOWMSKern = elementOWMSKern.find("dcterms:identifier", ns)
            if elementOWMSKern is None:
              dispatcher.utter_template("utter_product_not_found", tracker)
            else:
              url = elementOWMSKern.text
              dispatcher.utter_template("utter_sc_found", tracker, url="{0}".format(url))

        else:
          dispatcher.utter_template("utter_product_not_found", tracker)
      
        #if int(recordcount.text) > 1:
          #dispatcher.utter_template("utter_sc_more_found", tracker, items="{0}".format(int(recordcount.text)-1))
            
        return []
