import pandas as pd
import requests
from io import StringIO


class OverheidAlmanak:
    
    def __init__(self, url="https://almanak.overheid.nl/export/Gemeenten.csv"):
        self.url = url
        data = requests.get(self.url)
        stream = StringIO(data.text)
        self.df = pd.read_csv(stream, delimiter=";", quotechar='"').fillna(value="")

    def get_for_gemeente(self, gemeente):
        row = self.df[self.df["Afkorting"] == gemeente]
        if not row.empty:
            data = list(row.to_dict(orient="index").values())[0]
            data['Gemeenteraad (Partij (aantal zetels))'] = data['Gemeenteraad (Partij (aantal zetels))'].split(";")
            data['Plaatsen binnen deze gemeente'] = data['Plaatsen binnen deze gemeente'].split(",")
            return data
        else:
            return None

