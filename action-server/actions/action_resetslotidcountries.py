from rasa_sdk import Action
from rasa_sdk.events import SlotSet
from rasa_sdk import Tracker
import logging
logger = logging.getLogger(__name__)

class resetslotidcountries(Action):

    def name(self):
        return "action_resetslotidcountries"

    def run(self, dispatcher, tracker, domain):
        return [SlotSet("idcountries", None)]