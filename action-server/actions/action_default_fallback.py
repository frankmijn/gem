from rasa_sdk import Action
from rasa_sdk import Tracker
from typing import Text, Dict, Any, List
import logging
from rasa_sdk.events import SlotSet, FollowupAction, UserUtteranceReverted, ActionReverted, UserUttered, ActionExecuted

logger = logging.getLogger(__name__)


class ActionDefaultFallback(Action):
    def name(self):
        return "action_default_fallback"

    def run(self, dispatcher, tracker, domain):

        entities = tracker.latest_message.get("entities", [])
        logger.info(entities)


        for all_values in entities:
            logger.info('entity value')
            logger.info(all_values["value"])            
            if all_values["entity"] == "product" and (all_values["value"] != 'paspoort' and all_values["value"] != 'id-kaart' and all_values["value"] != 'verhuismelding'):

                # domain_check = 'utter_samenwerkendecatalogi_' + all_values["value"]
                # responses = domain.get('responses',domain.get('templates'))
                # if domain_check in responses.keys():

                intent_template = 'product_' + all_values["value"] + '_info'
                logger.info('intent_template')
                logger.info(intent_template)

            
                return [ActionExecuted("action_listen")] + [UserUttered("/" + intent_template, {"text": '/'+intent_template,
                "intent": {"name": intent_template, "confidence": 1.0},
                "entities": [{'start': 0, 'end': 10, 'value': all_values["value"], 'entity': 'product', 'confidence': 0.9863335898150871, 'extractor': 'CRFEntityExtractor', 'processors': ['EntityMatcher', 'EntitySynonymMapper']}]
                })]
   

        dispatcher.utter_template("utter_default", tracker)
        return []
