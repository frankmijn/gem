import uuid
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, FollowupAction, UserUtteranceReverted, ActionReverted
from rasa_sdk import Tracker
import logging
from .allmanak_api import allmanak_api_url
from .utils import process_event_text, transform_suggestions, transform_entities
logger = logging.getLogger(__name__)

class ActionGespreksgeschiedenis(Action):

    def name(self):
        return "action_gespreksgeschiedenis"

    def run(self, dispatcher, tracker, domain):
        entities = transform_entities(tracker)
        suggestions = transform_suggestions(domain)
        trackerlog = tracker.current_state() #haalt de volledige log op van het gesprek
        loglist = []
        bestandsnaam = uuid.uuid4().hex + ".txt" #maakt nieuw .txt bestand aan met als bestandsnaam een unieke identifier.
        
        url = "/tmp/" + bestandsnaam
        pathname = "/app/tmp/" + bestandsnaam

        previous_id = None
        # onderstaande regels lezen de trackerlog uit. Zoekt naar userinput & bot utters en plaatst deze in chronologische volgorde in de .txt file.
        for event in trackerlog["events"]:
            if 'text' not in event:
                continue
            
            if event['event'] == 'user':
                if not event.get("message_id"):
                    continue

                if event['text'].startswith("/greet"):
                    continue

                # Workaround for duplicate user events after clicking button
                # from ask_affirmation action, see: https://forum.rasa.com/t/user-messages-duplication/23746/29
                if event.get("message_id") == previous_id:
                    logger.info(f"Duplicate user event encountered, skipping {event}")
                    continue
                previous_id = event.get("message_id")

                processed_text = process_event_text(event, entities, suggestions)

                loglist.append("* Gebruiker: " + processed_text)
                with open(pathname, "a") as file:
                    file.write("* Gebruiker: " + processed_text + "\n")
            elif event['event'] == 'bot':
                processed_text = process_event_text(event, entities, suggestions)

                loglist.append("- Gem: " + processed_text)
                with open(pathname, "a") as file:
                    file.write("- Gem: " + processed_text + "\n")
            

        logger.info(loglist)
        logger.info(file)
        # uiteindelijk wordt de bijbehorende utter verzonden met daarin de download-URL van de chatgeschiedenis. 
        dispatcher.utter_template('utter_download_chatgeschiedenis', tracker, url="{0}".format(url))


