from rasa_sdk import Action
from rasa_sdk.events import SlotSet
from rasa_sdk import Tracker
import logging
logger = logging.getLogger(__name__)

class check_municipality_product_features(Action):

    def name(self):
        return "action_check_municipality_product_features"

    def run(self, dispatcher, tracker, domain):

        municipality = tracker.get_slot("municipality")
        product = tracker.get_slot("product")
        logging.info("Slot: %s", municipality)
        logging.info("Slot: %s", product)

        #kindly note that Utrecht is being used as our dummy municipality
        if municipality == "Utrecht":
            return [SlotSet("openPDC", False)]

        else:
            return [SlotSet("openPDC", True)]