import requests
import logging
import os
from typing import Dict, Text, Any, List, Union, Optional
from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker
from rasa_sdk import Action
from rasa_sdk.events import SlotSet
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction, REQUESTED_SLOT

from .content_api import ContentAPI
from .brp_api import BRPAPI
from .sc_api import SamenwerkendeCatalogiAPI
from .tools import Tools

logger = logging.getLogger(__name__)

class ApplyFormAction(FormAction):

    def name(self):
        # type: () -> Text
        """Unique identifier of the form"""

        return "form_apply"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""
        content_api = ContentAPI()
        required_slots = content_api.required_slots(tracker, "channels_apply")
        isappliable = tracker.get_slot("apply_is_appliable")
        isdigidchannel = tracker.get_slot("apply_is_digid_channel")
        isloggedin = tracker.get_slot("apply_is_digid_loggedin")
        iscontinuedigid = tracker.get_slot("apply_continue_digid")
        if isdigidchannel and isappliable:
            required_slots += ["apply_continue_digid"]
            if not isloggedin and iscontinuedigid:
                required_slots += ["digid_rid"]
        logger.info(required_slots)

        return required_slots 
        

    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
        """A dictionary to map required slots to
            - an extracted entity
            - intent: value pairs
            - a whole message
            or a list of them, where a first match will be picked"""

        return {
            "apply_continue_digid": [
                self.from_intent(intent="inform_affirmative", value=True),
                self.from_intent(intent="inform_denial", value=False),
            ],
            "digid_rid": [self.from_entity(entity="digid_rid", intent="validate_digid"),self.from_text()]
        }
        
    def inform_channels_deactivate(self,
               dispatcher: CollectingDispatcher,
               tracker: Tracker,
               ):
        """Define what the form has to do
            after all required slots are filled """
        logger.info("inform channels")
        content_api = ContentAPI()
        municipality = tracker.get_slot("municipality")
        product = tracker.get_slot("product")
        response = content_api.get(product, municipality)
        if not response:
            dispatcher.utter_template("utter_product_not_found", tracker)
            return []
        channel = tracker.get_latest_input_channel()
        appointmenturl = None
        onlineurl = None
        scurl = None
        chatbot = False
        sc_api = SamenwerkendeCatalogiAPI()
        sc_url = sc_api.getProductURL(product,municipality)
       
        if response["channels_apply"].get("appointment"):
            appointmenturl = response["channels_apply"]["appointment"]
        if response["channels_apply"].get("online"):
            onlineurl = response["channels_apply"]["online"]
            #override with sc url
            aanvraagurl = sc_api.getAanvraagURL(product,municipality)
            if aanvraagurl is not None:
                onlineurl = aanvraagurl
        if response["channels_apply"].get("chatbot_digid"):
            chatbot = True
        
        if (channel == "socketio"):
            message = ""
            if appointmenturl is not None and onlineurl is None:
                message += "Je kunt hier [online een afspraak]({0}) voor maken. ".format(appointmenturl)
            elif appointmenturl is not None and onlineurl is not None:
                message += "Je kunt dit [op de website]({0}) regelen of hier [online een afspraak]({1}) voor maken. ".format(onlineurl,appointmenturl)
            elif appointmenturl is None and onlineurl is not None:
                message += "Je kunt dit [op de website]({0}) regelen. ".format(onlineurl)
            else:
                message += ""
                
            if sc_url is not None:
                message += "Meer informatie is te vinden [op de website]({0})".format(sc_url)
                
            dispatcher.utter_message(message)
        elif (channel == "facebook_referral"):
            message = ""
        else:
            message = ""
            if appointmenturl is not None and onlineurl is None:
                message += "Je kunt hier online een afspraak voor maken. "
            elif appointmenturl is not None and onlineurl is not None:
                message += "Je kunt dit op de website regelen of hier online een afspraak voor maken.  "
            elif appointmenturl is None and onlineurl is not None:
                message += "Je kunt dit op de website regelen.  "
            else:
                message += ""
            message += "Meer informatie is te vinden op de website van {0}".format(municipality)    
            dispatcher.utter_message(message)
        return
   
    
    def validate_product(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Optional[Text]:
        """Validate postcode/huisnummer value."""
        content_api = ContentAPI()
        municipality = tracker.get_slot("municipality")
        product = tracker.get_slot("product") #must be "verhuismelding"
        response = content_api.get(product, municipality)
        channels = response["channels_apply"]
        isappliable = "chatbot_digid" in channels
        isdigidchannel = Tools.isdigidchannel(tracker)
        
        return {"apply_is_appliable": isappliable, "product": product, "apply_is_digid_channel": isdigidchannel}

    """def validate_apply_continue_digid(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Optional[Text]:
        #Validate postcode/huisnummer value.
        logger.info("VALIDATE APPLY CONTINUE DIGID")
        if value == False:
            self.inform_channels_deactivate(dispatcher,tracker)
        return {}"""

    def validate_digid_rid(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Optional[Text]:
        """Validate digid authentication and get BRP info."""
        bsn = value # FIXME: THIS IS A HACK ICW DUMMY LOGIN, should be following CGI/SAML protocol
        logger.info("In validate DigID RID")
        logger.info("Input channel: {}".format(tracker.get_latest_input_channel()))
        brp_api = BRPAPI()
        response = brp_api.get(value)
        if not response:
            dispatcher.utter_template("utter_bsn_invalid", tracker)
            return None

        slots = {
            "apply_is_digid_loggedin": True,
            "digid_rid": None,
            "np_bsn": response["BSN"],
            "np_va_woonplaats": response["Plaats"],
            "np_va_straat": response["Straat"],
            "np_va_huisnummer": response["Huisnummer"],
            "np_va_postcode": response["Postcode"],
            "np_pg_roepnaam": response["Roepnaam"],
            "np_pg_geboortedatum": response["Geboortedatum"],
            "np_pg_geslachtsnaam": response["Achternaam"],
            "np_pg_voornamen": response["Voornamen"],
            "np_pg_volledigenaam": None,
            "np_pg_geslacht": None,
            "np_pg_voorletters": response["Voorletters"],
            "np_pg_nationaliteit": None,
            "np_pg_voorvoegsel": response["Tussenvoegsel"],
            "np_pg_achternaam": response["Aanschrijfnaam"]
        }
        # FIXME dispatcher.utter_template("utter_digid_confirm", tracker)
        dispatcher.utter_message("Hallo "+response["Roepnaam"] + " " + response["Achternaam"]+"!")
        dispatcher.utter_message("Ik heb je gegevens voor me, laten we verder gaan :-)")

        return slots
            
    def submit(self,
               dispatcher: CollectingDispatcher,
               tracker: Tracker,
               domain: Dict[Text, Any]) -> List[Dict]:
        logger.info("SUBMIT")
        self.inform_channels_deactivate(dispatcher,tracker)
        return []


