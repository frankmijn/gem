from rasa_sdk import Action
from rasa_sdk.events import SlotSet, FollowupAction, UserUtteranceReverted, ActionReverted
from rasa_sdk import Tracker
import logging
logger = logging.getLogger(__name__)

class ActionOutofscope(Action):
    
    def name(self):
        return "action_out_of_scope"

    def run(self, dispatcher, tracker, domain):

        dispatcher.utter_message(template="utter_out_of_scope")
        return []






