from .brp_api import BRPAPI
from rasa_sdk import Action
from rasa_sdk.events import SlotSet

# gets current information matching a bsn

class ActionGetinfo(Action):
    def name(self):
        return "action_getinfo"

    def run(self, dispatcher, tracker, domain):
        bsn = tracker.get_slot("bsn")
        brp_api = BRPAPI()

        if bsn:
            info = brp_api.get(bsn)
            if info["Status"] :
                dispatcher.utter("Sorry, ik kon je BSN niet vinden.", tracker)
                return []
            else:
                return [
                    SlotSet('name', info["Voornamen"]),
                    SlotSet('curr_street', info["Straat"]),
                    SlotSet('curr_housenr', info["Huisnummer"]),
                    SlotSet('curr_postalcode', info["Postcode"]),
                    SlotSet('curr_city', info["Plaats"]),
                ]
        else:
            dispatcher.utter_template("utter_verhuizing_request_bsn", tracker)
            return []
