from rasa_sdk import Action
from rasa_sdk.events import SlotSet, FollowupAction, UserUtteranceReverted, ActionReverted
from rasa_sdk import Tracker
import logging
logger = logging.getLogger(__name__)

class ActionUnhappy(Action):

    def name(self):
        return "action_unhappy"

    def run(self, dispatcher, tracker, domain):
        intent = tracker.latest_message.get('intent')['name']
        logger.info(intent)

        if intent == "waaromvraagjedat":
            intent = tracker.latest_message.get('intent')['name']

            tracker1 = tracker.events
           
            logger.info(tracker1)

            # the variable name "last_utter" loops in a reversed list containing dictionaries with all events of a setion
            # selects all dictionaries that contain .keys() == "name"
            # returns value of "name" if it starts with "utter" or "action" and is not equal to "action_listen"
            last_utter = ""
            for dicts in tracker1[::-1]:
                if "name" in dicts.keys() and (dicts["name"][0:5] == "utter" or dicts["name"][0:6] == "action" and not dicts["name"] == "action_listen"):
                    last_utter += dicts["name"]
                    break

            logger.info('last_utter')
            logger.info(last_utter)

            utter_template = last_utter + '_waaromvraagjedat'
            
            responses = domain.get('responses',domain.get('templates'))
            if utter_template in responses.keys():
                logger.info("unhappy: waaromvraagjedat-template") 
                dispatcher.utter_template(utter_template, tracker)
                return [UserUtteranceReverted(), ActionReverted()]
            
            elif last_utter == 'utter_helpmore':
                dispatcher.utter_message(template="utter_waaromvraagjedat")
                return []

            else:

                logger.info("unhappy: waaromvraagjedat")
                dispatcher.utter_message(template="utter_waaromvraagjedat")
                return [UserUtteranceReverted(), ActionReverted()]
                

        if intent == "oepsikbedoel":

            entities = tracker.latest_message.get("entities", [])
            entities = {e["entity"]: e["value"] for e in entities}
            logger.info(entities)

            if "municipality" in entities.keys():
                logger.info('oepsikbedoelmunicipality')
                municipality = entities["municipality"]
                logger.info(municipality)
                return [ActionReverted(), SlotSet("municipality",municipality)]

            else:
                logger.info('oepsikbedoelgeenmunicipality')
                dispatcher.utter_message(template="inform_municipality")
                return [UserUtteranceReverted(), ActionReverted()]
                        

        return []
