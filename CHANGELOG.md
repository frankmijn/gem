Changelog
=========


* Sprint 5

** 2019-11-15
- Test: Added HTML reports to newman report scripts on *nix systems 
- Test: Added untested batch files for running newman test scripts on Windows systems 

** 2019-11-13 (Branch: master)
- Test: Newman docker environment added, see test/newman/README.md for more details

** 2019-11-07 (Branch: master)
- Rasa: Chitchat added using CBS opendata

** 2019-11-05 (Branch: master)
- Rasa: Escalation 
- Rasa: Rebuild action server is required

** 2019-10-30 (Branch: obi4wan)
- OBI4wan: Hacked OBI4wan LiveChat widget to transfer user session from Rasa (MrBot) to OBI4wan (LiveChat) 

** 2019-10-29 (Branch: obi4wan)
- Rasa: Added support for "clean" parameter to train_sandbox.sh and train.sh to clear the models directory
- Rasa: Cleanup escalation code. 

** 2019-10-23 (Branch: obi4wan)
- Rasa: Remove OBI4WAN_CLIENT_ID, OBI4WAN_USERNAME, OBI4WAN_PASSWORD, OBI4WAN_ENVIRONMENT, OBI4WAN_API from .env
- Rasa: OBI4WAN_APPID, OBI4WAN_GUID, OBI4WAN_DOMAINADDRESS need to be added to the .env

** 2019-10-08
- Rasa: Custom action talk_to_human enabled
- Rasa: OBI4WAN_CLIENT_ID, OBI4WAN_USERNAME, OBI4WAN_PASSWORD, OBI4WAN_ENVIRONMENT, OBI4WAN_API=https://monkey.obi4wan.com/ need to be added to the .env
- OBI4wan: Setup API user
- OBI4wan: Added OBI4wan [Livechat page](http://localhost/livechat) website


* Sprint 4

** 2019-10-01

- Misc.: added a template for creating mockups of chats /docs/MockupsChatBot/MockupChatbot.html
- Rasa: fixed interactive training for "training" model.

** 2019-09-25

- Rasa: .env is now also used by train.sh/.bat, makes updating to newer Rasa version easier (only works on Linux)
- Rasa: pinned to 1.3.6
- Rasa X: Currently broken, not compatible with 1.3.6? WONT FIX

** 2019-09-11 

- Rasa: ENDPOINTS_FILE in .env can be used to override the default endpoints.yml. If you want to use NLG. Set this to "endpoints-nlg.yml"
- Rasa, training: Renamed train-sandbox.bat|sh to train_sandbox.bat|sh inline with naming convention
- Rasa X: Currently broken. WONT FIX
- Rasa X: RASA_DATA in .env can be used to override the default training folder: "training". Set this to "training-sandbox" to load a different model.


** 2019-08-??

- RASA_VERSION=1.1.6 is required in your .env file for both Rasa and Rasa X


* Sprint 3

-

* Sprint 2

- 

* Sprint 1

- 