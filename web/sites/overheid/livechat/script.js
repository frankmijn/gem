const loadSessionID = () => {
  if (window.localStorage) {
    if (window.location.hash.length != 0) { 
      let sessionID = window.location.hash;
      sessionID = sessionID.substring(1);
      let storage = window.localStorage; 
      storage.setItem('obi-chat-guid',sessionID);
      //log("loadSessionID");
    }
  }
};

window.addEventListener('load',  loadSessionID);