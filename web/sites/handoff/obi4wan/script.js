let engageChat;
let chat;

const url = 'https://cloudstatic.obi4wan.com/chat/obi-launcher.js';
const guid = 'acd3da2b-4906-45da-8373-96e632c899de';
const dataConfig = true;
const chatUserName = 'ObiEngageDemo';
const initialMessageToEngage = 'Chat started';

const init = () => {
  loadScript();
  initMessageForm();
}

const loadScript = () => {
  var script = document.createElement('script');
  script.addEventListener('load', () => engageChat = engageChat || initEngageChat());
  script.setAttribute('src', url)
  script.setAttribute('id', 'obi-chat-launcher');
  script.setAttribute('data-guid', guid);
  script.setAttribute('data-config', dataConfig);
  document.head.appendChild(script);
};

const initEngageChat = () => {
  return OBI.chat({
    headless: true,
    enableLauncher: false,
    onChatInit: () => {
      chat = document.getElementById('chat');
      chatInitialized();
    },
    onNewMessage: (message) => {
      // Both user and agent messages will be send to onNewMessage
      if (message.displayInWidget) addMessage(message);
    },
    onTyping: (isTyping) => {
      showTypingMessage(isTyping.typing);
    }
  });
};

const initMessageForm = () => {
  const form = document.getElementById('messageForm');
  const messageField = document.getElementById('message');
  form.addEventListener('submit', (e) => {
    e.preventDefault();
    engageChat.sendUserMessage({
      author: {url: null, name: chatUserName},
      content: messageField.value
    });
    form.reset();
  })
}

const chatInitialized = () => {

  engageChat.sendContextMessage({
    author: {url: null, name: chatUserName},
    displayInWidget: false,
    content: initialMessageToEngage
  }); 

  engageChat.isAvailable().then(function (isAvailable) {
    addMessage(createSystemMessage(isAvailable.available ? 'Agent is available' : 'Agent is not available'));
  });
  
}

const addMessage = (msg) => {
  const node = document.createElement('div');
  const meta = document.createElement('span');
  node.innerHTML = msg.content;
  node.classList.add('message', msg.type);
  meta.innerHTML = msg.author.name + ' - ' + new Date(msg.date).toLocaleString('nl-NL');
  meta.classList.add('meta');
  node.appendChild(meta);
  chat.appendChild(node);
}

const showTypingMessage = (typing) => {
  const node = document.getElementById('typing');
  node.setAttribute('class', typing ? '' : 'hidden')
}

const createSystemMessage = (msg) => {
  return {
    content: msg,
    type: 'system',
    date: new Date(),
    author: {
      name: 'system'
    }
  }
}

window.addEventListener('load', init);