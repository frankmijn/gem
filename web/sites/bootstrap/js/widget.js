// Retrieve the custom data
const script = document.getElementById("widget-script");
const municipality = script.dataset.municipality;

// Determine socket URL from the src used to include the widget script
const scriptUrl = script.src.split("/");
const baseUrl = scriptUrl[0] + "//" + scriptUrl[2] + "/";
const chatbotAvatarUrl = `${baseUrl}/static/img/avatar.png`;

// Retrieve the livechat script
const livechat = document.getElementById("livechat-script");

var initPayload = "/greet";
if(municipality) {
    initPayload = `/greet{\"municipality\":\"${municipality}\"}`
}

var customData = {};
if(municipality) {
    customData.municipality = municipality;
}

let onSocketEvent;

if(livechat) {
    switch(livechat.dataset.name) {
        // Currently only supports obi4wan
        case "obi4wan":
            customData.livechat = livechat.dataset.name;
            customData.guid = livechat.dataset.guid;
            onSocketEvent = {
                'bot_uttered': (data) => { if(data.history) {
                    // Store the municipality in the session, needed to restore
                    // the connection with Obi4Wan on page refresh
                    window.sessionStorage.setItem("municipality", data.municipality);

                    init(data.municipality, history=data.history);
                }},
            };
    }
}

// Set the username as custom data for Rasa
const queryString = window.location.search;
if(queryString) {
    const urlParams = new URLSearchParams(queryString);
    const username = urlParams.get("username");
    if(username) customData.username = username;
}

// Initialize the Rasa widget without livechat
WebChat.default.init({
    selector: "#webchat",
    initPayload: initPayload,
    interval: 1000, // 1000 ms between each message
    customData: customData, // arbitrary custom data. Stay minimal as this will be added to the socket
    socketUrl: baseUrl, // chatbot.tilburg.io
    title: "Gem",
    subtitle: "Je online hulp van de gemeente",
    inputTextFieldHint: "Typ hier je vraag...",
    onSocketEvent: onSocketEvent,
    connectingText: "Even geduld",
    profileAvatar: chatbotAvatarUrl,
    showCloseButton: true,
    fullScreenMode: false,
    params: {storage: "session"},
    customMessageDelay: (message) => {
        let delay = message.length * 20;
        if (delay > 2 * 1000) delay = 2000;
        if (delay < 500) delay = 500;
        return delay;
    }
})