#!/bin/bash

set -e

source .env

rasa_url=${RASA_URL:-http://localhost:5005}

echo "Starting containers"

docker-compose -f docker-compose.yml -f docker-compose.end2end.yml up -d

while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' $rasa_url)" != "200" ]]; do
    echo 'Waiting until Rasa is ready'
    docker-compose -f docker-compose.yml -f docker-compose.end2end.yml logs --tail 50 rasa
    sleep 5
done

echo "Running tests"

docker-compose -f docker-compose.yml -f docker-compose.end2end.yml run tests pytest end2end/ -v

echo "Stopping containers"

docker-compose -f docker-compose.yml -f docker-compose.end2end.yml down