@echo off  
rem clear model directory
IF "_%1" == "_clean" (
  echo "Cleaning Model directory"
  del /Q models\*.gz
) ELSE (
  echo "To clean the model directory: %0 clean"
)

rem Forced retraining of Rasa Core and NLU
setlocal
FOR /F "tokens=*" %%i in ('type .env') do SET %%i
docker run -v %cd%/rasa:/app -v %cd%/models:/app/models -v %cd%/rasa/custom:/build/lib/python3.6/site-packages/custom rasa/rasa:%RASA_VERSION% data split nlu
docker run -v %cd%/rasa:/app -v %cd%/models:/app/models -v %cd%/rasa/custom:/build/lib/python3.6/site-packages/custom rasa/rasa:%RASA_VERSION% test nlu -u train_test_split/test_data.md
docker run -v %cd%/rasa:/app -v %cd%/models:/app/models -v %cd%/rasa/custom:/build/lib/python3.6/site-packages/custom rasa/rasa:%RASA_VERSION% test core --stories test_stories.md --out results
endlocal

echo "Training done"